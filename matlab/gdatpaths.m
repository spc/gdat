function gdatpaths(remove)
%
% add paths for gdat directory
%
% assumes gdat already available
%
if nargin < 1
  remove = false;
end

a=which('gdat');
path_gdat = fileparts(a);
if ~findstr(path,[path_gdat,':'])
  error('Folder %s containing gdat.m is not in your path!!',path_gdat);
end

if isempty(a), error('gdat is not in the MATLAB path'); end
a = fileparts(a);

% machines=[{'JET'} {'TCV'} {'AUG'} {'D3D'}];
% machines=[{'JET'} {'TCV'} {'AUG'} {'KSTAR'}];
machines=[{'JET'} {'TCV'} {'AUG'} {'D3D'} {'KSTAR'} {'TCV_IMAS'} {'AUG_IMAS'} {'CHDF'} {'IMAS'}];

add_paths = cell(1,length(machines));
for i=1:numel(machines)
  add_paths{i}=genpath(fullfile(a,machines{i}));
end
if ~remove
  addpath(add_paths{:});
else
  rmpath(add_paths{:});
end
