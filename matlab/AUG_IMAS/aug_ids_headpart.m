function [ids_generic, params_ids_generic] = aug_ids_headpart(shot,ids_in,ids_name,varargin);
%
% [ids_generic, params_ids_generic] = aug_ids_headpart(shot,ids_in,ids_name,varargin);
%
% parses inputs and fill in ids_properties
%
%
% varargin options:
%
% 'comment': comment to include in ids_properties
% 'homogeneous_time': homogeneous_time in ids_properties: 1 (default) if the whole ids has same time, 0 otherwise
%
%
% example:
%   [ids_equilibrium, params_ids_equilibrium] = aug_ids_headpart(shot,ids_equil_empty,'equilibrium','comment','your comment');
%
%

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('ids_in', struct([]), @(x) (isstruct(x)));
p.addOptional('ids_name', '', @(x) (ischar(x))); % char
p.addOptional('comment', 'default comment', @(x) isempty(x) || ischar(x)); % char
p.addOptional('homogeneous_time', 1, @(x) (isnumeric(x) && isscalar(x) && (x == round(x))));

p.parse(shot,ids_in,ids_name);
defaults_ids_generic = p.Results; % to keep track of defaults
params = p.Results;
if nargin >= 4
  p.parse(shot,'ids_in',ids_in,'ids_name',ids_name,varargin{:});
  params = p.Results;
end

% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  params = rmfield(params,unique([names(mask); p.UsingDefaults.']));
end

params_ids_generic = params;

ids_generic = ids_in;

%
% ids_properties
%
ids_generic.ids_properties.comment = params_ids_generic.comment;
ids_generic.ids_properties.homogeneous_time = params_ids_generic.homogeneous_time;
ids_generic.ids_properties.source = ['AUG shot #' num2str(params_ids_generic.shot)];
ids_generic.ids_properties.provider = ['aug_get_ids_' ids_name];
ids_generic.ids_properties.creation_date = date;
