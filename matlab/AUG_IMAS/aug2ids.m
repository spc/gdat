function [ids_from_aug,varargout] = aug2ids(shot,varargin);
%
%   [ids_from_aug,varargout] = aug2ids(shot,varargin);
%
% Assumes you have done:
% >> addpath ~g2osaute/public/matlab9_11_2016 (on the gateway)
% using the tunnel made in another session like: ssh -lusername -L 8001:lxmdsplus.aug.ipp.mpg.de:8000 gate1.aug.ipp.mpg.de
% >> mdsconnect('localhost:8001') 
%
% varargin{1}: ids to load, by default all defined so far (if empty or empty string or not given)
%             {'equilibrium'} or a subset
%
% varargout{1}: return also the ids in array of structure with the names, to allow easy use of plotallids
%

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('ids_names', {'equilibrium'}, @(x) isempty(x) | (ischar(x) || iscell(x))); % char or cell array

p.parse;
defaults_aug2ids = p.Results; % to keep track of defaults

if nargin==1
  p.parse(shot);
  params = p.Results;
elseif nargin>=2
  p.parse(shot,varargin{:});
  params = p.Results;
else
  p.parse;
  p.Results
  return
end

% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  params = rmfield(params,unique([names(mask); p.UsingDefaults.']));
  if ~isfield(params, 'shot') || isnan(params.shot)
    warning('No shot entered');
    return
  end
  p.parse(params.shot,rmfield(params,'shot'));
  params = p.Results;
end

% check ids_names
if ~isfield(params,'ids_names')
  disp(['do not provide any ids_names or choose from : '])
  ids_from_aug.ids_names_available = defaults_aug2ids.ids_names;
  defaults_aug2ids.ids_names
  return
end
if ischar(params.ids_names)
  params.ids_names = {params.ids_names};
end
ids_names_ok = params.ids_names;
for i=1:length(params.ids_names)
  ij = strcmp(params.ids_names{i},defaults_aug2ids.ids_names);
  if ~any(ij)
    disp(['ids name: ' params.ids_names{i} ' is not available yet, ask O. Sauter'])
    ids_names_ok = setdiff(ids_names_ok,params.ids_names{i});
  end
end
params.ids_names = ids_names_ok;

params_aug2ids = params;

if isempty(params_aug2ids.ids_names)
  disp('no ids names available')
  return
end

for i=1:length(params_aug2ids.ids_names)
  ids_to_get = params_aug2ids.ids_names{i};
  tmp = gdat(shot,'ids','source',ids_to_get,'machine','aug');
  ids_from_aug.(ids_to_get) = tmp.(ids_to_get);
  ids_from_aug.([ids_to_get '_description']) = tmp.([ids_to_get '_description']);
end

if nargout>=2
  try
    for i=1:length(params_aug2ids.ids_names)
      ids_to_get = params_aug2ids.ids_names{i};
      varargout{1}.ids{i} = ids_from_aug.(ids_to_get);
      varargout{1}.idsname{i} = ids_to_get;
      varargout{1}.ids{i}.idsname = ids_to_get;
    end
  catch
    varargout{1} = [];
    disp('problems to fill in varargout')
  end
end
