function [ids_summary_out,ids_summary_out_description] = ids_summary_disruption(shot, ids_summary_in, gdat_params, varargin)
%
%    [ids_summary_out,ids_summary_out_description] = tcv_ids_ip(shot, ids_summary_in, gdat_params, varargin);
%
% fill in subnode "disruption" expected by ids_summary, for machine gdat_params.machine
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% Get data
switch lower(params_eff_ref.machine)
 case 'tcv'
 otherwise
end

% Preallocate dimension
ids_summary_out = ids_summary_in;
ids_summary_out_description.disruption.time = 'time of disruption obtained from...to fill in description per subnode';

% Put data into ids structure
