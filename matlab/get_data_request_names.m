function [data_request_names] = get_data_request_names;
%
% get list of presently available data_request names grouped and with description
%
% at this stage using first 3 columns of file gdat_data_request_names.xlsx
%

expected_machines = [{'aug'}, {'jet'}, {'tcv'}]; % substrutures created for these at this stage (all means all of these)
for j=1:length(expected_machines)
  data_request_names.(expected_machines{j}) = [];
end

filename='gdat_data_request_names_rho.xlsx';

[numeric_struct,text_struct,raw_struct]=xlsread(filename);

% use text structure
[n1,n2]=size(text_struct);
if prod(n1,n2)==0 || n2<3
  disp(['problems with file ' filename])
  return
end

% assume 1st column has data_request names with the headline "data_request"
ij=find(strcmp('data_request',strtrim(lower(text_struct{1,1})))==1);
if isempty(ij) || ij+1>=n1
  disp(['problems with file ' filename '; ''data_request'' not found in 1st column or no extra lines'])
  return
end

ij_1strow = ij+1;
for iline=ij+1:n1
  if isempty(text_struct{iline,1}); keyboard; end
  % extra machine
  validity = text_struct{iline,3};
  if ~isempty(regexpi(validity,'all'))
    data_request_names.all.(strtrim(lower(text_struct{iline,1}))).description = strtrim(text_struct{iline,2});
  else
    for j=1:length(expected_machines)
      if ~isempty(regexpi(validity,expected_machines{j}))
        data_request_names.(lower(expected_machines{j})).(strtrim(lower(text_struct{iline,1}))).description = strtrim(text_struct{iline,2});
      end
    end
  end
end
