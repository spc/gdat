function help_struct = jet_help_parameters(parameter_list)
%
% retrieve from present table the relevant help lines for the parameter_list{:}
% should come from sqlite database at some point...
%
% return the whole help structure if parameter_list empty or not provided
%
% do: 
%      help_struct = jet_help_parameters(fieldnames(gdat_data.gdat_params));
% 
% to get relevant help description
%

% Defaults
help_struct_all = struct(...
    'data_request', ['automatically filled in by gdat, name of request used in gdat call.' char(10) ...
                    'contains current list of keywords if gdat called with no arguments: aa=gdat;' char(10) ...
                    'Note shot value should not be in params so params can be used to load same data from another shot']  ...
    ,'machine', 'machine name like ''TCV'', ''JET'', case insensitive' ...
    ,'doplot', '0 (default), if 1 calls gdat_plot for a new figure, -1 plot over current figure with hold all, see gdat_plot for details' ...
    ,'liuqe','liuqe version 1 (default), 2, 3 for LIUQE1, 2, 3 resp. or -1 for model values' ...
    ,'nverbose','1 (default) displays warnings, 0: only errors, >=3: displays all extra information' ...
    );

% JET related
% $$$ help_struct_all.cxrs_plot = '0 (default) no plots, 1 get plots from CXRS_get_profiles see ''help CXRS_get_profiles'' for k_plot values';
% $$$ help_struct_all.time_interval = ['if provided sets a specific time interval [tstart tend].' ...
% $$$                     char(10) 'cxrs: (time_interval can have several nbs) take data and average over time interval(s) only, plots from CXRS_get_profiles are then provided' ...
% $$$                     ' as well'];
help_struct_all.fit_tension = ['smoothing value used in interpos fitting routine, -30 means ''30 times default value'', thus -1 often a' ...
                    ' good value' char(10) ...
                   ];
help_struct_all.time = 'time(s) value(s) if relevant, for example eqdsk is provided by default only for time=1.0s';
help_struct_all.zshift = 'vertical shift of equilibrium, either for eqdsk (1 or -99  to shift to zaxis=0) or for mapping measurements on to rho surfaces [m]';
help_struct_all.cocos = ['cocos value desired in output, uses eqdsk_cocos_transform. Note should use latter if a specific Ip and/or B0 sign' ...
                    'is wanted. See O. Sauter et al Comput. Phys. Commun. 184 (2013) 293'];
% $$$ help_struct_all.edge = '0 (default), 1 to get edge Thomson values';
help_struct_all.fit = '0, no fit profiles, 1 (default) if fit profiles desired as well, relevant for _rho profiles. See also fit_type';
help_struct_all.fit_ne_edge = '-1, no edge value imposed, >0, value at boundary for all times';
help_struct_all.fit_te_edge = '-1, no edge value imposed, >0, value at boundary for all times';
help_struct_all.fit_mask = '-1, no mask, [i1 i2 ...] index of channels to mask, plot(gdat_data.data) to check indices)';
% $$$ help_struct_all.fit_type = 'provenance of fitted profiles ''conf'' (default) from conf nodes, ''avg'' or ''local'' for resp. proffit: nodes';
help_struct_all.source = 'sxr: ''H'' (default), camera name ''T'', ''V'' ; for Vloop: ''ppf''(default),''jpf''';
help_struct_all.camera = ['[] (default, all), [i1 i2 ...] chord nbs ([1 3 5] if only chords 1, 3 and 5 are desired), ''central'' uses H10'];
%help_struct_all. = '';

%help_struct_all. = '';



if ~exist('parameter_list') || isempty(parameter_list)
  help_struct = help_struct_all;
  return
end

if iscell(parameter_list)
  parameter_list_eff = parameter_list;
elseif ischar(parameter_list)
  % assume only one parameter provided as char string
  parameter_list_eff{1} = parameter_list;
else
  disp(['unknown type of parameter_list in jet_help_parameters.m'])
  parameter_list
  return
end

fieldnames_all = fieldnames(help_struct_all);
for i=1:length(parameter_list_eff)
  if any(strcmp(fieldnames_all,parameter_list_eff{i}))
    help_struct.(parameter_list_eff{i}) = help_struct_all.(parameter_list_eff{i});
  end
end
