function mapping = jet_requests_mapping(data_request)
%
% Information pre-defined for gdat_jet, you can add cases here to match official cases in gdat_jet, allowing backward compatibility
%

% Defaults
mapping = struct(...
  'label', '', ...
  'method', '', ...
  'expression','', ...
  'timedim', -1, ...     % dim which is the time is the database, to copy in .t, the other dims are in .x (-1 means last dimension)
  'gdat_timedim',[], ...  % if need to reshape data and dim orders to have timedim as gdat_timedim (shifting time to gdat_timedim)
  'min', -inf, ...
  'max', inf);
% Note that gdat_timedim is set to timedim at end of this function if empty
% gdat_timedim should have effective index of the time dimension in gdat

if ~exist('data_request') || isempty(data_request)
  return
end

% default label: data_request keyword itself
mapping.label = data_request;

% for JET, following choices are set so far:
% method = 'signal' then expression contains the shotfile, diagnostic and if needed the experiment
%                expression is a cell array
% method = 'expression', then expression is the expression to return gdat_tmp...
% method = 'switchcase', then there will be a specific case within gdat_jet (usual case when not directly a signal)
%
% label is used for plotting
if iscell(data_request) % || (~ischar(data_request) && length(data_request)>1)
  mapping.label = data_request;
  mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
  mapping.expression = data_request;
  mapping.gdat_timedim = mapping.timedim;
  return
end

switch lower(data_request)
  case 'a_minor'
    mapping.label = 'a\_minor';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'CR0'}];
  case 'b0'
    mapping.label = 'B_0';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'MAGN'},{'BVAC'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''ppf'',''MAGN'',''BVAC''}; ' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);gdat_tmp.r0=2.96;'];
  case 'beta'
    mapping.label = 'beta\_tor diag [%]';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'BTTD'}];
  case 'betan'
    mapping.label = '\beta_N';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'BTND'}];
  case 'betap'
    mapping.label = '\beta_p';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'BTPD'}];
  case 'cxrs'
    mapping.timedim = 2;
    mapping.label = 'cxrs';
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'delta'
    mapping.timedim = 1;
    mapping.label = 'delta';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''delta_bottom''; ' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);params_eff.data_request=''delta_top'';' ...
                    'gdat_tmp2=gdat_jet(shot,params_eff);gdat_tmp.data = 0.5.*(gdat_tmp.data+gdat_tmp2.data);' ...
                    'gdat_tmp.label=gdat_data.label; gdat_tmp.gdat_request=gdat_data.gdat_request;'];
  case 'delta_top'
    mapping.label = 'delta\_top';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'EFIT'},{'TRIU'}];
  case 'delta_bottom'
    mapping.label = 'delta\_bottom';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'EFIT'},{'TRIL'}];
  case {'ece', 'eced', 'ece_rho', 'eced_rho'}
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'eqdsk'
    mapping.timedim = 2;
    mapping.method = 'switchcase'; % could use function make_eqdsk directly?
    mapping.expression = '';
  case 'equil'
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase'; % could use function make_eqdsk directly?
    mapping.expression = '';
  case {'gas','gasm','gash','eler'}
    mapping.label = 'eler';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'gash'},{'eler'}];
  case 'halpha'
    mapping.label = 'Halpha';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'jpf'},{'dd'},{'s3-ad08'}];
  case 'ioh'
    mapping.timedim = 1;
    mapping.label = 'I ohmic transformer';
    mapping.method = 'signal';
    mapping.expression = [];
  case 'ip'
    mapping.timedim = 1;
    mapping.label = 'Plasma current';
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'efit'},{'xip'}];
    mapping.expression = [{'ppf'},{'MAGN'},{'IPLA'}];
    mapping.units = 'A';
  case 'kappa'
    mapping.timedim = 1;
    mapping.label = '\kappa';
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'EHTR'},{'ELON'}];
    mapping.expression = [{'ppf'},{'EFIT'},{'ELON'}];
  case {'li', 'l_i'}
    mapping.timedim = 1;
    mapping.label = 'l_i_3';
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'EFIT'},{'LI3M'}];
  case {'locked', 'locked_mode'}
    mapping.timedim = 1;
    mapping.label = 'locked mode';
    mapping.method = 'signal';
    mapping.expression = [{'jpf'},{'DA'},{'C2-LOCA'}];
  case 'mhd'
    mapping.timedim = 1;
    mapping.label = 'n=1 and n=2 amplitude';
    mapping.timedim = 1;
    mapping.method = 'switchcase';
% $$$  mapping.method = 'expression'; % {'JPF','DA','C1M-H306'} {'JPF','DA','C1M-T009'} (big) {'JPF','DA','C1M-MIT27'}{'jpf'  'da'  'c1m-h304'} nfft=1024*3;
% $$$   mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''JPF'',''DA'',''C1H-I301''}; ' ...
% $$$                     'gdat_tmp=gdat_jet(shot,params_eff);gdat_tmp.data=reshape(gdat_tmp.data,length(gdat_tmp.data),1 );' ...
% $$$             'gdat_tmp.dim{1}=gdat_tmp.t;gdat_tmp.dim{2}=[1 2];gdat_tmp.x=gdat_tmp.dim{2};' ...
% $$$             'gdat_tmp.n_1.data = gdat_tmp.data;gdat_tmp.n_1.data_request=params_eff.data_request;' ...
% $$$             'params_eff.data_request={''JPF'',''DA'',''C1H-I302''};' ...
% $$$             'gdat_tmp2=gdat_jet(shot,params_eff);gdat_tmp.data(:,2)=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
% $$$             'gdat_tmp.n_2.data = gdat_tmp2.data;gdat_tmp.n_2.data_request=params_eff.data_request;gdat_tmp.label={''n=1'',''n=2''};' ...
% $$$             'gdat_tmp.full_path=''n=1 in data and .n_1; .n_2'';' ...
% $$$             'gdat_tmp.gdat_request=''mhd'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;'];

% $$$   mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''JPF'',''DA'',''C1M-T009''}; ' ...
% $$$                     'gdat_tmp=gdat_jet(shot,params_eff);gdat_tmp.data=reshape(gdat_tmp.data,length(gdat_tmp.data),1 );' ...
% $$$             'gdat_tmp.dim{1}=gdat_tmp.t;gdat_tmp.dim{2}=[1 2];gdat_tmp.x=gdat_tmp.dim{2};' ...
% $$$             'gdat_tmp.n_1.data = gdat_tmp.data;gdat_tmp.n_1.data_request=params_eff.data_request;' ...
% $$$             'params_eff.data_request={''JPF'',''DA'',''C1M-H305''};' ...
% $$$             'gdat_tmp2=gdat_jet(shot,params_eff);gdat_tmp.data(:,2)=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
% $$$             'gdat_tmp.n_2.data = gdat_tmp2.data;gdat_tmp.n_2.data_request=params_eff.data_request;gdat_tmp.label={''n=1'',''n=2''};' ...
% $$$             'gdat_tmp.full_path=''n=1 in data and .n_1; .n_2'';' ...
% $$$             'gdat_tmp.gdat_request=''mhd'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;' ...
% $$$                     ''];
  case 'mhd_ampl'
    mapping.timedim = 1;
    mapping.label = 'n=1 and n=2 amplitude';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''JPF'',''DA'',''C1H-G101''}; ' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);gdat_tmp.data=reshape(gdat_tmp.data,length(gdat_tmp.data),1 );' ...
                    'gdat_tmp.dim{1}=gdat_tmp.t;gdat_tmp.dim{2}=[1 2];gdat_tmp.x=gdat_tmp.dim{2};' ...
                    'gdat_tmp.n_1.data = gdat_tmp.data;gdat_tmp.n_1.data_request=params_eff.data_request;' ...
                    'params_eff.data_request={''JPF'',''DA'',''C1H-G102''};' ...
                    'gdat_tmp2=gdat_jet(shot,params_eff);gdat_tmp.data(:,2)=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_2.data = gdat_tmp2.data;gdat_tmp.n_2.data_request=params_eff.data_request;gdat_tmp.label={''n=1'',''n=2''};' ...
                    'gdat_tmp.full_path=''n=1 amplitude in data and .n_1; .n_2'';' ...
                    'gdat_tmp.gdat_request=''mhd_ampl'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;'];
  case 'ne'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
  case 'neint'
    mapping.timedim = 1;
    mapping.label = 'line integrated el. density';
    mapping.method = 'signal';
    mapping.expression = [{'DCN'},{'H-1'},{'JETD'}];
  case 'nel'
    mapping.timedim = 1;
    mapping.label = 'nel';
    mapping.method = 'signal';
    mapping.expression = [{'ppf'},{'KG1L'},{'LAD3'}];
    mapping.expression = [{'ppf'},{'HRTX'},{'NELA'}];
  case 'ne_rho'
    mapping.timedim = 2;
    mapping.label = 'ne';
    mapping.method = 'switchcase';
  case 'nete'
    mapping.timedim = 2;
    mapping.label = 'ne and Te'; % from chain2 or hrts with interpos fits
    mapping.method = 'switchcase';
  case 'nete_rho'
    mapping.timedim = 2;
    mapping.label = 'ne and Te';
    mapping.method = 'switchcase';
  case {'ng','ngreenwald','n_greenwald'}
    mapping.timedim = 1;
    mapping.label = 'nG=Ip[MA]/(\pi a^2)*1e20 on nel times';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);' ...
                    'if ~isempty(gdat_tmp.t);params_eff.data_request=''ip'';' ...
                    'gdat_tmp2=gdat_jet(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'params_eff.data_request=''a_minor'';' ...
                    'gdat_tmp3=gdat_jet(shot,params_eff);ij=find(gdat_tmp3.data==0);gdat_tmp3.data(ij)=NaN;' ...
                    'tmp_data3=interp1(gdat_tmp3.t,gdat_tmp3.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = abs(tmp_data2*1e-6./pi./(tmp_data3.^2+1e-5));' ...
                    'ij=find(gdat_tmp.data<0 | gdat_tmp.data>4);gdat_tmp.data(ij)=NaN;gdat_tmp.data = gdat_tmp.data * 1e20;end;'];
  case {'ngf','greenwald_fraction','f_greenwald','ng_fraction'}
    mapping.timedim = 1;
    mapping.label = 'Greenwald\_fraction=n\_el/n\_G';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);if ~isempty(gdat_tmp.t);params_eff.data_request=''n_greenwald'';' ...
                    'gdat_tmp2=gdat_jet(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = gdat_tmp.data./(tmp_data2+1e-5);end;'];
  case 'ni'
    mapping.method = 'switchcase'; % especially since might have option fit, etc
  case {'phi_tor', 'phitor', 'toroidal_flux'}
    mapping.label = 'toroidal_flux';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EQUI'},{'PHIT'}]; % note this is only chain2, so should check with efit...
    % there is FTOR from EFIT but it's on psinorm
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''PPF''},{''EFIT''},{''FTOR''}];' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);gdat_tmp.x=sqrt(gdat_tmp.x);gdat_tmp.dim{1}=gdat_tmp.x;'];
  case {'p_lh', 'plh'}
    mapping.label = 'p\_lh threshold';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);if ~isempty(gdat_tmp.t);params_eff.data_request=''b0'';gdat_tmpb0=gdat_jet(shot,params_eff);' ...
                    'params_eff.data_request=''surface_edge'';gdat_tmp_s=gdat_jet(shot,params_eff);' ...
                    'if ~isempty(gdat_tmp_s.t); s_nel=interpos(gdat_tmp_s.t,gdat_tmp_s.data,gdat_tmp.t,-1.);b0_nel=interpos(gdat_tmpb0.t,gdat_tmpb0.data,gdat_tmp.t,-1.);' ...
                    'gdat_tmp.data = 0.0488e6.*(gdat_tmp.data/1e20).^0.717.*abs(b0_nel).^0.803.*s_nel.^0.941;else;gdat_tmp=gdat_tmp_s;end;' ...
                    'gdat_tmp.label=''P\_LH threshold'';end'];
  case {'p_lh_a_r', 'plh_a_r'}
    mapping.label = 'p\_lh threshold';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_jet(shot,params_eff);if ~isempty(gdat_tmp.t);params_eff.data_request=''b0'';gdat_tmpb0=gdat_jet(shot,params_eff);' ...
                    'params_eff.data_request=''a_minor'';gdat_tmp_a=gdat_jet(shot,params_eff);' ...
                    'a_nel=interpos(gdat_tmp_a.t,gdat_tmp_a.data,gdat_tmp.t,-1.);b0_nel=interpos(gdat_tmpb0.t,gdat_tmpb0.data,gdat_tmp.t,-1.);' ...
                    'gdat_tmp.data = 2.15e6.*abs(gdat_tmp.data/1e20).^0.782.*abs(b0_nel).^0.772.*abs(a_nel).^0.975.*abs(gdat_tmpb0.r0).^0.999;' ...
                    'gdat_tmp.label=''P\_LH threshold'';end;'];
  case {'p_ohmic', 'p_ohm', 'pohm'}
    mapping.label = 'p\_ohmic';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'POHM'}];
  case {'p_rad', 'prad'}
    mapping.label = 'p\_rad';
    mapping.timedim = 1;
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'PPF'},{'bolo'},{'topi'}];
    mapping.method = 'switchcase';
  case 'powers'
    mapping.timedim = 1;
    mapping.label = 'various powers';
    mapping.method = 'switchcase';
  case 'psi_axis'
    mapping.label = 'psi\_axis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'FAXS'}];
  case 'psi_edge'
    mapping.label = 'psi\_edge';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'FBND'}];
  case 'q0'
    mapping.label = 'q_0';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'QAX'}];
    % mapping.expression = [{'PPF'},{'EFIT'},{'QAXM'}];
  case 'q95'
    mapping.label = 'q_{95}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'Q95'}];
  case 'q_edge'
    mapping.label = 'q_{edge}}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'QWL'}];
  case 'q_rho'
    mapping.timedim = 2;
    mapping.gdat_timedim = 2;
    mapping.label = 'q';
    mapping.method = 'switchcase';
  case 'rgeom'
    mapping.label = 'Rgeom';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'RGEO'}];
  case 'r_inboard'
    mapping.label = 'R\_inboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'RMJI'}];
  case 'r_outboard'
    mapping.label = 'R\_outboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'RMJO'}];
  case 'rhotor'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor';
  case 'rhotor_edge'
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_edge';
  case {'rhotor_norm','rhotornorm'}
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_norm';
  case 'rhovol'
    mapping.timedim = 2;
    mapping.label = 'rhovol\_norm';
    mapping.method = 'switchcase';
  case 'rmag'
    mapping.label = 'R\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'RMAG'}];
  case {'rnt', 'neutron_rate'}
    mapping.label = 'neutron\_rate';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'TIN'},{'RNT'}];
  case {'sxr', 'sxr_s40', 'sxr_htv'}
    mapping.timedim = 1;
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
  case {'tbeo'}
    mapping.label = 'TBEO';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EDG8'},{'TBEO'}];
  case 'te'
    mapping.timedim = 2;
    mapping.label = 'Te';
    mapping.method = 'switchcase';
  case 'te_rho'
    mapping.timedim = 2;
    mapping.label = 'Te';
    mapping.method = 'switchcase';
  case 'ti'
    mapping.label = 'Ti';
    mapping.method = 'switchcase';
  case 'vloop'
    mapping.label = 'Vloop Upper Restraint Ring Flux Flux Loop, non-integrated';
    mapping.timedim = 1;
    mapping.label = 'Vloop';
    mapping.method = 'switchcase';
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'PPF'},{'MAGN'},{'VL'}]; % tension -1e2 (3rd signal good one
% $$$   mapping.expression = [{'JPF'},{'DA'},{'C2-VLRRU'}]; % tension -1e4
  case 'volume'
    mapping.label = 'Volume';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'VOLM'}];
  case 'volume_rho'
    mapping.timedim = 2;
    mapping.label = 'volume\_norm';
    mapping.method = 'switchcase';
  case {'wmhd', 'w_mhd'}
    mapping.label = 'W_mhd';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'WP'}];
  case 'zeff'
    mapping.label = 'zeff from KS3';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'KS3'},{'ZEFV'}];
  case 'zgeom'
    mapping.label = 'Z_{geom}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'ZGEO'}];
  case 'zmag'
    mapping.label = 'Z\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'PPF'},{'EFIT'},{'ZMAG'}];
    %
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % extra JET cases (not necessarily in official data_request name list)
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
  case 'transp'
    mapping.label = 'transp output';
    mapping.method = 'switchcase';


  otherwise
    mapping.label = data_request;
    mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
    mapping.expression = data_request;

end

if isempty(mapping.gdat_timedim)
  mapping.gdat_timedim = mapping.timedim;
end
