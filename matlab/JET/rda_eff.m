function [data,x,time,hsig,error]=rda_eff(shot,pftype,tracename,varargin);
%
% gets data using RDA or mdsplus
% 1D arrays: assumes dimension is time
% 2D arrays: assumes data vs (x,time)
% 3D arrays: assumes data vs (x,time,hsig) (for mdsplus)
%
% varargin{1}: time interval or timevalue, will get data closest to that time or within that time interval
%              (DOES NOT WORK YET)
%
% examples:
%          [data,x,time,hsig,error]=rda_eff(51994,'ppf','efit/xip');
%          [data,x,time,hsig,error]=rda_eff(52206,'ppf','equi/rmji?uid=jetthg+seq=122');
%
% set global variable: usemdsplus to decide if RDA or mdsplus is used:
%    >> global usemdsplus
%    >> usemdsplus=1 % means use mds to get data (default if not defined)
%    >> usemdsplus=0 % means use jetreaddata routine (RDA)
%       if ~exist('usemdsplus'); usemdsplus=1; end
%

global usemdsplus
if isempty(usemdsplus); usemdsplus=1; end
time_int=[];
if nargin>=4 & ~isempty(varargin{1})
  time_int=varargin{1};
end

if usemdsplus
  % use mdsplus
  
  if ~unix('test -d /home/duval/mdsplus')
    addpath('/home/duval/mdsplus')
  end
  mdsconnect('mdsplus.jet.efda.org');

  % defines trace to fetch
  % after '?' specific details
  separator='+';
  mainseparator='?';
  imaintrace=findstr(mainseparator,tracename);
  if isempty(imaintrace)
    maintrace=tracename;
    uid=[];
    seq=[];
    diag=[];
    type=[];
  else
    maintrace=tracename(1:imaintrace-1);
    rest=tracename(imaintrace+1:end);
    % gets uid if any
    iuid=findstr('uid=',rest);
    if isempty(iuid)
      uid=[];
    else
      ii=findstr(separator,rest(iuid:end));
      if isempty(ii)
        uid=rest(iuid+4:end);
      else
        uid=rest(iuid+4:iuid+ii(1)-2);
      end
    end
    % gets seq if any
    iseq=findstr('seq=',rest);
    if isempty(iseq)
      seq=[];
    else
      ii=findstr(separator,rest(iseq:end));
      if isempty(ii)
        seq=rest(iseq+4:end);
      else
        seq=rest(iseq+4:iseq+ii(1)-2);
      end
    end
    % gets type if any
    itype=findstr('type=',rest);
    if isempty(itype)
      type=[];
    else
      ii=findstr(separator,rest(itype:end));
      if isempty(ii)
        type=rest(itype+5:end);
      else
        type=rest(itype+5:itype+ii(1)-2);
      end
    end
    % gets diag if any
    idiag=findstr('diag=',rest);
    if isempty(idiag)
      diag=[];
    else
      ii=findstr(separator,rest(idiag:end));
      if isempty(ii)
        diag=rest(idiag+5:end);
      else
        diag=rest(idiag+5:idiag+ii(1)-2);
      end
    end
    
  end
  
  % fetch value
  if ~isempty(uid)
    eval(['u=mdsvalue(''_sig=ppfuid("' uid '")'');'])
  end
  if strcmpi(type,'lpf')
    pftype=[type '/' diag];
  end
  traceeff=[pftype '/' maintrace];
  if ~isempty(seq)
    traceeff=[traceeff '/' num2str(seq)];
  end
  user=getenv('USER');
  eval(['[data,error]=mdsvalue(''_rdaeff' user '=jet("' traceeff '",' num2str(shot) ')'');'])
  hsig=[];
  ss=size(data);
  nbofdim=length(ss);
  if ss(end)==1; nbofdim=nbofdim-1; end
  nbofdim=max(nbofdim,1);
  switch nbofdim
  case 1
    eval(['time=mdsvalue(''dim_of(_rdaeff' user ',0)'');']);
    x=[];
    if isempty(time) & length(data)>1e6 & strcmpi(type,'lpf') & strcmpi(diag,'kc1f')
      mdsdisconnect;
      mdsconnect('mdsplus.jet.efda.org');
      eval(['aaa=mdsvalue(''_tc91=jet("jpf/da/c1-tc91",' num2str(shot) ');1'');'])
      taaa=mdsvalue('_ttc91=dim_of(_tc91,0);_ttc91[0]');
      time=linspace(taaa+1e-6,taaa+4,length(data))';
    end
    if isempty(time) & length(data)>1e6 & strcmpi(type,'lpf') & strcmpi(diag,'cats1')
      ichannel=findstr(':00',maintrace);
      iblock=str2num(maintrace(ichannel+3));
      mdsdisconnect;
      mdsconnect('mdsplus.jet.efda.org');
      taaa=39.9989+(iblock-1)*8;
      time=linspace(taaa,taaa+8-4e-6,length(data))';
    end
  case 2
    eval(['x=mdsvalue(''dim_of(_rdaeff' user ',0)'');']);
    eval(['time=mdsvalue(''dim_of(_rdaeff' user ',1)'');']);

  case 3
    eval(['x=mdsvalue(''dim_of(_rdaeff' user ',0)'');']);
    eval(['time=mdsvalue(''dim_of(_rdaeff' user ',1)'');']);
    disp('3rd dimension in hsig!!!!!!!!!!!!!!!!!!!!!!!!!')
    eval(['hsig=mdsvalue(''dim_of(_rdaeff' user ',2)'');']);

  otherwise
    disp([' more than 3 dimensions for ' num2str(shot) ' ; ' pftype '/' tracename])
    error('in rda_eff')
    
  end

  mdsdisconnect;
  if ~unix('test -d /home/duval/mdsplus')
    rmpath('/home/duval/mdsplus')
  end

else
  % use RDA
  [a,time,x,hsig,error]=jetreaddata(['http://data.jet.uk/' pftype '/' num2str(shot) '/' tracename]);
  % transpose data as output in C format, reversed from Fortran and matlab standard
  ss=size(a);
  nbofdim=length(ss);
  if ss(end)==1; nbofdim=nbofdim-1; end
  nbofdim=max(nbofdim,1);
  if nbofdim==1
    data=a;
  else
    data=a';
  end
end

% to prevent problems when trace empty and time become string
if ischar(time)
  time=[];
end
if ischar(x)
  x=[];
end
if isempty(x) & ~isempty(data) & data==0
  data=[];
end

if isempty(data)
  x=[];
  time=[];
end
