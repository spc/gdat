function list = tcv_available_ids
%
% functions to get ids structure xxx from tcv data are called: tcv_get_ids_xxx.m
% return all these functions assuming they are in the same folder
%

[a1,a2]=fileparts(mfilename('fullpath'));
bb=dir([a1 '/tcv_get_ids_*.m']);
for i=1:numel(bb)
  list{i} = bb(i).name(13:end-2);
end

