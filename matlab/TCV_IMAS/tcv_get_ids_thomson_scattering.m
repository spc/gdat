function [ids_thomson_scattering,ids_thomson_scattering_description] =  tcv_get_ids_thomson_scattering(shot, ids_thomson_scattering_empty, gdat_params,varargin)
%
% [ids_thomson_scattering] =  tcv_get_ids_thomson_scattering(shot, ids_thomson_scattering_empty,varargin);
%
% Get the thomson scattering diagnostics data
%
% ids_thomson_scattering_empty should at least be the empty thomson_scattering ids structure in input
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end

if exist('gdat_params','var')
  [ids_thomson_scattering, ~] = tcv_ids_headpart(shot, ids_thomson_scattering_empty,'thomson_scattering','homogeneous_time',0,'gdat_params',gdat_params,varargin{:});
  % All system
  params_eff = gdat_params;
  if ~isfield(gdat_params,'systems') || isempty(gdat_params,'systems')
    params_eff.systems      = 'all';
  end
  params_eff.data_request = 'ne';
  ne = gdat_tcv(shot,params_eff);
  params_eff.data_request = 'te';
  te = gdat_tcv(shot,params_eff);
else
  [ids_thomson_scattering, ~] = tcv_ids_headpart(shot, ids_thomson_scattering_empty,'thomson_scattering','homogeneous_time',0,varargin{:});
  % Main system
  ne = gdat_tcv(shot, 'ne', 'systems','all');
  te = gdat_tcv(shot, 'te', 'systems','all');
end
ids_thomson_scattering_description = struct();

status = ~ischar(ne.data) & ~ischar(te.data);

if status
  nchannel = size(ne.data_raw);
  ids_thomson_scattering.channel(1:nchannel) = ids_thomson_scattering.channel(1);
  for ii = 1:nchannel
    system = ne.system{ii};
    ids_thomson_scattering.channel{ii}.name = sprintf('%03d/%s',ii,system);
    ids_thomson_scattering_description.channel{ii}.name = sprintf('TS system %s, global index from order in \results::thomson tree',system);
    ids_thomson_scattering.channel{ii}.identifier = sprintf('Z=%+5.3gm/%s',ne.x(ii),system);
    ids_thomson_scattering_description.channel{ii}.identifier = 'Identifier from Vertical position';
    ids_thomson_scattering.channel{ii}.position.r = 0.9;
    ids_thomson_scattering_description.channel{ii}.position.r = 'Fixed R=0.9m';
    ids_thomson_scattering.channel{ii}.position.z = ne.x(ii);
    ids_thomson_scattering_description.channel{ii}.position.z = 'Vertical position ne.x from gdat_tcv(shot,''ne''''systems'',''all'')';
    ids_thomson_scattering.channel{ii}.t_e.data = reshape(te.data(ii,:),[],1);
    ids_thomson_scattering_description.channel{ii}.t_e.data = 'te.data from gdat_tcv(shot,''te''''systems'',''all'')';
    if (ids_thomson_scattering.ids_properties.homogeneous_time == 0)
      ids_thomson_scattering.channel{ii}.t_e.time = reshape(te.t,[],1);
      ids_thomson_scattering_description.channel{ii}.t_e.time = 'te.t from gdat_tcv(shot,''te''''systems'',''all'')';
    end
    ids_thomson_scattering.channel{ii}.n_e.data = reshape(ne.data_raw(ii,:),[],1);
    ids_thomson_scattering_description.channel{ii}.n_e.data = 'ne.data_raw from gdat_tcv(shot,''ne''''systems'',''all'')';
    if (ids_thomson_scattering.ids_properties.homogeneous_time == 0)
      ids_thomson_scattering.channel{ii}.n_e.time = reshape(ne.t,[],1);
      ids_thomson_scattering_description.channel{ii}.n_e.time = 'ne.t from gdat_tcv(shot,''ne''''systems'',''all'')';
    end
    switch error_bar
      case 'delta'
        ids_thomson_scattering.channel{ii}.t_e.data_error_upper = reshape(abs(te.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.t_e.data_error_upper = ['abs(te.error_bar) from gdat_tcv(shot,''te''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.n_e.data_error_upper = reshape(abs(ne.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.n_e.data_error_upper = ['abs(ne.error_bar) from gdat_tcv(shot,''ne''''systems'',''all'') for case ',error_bar];
      case 'delta_with_lower'
        ids_thomson_scattering.channel{ii}.t_e.data_error_upper = reshape(abs(te.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.t_e.data_error_upper = ['abs(te.error_bar) from gdat_tcv(shot,''te''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.t_e.data_error_lower = reshape(abs(te.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.t_e.data_error_lower = ['abs(te.error_bar) from gdat_tcv(shot,''te''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.n_e.data_error_upper = reshape(abs(ne.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.n_e.data_error_upper = ['abs(ne.error_bar) from gdat_tcv(shot,''ne''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.n_e.data_error_lower = reshape(abs(ne.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.n_e.data_error_lower = ['abs(ne.error_bar) from gdat_tcv(shot,''ne''''systems'',''all'') for case ',error_bar];
      case 'added'
        ids_thomson_scattering.channel{ii}.t_e.data_error_upper = reshape(te.data(ii,:)+abs(te.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.t_e.data_error_upper = ['te.data+abs(te.error_bar) from gdat_tcv(shot,''te''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.t_e.data_error_lower = reshape(te.data(ii,:)-abs(te.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.t_e.data_error_lower = ['te.data-abs(te.error_bar) from gdat_tcv(shot,''te''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.n_e.data_error_upper = reshape(ne.data(ii,:)+abs(ne.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.n_e.data_error_upper = ['ne.data+abs(ne.error_bar) from gdat_tcv(shot,''ne''''systems'',''all'') for case ',error_bar];
        ids_thomson_scattering.channel{ii}.n_e.data_error_lower = reshape(ne.data(ii,:)-abs(ne.error_bar(ii,:)),[],1);
        ids_thomson_scattering_description.channel{ii}.n_e.data_error_lower = ['ne.data-abs(ne.error_bar) from gdat_tcv(shot,''ne''''systems'',''all'') for case ',error_bar];
    end
  end
  if (ids_thomson_scattering.ids_properties.homogeneous_time == 1)
    ids_thomson_scattering.time = ne.time(:);
    ids_thomson_scattering_description.time = 'ne.t from gdat_tcv(shot,''ne'')';
  end
end

% make arrays not filled in empty

% cocos automatic transform
if false && exist('ids_generic_cocos_nodes_transformation_symbolic','file') % Disabled until added to COCOStransform
  [ids_thomson_scattering,~]=ids_generic_cocos_nodes_transformation_symbolic(ids_thomson_scattering,'thomson_scattering',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
