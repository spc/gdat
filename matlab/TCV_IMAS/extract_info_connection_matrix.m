function info_pf_active = extract_info_connection_matrix(ids_pf_active)
%
% info_pf_active = extract_info_connection_matrix(ids_pf_active);
%
% Extract the following information from pf_active and connection
% matrix and add them to the ids_pf_active structure
%
% -------------------------------------------
% Which coils belong to a specific circuit added to
% pf_active.circuit{i}.coils_belonging_to_circuit
%
% Which power supply belongs  to a spefic circuit added to
% pf_active.circuit{i}.supplies_belonging_to_circuit
%
% Which circuit does a given coil belong to added to 
% pf_active.coil{i}.belonged_circuit
%
% Which circuit does a given power supply belong to
% pf_active.supply{i}.belonged_circuit
%

info_pf_active = ids_pf_active;

info_pf_active.ntotcoils = numel(ids_pf_active.coil);
info_pf_active.ntotcircuits = numel(ids_pf_active.circuit);
info_pf_active.ntotsupplies = numel(ids_pf_active.supply);
info_pf_active.ntotelements = info_pf_active.ntotcoils + info_pf_active.ntotsupplies;

for ii = 1:info_pf_active.ntotcircuits
    
    % Supplies belonging to the circuit
    supplies_of_circuit = {};
    supplies_of_circuit_ind = [];
    counter = 0;
    for jj=1:info_pf_active.ntotsupplies
        index = (2*(jj-1) + 1);
        if sum(ids_pf_active.circuit{ii}.connections(:,index)) == 1
            counter = counter +1;
            supplies_of_circuit{counter} = ids_pf_active.supply{jj}.name;
            supplies_of_circuit_ind(counter) = jj;
        end
    end
    info_pf_active.circuit{ii}.supplies_belonging_to_circuit = supplies_of_circuit;
    info_pf_active.circuit{ii}.supplies_ind_belonging_to_circuit = supplies_of_circuit_ind;
    
    % Find to which circuit each supply belongs to
    for jj=supplies_of_circuit_ind
        info_pf_active.supply{jj}.belonged_circuit = ids_pf_active.circuit{ii}.name;
        info_pf_active.supply{jj}.belonged_circuit_ind  = ii;
    end
    
    
    % Coils belonging to the circuit
    coils_of_circuit = {};
    coils_of_circuit_ind = [];
    counter = 0;
    for jj=1:info_pf_active.ntotcoils
        index = (2*(jj-1) + 1 + 2*info_pf_active.ntotsupplies);
        if sum(ids_pf_active.circuit{ii}.connections(:,index)) == 1
            counter = counter +1;
            coils_of_circuit{counter} = ids_pf_active.coil{jj}.name;
            coils_of_circuit_ind(counter) = jj;
        end
    end
    info_pf_active.circuit{ii}.coils_belonging_to_circuit = coils_of_circuit;
    info_pf_active.circuit{ii}.coils_ind_belonging_to_circuit = coils_of_circuit_ind;
    
    % Find to which circuit each coil belongs to
    for jj=coils_of_circuit_ind 
        info_pf_active.coil{jj}.belonged_circuit = ids_pf_active.circuit{ii}.name;
        info_pf_active.coil{jj}.belonged_circuit_ind  = ii;
    end

    
    
end



    




