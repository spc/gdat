function [ids_put_status] = ids2database(shot,run,occurence,ids2put,tree_user,tree_tokamak,tree_majorversion,imas_backend,varargin);
%
%   [ids_put_status] = ids2database(shot,run,occurence,ids2put,varargin);
%
% open/create shot,run and put the ids from ids2put.idsnames (names from fieldnames(ids2put) && within IDS_list)
% from new ..._env routines, now user/publi database name, tokamak name and major UAL version (3 or 4 at  this stage) need to be specified
% tree_name: getenv('USER') by default, can be 'public'
% tree_tokamak: 'tcv' by default
% tree_majorversion: '3' by default
% imas_backend: "MDSplus" or "HDF5"
%
% varargin{1}: tbd
%
%
ids_put_status = 0;

tree_user_default = getenv('USER');
tree_tokamak_default = 'tcv';
tree_majorversion_default = '3';
imas_backend_default = 'MDSplus';

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('run', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('occurence', 0, @(x) isempty(x) || (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('ids2put', struct([]), @(x) (isstruct(x)));
p.addOptional('tree_user', tree_user_default, @(x) (isempty(x) || ischar(x)));
p.addOptional('tree_tokamak', tree_tokamak_default, @(x) (isempty(x) || ischar(x)));
p.addOptional('tree_majorversion', tree_majorversion_default, @(x) (ischar(x)));
p.addOptional('imas_backend', imas_backend_default, @(x) (isempty(x) || ischar(x)));

p.parse;
defaults_ids2database = p.Results; % to keep track of defaults

if nargin==1
  p.parse(shot);
  params = p.Results;
elseif nargin==2
  p.parse(shot,run);
  params = p.Results;
elseif nargin==3 && ~ischar(occurence)
  p.parse(shot,run,occurence);
  params = p.Results;
elseif nargin==4
  p.parse(shot,run,occurence,ids2put);
  params = p.Results;
elseif nargin==5
  p.parse(shot,run,occurence,ids2put,tree_user);
  params = p.Results;
elseif nargin==6
  p.parse(shot,run,occurence,ids2put,tree_user,tree_tokamak);
  params = p.Results;
elseif nargin==7
  p.parse(shot,run,occurence,ids2put,tree_user,tree_tokamak,tree_majorversion);
  params = p.Results;
elseif nargin==8
  p.parse(shot,run,occurence,ids2put,tree_user,tree_tokamak,tree_majorversion,imas_backend);
  params = p.Results;
elseif nargin>=9
  p.parse(shot,run,occurence,ids2put,tree_user,tree_tokamak,tree_majorversion,imas_backend,varargin{:});
  params = p.Results;
else
  p.parse;
  p.Results
  return
end

% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  ij=find(mask==1);
  for i=1:length(ij)
    params.(names{ij(i)}) = defaults_ids2database.(names{ij(i)});
  end
  mask2 = structfun(@isempty,params);
  if any(mask2),
    % rm remaining empty fields
    params = rmfield(params,unique([names(mask2)]));
  end
end

params_ids2database = params;
% check ids_names
ids_names=fieldnames(ids2put);
[dummy1]=which('ids_list');
if isempty(dummy1)
  ids_full_list = tcv_available_ids;
  warning(['IDS_list not available, quick fix introducing list of ids available for TCV: ' fprintf('%s ',ids_full_list{:}) char(10)]);
else
  % get effective function between ids_list and IDS_list (which or exist not sufficient)
  [dummy2,ids_list_eff]=fileparts(dummy1);
  ids_full_list = feval(ids_list_eff);
end
ids_names_notok = setdiff(ids_names,ids_full_list);
if ~isempty(ids_names_notok)
  disp(['these subfields are not ids names, so not used: ' sprintf('%s ',ids_names_notok{:})])
end
ids_names_ok = intersect(ids_full_list,ids_names);

if isempty(ids_names_ok)
  disp('no ids names available compatible with IDS_list')
  return
end

if ~ismember(params_ids2database.imas_backend, {'MDSplus', 'HDF5'})
    warning(['Unknown imas_backend: ', imas_backend, ', selecting default: ',imas_backend_default]);
    params_ids2database.imas_backend = imas_backend_default;
end

% Get IMAS backend ID
switch params_ids2database.imas_backend
  case "HDF5"
    backendid=13;
  case "MDSplus"
    backendid=12;
end

try
  %% Initialize the three
  shot_is_new = 1; % seems only create is ok even if does already exist
  mdsplustree0=getenv('MDSPLUS_TREE_BASE_0');
  [aa,bb]=unix(['ls ' mdsplustree0 '/*' num2str(shot) num2str(run,'%4.4d') '*.tree']);
  if aa==0
    shot_is_new = 0;
  end
  if shot_is_new
    idx = imas_create_env_backend(shot, run, params_ids2database.tree_user,params_ids2database.tree_tokamak, params_ids2database.tree_majorversion, backendid)
  else
    idx  = imas_open_env_backend(shot,run,params_ids2database.tree_user,params_ids2database.tree_tokamak,params_ids2database.tree_majorversion,backendid); %
  end

  %% Put the field
  for i=1:length(ids_names_ok)
    ids_to_get = ids_names_ok{i};
    if occurence <= 0
      ids_put(idx,ids_to_get,ids2put.(ids_to_get));
    else
      ids_put(idx,[ids_to_get '/' num2str(occurence)],ids2put.(ids_to_get));
    end
  end

  %% Close the file
  imas_close(idx)
catch ME
  disp('problems in putting data in database:')
  rethrow(ME)
end
