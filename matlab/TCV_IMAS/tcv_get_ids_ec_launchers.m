function [ids_ec_launchers,ids_ec_launchers_description,varargout] = tcv_get_ids_ec_launchers(shot,ids_ec_launchers_empty, gdat_params,varargin);
%
%  [ids_ec_launchers,ids_ec_launchers_description,varargout] = tcv_get_ids_ec_launchers(shot,ids_ec_launchers_empty,varargin);
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

if exist('gdat_params')
  [ids_ec_launchers, params_ec_launchers] = tcv_ids_headpart(shot,ids_ec_launchers_empty,'ec_launchers','homogeneous_time',0, ...
          'gdat_params',gdat_params,varargin{:});
else
  [ids_ec_launchers, params_ec_launchers] = tcv_ids_headpart(shot,ids_ec_launchers_empty,'ec_launchers','homogeneous_time',0,varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try;params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% As a general rule, for a new substructure under the main ids, construct a local structure like:
% "global_quantities" with subfields being the relevant data to get and a local structure:
% "global_quantities_desc" which contains the same subfields themselves containing the gdat string aftre shot used
%

params_eff = params_eff_ref;
params_eff.data_request = 'powers';
pow=gdat_tcv(shot,params_eff);
pow_desc = params_eff.data_request;
if isempty(pow.ec.t) || isempty(pow.ec.data) || max(pow.ec.data(:,end))<1e-10
  ids_ec_launchers_description.comment = 'no power';
  return
end

nb_launchers = size(pow.ec.data,2)-1; % at this stage keep same numbering as standard TCV launchers before mid-2019

ids_ec_launchers.beam(1:nb_launchers) = ids_ec_launchers.beam(1); % copy empty structure for all units, then fill in
ids_ec_launchers_description.comment = ['assume ' num2str(nb_launchers) ' launchers'];

if shot>=63000
  warning('not sure if set-up correct for new launchers, beams, gyrotrons. Check with O. Sauter')
  %  return
end

% $$$ ids_ec_launchers.time = pow.ec.t; if homogeneous time
% $$$ ids_ec_launchers_description.time = 'time from power';
[r_phi0,z_phi0,r1,z1,rc,zc,thetphi_L,thetphi_tor,raysbylauncher,powers,fname,z_axis,launch_params] = ...
    toray_raygeom_TCV(shot,pow.ec.t(1:10:end));
ids_ec_launchers_description.launch_params = launch_params;

for iant=1:nb_launchers
  ids_ec_launchers.beam{iant}.identifier = ['L' num2str(iant)];
  switch iant
   case {1,4}
    ids_ec_launchers.beam{iant}.name = 'LFS equatorial launcher';
   case {2, 3, 5, 6}
    ids_ec_launchers.beam{iant}.name = 'LFS top launcher';
   otherwise
    ids_ec_launchers.beam{iant}.name = 'TOP launcher';
  end
  if any(isfinite(pow.ec.data(:,iant)))
    ids_ec_launchers.beam{iant}.power_launched.data = pow.ec.data(:,iant);
    ids_ec_launchers.beam{iant}.power_launched.time = pow.ec.t;
    ids_ec_launchers_description.beam{iant}.power_launched = 'from gdat powers .ec';
    ids_ec_launchers_description.beam{iant}.launching_position = 'from launch_params obtained from toray_raygeom_TCV';
  else
    ids_ec_launchers_description.beam{iant}.power_launched = 'no power for this launcher from gdat powers .ec';
  end
  if length(launch_params)>=iant && ~isempty(launch_params{iant})
    % find 1st non-empty data
    it_ok{iant} = [];
    for it=1:length(launch_params{iant})
      if ~isempty(launch_params{iant}{it})
        it_ok{iant}(end+1) = it;
      end
    end
    % non time-dependent quantities, take 1st ok values
    ids_ec_launchers.beam{iant}.frequency.time = [pow.ec.t(1) pow.ec.t(end)];
    ids_ec_launchers.beam{iant}.frequency.data =[launch_params{iant}{it_ok{iant}(1)}.freq launch_params{iant}{it_ok{iant}(end)}.freq];
    ids_ec_launchers.beam{iant}.mode = -1; % at this stage assume X mode always, to change when available
    for i=1:length(it_ok{iant})
      r0 = sqrt(launch_params{iant}{it_ok{iant}(i)}.x0.^2 + launch_params{iant}{it_ok{iant}(i)}.y0.^2) / 100.; % in [m]
      time_launch = launch_params{iant}{it_ok{iant}(i)}.time;
      ids_ec_launchers.beam{iant}.launching_position.r(i) = r0;
      ids_ec_launchers.beam{iant}.launching_position.z(i) = launch_params{iant}{it_ok{iant}(i)}.z0/100.;
      ids_ec_launchers.beam{iant}.launching_position.phi(i) = atan2(launch_params{iant}{it_ok{iant}(i)}.y0/100,r0);
      ids_ec_launchers.beam{iant}.time(i) = time_launch;

      % define angles based on TORAY angles, as discussed with F. Poli Oct 2024
      % Reminder: theta_toray, zero on z-axis, pointing inwards with 90deg
      %           phi_toray, 0deg=outwards radial, 180deg=pointing
      %           towards core, +: clockwise, -: counter-clockwise
      % IDS angles have same convention as TORBEAM and GRAY
      pol_angle_ids = launch_params{iant}{it_ok{iant}(i)}.theta_toray-90;
      tor_angle_ids = launch_params{iant}{it_ok{iant}(i)}.phi_toray-180;

      ids_ec_launchers.beam{iant}.steering_angle_pol(i) = pol_angle_ids*pi/180;
      ids_ec_launchers.beam{iant}.steering_angle_tor(i) = tor_angle_ids*pi/180;
      ids_ec_launchers.beam{iant}.spot.size(1,i) = 0.023;
      ids_ec_launchers.beam{iant}.spot.size(2,i) = 0.012;
      ids_ec_launchers.beam{iant}.spot.angle(i) = 0.0;
      ids_ec_launchers.beam{iant}.phase.curvature(1,i) = 1./1.88;
      ids_ec_launchers.beam{iant}.phase.curvature(2,i) = 1./-0.95; % negative in launch_params...
      ids_ec_launchers.beam{iant}.phase.angle(i) = 0.0;
    end
  end
end

% cocos automatic transform
if exist('ids_generic_cocos_nodes_transformation_symbolic') == 2

  [ids_ec_launchers,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_ec_launchers,'ec_launchers',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
