function [ids_struct_out,ids_struct_out_description] =  tcv_ids_wall_description_2d(ids_structures)

ids_struct_out =  ids_structures;

% There might be many 2d descriptions
%% Type 0 "index = 0 for equilibrium codes (single closed limiter and vessel)"

ids_struct_out{1}.type.index = 0;
% limiter
r_l = mdsdata('STATIC("R_T")');
z_l = mdsdata('STATIC("Z_T")');
ids_struct_out{1}.limiter.unit{1}.outline.r = r_l;
ids_struct_out_description{1}.limiter.unit{1}.outline_r = 'from ''STATIC("R_T")''';
ids_struct_out{1}.limiter.unit{1}.outline.z = z_l;
ids_struct_out_description{1}.limiter.unit{1}.outline_z = 'from ''STATIC("Z_T")''';

% vessel
r_v_in = mdsdata('STATIC("R_V:IN")');
z_v_in = mdsdata('STATIC("Z_V:IN")');
r_v_out = mdsdata('STATIC("R_V:OUT")');
z_v_out = mdsdata('STATIC("Z_V:OUT")');
ids_struct_out{1}.vessel.unit{1}.annular.outline_inner.r = r_v_in ;
ids_struct_out_description{1}.vessel.unit{1}.annular.outline_inner.r = 'from ''STATIC("R_V:IN")''';
ids_struct_out{1}.vessel.unit{1}.annular.outline_inner.z = z_v_in;
ids_struct_out_description{1}.vessel.unit{1}.annular.outline_inner.z = 'from ''STATIC("Z_V:IN")''';
ids_struct_out{1}.vessel.unit{1}.annular.outline_outer.r = r_v_out;
ids_struct_out_description{1}.vessel.unit{1}.annular.outline_outer.r = 'from ''STATIC("R_V:OUT")''';
ids_struct_out{1}.vessel.unit{1}.annular.outline_outer.z = z_v_out;
ids_struct_out_description{1}.vessel.unit{1}.annular.outline_outer.z = 'from ''STATIC("Z_V:OUT")''';

% The value of the resistivity is taken as an average of the value in Fig.3
% of Jean-Marc LIUQE paper.
ids_struct_out_description{1}.vessel.unit{1}.annular.resistivity = 0.25*1e-3; %[ohm m]
ids_struct_out{1}.vessel.unit{1}.annular.resistivity = 0.25*1e-3; %[ohm m]
%% TODO Add the other description of the wall and the input pharser




