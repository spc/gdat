function [ids_core_profiles,ids_core_profiles_description,varargout] = ...
  tcv_get_ids_core_profiles(shot,ids_equil_empty,gdat_params,varargin)
%
% [ids_core_profiles,ids_core_profiles_description,varargout] = ...
%     tcv_get_ids_core_profiles(shot,ids_equil_empty,gdat_params,varargin);
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call,
%              in particular error_bar options
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end

machine = 'tcv';
tens_time = -1;
tens_rho = -0.1;

if exist('gdat_params','var')
  [ids_core_profiles, params_cores_profiles] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_profiles','gdat_params',gdat_params,varargin{:});
else
  [ids_core_profiles, params_cores_profiles] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_profiles',varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% initialize description
ids_core_profiles_description = [];

%% setup profiles_1d
% base all from times for nete_rho (which should be conf by default)
params_eff = params_eff_ref;
params_eff.data_request='ne_rho'; params_eff.fit = 1;
temp_1d.ne_rho = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.ne_rho = params_eff.data_request;
params_eff.data_request = 'te_rho';
temp_1d.te_rho = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.te_rho = params_eff.data_request;
% compute grids_1d for fit rhopol grid, need a gdat_data like structure
temp_1d.fit.te_rho = temp_1d.te_rho.fit;
temp_1d.fit.te_rho.gdat_params = temp_1d.te_rho.gdat_params;
temp_1d.fit.te_rho.shot = temp_1d.te_rho.shot;
temp_1d.fit.te_rho = get_grids_1d(temp_1d.fit.te_rho,1,1);

params_eff.data_request = 'psi_axis';
temp_1d.psi_axis = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.psi_axis = params_eff.data_request;
params_eff.data_request = 'psi_edge';
temp_1d.psi_edge = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.psi_edge = params_eff.data_request;

temp_1d.fit.ne_rho = temp_1d.ne_rho.fit;
if isempty(temp_1d.te_rho.fit.t)
  disp('te_rho.fit')
  temp_1d.te_rho.fit
  warning('may need to run analysis again for profiles fitting, ask O. Sauter')
  return
end

ids_core_profiles.time = temp_1d.te_rho.fit.t;
ids_core_profiles_description.time = ['.t subfield from: [''te_rho'',''subnode'',''fit.t''] thus from ' temp_1d.te_rho.fit.data_fullpath];

% make empty cell arrays for subnodes not filled in before copying default structure to all times
ids_core_profiles.profiles_1d{1}.ion{1}.state = {};
ids_core_profiles.profiles_1d{1}.neutral{1}.state = {};
ids_core_profiles.profiles_1d(1:length(ids_core_profiles.time)) = ids_core_profiles.profiles_1d(1);

% As a general rule, for a new substructure under the main ids, construct a local structure like:
% "global_quantities" with subfields being the relevant data to get and a local structure:
% "global_quantities_desc" which contains the same subfields themselves containing the gdat string aftre shot used

%% vacuum_toroidal_field and time, using homogeneous
params_eff = params_eff_ref;
params_eff.data_request='b0';
vacuum_toroidal_field.b0=gdat(params_cores_profiles.shot,params_eff);
vacuum_toroidal_field_desc.b0 = params_eff.data_request;
ids_core_profiles.vacuum_toroidal_field.r0 = vacuum_toroidal_field.b0.r0;
ids_core_profiles.vacuum_toroidal_field.b0 = interpos(63,vacuum_toroidal_field.b0.t,vacuum_toroidal_field.b0.data,ids_core_profiles.time,-1);
ids_core_profiles_description.vacuum_toroidal_field = [vacuum_toroidal_field_desc.b0 ' on ids_core_profiles.time, with interpos(63)'];

%% global_quantities
% data into local global_quantities.* structure with correct end names and
% global_quantities_desc.* with description.
% Use temp.* and temp_desc.* structures for temporary data

params_eff.data_request='ip';
global_quantities.ip = gdat(params_cores_profiles.shot,'ip','machine',machine);
global_quantities_desc.ip = params_eff.data_request;

params_eff.data_request = 'ohm_data';
current_non_inductive = gdat(params_cores_profiles.shot,params_eff);
global_quantities.current_non_inductive.data = current_non_inductive.ohm.ohm_data.cd_tot.data;
global_quantities.current_non_inductive.t = current_non_inductive.ohm.ohm_data.cd_tot.t;
global_quantities_desc.current_non_inductive = current_non_inductive.ohm.ohm_data.cd_tot.label;

params_eff.data_request = 'bs_data';
current_bootstrap = gdat(params_cores_profiles.shot,params_eff);
global_quantities.current_bootstrap.data = current_bootstrap.bs.bs_data.cd_tot.data;
global_quantities.current_bootstrap.t = current_bootstrap.bs.bs_data.cd_tot.t;
global_quantities_desc.current_bootstrap = current_bootstrap.bs.bs_data.cd_tot.label;

params_eff.data_request = 'vloop';
global_quantities.v_loop = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.v_loop = params_eff.data_request;
params_eff.data_request = 'li';
global_quantities.li_3 = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.li_3 = params_eff.data_request;
params_eff.data_request = 'beta';
global_quantities.beta_tor = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.beta_tor = params_eff.data_request;
params_eff.data_request = 'betan';
global_quantities.beta_tor_norm = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.beta_tor_norm = params_eff.data_request;
params_eff.data_request = 'betap';
global_quantities.beta_pol = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.beta_pol = params_eff.data_request;
params_eff.data_request = 'w_mhd';
global_quantities.energy_diamagnetic = gdat(params_cores_profiles.shot,params_eff);
global_quantities_desc.energy_diamagnetic = params_eff.data_request;
params_eff.data_request = 'results.conf:z_eff'; params_eff_fit1=params_eff; params_eff_fit1.fit=1;
global_quantities.z_eff_resistive = gdat(params_cores_profiles.shot,params_eff_fit1);
global_quantities_desc.z_eff_resistive = params_eff_fit1.data_request;

ids_core_profiles_description.global_quantities = global_quantities_desc;
global_quantities_fieldnames = fieldnames(global_quantities);
ids_core_profiles_global_quantities_fieldnames = fieldnames(ids_core_profiles.global_quantities);
global_quantities_fieldnames_eff = intersect(global_quantities_fieldnames,ids_core_profiles_global_quantities_fieldnames);
special_fields = {''}; % fields needing non-automatic treatments
for i=1:length(global_quantities_fieldnames_eff)
  if ~any(strcmp(global_quantities_fieldnames_eff{i},special_fields))
    if ~isstruct(ids_core_profiles.global_quantities.(global_quantities_fieldnames_eff{i}))
      ids_core_profiles.global_quantities.(global_quantities_fieldnames_eff{i}) = ...
          interpos(global_quantities.(global_quantities_fieldnames_eff{i}).t, ...
          global_quantities.(global_quantities_fieldnames_eff{i}).data,ids_core_profiles.time,tens_time);
    else
      special_fields{end+1} = global_quantities_fieldnames_eff{i};
    end
  end
end

%% profiles_1d (cannot use eqdsk since not same radial mesh)

% prepare area for ids_core_profiles.profiles_1d{*}.grid.area
params_eff.data_request = 'area_rho';
temp_1d.area = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.area = params_eff.data_request;
for ir=1:length(temp_1d.area.x) % map tmp_1d.area to core_profiles.time
  area_cpt(ir,:) = interpos(temp_1d.area.t,temp_1d.area.data(ir,:),ids_core_profiles.time,tens_time);
end

it_thom = iround_os(temp_1d.te_rho.t,ids_core_profiles.time);
for it=1:length(ids_core_profiles.time)
  % fill grid
  ids_core_profiles.profiles_1d{it}.grid.rho_tor_norm = temp_1d.fit.te_rho.grids_1d.rhotornorm(:,it);
  ids_core_profiles.profiles_1d{it}.grid.rho_tor = temp_1d.fit.te_rho.grids_1d.rhotornorm(:,it) ...
      .* temp_1d.fit.te_rho.grids_1d.rhotor_edge(it);
  ids_core_profiles.profiles_1d{it}.grid.rho_pol_norm = temp_1d.fit.te_rho.grids_1d.rhopolnorm;
  ids_core_profiles.profiles_1d{it}.grid.psi = temp_1d.fit.te_rho.grids_1d.psi(:,it);
  ids_core_profiles.profiles_1d{it}.grid.psi_magnetic_axis = 0. * temp_1d.fit.te_rho.grids_1d.psi(1,it);
  ids_core_profiles.profiles_1d{it}.grid.psi_boundary = temp_1d.fit.te_rho.grids_1d.psi(end,it);
  ids_core_profiles.profiles_1d{it}.grid.volume = temp_1d.fit.te_rho.grids_1d.rhovolnorm(:,it).^2 ...
      .* temp_1d.fit.te_rho.grids_1d.volume_edge(it);
  ids_core_profiles.profiles_1d{it}.grid.area = interpos(temp_1d.area.x,area_cpt(:,it),temp_1d.fit.te_rho.grids_1d.rhopolnorm, ...
          tens_rho,[1 2],[0 area_cpt(end,it)]);
  % fill time
  ids_core_profiles.profiles_1d{it}.time = ids_core_profiles.time(it);
  % fill electrons struct
  ids_core_profiles.profiles_1d{it}.electrons.temperature = temp_1d.fit.te_rho.data(:,it);
  ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured = temp_1d.te_rho.data(:,it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.time_measurement = temp_1d.te_rho.t(it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.rho_tor_norm = temp_1d.te_rho.grids_1d.rhotornorm(:,it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.source = {'Thomson, interpos fit'};
  ids_core_profiles.profiles_1d{it}.electrons.density = temp_1d.fit.ne_rho.data(:,it);
  ids_core_profiles.profiles_1d{it}.electrons.density_thermal = ids_core_profiles.profiles_1d{it}.electrons.density;
  ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured = temp_1d.ne_rho.data(:,it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.density_fit.time_measurement = temp_1d.ne_rho.t(it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.density_fit.rho_tor_norm = temp_1d.ne_rho.grids_1d.rhotornorm(:,it_thom(it));
  ids_core_profiles.profiles_1d{it}.electrons.density_fit.source = {'Thomson, interpos fit'};
  ids_core_profiles.profiles_1d{it}.electrons.pressure_thermal = 1.6022e-19.*ids_core_profiles.profiles_1d{it}.electrons.density_thermal ...
      .* ids_core_profiles.profiles_1d{it}.electrons.temperature;
  % fill zeff
  ids_core_profiles.profiles_1d{it}.zeff = global_quantities.z_eff_resistive.data(it) .* ...
      ones(size(ids_core_profiles.profiles_1d{it}.electrons.density));
end

zeff_error = 0.5;
switch error_bar
 case 'delta'
  for it=1:length(ids_core_profiles.time)
    ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_upper = temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_upper = temp_1d.ne_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.zeff_error_upper = zeff_error .* ones(size(ids_core_profiles.profiles_1d{it}.zeff));
  end
 case 'delta_with_lower'
  for it=1:length(ids_core_profiles.time)
    ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_upper = temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_lower = ...
        ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_upper;
    ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_upper = temp_1d.ne_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_lower = ...
        ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_upper;
    ids_core_profiles.profiles_1d{it}.zeff_error_upper = zeff_error .* ones(size(ids_core_profiles.profiles_1d{it}.zeff));
    ids_core_profiles.profiles_1d{it}.zeff_error_lower = ids_core_profiles.profiles_1d{it}.zeff_error_upper;
  end
 case 'added'
  for it=1:length(ids_core_profiles.time)
    ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_upper = ...
        ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured ...
        + temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured_error_lower = ...
        ids_core_profiles.profiles_1d{it}.electrons.temperature_fit.measured ...
        - temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_upper = ...
        ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured ...
        + temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured_error_lower = ...
        ids_core_profiles.profiles_1d{it}.electrons.density_fit.measured ...
        - temp_1d.te_rho.error_bar(:,it_thom(it));
    ids_core_profiles.profiles_1d{it}.zeff_error_upper = min(6,ids_core_profiles.profiles_1d{it}.zeff ...
        + zeff_error .* ones(size(ids_core_profiles.profiles_1d{it}.zeff)));
    ids_core_profiles.profiles_1d{it}.zeff_error_upper = max(1,ids_core_profiles.profiles_1d{it}.zeff ...
        - zeff_error .* ones(size(ids_core_profiles.profiles_1d{it}.zeff)));
  end
 otherwise
  error(['tcv_ids_bpol_loop: error_bar option not known: ' error_bar])
end

%% ion struct
% assume only D if no CXRS (need to ask how to check if H...)
params_eff_fit1.data_request = 'cxrs';
try
  temp_1d.cxrs_rho = gdat(params_cores_profiles.shot,params_eff_fit1);
  temp_1d_desc.cxrs_rho = params_eff_fit1.data_request;
catch
  temp_1d.cxrs_rho.data = [];
  temp_1d_desc.cxrs_rho = ['Problem with gdat ' params_eff_fit1.data_request ' ; no data'];
end
params_eff_fit1.data_request = 'results.conf:ti';
temp_1d.ti_conf_rho = gdat(params_cores_profiles.shot,params_eff_fit1);
temp_1d_desc.ti_conf_rho = params_eff_fit1.data_request;
params_eff_fit1.data_request = 'results.conf:ni';
temp_1d.ni_conf_rho = gdat(params_cores_profiles.shot,params_eff_fit1);
temp_1d_desc.ni_conf_rho = params_eff_fit1.data_request;
if ~isempty(temp_1d.cxrs_rho.data)
  data_fullpath_raw = 'Ti(C sometimes B) from cxrs system 1 to 3';
  temp_1d.ti.raw = temp_1d.cxrs_rho.ti.raw;
  temp_1d.ti.raw.shot = temp_1d.cxrs_rho.shot;temp_1d.ti.raw.gdat_params = temp_1d.cxrs_rho.gdat_params;
  temp_1d.ti.raw.x =temp_1d.cxrs_rho.ti.raw.rho; temp_1d.ti.raw.t =temp_1d.cxrs_rho.t;
  if ~isempty(temp_1d.cxrs_rho.ti.raw.data)
    data_fullpath_fit = 'Ti from fit from cxrs thus Ti(C)';
    temp_1d.ti.raw =get_grids_1d(temp_1d.ti.raw,2,1);
  else
    data_fullpath_fit = 'Ti from fit in CONF node';
  end
else
  data_fullpath_fit = 'Ti from fit in CONF node';
end
temp_1d_desc.ti.raw = data_fullpath_fit;
temp_1d.ti.fit = temp_1d.ti_conf_rho;
temp_1d.ti.fit =get_grids_1d(temp_1d.ti.fit,1,1);
temp_1d_desc.ti.fit = temp_1d_desc.ti_conf_rho;
temp_1d.ni.fit = temp_1d.ni_conf_rho;
temp_1d.ni.fit =get_grids_1d(temp_1d.ni.fit,1,1);
temp_1d_desc.ni.fit = temp_1d_desc.ni_conf_rho;
it_ti = iround_os(temp_1d.ti.fit.t,ids_core_profiles.time);
% assumed 1 impurity with Zp=6
Zp = 6.;
for it=1:length(ids_core_profiles.time)
  % Duplicate ion substructure
  ids_core_profiles.profiles_1d{it}.ion(1:2) = ids_core_profiles.profiles_1d{it}.ion(1);

  ids_core_profiles.profiles_1d{it}.ion{1}.element{1}.a = 2.;
  ids_core_profiles.profiles_1d{it}.ion{1}.element{1}.z_n = 1;
  ids_core_profiles.profiles_1d{it}.ion{1}.element{1}.atoms_n = 1;
  ids_core_profiles.profiles_1d{it}.ion{1}.z_ion = 1;
  ids_core_profiles.profiles_1d{it}.ion{1}.multiple_states_flag = 0;
  ids_core_profiles.profiles_1d{it}.ion{1}.temperature = temp_1d.ti.fit.data(:,it_ti(it));
  ids_core_profiles.profiles_1d{it}.ion{1}.density = (temp_1d.ni.fit.data(:,it_ti(it)).*Zp-ids_core_profiles.profiles_1d{it}.electrons.density)./(Zp-1.);
  ids_core_profiles.profiles_1d{it}.ion{1}.density_thermal = ids_core_profiles.profiles_1d{it}.ion{1}.density;
  ids_core_profiles.profiles_1d{it}.ion{1}.pressure_thermal = 1.6022e-19.*ids_core_profiles.profiles_1d{it}.ion{1}.density_thermal ...
      .* ids_core_profiles.profiles_1d{it}.ion{1}.temperature;
  ids_core_profiles.profiles_1d{it}.ion{1}.label = 'D+';
  %
  ids_core_profiles.profiles_1d{it}.t_i_average = ids_core_profiles.profiles_1d{it}.ion{1}.temperature;
  ids_core_profiles.profiles_1d{it}.n_i_thermal_total = ids_core_profiles.profiles_1d{it}.ion{1}.density_thermal;
  ids_core_profiles.profiles_1d{it}.pressure_ion_total = 1.6022e-19 .* ids_core_profiles.profiles_1d{it}.n_i_thermal_total ...
      .* ids_core_profiles.profiles_1d{it}.t_i_average;
  ids_core_profiles.profiles_1d{it}.pressure_thermal = ids_core_profiles.profiles_1d{it}.pressure_ion_total ...
      + ids_core_profiles.profiles_1d{it}.electrons.pressure_thermal;
  %
  % C from zeff and above Ti at this stage, should take from cxrs if available but then add something for Zeff matching above
  %
  ids_core_profiles.profiles_1d{it}.ion{2}.element{1}.a = 12.;
  ids_core_profiles.profiles_1d{it}.ion{2}.element{1}.z_n = 6.;
  ids_core_profiles.profiles_1d{it}.ion{2}.element{1}.atoms_n = 1.;
  ids_core_profiles.profiles_1d{it}.ion{2}.z_ion = 6.;
  ids_core_profiles.profiles_1d{it}.ion{2}.multiple_states_flag = 0;
  ids_core_profiles.profiles_1d{it}.ion{2}.temperature = ids_core_profiles.profiles_1d{it}.ion{1}.temperature;
  ids_core_profiles.profiles_1d{it}.ion{2}.density = (ids_core_profiles.profiles_1d{it}.electrons.density - ids_core_profiles.profiles_1d{it}.ion{1}.density) ./ ids_core_profiles.profiles_1d{it}.ion{2}.z_ion;
  ids_core_profiles.profiles_1d{it}.ion{2}.density_thermal = ids_core_profiles.profiles_1d{it}.ion{2}.density;
  ids_core_profiles.profiles_1d{it}.ion{2}.pressure_thermal = 1.6022e-19.*ids_core_profiles.profiles_1d{it}.ion{2}.density_thermal ...
      .* ids_core_profiles.profiles_1d{it}.ion{2}.temperature;
  ids_core_profiles.profiles_1d{it}.ion{2}.label = 'C6+';
  % average/sums
  ids_core_profiles.profiles_1d{it}.t_i_average = ids_core_profiles.profiles_1d{it}.ion{1}.temperature;
  ids_core_profiles.profiles_1d{it}.n_i_thermal_total = ids_core_profiles.profiles_1d{it}.ion{1}.density_thermal + ...
      ids_core_profiles.profiles_1d{it}.ion{2}.density_thermal;
  ids_core_profiles.profiles_1d{it}.pressure_ion_total = 1.6022e-19 .* ids_core_profiles.profiles_1d{it}.n_i_thermal_total ...
      .* ids_core_profiles.profiles_1d{it}.t_i_average;
  ids_core_profiles.profiles_1d{it}.pressure_thermal = ids_core_profiles.profiles_1d{it}.pressure_ion_total ...
      + ids_core_profiles.profiles_1d{it}.electrons.pressure_thermal;
end
if ~isempty(temp_1d.cxrs_rho.data) && ~isempty(temp_1d.cxrs_rho.ti.fit.data)
  it_raw = iround_os(temp_1d.ti.raw.t,ids_core_profiles.time);
  for it=1:length(ids_core_profiles.time)
    % ids_core_profiles.profiles_1d{it}.ion{1}.temperature_fit = temp_1d.ti.fit(:,it_ti(it));
    ids_core_profiles.profiles_1d{it}.ion{1}.density_fit.source = {'from Zeff and ne profile'};
    ids_core_profiles.profiles_1d{it}.t_i_average_fit.measured = temp_1d.ti.raw.data(:,it_raw(it));
    ids_core_profiles.profiles_1d{it}.t_i_average_fit.source = {'from CXRS on C usually'};
  end
end

%% q profile and magnetic shear
params_eff.data_request = 'q_rho';
temp_1d.q = gdat(params_cores_profiles.shot,params_eff);
temp_1d_desc.q = params_eff.data_request;
for ir=1:length(temp_1d.q.x) % map tmp_1d.q to core_profiles.time
  q_cpt(ir,:) = interpos(temp_1d.q.t,temp_1d.q.data(ir,:),ids_core_profiles.time,tens_time);
end

for it=1:length(ids_core_profiles.time)
  ij=isfinite(q_cpt(:,it));
  if sum(ij) >= 5
    [ids_core_profiles.profiles_1d{it}.q] = interpos(temp_1d.q.x(ij),q_cpt(ij,it),temp_1d.fit.te_rho.grids_1d.rhopolnorm, ...
          tens_rho,[1 0],[0 0]);
    [qfit,dqdrhotor] = interpos(ids_core_profiles.profiles_1d{it}.grid.rho_tor,ids_core_profiles.profiles_1d{it}.q,tens_rho,[1 0],[0 0]);
    ids_core_profiles.profiles_1d{it}.magnetic_shear = ids_core_profiles.profiles_1d{it}.grid.rho_tor./ids_core_profiles.profiles_1d{it}.q ...
        .* dqdrhotor;
  elseif sum(ij) > 0
    ids_core_profiles.profiles_1d{it}.q = interp1(temp_1d.q.x(ij),q_cpt(ij,it),temp_1d.fit.te_rho.grids_1d.rhopolnorm);
    ids_core_profiles.profiles_1d{it}.magnetic_shear = -9.e40;
  else
    ids_core_profiles.profiles_1d{it}.q = -9.e40;
    ids_core_profiles.profiles_1d{it}.magnetic_shear = -9.e40;
  end
end
temp_1d_desc.magnetic_shear = 'from interpos with rhotor';

%% Current densities

for it=1:length(ids_core_profiles.time)
  ids_core_profiles.profiles_1d{it}.j_bootstrap = ...
    current_bootstrap.bs.bs_data.cd_dens.data(:,it);
  temp_1d_desc.j_bootstrap = current_bootstrap.bs.bs_data.cd_dens.label;
  ids_core_profiles.profiles_1d{it}.j_ohmic = ...
    current_non_inductive.ohm.ohm_data.cd_dens.data(:,it);
  temp_1d_desc.j_ohmic = current_non_inductive.ohm.ohm_data.cd_dens.label;
end

%% parallel conductivity (neoclassical)
signeo = gdat(params_cores_profiles.shot,'\results::ibs:signeo');
for it=1:length(ids_core_profiles.time)
  ids_core_profiles.profiles_1d{it}.conductivity_parallel = signeo.data(:,it);
  temp_1d_desc.conductivity_parallel = signeo.label;
end

%% add descriptions for profiles_1d
ids_core_profiles_description.profiles_1d = temp_1d_desc;

if nargin <= 2
  ids_core_profiles.code.name = ['tcv_get_ids_core_profiles, within gdat, with shot= ' num2str(params_cores_profiles.shot) ];
else
  ids_core_profiles.code.name = ['tcv_get_ids_core_profiles, within gdat, with shot= ' num2str(params_cores_profiles.shot) '; varargin: ' varargin{:}];
end
ids_core_profiles_description.code.name = ids_core_profiles.code.name;

ids_core_profiles.code.output_flag = zeros(size(ids_core_profiles.time));

% make arrays not filled in empty:
for it=1:length(ids_core_profiles.time)
  ids_core_profiles.profiles_1d{it}.neutral = {};
end

% cocos automatic transform
if ~isempty(which('ids_generic_cocos_nodes_transformation_symbolic'))
  [ids_core_profiles,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_core_profiles,'core_profiles',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
