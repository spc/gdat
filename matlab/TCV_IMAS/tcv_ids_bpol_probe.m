function [sub_ids_struct_out,sub_ids_struct_out_description, varargout] =  tcv_ids_bpol_probe(shot, ids_structures, gdat_params, varargin)
%
% [sub_ids_struct_out,sub_ids_struct_out_description, varargout] =  tcv_ids_bpol_probe(shot, ids_structures, gdat_params, varargin)
%
% Get magnetics.bpol_probe
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% Get data
params_eff = params_eff_ref;
params_eff.data_request = '\MAGNETICS::BPOL_003';
tmp = gdat_tcv( shot,params_eff);
% tmp_desc = params_eff.data_request;
names = tmp.dim{2};
time = tmp.dim{1};
data = tmp.data;
ang_m = mdsvalue('static("ANG_M")[$1]',names);
ang_m_desc = 'from ''static("ANG_M")''';
r_m = mdsvalue('static("R_M")[$1]',names);
r_m_desc = 'from ''static("R_M")''';
z_m = mdsvalue('static("Z_M")[$1]',names);
z_m_desc = 'from ''static("Z_M")''';
area = mdsvalue('\magnetics::bpol_003:area');
area_desc = 'from ''\magnetics::bpol_003_area''';

% from mapping file from Holger:
probes_length = 2.400000D-02;

% Preallocate output structure
Nprobes = size(tmp.data,2);
sub_ids_struct_out(1:Nprobes) = ids_structures;
sub_ids_struct_out_description = cell(1,Nprobes);

for ii=1:Nprobes
    sub_ids_struct_out{ii}.name  = names{ii};
    sub_ids_struct_out_description{ii}.name  = names{ii};
    sub_ids_struct_out{ii}.position.r  = r_m(ii);
    sub_ids_struct_out_description{ii}.position_r  = r_m_desc;
    sub_ids_struct_out{ii}.position.z  = z_m(ii);
    sub_ids_struct_out_description{ii}.position_z  = z_m_desc;
    sub_ids_struct_out{ii}.field.data  =  data(:,ii);
    sub_ids_struct_out_description{ii}.field_data  =  'from ''\MAGNETICS::BPOL_003''';
    sub_ids_struct_out{ii}.field.time  =  time;

    sub_ids_struct_out{ii}.toroidal_angle  =  0.;
    sub_ids_struct_out{ii}.poloidal_angle  = -ang_m(ii);
    sub_ids_struct_out{ii}.area = area(ii);
    sub_ids_struct_out{ii}.length = probes_length;
    sub_ids_struct_out_description{ii}.poloidal_angle  = ang_m_desc;
    sub_ids_struct_out_description{ii}.toroidal_angle = 'set to 0';
    sub_ids_struct_out_description{ii}.area = area_desc;
    sub_ids_struct_out_description{ii}.length = 'from array in machine description file';
end

fixed_error = 0.01;
switch error_bar
 case 'delta'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.field.data_error_upper = fixed_error.*ones(size(sub_ids_struct_out{ii}.field.data));% TO BE FOUND;
    sub_ids_struct_out_description{ii}.field_data_error_upper = ['from fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.field_data_error_lower = 'not provided since symmetric';
  end
 case 'delta_with_lower'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.field.data_error_upper = fixed_error.*ones(size(sub_ids_struct_out{ii}.field.data));
    sub_ids_struct_out{ii}.field.data_error_lower = sub_ids_struct_out{ii}.field.data_error_upper;
    sub_ids_struct_out_description{ii}.field_data_error_upper = ['from fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.field_data_error_lower = ['from fixed error value in case ' error_bar];
  end
 case 'added'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.field.data_error_upper = sub_ids_struct_out{ii}.field.data + fixed_error.*ones(size(sub_ids_struct_out{ii}.field.data));
    sub_ids_struct_out{ii}.field.data_error_lower = sub_ids_struct_out{ii}.field.data - fixed_error.*ones(size(sub_ids_struct_out{ii}.field.data));
    sub_ids_struct_out_description{ii}.field_data_error_upper = ['from data + fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.field_data_error_lower = ['from data - fixed error value in case ' error_bar];
  end
 otherwise
  error(['tcv_ids_bpol_loop: error_bar option not known: ' error_bar])
end
