function [ids_summary,ids_summary_description,varargout] = tcv_get_ids_summary(shot,ids_equil_empty, gdat_params,varargin)
%
%  [ids_summary,ids_summary_description,varargout] = tcv_get_ids_summary(shot,ids_equil_empty,varargin);
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar and cocos_out options
%

if exist('gdat_params','var')
  [ids_summary, params_summary] = tcv_ids_headpart(shot,ids_equil_empty,'summary','gdat_params',gdat_params,varargin{:});
else
  [ids_summary, params_summary] = tcv_ids_headpart(shot,ids_equil_empty,'summary',varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end

% As a general rule, for a new substructure under the main ids, construct a local structure like:
% "global_quantities" with subfields being the relevant data to get and a local structure:
% "global_quantities_desc" which contains the same subfields themselves containing the gdat string after shot used
%
% vacuum_toroidal_field and time, using homogeneous
%
%% liuqe.m at this stage is missing correction 0.996, so use std source by time of default liuqe to make sure
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% code description
[abcpath,abcfile,abcext]=fileparts(mfilename('fullpath'));
[aa1,aa2]=unix(['cd ' abcpath '; git describe --tags --first-parent --abbrev=11 --long --dirty --always']);
git_release_hash='gdat git hash not found';
if aa1==0; git_release_hash = aa2(1:end-1); end % avoid newline
[aa1,aa2]=unix(['cd ' abcpath '; git config --get remote.origin.url']);
git_url = 'gdat git url not found';
if aa1==0; git_url = aa2(1:end-1); end
ids_summary.code.name = 'gdat';
ids_summary.code.version = git_release_hash;
ids_summary.code.repository = git_url;

%% boundary
% use equilibrium time as reference, so load first elongation(time)

params_eff = params_eff_ref;
params_eff.data_request='ip_trapeze';
ip_trapeze = gdat(params_summary.shot,params_eff);
ipsign = mdsvalue('\pcs::data:iohfb');
i_t_end = find(ipsign.*ip_trapeze.data(end:-1:1) > 5e3); % an offset might cause a problem...
i_t_end = length(ip_trapeze.t)-i_t_end(1)+1;
t_end = ip_trapeze.t(i_t_end);
b0sign = mdsvalue('\pcs::data:if36fb');
params_eff = params_eff_ref;
params_eff.data_request='kappa';
kappa = gdat(params_summary.shot,params_eff);
% to get liuqe time array for main time, but add points before and after according to ip_trapeze, to have other data when available
% aim for 100 points
dt_min = 1e-3;
t_before = [0:max(3*dt_min,(kappa.t(1)-0)/10.):kappa.t(1)]';
t_after = [t_end:min(-2*dt_min,(kappa.t(end)-t_end)/10.):kappa.t(end)]'; t_after = t_after(end:-1:1); % to make sure to have lst point
nb_points_remaining = 100 - numel(t_before) - numel(t_after);
ids_summary.time = unique([t_before; [kappa.t(1):max((kappa.t(end)-kappa.t(1))/(nb_points_remaining-1),dt_min):kappa.t(end)]'; t_after]);
ids_summary_description.time = 'time from gdat(shot,''kappa'') and adding from 0 up to t(ip_trapeze(end))';
params_eff = params_eff_ref;

boundary.elongation = kappa;
boundary_desc.elongation = kappa.gdat_params.data_request;
params_eff.data_request = 'r_geom';
boundary.geometric_axis_r = gdat(params_summary.shot,params_eff);
boundary_desc.geometric_axis_r = params_eff.data_request;
params_eff.data_request = 'z_geom';
boundary.geometric_axis_z = gdat(params_summary.shot,params_eff);
boundary_desc.geometric_axis_z = params_eff.data_request;
params_eff.data_request = 'r_mag';
boundary.magnetic_axis_r = gdat(params_summary.shot,params_eff);
boundary_desc.magnetic_axis_r = params_eff.data_request;
params_eff.data_request = 'z_mag';
boundary.magnetic_axis_z = gdat(params_summary.shot,params_eff);
boundary_desc.magnetic_axis_z = params_eff.data_request;
params_eff.data_request = 'a_minor';
boundary.minor_radius = gdat(params_summary.shot,params_eff);
boundary_desc.minor_radius = params_eff.data_request;
params_eff.data_request = 'delta_bottom';
boundary.triangularity_lower = gdat(params_summary.shot,params_eff);
boundary_desc.triangularity_lower = params_eff.data_request;
params_eff.data_request = 'delta_top';
boundary.triangularity_upper = gdat(params_summary.shot,params_eff);
boundary_desc.triangularity_upper = params_eff.data_request;
% $$$ params_eff.data_request = 'tcv_eq(''''n_xpts'''',''''liuqe.m'''')';
% $$$ temp.n_x_point = gdat(params_summary.shot,params_eff);
% $$$ temp_desc.n_x_point = params_eff.data_request;
% $$$ params_eff.data_request = 'tcv_eq(''''r_xpts'''',''''liuqe.m'''')';
% $$$ temp.r_x_point = gdat(params_summary.shot,params_eff);
% $$$ temp_desc.r_x_point = params_eff.data_request;
% $$$ params_eff.data_request = 'tcv_eq(''''z_xpts'''',''''liuqe.m'''')';
% $$$ temp.z_x_point = gdat(params_summary.shot,params_eff);
% $$$ temp_desc.z_x_point = params_eff.data_request;

boundary_fieldnames = fieldnames(boundary);
special_fields = {}; % fields needing non-automatic treatments
for i=1:numel(boundary_fieldnames)
  if ~any(strcmp(boundary_fieldnames{i},special_fields))
    if ~isstruct(ids_summary.boundary.(boundary_fieldnames{i}).value)
      ids_summary.boundary.(boundary_fieldnames{i}).value = boundary.(boundary_fieldnames{i}).data;
      ids_summary.boundary.(boundary_fieldnames{i}).value = interpos(21,boundary.(boundary_fieldnames{i}).t, ...
          boundary.(boundary_fieldnames{i}).data,ids_summary.time);
      ids_summary.boundary.(boundary_fieldnames{i}).source = ['gdat request: ' boundary_desc.(boundary_fieldnames{i})];
    else
      special_fields{end+1} = boundary_fieldnames{i};
    end
  end
end

% special case
if ~isempty(special_fields)
% $$$   for it=1:ntime
% $$$     ids_summary.time_slice{it}.boundary.magnetic_axis.r = temp.r_magnetic_axis.data(it);
% $$$     ids_summary.time_slice{it}.boundary.magnetic_axis.z = temp.z_magnetic_axis.data(it);
% $$$     ids_summary.time_slice{it}.boundary.psi_axis = temp.psi_axis.data(it) + ...
% $$$         ids_summary.time_slice{it}.boundary.psi_boundary;
% $$$     [zz,izz]=min(temp.q_rho.data(:,it));
% $$$     ids_summary.time_slice{it}.boundary.q_min.value = zz;
% $$$     ids_summary.time_slice{it}.boundary.q_min.rho_tor_norm = temp.q_rho.grids_1d.rhotornorm(izz);
% $$$   end
end

%% disruption, use specific function
% to be decided if changes only ids_summary_out.disruption or also elsewhere..., if ids_summary_out.time is changed,
% then all the other data should be related, thus call this first?

[ids_summary_out,ids_summary_out_description] = ids_summary_disruption(shot, ids_summary, gdat_params);

%% gas: bottom/top?, deuterium, helium_xx, ?
params_eff.data_request = 'gas_flux';
gas_injection_rates.deuterium = gdat(params_summary.shot,params_eff);
gas_injection_rates_desc.deuterium = params_eff.data_request;

gas_injection_rates_fieldnames = fieldnames(gas_injection_rates);
special_fields = {}; % fields needing non-automatic treatments
% assume all gas_injection_rates have same time base since called with same gdat_params for liuqe
for i=1:numel(gas_injection_rates_fieldnames)
  if ~any(strcmp(gas_injection_rates_fieldnames{i},special_fields))
    if ~isstruct(ids_summary.gas_injection_rates.(gas_injection_rates_fieldnames{i}).value)
      ids_summary.gas_injection_rates.(gas_injection_rates_fieldnames{i}).value = interpos(21, ...
          gas_injection_rates.(gas_injection_rates_fieldnames{i}).t,gas_injection_rates.(gas_injection_rates_fieldnames{i}).data, ...
          ids_summary.time);
      ids_summary.gas_injection_rates.(gas_injection_rates_fieldnames{i}).source = ['gdat request: ' gas_injection_rates_desc.(gas_injection_rates_fieldnames{i})];
    else
      special_fields{end+1} = gas_injection_rates_fieldnames{i};
    end
  end
end
%params_eff.doplot=1;

%% global_quantities
params_eff.data_request = 'b0';
global_quantities.b0 = gdat(params_summary.shot,params_eff); % do not add 'source','liuqe' to get all times
global_quantities_desc.b0 = [params_eff.data_request ' ; ' global_quantities.b0.data_fullpath];
params_eff.data_request = 'beta_pol';
global_quantities.beta_pol = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_pol = [params_eff.data_request ' ; ' global_quantities.beta_pol.data_fullpath];
params_eff.data_request = 'beta_pol';
global_quantities.beta_pol_mhd = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_pol_mhd = [params_eff.data_request ' ; ' global_quantities.beta_pol_mhd.data_fullpath];
params_eff.data_request = 'beta_tor';
global_quantities.beta_tor = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_tor = [params_eff.data_request ' ; ' global_quantities.beta_tor.data_fullpath];
params_eff.data_request = 'beta_tor';
global_quantities.beta_tor_mhd = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_tor_mhd = [params_eff.data_request ' ; ' global_quantities.beta_tor_mhd.data_fullpath];
params_eff.data_request = 'betan';
global_quantities.beta_tor_norm = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_tor_norm = [params_eff.data_request ' ; ' global_quantities.beta_tor_norm.data_fullpath];
params_eff.data_request = 'betan';
global_quantities.beta_tor_norm_mhd = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_tor_norm_mhd = [params_eff.data_request ' ; ' global_quantities.beta_tor_norm_mhd.data_fullpath];
params_eff.data_request = 'betan';
global_quantities.beta_tor_thermal_norm = gdat(params_summary.shot,params_eff);
global_quantities_desc.beta_tor_thermal_norm = [params_eff.data_request ' ; ' global_quantities.beta_tor_thermal_norm.data_fullpath];
params_eff.data_request = '\tcv_shot::top.results.ibs:ibs';
global_quantities.current_bootstrap = gdat(params_summary.shot,params_eff);
global_quantities_desc.current_bootstrap = [params_eff.data_request ' ; ' global_quantities.current_bootstrap.data_fullpath];
params_eff.data_request = '\tcv_shot::top.results.ibs:icd';
global_quantities.current_non_inductive = gdat(params_summary.shot,params_eff);
global_quantities_desc.current_non_inductive = [params_eff.data_request ' ; ' global_quantities.current_non_inductive.data_fullpath];
params_eff.data_request = 'w_mhd';
global_quantities.energy_diamagnetic = gdat(params_summary.shot,params_eff);
global_quantities_desc.energy_diamagnetic = [params_eff.data_request ' ; ' global_quantities.energy_diamagnetic.data_fullpath];
params_eff.data_request = 'tau_tot';
global_quantities.tau_energy = gdat(params_summary.shot,params_eff);
global_quantities_desc.tau_energy = [params_eff.data_request ' ; ' global_quantities.tau_energy.data_fullpath];
params_eff.data_request = '\tcv_shot::top.results.conf:we';
global_quantities.energy_electrons_thermal = gdat(params_summary.shot,params_eff);
global_quantities_desc.energy_electrons_thermal = [params_eff.data_request ' ; ' global_quantities.energy_electrons_thermal.data_fullpath];
params_eff.data_request = '\tcv_shot::top.results.conf:wi';
global_quantities.energy_ion_total_thermal = gdat(params_summary.shot,params_eff);
global_quantities_desc.energy_ion_total_thermal = [params_eff.data_request ' ; ' global_quantities.energy_ion_total_thermal.data_fullpath];
params_eff.data_request = 'w_mhd';
global_quantities.energy_mhd = gdat(params_summary.shot,params_eff);
global_quantities_desc.energy_mhd = [params_eff.data_request ' ; ' global_quantities.energy_mhd.data_fullpath];
params_eff.data_request = '.conf:we + .conf:wi';
global_quantities.energy_thermal = global_quantities.energy_electrons_thermal;
global_quantities.energy_thermal.data = global_quantities.energy_thermal.data + global_quantities.energy_ion_total_thermal.data;
global_quantities.energy_thermal.data_fullpath = [global_quantities.energy_thermal.data_fullpath ' + wi'];
global_quantities_desc.energy_thermal = [params_eff.data_request ' ; ' global_quantities.energy_thermal.data_fullpath];
params_eff.data_request = 'ngf';
global_quantities.greenwald_fraction = gdat(params_summary.shot,params_eff);
global_quantities_desc.greenwald_fraction = [params_eff.data_request ' ; ' global_quantities.greenwald_fraction.data_fullpath];
params_eff.data_request = 'h98y2';
global_quantities.h_98 = gdat(params_summary.shot,params_eff);
global_quantities_desc.h_98 = [params_eff.data_request ' ; ' global_quantities.h_98.data_fullpath];
params_eff.data_request = 'ip';
global_quantities.ip = gdat(params_summary.shot,params_eff);
global_quantities_desc.ip = [params_eff.data_request ' ; ' global_quantities.ip.data_fullpath];
params_eff.data_request = 'li';
global_quantities.li = gdat(params_summary.shot,params_eff);
global_quantities_desc.li = [params_eff.data_request ' ; ' global_quantities.li.data_fullpath];
params_eff.data_request = '\tcv_shot::top.results.conf:ptot_ohm';
global_quantities.power_ohm = gdat(params_summary.shot,params_eff);
global_quantities_desc.power_ohm = [params_eff.data_request ' ; ' global_quantities.power_ohm.data_fullpath];
params_eff.data_request = '\results::bolo:prad:total';
global_quantities.power_radiated = gdat(params_summary.shot,params_eff);
if strcmp(lower(global_quantities.power_radiated.units),'kw')
  global_quantities.power_radiated.data = global_quantities.power_radiated.data*1e3; %in kW
end
global_quantities_desc.power_radiated = [params_eff.data_request ' ; ' global_quantities.power_radiated.data_fullpath];
params_eff.data_request = 'q95';
global_quantities.q_95 = gdat(params_summary.shot,params_eff);
global_quantities_desc.q_95 = [params_eff.data_request ' ; ' global_quantities.q_95.data_fullpath];
params_eff.data_request = 'b0';
global_quantities.b0 = gdat(params_summary.shot,params_eff);
global_quantities_desc.b0 = [params_eff.data_request ' ; ' global_quantities.b0.data_fullpath];
params_eff.data_request = 'r0';
global_quantities.r0 = global_quantities.b0;
global_quantities.r0.data = global_quantities.r0.r0;
global_quantities.r0.dim = [];global_quantities.r0.t=[]; global_quantities.r0.dimunits = [];
global_quantities.r0.units = 'm';
global_quantities.r0.data_fullpath = ['gdat(b0).r0'];
global_quantities_desc.r0 = [params_eff.data_request ' ; ' global_quantities.r0.data_fullpath];
params_eff.data_request = 'vloop';
global_quantities.v_loop = gdat(params_summary.shot,params_eff);
tensvloop=-1e3;
global_quantities.v_loop.data = interpos(global_quantities.v_loop.t,-global_quantities.v_loop.data,tensvloop);
global_quantities_desc.v_loop = [params_eff.data_request ' ; -1* interpos of ' global_quantities.v_loop.data_fullpath{1} ...
                    ' ' global_quantities.v_loop.data_fullpath{end} ' with tens=' num2str(tensvloop)];
params_eff.data_request = 'volume';
global_quantities.volume = gdat(params_summary.shot,params_eff);
global_quantities_desc.volume = [params_eff.data_request ' ; ' global_quantities.volume.data_fullpath];

special_fields = {'r0'}; % fields needing non-automatic treatments
global_quantities_fieldnames = fieldnames(global_quantities);
for i=1:numel(global_quantities_fieldnames)
  if ~any(strcmp(global_quantities_fieldnames{i},special_fields))
    if ~isstruct(ids_summary.global_quantities.(global_quantities_fieldnames{i}).value) && ...
        ~isempty(global_quantities.(global_quantities_fieldnames{i}).data)
      % setup mask to get rid of nans
      mask = ~isnan(global_quantities.(global_quantities_fieldnames{i}).data);
      % interpolate quantity on ids_summary.time
      ids_summary.global_quantities.(global_quantities_fieldnames{i}).value = ...
        interpos(21,global_quantities.(global_quantities_fieldnames{i}).t(mask),...
        global_quantities.(global_quantities_fieldnames{i}).data(mask), ...
          ids_summary.time);
      ids_summary.global_quantities.(global_quantities_fieldnames{i}).source = ['gdat request: ' global_quantities_desc.(global_quantities_fieldnames{i})];
    elseif ~isempty(global_quantities.(global_quantities_fieldnames{i}).data)
      special_fields{end+1} = global_quantities_fieldnames{i};
    end
  end
end

% special case
ids_summary.global_quantities.r0.value = global_quantities.r0.data;
ids_summary.global_quantities.r0.source = ['gdat request: ' global_quantities_desc.r0];

%% volume average
% cst zeff
params_eff.data_request = '\tcv_shot::top.results.ibs:z_eff';
volume_average.zeff = gdat(params_summary.shot,params_eff);
volume_average_desc.zeff = [params_eff.data_request ' ; ' volume_average.zeff.data_fullpath ' (not vol avrg but to match Iohm)'];

special_fields = {}; % fields needing non-automatic treatments
volume_average_fieldnames = fieldnames(volume_average);
for i=1:numel(volume_average_fieldnames)
  if ~any(strcmp(volume_average_fieldnames{i},special_fields))
    if ~isstruct(ids_summary.volume_average.(volume_average_fieldnames{i}).value)
      ids_summary.volume_average.(volume_average_fieldnames{i}).value = interpos(21, ...
          volume_average.(volume_average_fieldnames{i}).t,volume_average.(volume_average_fieldnames{i}).data, ...
          ids_summary.time);
      ids_summary.volume_average.(volume_average_fieldnames{i}).source = ['gdat request: ' volume_average_desc.(volume_average_fieldnames{i})];
    else
      special_fields{end+1} = volume_average_fieldnames{i};
    end
  end
end

%% line average
params_eff.data_request = 'nel';
line_average.n_e = gdat(params_summary.shot,params_eff);
line_average_desc.n_e = [params_eff.data_request ' ; ' volume_average.zeff.data_fullpath ' (not vol avrg but to match Iohm)'];

special_fields = {}; % fields needing non-automatic treatments
line_average_fieldnames = fieldnames(line_average);
for i=1:numel(line_average_fieldnames)
  if ~any(strcmp(line_average_fieldnames{i},special_fields))
    if ~isstruct(ids_summary.line_average.(line_average_fieldnames{i}).value)
      ids_summary.line_average.(line_average_fieldnames{i}).value = interpos(21, ...
          line_average.(line_average_fieldnames{i}).t,line_average.(line_average_fieldnames{i}).data, ...
          ids_summary.time);
      ids_summary.line_average.(line_average_fieldnames{i}).source = ['gdat request: ' line_average_desc.(line_average_fieldnames{i})];
    else
      special_fields{end+1} = line_average_fieldnames{i};
    end
  end
end


%%

% cocos automatic transform
if ~isempty(which('ids_generic_cocos_nodes_transformation_symbolic'))
  [ids_summary,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_summary,'summary',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
