function [ids_magnetics,ids_magnetics_description,varargout] = tcv_get_ids_magnetics(shot, ids_magnetics_empty, gdat_params, varargin)
%
%  [ids_magnetics,ids_magnetics_description,varargout] = tcv_get_ids_magnetics(shot, ids_magnetics_empty, varargin);
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

if exist('gdat_params','var')
  [ids_magnetics, params_magnetics] = tcv_ids_headpart(shot, ids_magnetics_empty,'magnetics','homogeneous_time',0, ...
          'gdat_params',gdat_params,varargin{:});
else
  [ids_magnetics, params_magnetics] = tcv_ids_headpart(shot, ids_magnetics_empty,'magnetics','homogeneous_time',0,varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end

[ids_magnetics.bpol_probe,ids_magnetics_description.bpol_probe]= tcv_ids_bpol_probe(params_magnetics.shot, ids_magnetics.bpol_probe(1),gdat_params);
[ids_magnetics.flux_loop,ids_magnetics_description.flux_loop]= tcv_ids_flux_loop(params_magnetics.shot, ids_magnetics.flux_loop(1),gdat_params);
[ids_magnetics.method,ids_magnetics_description.method]= tcv_ids_ip(params_magnetics.shot, ids_magnetics.method(1),gdat_params);

% make arrays not filled in empty
ids_magnetics.b_field_pol_probe = {};
ids_magnetics.b_field_tor_probe = {};
ids_magnetics.rogowski_coil = {};

% cocos automatic transform
if ~isempty(which('ids_generic_cocos_nodes_transformation_symbolic'))
  [ids_magnetics,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_magnetics,'magnetics',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
