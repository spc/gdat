function [ combined_structure] =  tcv_ids_pf_active_definition(doplot)
% All circuits are connected in series and has only 1 power supply, so they
% share the same current.

if nargin < 1, doplot = 0;end

% NOTE: The sign for T_00* comes from the connection matrix in the static
% tree "T_C_A".
% TODO: We could probably rebuild the connection matrix from the static
% tree node "T_C_A".

% {Coil name}, [connection side identifier]
error_fixed = 200.; % This is the error assigned by LIUQE to each circuit/PS
                    % Since all coils are serially connected to their PS,
                    % this is also the error for individual coil currents.
coil_names_and_current_sign = {...
  {'A_001'}, 1, error_fixed; ... % Circuit 1
  {'B_001', 'B_002',  'C_001',  'C_002',  'D_001',  'D_002'}, [1, 1, 1, 1, 1, 1], ...
    error_fixed*[1, 1, 1, 1, 1, 1]; ... % Circuit 2
  {'E_001'}, 1, error_fixed; ... % Circuit 3
  {'E_002'}, 1, error_fixed; ... % Circuit 4
  {'E_003'}, 1, error_fixed; ... % Circuit 5
  {'E_004'}, 1, error_fixed; ... % Circuit 6
  {'E_005'}, 1, error_fixed; ... % Circuit 7
  {'E_006'}, 1, error_fixed; ... % Circuit 8
  {'E_007'}, 1, error_fixed; ... % Circuit 9
  {'E_008'}, 1, error_fixed; ... % Circuit 10
  {'F_001'}, 1, error_fixed; ... % Circuit 11
  {'F_002'}, 1, error_fixed; ... % Circuit 12
  {'F_003'}, 1, error_fixed; ... % Circuit 13
  {'F_004'}, 1, error_fixed; ... % Circuit 14
  {'F_005'}, 1, error_fixed; ... % Circuit 15
  {'F_006'}, 1, error_fixed; ... % Circuit 16
  {'F_007'}, 1, error_fixed; ... % Circuit 17
  {'F_008'}, 1, error_fixed; ... % Circuit 18
  {'G_001', 'G_002', 'G_003', 'G_004', 'G_005', 'G_006'}, [ 1, 1, 1,-1,-1,-1], error_fixed*[ 1, 1, 1, 1, 1, 1]; ... % Circuit 19 Lower coils connected in opposite direcetion
  {'T_001', 'T_002', 'T_003'}, [-1,-1, 1], error_fixed*[1, 1, 1]; ...% Circuit 20 %T003 in opposite direction
  };
power_supply_names_and_current_sign = {...
  {'OH1'}, 1;... % Circuit 1
  {'OH2'}, 1;... % Circuit 2
  {'E1'},  1;... % Circuit 3
  {'E2'},  1;... % Circuit 4
  {'E3'},  1;... % Circuit 5
  {'E4'},  1;... % Circuit 6
  {'E5'},  1;... % Circuit 7
  {'E6'},  1;... % Circuit 8
  {'E7'},  1;... % Circuit 9
  {'E8'},  1;... % Circuit 10
  {'F1'},  1;... % Circuit 11
  {'F2'},  1;... % Circuit 12
  {'F3'},  1;... % Circuit 13
  {'F4'},  1;... % Circuit 14
  {'F5'},  1;... % Circuit 15
  {'F6'},  1;... % Circuit 16
  {'F7'},  1;... % Circuit 17
  {'F8'},  1;... % Circuit 18
  {'FPS'}, 1;... % Circuit 19
  {'T'},   1;... % Circuit 20
  };

coil_names                = coil_names_and_current_sign(:,1);
coil_current_sign         = coil_names_and_current_sign(:,2);
coil_current_error        = coil_names_and_current_sign(:,3);
power_supply_names        = power_supply_names_and_current_sign(:,1);
power_supply_current_sign = power_supply_names_and_current_sign(:,2);


circuit_names = power_supply_names;

% mds path to be called with gdat
mds_paths = {...
  '\magnetics::ipol[*,"OH_001"]';... % Circuit 1
  '\magnetics::ipol[*,"OH_002"]'; ... % Circuit 2
  '\magnetics::ipol[*,"E_001"]';... % Circuit 3
  '\magnetics::ipol[*,"E_002"]';...% Circuit 4
  '\magnetics::ipol[*,"E_003"]';...% Circuit 5
  '\magnetics::ipol[*,"E_004"]';...% Circuit 6
  '\magnetics::ipol[*,"E_005"]';...% Circuit 7
  '\magnetics::ipol[*,"E_006"]';...% Circuit 8
  '\magnetics::ipol[*,"E_007"]';...% Circuit 9
  '\magnetics::ipol[*,"E_008"]'; ...% Circuit 10
  '\magnetics::ipol[*,"F_001"]';...% Circuit 11
  '\magnetics::ipol[*,"F_002"]';...% Circuit 12
  '\magnetics::ipol[*,"F_003"]';...% Circuit 13
  '\magnetics::ipol[*,"F_004"]';...% Circuit 14
  '\magnetics::ipol[*,"F_005"]';...% Circuit 15
  '\magnetics::ipol[*,"F_006"]';...% Circuit 16
  '\magnetics::ipol[*,"F_007"]';...% Circuit 17
  '\magnetics::ipol[*,"F_008"]';... % Circuit 18
  '"G_001" is_in \magnetics::ipol:dim ? \magnetics::ipol[*,"G_001"] : make_signal(zero(shape(data(\magnetics::ipol))[0],1.0),*,dim_of(\magnetics::ipol,0))';... % G coils % Circuit 19
  '\magnetics::iphi';... % Connection between tf coils % Circuit 20
  };

% Combined structure
combined_structure = struct();
combined_structure.coil_names = coil_names;
combined_structure.power_supply_names = power_supply_names;
combined_structure.mds_paths = mds_paths;
combined_structure.coil_current_signs = coil_current_sign;
combined_structure.coil_current_error = coil_current_error;
combined_structure.power_supply_current_signs = power_supply_current_sign;
combined_structure.circuit_names = circuit_names;
combined_structure = get_circuiting(combined_structure,doplot); %Add circuiting information

%% Create the connection matrix from the previous information
function circuit_struct = get_circuiting(circuit_struct,doplot)
if nargin<2, doplot=0;end
% Get dimension of the circuits
circuit_struct.ntotcircuits = numel(circuit_struct.circuit_names);
circuit_struct.ntotpowersupplies = numel(circuit_struct.power_supply_names);
circuit_struct.ntotcoils = numel([circuit_struct.coil_names{1:end}]);
circuit_struct.ncoilpercircuit = zeros(1,circuit_struct.ntotcircuits);
for ii=1:circuit_struct.ntotcircuits
  circuit_struct.ncoilpercircuit(ii) =  numel(circuit_struct.coil_names{ii});
end

% Each circuit has only 1 power supply-> number of elements per circuit =
% number of nodes per circuit= number of coils per circuit + 1;
circuit_struct.nnodespercircuit = circuit_struct.ncoilpercircuit + 1;
circuit_struct.nelementspercircuit = circuit_struct.nnodespercircuit;
circuit_struct.ntotelements = sum(circuit_struct.nnodespercircuit);

circuit_struct.connection_matrix = struct([]);

coil_column_index = 2*circuit_struct.ntotpowersupplies ;
for ii=1:circuit_struct.ntotcircuits
  circuit_connection_matrix = zeros(circuit_struct.nnodespercircuit(ii), 2*circuit_struct.ntotelements);

  % Put power supply connection
  power_supply_index = ii;
  if circuit_struct.power_supply_current_signs{power_supply_index} == 1
    circuit_connection_matrix(1,2*(power_supply_index-1)+2) = 1;
    circuit_connection_matrix(circuit_struct.nnodespercircuit(ii),2*(power_supply_index-1)+1) = 1;
  elseif circuit_struct.power_supply_current_signs{power_supply_index} == -1
    circuit_connection_matrix(1,2*(power_supply_index-1)+1) = 1;
    circuit_connection_matrix(circuit_struct.nnodespercircuit(ii),2*(power_supply_index-1)+2) = 1;
  end

  % Put coil connection
  for jj=1:circuit_struct.ncoilpercircuit(ii)
    if circuit_struct.coil_current_signs{ii}(jj) == 1
      circuit_connection_matrix(jj, coil_column_index + 2*(jj-1) + 1  ) = 1;
      circuit_connection_matrix(jj + 1, coil_column_index + 2*(jj-1) + 2 ) = 1;

    elseif circuit_struct.coil_current_signs{ii}(jj) == -1
      circuit_connection_matrix(jj, coil_column_index + 2*(jj-1) + 2  ) = 1;
      circuit_connection_matrix(jj + 1, coil_column_index + 2*(jj-1) + 1 ) = 1;
    end
  end

  coil_column_index = coil_column_index + 2*circuit_struct.ncoilpercircuit(ii);
  circuit_struct.connection_matrix{ii} = circuit_connection_matrix;

  % Plot all the connaction matrices as a check
  if doplot
    plot_connection_matrix(circuit_connection_matrix, circuit_struct.power_supply_names, circuit_struct.coil_names);
  end

end

%% Plot connection matrix
function plot_connection_matrix(mat, psnames, cnames) %, circuitname, circuit
% Plot the value of a matrix in separated block
figure
b = zeros(size(mat)+1);
b(1:end-1, 1:end-1) = mat;

pcolor(b)

yti = (1:size(b,1))+0.5;
xti = (1:size(b,2))+0.5;

ylab = cellstr(num2str((1:size(b,1)-1).'));
index = 0;
xlab = cell(1,numel(psnames));
% Get the labels
for ii=1:numel(psnames)
  index = index +1;
  xlab{index} = [psnames{ii}{1} 'in'];
  index = index +1;
  xlab{index} = [psnames{ii}{1} 'out'];
end
for ii=1:numel(cnames)
  for jj=1:numel(cnames{ii})

    index = index +1;
    xlab{index} = [cnames{ii}{jj} 'in'];
    index = index +1;
    xlab{index} = [cnames{ii}{jj} 'out'];
  end
end

shg
axis ij
ax = gca;
colormap(bone(2))
xlabel('Element name');
ylabel('Node');
set(ax,'Xtick', xti, 'Ytick', yti, 'XTickLabel', xlab, 'YTickLabel', ylab')
