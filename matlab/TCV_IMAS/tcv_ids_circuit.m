function [ids_struct_out,ids_struct_out_description] =  tcv_ids_circuit(shot, ids_structures, gdat_params, varargin)
%
% [ids_struct_out,ids_struct_out_description] =  tcv_get_ids_circuit(shot, ids_structures, gdat_params, varargin);
%
% Get power supply/coils names for each circuit
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try;params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

%% Get power supply/coils names for each circuit.
[tcv_circuit_info] =  tcv_ids_pf_active_definition();

% Preallocate memory and get data
ids_struct_out(1:tcv_circuit_info.ntotcircuits) = ids_structures;
params_eff = params_eff_ref;
for ii=1:tcv_circuit_info.ntotcircuits
  if shot == -1 % model shot
    % replace by dummy
    tmpdata.dim{1} = [];
    tmpdata.data = [];
    warning('no time data loaded for shot %d',shot);
    ids_struct_out_description{ii}.current = 'not loaded';
  else
      params_eff.data_request = ['' tcv_circuit_info.mds_paths{ii} ''];
      tmpdata = gdat_tcv(shot,params_eff);;
      ids_struct_out_description{ii}.current = ['from ' tmpdata.data_fullpath];
  end
  ids_struct_out{ii}.current.data = tmpdata.data;
  ids_struct_out{ii}.current.time = tmpdata.dim{1};


  ids_struct_out{ii}.connections = tcv_circuit_info.connection_matrix{ii};
  ids_struct_out_description{ii}.connections = ...
      ['aa=tcv_ids_pf_active_definition, then from aa.connection_matrix{' num2str(ii) '}'];
  ids_struct_out{ii}.name = tcv_circuit_info.circuit_names{ii}{1};
  ids_struct_out_description{ii}.name = ...
      ['aa=tcv_ids_pf_active_definition, then from aa.circuit_names{' num2str(ii) '}{1}'];
end

fixed_error_fraction = 0.03;
switch error_bar
 case 'delta'
  for ii=1:tcv_circuit_info.ntotcircuits
    ids_struct_out{ii}.current.data_error_upper = fixed_error_fraction.*max(abs(ids_struct_out{ii}.current.data)) ...
        .* ones(size(ids_struct_out{ii}.current.data));
    ids_struct_out_description{ii}.current_data_error_upper = ['from fixed_fraction (' num2str(fixed_error_fraction) ...
                    ') *max() in case: ' error_bar];
    ids_struct_out_description{ii}.current_data_error_lower = ['not provided since symmetric'];
  end
 case 'delta_with_lower'
  for ii=1:tcv_circuit_info.ntotcircuits
    ids_struct_out{ii}.current.data_error_upper = fixed_error_fraction.*max(abs(ids_struct_out{ii}.current.data)) ...
        .* ones(size(ids_struct_out{ii}.current.data));
    ids_struct_out{ii}.current.data_error_lower = ids_struct_out{ii}.current.data_error_upper;
    ids_struct_out_description{ii}.current_data_error_upper = ['from fixed_fraction (' num2str(fixed_error_fraction) ...
                    ') *max() in case: ' error_bar];
    ids_struct_out_description{ii}.current_data_error_lower = ids_struct_out_description{ii}.current_data_error_upper;
  end
 case 'added'
  for ii=1:tcv_circuit_info.ntotcircuits
    ids_struct_out{ii}.current.data_error_upper = ids_struct_out{ii}.current.data + ...
          fixed_error_fraction.*max(abs(ids_struct_out{ii}.current.data)).*ones(size(ids_struct_out{ii}.current.data));
    ids_struct_out{ii}.current.data_error_lower = ids_struct_out{ii}.current.data - ...
          fixed_error_fraction.*max(abs(ids_struct_out{ii}.current.data)).*ones(size(ids_struct_out{ii}.current.data));
    ids_struct_out_description{ii}.current_data_error_upper = ['from data + fixed_fraction (' num2str(fixed_error_fraction) ...
                    ') *max() in case: ' error_bar];
    ids_struct_out_description{ii}.current_data_error_lower = ['from data - fixed_fraction (' num2str(fixed_error_fraction) ...
                    ') *max() in case: ' error_bar];
  end
 otherwise
  error(['tcv_ids_bpol_loop: error_bar option not known: ' error_bar])
end
