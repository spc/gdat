function info_pf_active = plot_connection_matrices(ids_pf_active)
%
% info_pf_active = plot_connection_matrices(ids_pf_active);
%

info_pf_active = extract_info_connection_matrix(ids_pf_active);

for kk=1:info_pf_active.ntotcircuits
  figure
  mat = info_pf_active.circuit{kk}.connections;
  b = zeros([size(mat)]+1);
  b(1:end-1, 1:end-1) = mat;
  pcolor(b)
  yti = [1:size(b,1)]+0.5;
  xti = [1:size(b,2)]+0.5;
  
  ylab = cellstr(num2str([1:size(b,1)-1]'));
  index = 0;
  xlab = {};
  
  % Get the labels
  for ii=1:info_pf_active.ntotsupplies
    index = index +1;
    
    if any(ii==info_pf_active.circuit{kk}.supplies_ind_belonging_to_circuit)
      addcolor = '\color{red}';
    else
      addcolor = '\color{green}';
    end
    
    xlab{index} = [addcolor info_pf_active.supply{ii}.name 'in'];
    index = index +1;
    xlab{index} = [addcolor info_pf_active.supply{ii}.name 'out'];
  end
  for ii=1:info_pf_active.ntotcoils
    if any(ii==info_pf_active.circuit{kk}.coils_ind_belonging_to_circuit)
      addcolor = '\color{red}';
    else
      addcolor = '\color{black}';
    end
    index = index +1;
    xlab{index} = [addcolor info_pf_active.coil{ii}.name 'in'];
    index = index +1;
    xlab{index} = [addcolor info_pf_active.coil{ii}.name 'out'];
  end
  
  xlab = strrep(xlab,'_','\_'); % since we need the tex interpreter for colors
  
  shg
  axis ij
  ax = gca;
  colormap(bone(2))
  xlabel('Element name. (red) Elemement belonging to circuit. (green) power supplies. (black) coils');
  ylabel('Node');
  title(['Circuit ' num2str(kk) ': ' info_pf_active.circuit{kk}.name])
  set(ax,'Xtick', xti, 'Ytick', yti, 'XTickLabel', xlab, 'YTickLabel', ylab', 'XTickLabelRotation', 90)
  
end
