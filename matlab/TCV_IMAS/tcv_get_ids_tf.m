function [ids_tf,ids_tf_description] =  tcv_get_ids_tf(shot, ids_tf_empty, gdat_params,varargin)
%
% [ids_tf] =  tcv_get_ids_tf(shot, ids_tf_empty,varargin);
%
% Get the vacuum toroidal field
%
% ids_tf_empty should at least be the empty tf ids structure in input
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

ids_tf = ids_tf_empty;

if exist('gdat_params','var')
  [ids_tf, params_tf] = tcv_ids_headpart(shot, ids_tf_empty,'tf','homogeneous_time',0,'gdat_params',gdat_params,varargin{:});
  params_eff = gdat_params;
  params_eff.data_request = 'b0';
  tmp = gdat_tcv(shot,params_eff);
else
  [ids_tf, params_tf] = tcv_ids_headpart(shot, ids_tf_empty,'tf','homogeneous_time',0,varargin{:});
  tmp = gdat_tcv(shot, 'b0');
end

if ~ischar(tmp.data)
  ids_tf.r0 = tmp.r0;
  ids_tf_description.r0 = ' b0.r0 from gdat_tcv(shot, ''b0'')';
  ids_tf.b_field_tor_vacuum_r.data = tmp.data .* ids_tf.r0;
  ids_tf_description.b_field_tor_vacuum_r.data = ' gdat_tcv(shot, ''b0'') * .r0';
  ids_tf.b_field_tor_vacuum_r.data_error_upper = 19.2e-6*200; %TODO: Check the 200A value (might be more since TOR goes up to 70kA)
  ids_tf_description.b_field_tor_vacuum_r.data_error_upper = ' Error on coil measurement is 200A, b0.r0 is 19.2e-6*iphi';
  ids_tf.b_field_tor_vacuum_r.time = tmp.t;
else
  % leave defaults
  ids_tf_description = struct([]);
end

% make arrays not filled in empty: not the case for magnetics
ids_tf.coil = {};
ids_tf.field_map = {};

% cocos automatic transform
if exist('ids_generic_cocos_nodes_transformation_symbolic','file')
  [ids_tf,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_tf,'tf',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
