function [ids_struct_out,ids_struct_out_description] = tcv_ids_ip(shot, ids_structures, gdat_params, varargin)
%
%    [ids_struct_out,ids_struct_out_description] = tcv_ids_ip(shot, ids_structures, gdat_params, varargin);
%
% Get magnetics.method{1}.ip
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% Get data
params_eff = params_eff_ref;
params_eff.data_request = 'ip_trapeze';
tmp = gdat_tcv(shot,params_eff);
% tmp_desc = params_eff.data_request;
time = tmp.dim{1};
data = tmp.data;
params_eff.data_request = '\results::dmlcor';
tmpdml = gdat_tcv(shot,params_eff);
% tmpdml_desc = params_eff.data_request;

% Preallocate dimension
ids_struct_out = ids_structures;

% Put data into ids structure
ids_struct_out{1}.ip.data = data;
ids_struct_out_description{1}.ip = ['from ' tmp.data_fullpath];
ids_struct_out{1}.ip.time = time;

ids_struct_out{1}.diamagnetic_flux.data = tmpdml.data;
ids_struct_out_description{1}.diamagnetic_flux = ['from ' tmpdml.data_fullpath];
ids_struct_out{1}.diamagnetic_flux.time = tmpdml.t;

fixed_error_ip = 24e3;
fixed_error_diamagnetic_flux = 0.13e-3;
switch error_bar
 case 'delta'
  ids_struct_out{1}.ip.data_error_upper = fixed_error_ip.*ones(size(ids_struct_out{1}.ip.data));
  ids_struct_out_description{1}.ip_data_error_upper = ['from fixed error value in case ' error_bar];
  ids_struct_out_description{1}.ip_data_error_lower = 'not provided since symmetric';
  ids_struct_out{1}.diamagnetic_flux.data_error_upper = fixed_error_diamagnetic_flux.*ones(size(ids_struct_out{1}.diamagnetic_flux.data));
  ids_struct_out_description{1}.diamagnetic_flux_data_error_upper = ['from fixed error value in case ' error_bar];
  ids_struct_out_description{1}.diamagnetic_flux_data_error_lower = 'not provided since symmetric';
 case 'delta_with_lower'
  ids_struct_out{1}.ip.data_error_upper = fixed_error_ip.*ones(size(ids_struct_out{1}.ip.data));
  ids_struct_out{1}.ip.data_error_lower = ids_struct_out{1}.ip.data_error_upper;
  ids_struct_out_description{1}.ip_data_error_upper = ['from fixed error value in case ' error_bar];
  ids_struct_out_description{1}.ip_data_error_lower = ids_struct_out_description{1}.ip_data_error_upper;
  ids_struct_out{1}.diamagnetic_flux.data_error_upper = fixed_error_diamagnetic_flux.*ones(size(ids_struct_out{1}.diamagnetic_flux.data));
  ids_struct_out{1}.diamagnetic_flux.data_error_lower = ids_struct_out{1}.diamagnetic_flux.data_error_upper;
  ids_struct_out_description{1}.diamagnetic_flux_data_error_upper = ['from fixed error value in case ' error_bar];
  ids_struct_out_description{1}.diamagnetic_flux_data_error_lower = ids_struct_out_description{1}.diamagnetic_flux_data_error_upper;
 case 'added'
  ids_struct_out{1}.ip.data_error_upper = ids_struct_out{1}.ip.data + fixed_error_ip.*ones(size(ids_struct_out{1}.ip.data));
  ids_struct_out{1}.ip.data_error_lower = ids_struct_out{1}.ip.data - fixed_error_ip.*ones(size(ids_struct_out{1}.ip.data));
  ids_struct_out_description{1}.ip_data_error_upper = ['from data + fixed error value in case ' error_bar];
  ids_struct_out_description{1}.ip_data_error_lower = ['from data - fixed error value in case ' error_bar];
  ids_struct_out{1}.diamagnetic_flux.data_error_upper = ids_struct_out{1}.diamagnetic_flux.data + fixed_error_diamagnetic_flux.*ones(size(ids_struct_out{1}.diamagnetic_flux.data));
  ids_struct_out{1}.diamagnetic_flux.data_error_lower = ids_struct_out{1}.diamagnetic_flux.data - fixed_error_diamagnetic_flux.*ones(size(ids_struct_out{1}.diamagnetic_flux.data));
  ids_struct_out_description{1}.diamagnetic_flux_data_error_upper = ['from data + fixed error value in case ' error_bar];
  ids_struct_out_description{1}.diamagnetic_flux_data_error_lower = ['from data - fixed error value in case ' error_bar];
 otherwise
  error(['tcv_ids_ip: error_bar option not known: ' error_bar])
end
