function [ids_pf_active,ids_pf_active_description,varargout] = tcv_get_ids_pf_active(shot, ids_pf_active_empty, gdat_params, varargin)
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

% Input pharser
if exist('gdat_params','var')
  [ids_pf_active, params] = tcv_ids_headpart(shot, ids_pf_active_empty,'pf_active','homogeneous_time',0,'gdat_params',gdat_params,varargin{:});
else
  [ids_pf_active, params] = tcv_ids_headpart(shot, ids_pf_active_empty,'pf_active','homogeneous_time',0,varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end

% Get subfield
[ids_pf_active.coil,ids_pf_active_description.coil]= tcv_ids_coil(params.shot, ids_pf_active.coil(1),gdat_params);
[ids_pf_active.circuit,ids_pf_active_description.circuit]= tcv_ids_circuit(params.shot, ids_pf_active.circuit(1),gdat_params);
[ids_pf_active.supply,ids_pf_active_description.supply]= tcv_ids_supply(params.shot, ids_pf_active.supply(1),gdat_params);

% make arrays not filled in empty: not the case for magnetics
ids_pf_active.vertical_force = {};

% cocos automatic transform
if exist('ids_generic_cocos_nodes_transformation_symbolic','file')
  [ids_pf_active,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_pf_active,'pf_active',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
