function [ids_from_tcv,varargout] = tcv2ids2database(shot,run_out,varargin);
%
% [ids_from_tcv,varargout] = tcv2ids2database(shot,run_out,varargin)
%
% script to get TCV data as ids and then write them on the database
%
% Uses function tcv2ids and ids2database
% Assumes you connected to the right port like: mdsconnect('localhost:5555'); (if not tries that one)
%
% varargin: 'occurence': occurence value
% varargin: 'ids_names': cell, ids to load, by default all defined so far (if empty or empty string or not given)
%             see tcv_available_ids for the list of IDSs available for TCV
% varargin: 'imas_backend': "MDSplus" or "HDF5"
% varargin: 'error_bar': type (string)
%                'delta' or empty (default): meaning difference in upper is set (standard error_bar
%                'added': errorbar is added: upper=data+delta and lower=data-delta
%                'delta_with_lower': as 'delta' but lower also set
%           'tree_user': getenv('USER') by default, user for database (could be 'public')
%           'tree_tokamak': tokamak name, default ('tcv')
%           'tree_majorversion': default '3'
%           'cocos_out': (default 11) cocos to transform ids from TCV cocos_in=17 to cocos_out
%           'ipsign_out': if a specific sign fo Ip is desired in output within the cocos_out system (default 0=no specific sign)
%           'b0sign_out': if a specific sign fo B0 is desired in output within the cocos_out system (default 0=no specific sign)
%           'time_out': if 2 values provided: get all time data within that time interval
%                       otherwise get values at these times provided in time_out (with linear interpolation and cst extrapolation)
%           'trialindx': trial_indx for relevant nodes, in particular CONF kinetic profiles nodes
%
% example:
%   ids_from_tcv = tcv2ids2database(62745,9999,'ids_names',{'pf_active'},'error_bar','added'); % to test only one ids
%   ids_from_tcv = tcv2ids2database(62745,9999,'error_bar','added'); % to get all default ids's and old errorbar type
%   ids_from_tcv = tcv2ids2database(62745,9999,'ids_names',{'summary'},'imas_backend','HDF5'); % Save data in imas HDF5 backend instead of MDSplus
%

ids_from_tcv = [];

% mdsconnect('localhost:5555');
aa=mdsvalue('1+2');
if isempty(aa) || aa ~= 3
  mdsconnect('localhost:5555');
  aa=mdsvalue('1+2');
  if isempty(aa) || aa ~= 3
    error('problem with mdsconnect?');
  end
end

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('run_out', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('occurence', 0, @(x) isempty(x) || (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
ids_names = tcv_available_ids;
p.addOptional('ids_names', ids_names, @(x) isempty(x) || iscell(x)); % char or cell array
p.addOptional('imas_backend', 'MDSplus', @(x) isempty(x) || ischar(x));
p.addOptional('error_bar', 'delta', @(x) isempty(x) || ischar(x) ); % char or cell array
p.addOptional('tree_user', getenv('USER'), @(x) isempty(x) || ischar(x) ); % char
p.addOptional('tree_tokamak', 'tcv', @(x) isempty(x) || ischar(x) ); % char
p.addOptional('tree_majorversion', '3', @(x) isempty(x) || ischar(x) ); % char
p.addOptional('cocos_out', 11, @(x) isempty(x) || isnumeric(x) );
p.addOptional('ipsign_out', 0, @(x) isempty(x) || (isscalar(x) && (x==0 || x==-1 || x==+1)) );
p.addOptional('b0sign_out', 0, @(x) isempty(x) || (isscalar(x) && (x==0 || x==-1 || x==+1)) );
p.addOptional('nverbose', 1, @(x) isempty(x) || isnumeric(x) );
p.addOptional('time_out', [], @(x) isempty(x) || isnumeric(x) );
p.addOptional('trialindx', [], @(x) isempty(x) || isnumeric(x) );

p.parse;
defaults_tcv2ids2database = p.Results; % to keep track of defaults

if nargin == 1
  p.parse(shot);
  params = p.Results;
elseif nargin == 2
  p.parse(shot,run_out);
  params = p.Results;
elseif nargin > 2
  p.parse(shot,run_out,varargin{:});
  params = p.Results;
else
  p.parse;
  p.Results
  return
end

params.imas_backend = upper(params.imas_backend);

% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  params = rmfield(params,unique([names(mask); p.UsingDefaults.']));
  if ~isfield(params, 'shot') || isnan(params.shot)
    warning('No shot entered');
    return
  end
  p.parse(params.shot,rmfield(params,'shot'));
  params = p.Results;
end

ids_names_ok = params.ids_names;
for i=1:length(params.ids_names)
  ij = strcmp(params.ids_names{i},defaults_tcv2ids2database.ids_names);
  if ~any(ij)
    disp(['ids name: ' params.ids_names{i} ' is not available yet, ask O. Sauter'])
    ids_names_ok = setdiff(ids_names_ok,params.ids_names{i});
  end
end
params.ids_names = ids_names_ok;
params_tcv2ids2database = params;
params_not_in_tcv2ids = {'run_out','occurence','tree_user','tree_tokamak','tree_majorversion','imas_backend'};
params_tcv2ids = rmfield(params_tcv2ids2database,params_not_in_tcv2ids);

[ids_from_tcv,idsok] = tcv2ids(shot,params_tcv2ids);
ids_from_tcv.params_tcv2ids2database = params_tcv2ids2database;

% check that coreprofiles is ok, could mean anaprofs did not succeed
if isfield(ids_from_tcv,'core_profiles')
  if isempty(ids_from_tcv.core_profiles.time)
    warning([char(10) '*************' char(10) ...
           'core_profiles.time is empty, probably no data in CONF nodes for TCV, check with hldsi(shot) command on SPC-EPFL lacs computers' ...
           char(10) '*************' char(10)]);
    return
  end
end


%% can plot with: [plotids_H] = plotids([],idsok.ids{1},idsok.ids{2});
if isfield(ids_from_tcv,'tf')
  % avoid present problem of not saving data if ids_from_tcv.tf.field_map{1}.time is not set
  if length(ids_from_tcv.tf.field_map)>0 && abs(ids_from_tcv.tf.field_map{1}.time+9.e40)<1e-5
    ids_from_tcv.tf.field_map{1}.time = -8.9000e+40;
    disp(['WARNING: ids_from_tcv.tf.field_map{1}.time changed to ' num2str(ids_from_tcv.tf.field_map{1}.time) ' to avoid writing error']);
  end
end
%%

[ids_put_status] = ids2database(shot,run_out,ids_from_tcv.params_tcv2ids2database.occurence,ids_from_tcv, ...
          params_tcv2ids2database.tree_user,params_tcv2ids2database.tree_tokamak,params_tcv2ids2database.tree_majorversion, ...
          params_tcv2ids2database.imas_backend);

ids_from_tcv.ids_put_status = ids_put_status;

% [plotids_H] = plotids([],idsok.ids);

% tic;[TTTe,idse]=evalc('ids_get(expIdx, ''equilibrium'')');toc
% tic;[TTTm,idsm]=evalc('ids_get(expIdx, ''magnetics'')');toc
% tic;[TTTp,idsp]=evalc('ids_get(expIdx, ''pf_active'')');toc
% tic;[TTTt,idst]=evalc('ids_get(expIdx, ''tf'')');toc
% tic;[TTTw,idsw]=evalc('ids_get(expIdx, ''wall'')');toc
% tic;[TTTw,idsw]=evalc('ids_get(expIdx, ''ec_launchers'')');toc
% tic;[TTTw,idsw]=evalc('ids_get(expIdx, ''nbi'')');toc
