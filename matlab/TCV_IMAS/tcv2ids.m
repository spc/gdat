function [ids_from_tcv,varargout] = tcv2ids(shot,varargin);
%
%   [ids_from_tcv,ids_aos] = tcv2ids(shot,varargin);
%
% Assumes you have done:
% >> addpath ~g2osaute/public/matlab9_11_2016 (on the gateway)
% >> mdsconnect('localhost:5555') % using the tunnel made in another session like: ssh -L 5555:tcvdata:8000 username@lac911.epfl.ch
%
% in another window do the tunnel: ssh -L 5555:tcvdata:8000 username@lac911.epfl.ch
% addpath ~g2osaute/public/matlab9_11_2016
% mdsconnect('localhost:5555')
%
% varargin: 'ids_names': cell, idss to load, by default all defined so far (if empty or empty string or not given)
%             see tcv_available_ids for the list of IDSs available for TCV
%           'error_bar': type (string)
%                empty or 'delta' (default): meaning difference in upper is set (standard error_bar
%                'added': errorbar is added: upper=data+delta and lower=data-delta
%                'delta_with_lower': as 'delta' but lower also set
%           'cocos_out': (default 11) cocos to transform ids from TCV cocos_in=17 to cocos_out
%           'ipsign_out': if a specific sign fo Ip is desired in output within the cocos_out system (default 0=no specific sign)
%           'b0sign_out': if a specific sign fo B0 is desired in output within the cocos_out system (default 0=no specific sign)
%           'nverbose': (default 1), set it to 3 to have more messages, for example about not fully valid nodes when doing transformation (empty or Nans)
%           'time_out': if 2 values provided: get all time data within that time interval
%                       otherwise get values at these times provided in time_out (with linear interpolation and cst extrapolation)
%           'trialindx': trial_indx for relevant nodes, in particular CONF kinetic profiles nodes
%           'liuqe': liuqe indx for equilibrium, only 1,2,3 are available
%
% Outputs
% varargout{1}: return also the ids in array of structure with the names, to allow easy use of plotallids
%

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
ids_names = tcv_available_ids;
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('ids_names', ids_names, @(x) isempty(x) || iscell(x) ); % char or cell array
p.addOptional('error_bar', 'delta', @(x) isempty(x) || ischar(x) ); % char or cell array
p.addOptional('cocos_out', 11, @(x) isempty(x) || isnumeric(x) ); % char
p.addOptional('ipsign_out', 0, @(x) isempty(x) || (isscalar(x) && (x==0 || x==-1 || x==+1)) );
p.addOptional('b0sign_out', 0, @(x) isempty(x) || (isscalar(x) && (x==0 || x==-1 || x==+1)) );
p.addOptional('nverbose', 1, @(x) isempty(x) || isnumeric(x) );
p.addOptional('time_out', [], @(x) isempty(x) || isnumeric(x) );
p.addOptional('trialindx', [], @(x) isempty(x) || isnumeric(x) );
p.addOptional('liuqe', [], @(x) isempty(x) || isnumeric(x) );

params_not_for_gdat_params = {'shot','ids_names'};

p.parse;
defaults_tcv2ids = p.Results; % to keep track of defaults

if nargin==1
  p.parse(shot);
  params = p.Results;
elseif nargin>=2
  p.parse('shot',shot,varargin{:});
  params = p.Results;
else
  p.parse;
  p.Results
  return
end
% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  params = rmfield(params,unique([names(mask); p.UsingDefaults.']));
  if ~isfield(params, 'shot') || isnan(params.shot)
    warning('No shot entered');
    return
  end
  p.parse(params.shot,rmfield(params,'shot'));
  params = p.Results;
end

% check ids_names
if ~isfield(params,'ids_names')
  disp(['do not provide any ids_names or choose from : '])
  ids_from_tcv.ids_names_available = defaults_tcv2ids.ids_names;
  defaults_tcv2ids.ids_names
  return
end
if ischar(params.ids_names)
  params.ids_names = {params.ids_names};
end
ids_names_ok = params.ids_names;
for i=1:length(params.ids_names)
  ij = strcmp(params.ids_names{i},defaults_tcv2ids.ids_names);
  if ~any(ij)
    disp(['ids name: ' params.ids_names{i} ' is not available yet, ask O. Sauter'])
    ids_names_ok = setdiff(ids_names_ok,params.ids_names{i});
  end
end
params.ids_names = ids_names_ok;
params_tcv2ids = params;
ids_from_tcv.params_tcv2ids = params_tcv2ids;

if isempty(params_tcv2ids.ids_names)
  disp('no ids names available')
  return
end

bb=gdat_tcv;
gdat_params = bb.gdat_params; clear bb
aa = rmfield(params_tcv2ids,params_not_for_gdat_params);
gdat_params_fields = fieldnames(aa);
for i=1:length(gdat_params_fields)
  if ~isempty(aa.(gdat_params_fields{i})),
    gdat_params.(gdat_params_fields{i}) = aa.(gdat_params_fields{i});
  end
end
gdat_params.data_request = 'ids';
for i=1:length(params_tcv2ids.ids_names)
  ids_to_get = params_tcv2ids.ids_names{i};
  gdat_params.source = ids_to_get;
  tmp = gdat(shot,gdat_params);
  ids_from_tcv.(ids_to_get) = tmp.(ids_to_get);
  ids_from_tcv.([ids_to_get '_description']) = tmp.([ids_to_get '_description']);
end

if nargout>=2
  try
    for i=1:length(params_tcv2ids.ids_names)
      ids_to_get = params_tcv2ids.ids_names{i};
      varargout{1}.ids{i} = ids_from_tcv.(ids_to_get);
      varargout{1}.idsname{i} = ids_to_get;
      varargout{1}.ids{i}.idsname = ids_to_get;
    end
  catch
    varargout{1} = [];
    disp('problems to fill in varargout')
  end
end
