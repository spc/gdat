function S = ids_empty_iron_core
% function S = ids_empty_iron_core
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'segment',{{struct(...
'b_field',double([]),...
'b_field_error_index',int32(-999999999),...
'b_field_error_lower',double([]),...
'b_field_error_upper',double([]),...
'geometry',struct(...
'annulus',struct(...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'radius_inner',double(-9e+40),...
'radius_inner_error_index',int32(-999999999),...
'radius_inner_error_lower',double(-9e+40),...
'radius_inner_error_upper',double(-9e+40),...
'radius_outer',double(-9e+40),...
'radius_outer_error_index',int32(-999999999),...
'radius_outer_error_lower',double(-9e+40),...
'radius_outer_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'arcs_of_circle',struct(...
'curvature_radii',double([]),...
'curvature_radii_error_index',int32(-999999999),...
'curvature_radii_error_lower',double([]),...
'curvature_radii_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'geometry_type',int32(-999999999),...
'oblique',struct(...
'alpha',double(-9e+40),...
'alpha_error_index',int32(-999999999),...
'alpha_error_lower',double(-9e+40),...
'alpha_error_upper',double(-9e+40),...
'beta',double(-9e+40),...
'beta_error_index',int32(-999999999),...
'beta_error_lower',double(-9e+40),...
'beta_error_upper',double(-9e+40),...
'length_alpha',double(-9e+40),...
'length_alpha_error_index',int32(-999999999),...
'length_alpha_error_lower',double(-9e+40),...
'length_alpha_error_upper',double(-9e+40),...
'length_beta',double(-9e+40),...
'length_beta_error_index',int32(-999999999),...
'length_beta_error_lower',double(-9e+40),...
'length_beta_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'outline',struct(...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'rectangle',struct(...
'height',double(-9e+40),...
'height_error_index',int32(-999999999),...
'height_error_lower',double(-9e+40),...
'height_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'width',double(-9e+40),...
'width_error_index',int32(-999999999),...
'width_error_lower',double(-9e+40),...
'width_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'thick_line',struct(...
'first_point',struct(...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'second_point',struct(...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'thickness',double(-9e+40),...
'thickness_error_index',int32(-999999999),...
'thickness_error_lower',double(-9e+40),...
'thickness_error_upper',double(-9e+40))),...
'identifier','',...
'magnetisation_r',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'magnetisation_z',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'name','',...
'permeability_relative',double([]),...
'permeability_relative_error_index',int32(-999999999),...
'permeability_relative_error_lower',double([]),...
'permeability_relative_error_upper',double([]))
}},...
'time',double([]));
