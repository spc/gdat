function S = ids_empty_waves
% function S = ids_empty_waves
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'coherent_wave',{{struct(...
'beam_tracing',{{struct(...
'beam',{{struct(...
'e_field',struct(...
'minus',struct(...
'imaginary',double([]),...
'imaginary_error_index',int32(-999999999),...
'imaginary_error_lower',double([]),...
'imaginary_error_upper',double([]),...
'real',double([]),...
'real_error_index',int32(-999999999),...
'real_error_lower',double([]),...
'real_error_upper',double([])),...
'parallel',struct(...
'imaginary',double([]),...
'imaginary_error_index',int32(-999999999),...
'imaginary_error_lower',double([]),...
'imaginary_error_upper',double([]),...
'real',double([]),...
'real_error_index',int32(-999999999),...
'real_error_lower',double([]),...
'real_error_upper',double([])),...
'plus',struct(...
'imaginary',double([]),...
'imaginary_error_index',int32(-999999999),...
'imaginary_error_lower',double([]),...
'imaginary_error_upper',double([]),...
'real',double([]),...
'real_error_index',int32(-999999999),...
'real_error_lower',double([]),...
'real_error_upper',double([]))),...
'electrons',struct(...
'power',double([]),...
'power_error_index',int32(-999999999),...
'power_error_lower',double([]),...
'power_error_upper',double([])),...
'ion',{{struct(...
'element',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'atoms_n',int32(-999999999),...
'multiplicity',double(-9e+40),...
'multiplicity_error_index',int32(-999999999),...
'multiplicity_error_lower',double(-9e+40),...
'multiplicity_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'label','',...
'multiple_states_flag',int32(-999999999),...
'power',double([]),...
'power_error_index',int32(-999999999),...
'power_error_lower',double([]),...
'power_error_upper',double([]),...
'state',{{struct(...
'electron_configuration','',...
'label','',...
'power',double([]),...
'power_error_index',int32(-999999999),...
'power_error_lower',double([]),...
'power_error_upper',double([]),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40))
}},...
'length',double([]),...
'length_error_index',int32(-999999999),...
'length_error_lower',double([]),...
'length_error_upper',double([]),...
'phase',struct(...
'angle',double([]),...
'angle_error_index',int32(-999999999),...
'angle_error_lower',double([]),...
'angle_error_upper',double([]),...
'curvature',double([]),...
'curvature_error_index',int32(-999999999),...
'curvature_error_lower',double([]),...
'curvature_error_upper',double([])),...
'position',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'psi',double([]),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'theta',double([]),...
'theta_error_index',int32(-999999999),...
'theta_error_lower',double([]),...
'theta_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'power_flow_norm',struct(...
'parallel',double([]),...
'parallel_error_index',int32(-999999999),...
'parallel_error_lower',double([]),...
'parallel_error_upper',double([]),...
'perpendicular',double([]),...
'perpendicular_error_index',int32(-999999999),...
'perpendicular_error_lower',double([]),...
'perpendicular_error_upper',double([])),...
'power_initial',double(-9e+40),...
'power_initial_error_index',int32(-999999999),...
'power_initial_error_lower',double(-9e+40),...
'power_initial_error_upper',double(-9e+40),...
'spot',struct(...
'angle',double([]),...
'angle_error_index',int32(-999999999),...
'angle_error_lower',double([]),...
'angle_error_upper',double([]),...
'size',double([]),...
'size_error_index',int32(-999999999),...
'size_error_lower',double([]),...
'size_error_upper',double([])),...
'wave_vector',struct(...
'k_r',double([]),...
'k_r_error_index',int32(-999999999),...
'k_r_error_lower',double([]),...
'k_r_error_upper',double([]),...
'k_r_norm',double([]),...
'k_r_norm_error_index',int32(-999999999),...
'k_r_norm_error_lower',double([]),...
'k_r_norm_error_upper',double([]),...
'k_tor',double([]),...
'k_tor_error_index',int32(-999999999),...
'k_tor_error_lower',double([]),...
'k_tor_error_upper',double([]),...
'k_tor_norm',double([]),...
'k_tor_norm_error_index',int32(-999999999),...
'k_tor_norm_error_lower',double([]),...
'k_tor_norm_error_upper',double([]),...
'k_z',double([]),...
'k_z_error_index',int32(-999999999),...
'k_z_error_lower',double([]),...
'k_z_error_upper',double([]),...
'k_z_norm',double([]),...
'k_z_norm_error_index',int32(-999999999),...
'k_z_norm_error_lower',double([]),...
'k_z_norm_error_upper',double([]),...
'n_parallel',double([]),...
'n_parallel_error_index',int32(-999999999),...
'n_parallel_error_lower',double([]),...
'n_parallel_error_upper',double([]),...
'n_perpendicular',double([]),...
'n_perpendicular_error_index',int32(-999999999),...
'n_perpendicular_error_lower',double([]),...
'n_perpendicular_error_upper',double([]),...
'n_tor',int32([]),...
'varying_n_tor',int32(-999999999)))
}},...
'time',double(-9e+40))
}},...
'full_wave',{{struct(...
'b_field',struct(...
'bi_normal',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'normal',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'parallel',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}}),...
'e_field',struct(...
'bi_normal',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'minus',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'normal',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'parallel',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'plus',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}}),...
'grid',struct(...
'grid_subset',{{struct(...
'base',{{struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([]))
}},...
'dimension',int32(-999999999),...
'element',{{struct(...
'object',{{struct(...
'dimension',int32(-999999999),...
'index',int32(-999999999),...
'space',int32(-999999999))
}})
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'metric',struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([])))
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'path','',...
'space',{{struct(...
'coordinates_type',int32([]),...
'geometry_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'objects_per_dimension',{{struct(...
'geometry_content',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'object',{{struct(...
'boundary',{{struct(...
'index',int32(-999999999),...
'neighbours',int32([]))
}},...
'geometry',double([]),...
'geometry_2d',double([]),...
'geometry_2d_error_index',int32(-999999999),...
'geometry_2d_error_lower',double([]),...
'geometry_2d_error_upper',double([]),...
'geometry_error_index',int32(-999999999),...
'geometry_error_lower',double([]),...
'geometry_error_upper',double([]),...
'measure',double(-9e+40),...
'measure_error_index',int32(-999999999),...
'measure_error_lower',double(-9e+40),...
'measure_error_upper',double(-9e+40),...
'nodes',int32([]))
}})
}})
}}),...
'k_perpendicular',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'time',double(-9e+40))
}},...
'global_quantities',{{struct(...
'current_tor',double(-9e+40),...
'current_tor_error_index',int32(-999999999),...
'current_tor_error_lower',double(-9e+40),...
'current_tor_error_upper',double(-9e+40),...
'current_tor_n_tor',double([]),...
'current_tor_n_tor_error_index',int32(-999999999),...
'current_tor_n_tor_error_lower',double([]),...
'current_tor_n_tor_error_upper',double([]),...
'electrons',struct(...
'distribution_assumption',int32(-999999999),...
'power_fast',double(-9e+40),...
'power_fast_error_index',int32(-999999999),...
'power_fast_error_lower',double(-9e+40),...
'power_fast_error_upper',double(-9e+40),...
'power_fast_n_tor',double([]),...
'power_fast_n_tor_error_index',int32(-999999999),...
'power_fast_n_tor_error_lower',double([]),...
'power_fast_n_tor_error_upper',double([]),...
'power_thermal',double(-9e+40),...
'power_thermal_error_index',int32(-999999999),...
'power_thermal_error_lower',double(-9e+40),...
'power_thermal_error_upper',double(-9e+40),...
'power_thermal_n_tor',double([]),...
'power_thermal_n_tor_error_index',int32(-999999999),...
'power_thermal_n_tor_error_lower',double([]),...
'power_thermal_n_tor_error_upper',double([])),...
'frequency',double(-9e+40),...
'frequency_error_index',int32(-999999999),...
'frequency_error_lower',double(-9e+40),...
'frequency_error_upper',double(-9e+40),...
'ion',{{struct(...
'distribution_assumption',int32(-999999999),...
'element',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'atoms_n',int32(-999999999),...
'multiplicity',double(-9e+40),...
'multiplicity_error_index',int32(-999999999),...
'multiplicity_error_lower',double(-9e+40),...
'multiplicity_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'label','',...
'multiple_states_flag',int32(-999999999),...
'power_fast',double(-9e+40),...
'power_fast_error_index',int32(-999999999),...
'power_fast_error_lower',double(-9e+40),...
'power_fast_error_upper',double(-9e+40),...
'power_fast_n_tor',double([]),...
'power_fast_n_tor_error_index',int32(-999999999),...
'power_fast_n_tor_error_lower',double([]),...
'power_fast_n_tor_error_upper',double([]),...
'power_thermal',double(-9e+40),...
'power_thermal_error_index',int32(-999999999),...
'power_thermal_error_lower',double(-9e+40),...
'power_thermal_error_upper',double(-9e+40),...
'power_thermal_n_tor',double([]),...
'power_thermal_n_tor_error_index',int32(-999999999),...
'power_thermal_n_tor_error_lower',double([]),...
'power_thermal_n_tor_error_upper',double([]),...
'state',{{struct(...
'electron_configuration','',...
'label','',...
'power_fast',double(-9e+40),...
'power_fast_error_index',int32(-999999999),...
'power_fast_error_lower',double(-9e+40),...
'power_fast_error_upper',double(-9e+40),...
'power_fast_n_tor',double([]),...
'power_fast_n_tor_error_index',int32(-999999999),...
'power_fast_n_tor_error_lower',double([]),...
'power_fast_n_tor_error_upper',double([]),...
'power_thermal',double(-9e+40),...
'power_thermal_error_index',int32(-999999999),...
'power_thermal_error_lower',double(-9e+40),...
'power_thermal_error_upper',double(-9e+40),...
'power_thermal_n_tor',double([]),...
'power_thermal_n_tor_error_index',int32(-999999999),...
'power_thermal_n_tor_error_lower',double([]),...
'power_thermal_n_tor_error_upper',double([]),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40))
}},...
'n_tor',int32([]),...
'power',double(-9e+40),...
'power_error_index',int32(-999999999),...
'power_error_lower',double(-9e+40),...
'power_error_upper',double(-9e+40),...
'power_n_tor',double([]),...
'power_n_tor_error_index',int32(-999999999),...
'power_n_tor_error_lower',double([]),...
'power_n_tor_error_upper',double([]),...
'time',double(-9e+40))
}},...
'identifier',struct(...
'antenna_name','',...
'index_in_antenna',int32(-999999999),...
'type',struct(...
'description','',...
'index',int32(-999999999),...
'name','')),...
'profiles_1d',{{struct(...
'current_parallel_density',double([]),...
'current_parallel_density_error_index',int32(-999999999),...
'current_parallel_density_error_lower',double([]),...
'current_parallel_density_error_upper',double([]),...
'current_parallel_density_n_tor',double([]),...
'current_parallel_density_n_tor_error_index',int32(-999999999),...
'current_parallel_density_n_tor_error_lower',double([]),...
'current_parallel_density_n_tor_error_upper',double([]),...
'current_tor_inside',double([]),...
'current_tor_inside_error_index',int32(-999999999),...
'current_tor_inside_error_lower',double([]),...
'current_tor_inside_error_upper',double([]),...
'current_tor_inside_n_tor',double([]),...
'current_tor_inside_n_tor_error_index',int32(-999999999),...
'current_tor_inside_n_tor_error_lower',double([]),...
'current_tor_inside_n_tor_error_upper',double([]),...
'e_field_n_tor',{{struct(...
'minus',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])),...
'parallel',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])),...
'plus',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])))
}},...
'electrons',struct(...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([]),...
'power_inside_fast',double([]),...
'power_inside_fast_error_index',int32(-999999999),...
'power_inside_fast_error_lower',double([]),...
'power_inside_fast_error_upper',double([]),...
'power_inside_fast_n_tor',double([]),...
'power_inside_fast_n_tor_error_index',int32(-999999999),...
'power_inside_fast_n_tor_error_lower',double([]),...
'power_inside_fast_n_tor_error_upper',double([]),...
'power_inside_thermal',double([]),...
'power_inside_thermal_error_index',int32(-999999999),...
'power_inside_thermal_error_lower',double([]),...
'power_inside_thermal_error_upper',double([]),...
'power_inside_thermal_n_tor',double([]),...
'power_inside_thermal_n_tor_error_index',int32(-999999999),...
'power_inside_thermal_n_tor_error_lower',double([]),...
'power_inside_thermal_n_tor_error_upper',double([])),...
'grid',struct(...
'area',double([]),...
'area_error_index',int32(-999999999),...
'area_error_lower',double([]),...
'area_error_upper',double([]),...
'psi',double([]),...
'psi_boundary',double(-9e+40),...
'psi_boundary_error_index',int32(-999999999),...
'psi_boundary_error_lower',double(-9e+40),...
'psi_boundary_error_upper',double(-9e+40),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'psi_magnetic_axis',double(-9e+40),...
'psi_magnetic_axis_error_index',int32(-999999999),...
'psi_magnetic_axis_error_lower',double(-9e+40),...
'psi_magnetic_axis_error_upper',double(-9e+40),...
'rho_pol_norm',double([]),...
'rho_pol_norm_error_index',int32(-999999999),...
'rho_pol_norm_error_lower',double([]),...
'rho_pol_norm_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'surface',double([]),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double([]),...
'surface_error_upper',double([]),...
'volume',double([]),...
'volume_error_index',int32(-999999999),...
'volume_error_lower',double([]),...
'volume_error_upper',double([])),...
'ion',{{struct(...
'element',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'atoms_n',int32(-999999999),...
'multiplicity',double(-9e+40),...
'multiplicity_error_index',int32(-999999999),...
'multiplicity_error_lower',double(-9e+40),...
'multiplicity_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'label','',...
'multiple_states_flag',int32(-999999999),...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([]),...
'power_inside_fast',double([]),...
'power_inside_fast_error_index',int32(-999999999),...
'power_inside_fast_error_lower',double([]),...
'power_inside_fast_error_upper',double([]),...
'power_inside_fast_n_tor',double([]),...
'power_inside_fast_n_tor_error_index',int32(-999999999),...
'power_inside_fast_n_tor_error_lower',double([]),...
'power_inside_fast_n_tor_error_upper',double([]),...
'power_inside_thermal',double([]),...
'power_inside_thermal_error_index',int32(-999999999),...
'power_inside_thermal_error_lower',double([]),...
'power_inside_thermal_error_upper',double([]),...
'power_inside_thermal_n_tor',double([]),...
'power_inside_thermal_n_tor_error_index',int32(-999999999),...
'power_inside_thermal_n_tor_error_lower',double([]),...
'power_inside_thermal_n_tor_error_upper',double([]),...
'state',{{struct(...
'electron_configuration','',...
'label','',...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([]),...
'power_inside_fast',double([]),...
'power_inside_fast_error_index',int32(-999999999),...
'power_inside_fast_error_lower',double([]),...
'power_inside_fast_error_upper',double([]),...
'power_inside_fast_n_tor',double([]),...
'power_inside_fast_n_tor_error_index',int32(-999999999),...
'power_inside_fast_n_tor_error_lower',double([]),...
'power_inside_fast_n_tor_error_upper',double([]),...
'power_inside_thermal',double([]),...
'power_inside_thermal_error_index',int32(-999999999),...
'power_inside_thermal_error_lower',double([]),...
'power_inside_thermal_error_upper',double([]),...
'power_inside_thermal_n_tor',double([]),...
'power_inside_thermal_n_tor_error_index',int32(-999999999),...
'power_inside_thermal_n_tor_error_lower',double([]),...
'power_inside_thermal_n_tor_error_upper',double([]),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40))
}},...
'k_perpendicular',double([]),...
'k_perpendicular_error_index',int32(-999999999),...
'k_perpendicular_error_lower',double([]),...
'k_perpendicular_error_upper',double([]),...
'n_tor',int32([]),...
'power_density',double([]),...
'power_density_error_index',int32(-999999999),...
'power_density_error_lower',double([]),...
'power_density_error_upper',double([]),...
'power_density_n_tor',double([]),...
'power_density_n_tor_error_index',int32(-999999999),...
'power_density_n_tor_error_lower',double([]),...
'power_density_n_tor_error_upper',double([]),...
'power_inside',double([]),...
'power_inside_error_index',int32(-999999999),...
'power_inside_error_lower',double([]),...
'power_inside_error_upper',double([]),...
'power_inside_n_tor',double([]),...
'power_inside_n_tor_error_index',int32(-999999999),...
'power_inside_n_tor_error_lower',double([]),...
'power_inside_n_tor_error_upper',double([]),...
'time',double(-9e+40))
}},...
'profiles_2d',{{struct(...
'e_field_n_tor',{{struct(...
'minus',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])),...
'parallel',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])),...
'plus',struct(...
'amplitude',double([]),...
'amplitude_error_index',int32(-999999999),...
'amplitude_error_lower',double([]),...
'amplitude_error_upper',double([]),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([])))
}},...
'electrons',struct(...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([])),...
'grid',struct(...
'area',double([]),...
'area_error_index',int32(-999999999),...
'area_error_lower',double([]),...
'area_error_upper',double([]),...
'psi',double([]),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'theta_geometric',double([]),...
'theta_geometric_error_index',int32(-999999999),...
'theta_geometric_error_lower',double([]),...
'theta_geometric_error_upper',double([]),...
'theta_straight',double([]),...
'theta_straight_error_index',int32(-999999999),...
'theta_straight_error_lower',double([]),...
'theta_straight_error_upper',double([]),...
'type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'volume',double([]),...
'volume_error_index',int32(-999999999),...
'volume_error_lower',double([]),...
'volume_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'ion',{{struct(...
'element',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'atoms_n',int32(-999999999),...
'multiplicity',double(-9e+40),...
'multiplicity_error_index',int32(-999999999),...
'multiplicity_error_lower',double(-9e+40),...
'multiplicity_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'label','',...
'multiple_states_flag',int32(-999999999),...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([]),...
'state',{{struct(...
'electron_configuration','',...
'label','',...
'power_density_fast',double([]),...
'power_density_fast_error_index',int32(-999999999),...
'power_density_fast_error_lower',double([]),...
'power_density_fast_error_upper',double([]),...
'power_density_fast_n_tor',double([]),...
'power_density_fast_n_tor_error_index',int32(-999999999),...
'power_density_fast_n_tor_error_lower',double([]),...
'power_density_fast_n_tor_error_upper',double([]),...
'power_density_thermal',double([]),...
'power_density_thermal_error_index',int32(-999999999),...
'power_density_thermal_error_lower',double([]),...
'power_density_thermal_error_upper',double([]),...
'power_density_thermal_n_tor',double([]),...
'power_density_thermal_n_tor_error_index',int32(-999999999),...
'power_density_thermal_n_tor_error_lower',double([]),...
'power_density_thermal_n_tor_error_upper',double([]),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40))
}},...
'n_tor',int32([]),...
'power_density',double([]),...
'power_density_error_index',int32(-999999999),...
'power_density_error_lower',double([]),...
'power_density_error_upper',double([]),...
'power_density_n_tor',double([]),...
'power_density_n_tor_error_index',int32(-999999999),...
'power_density_n_tor_error_lower',double([]),...
'power_density_n_tor_error_upper',double([]),...
'time',double(-9e+40))
}},...
'wave_solver_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''))
}},...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'magnetic_axis',struct(...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'time',double([]),...
'vacuum_toroidal_field',struct(...
'b0',double([]),...
'b0_error_index',int32(-999999999),...
'b0_error_lower',double([]),...
'b0_error_upper',double([]),...
'r0',double(-9e+40),...
'r0_error_index',int32(-999999999),...
'r0_error_lower',double(-9e+40),...
'r0_error_upper',double(-9e+40)));
