function S = ids_list_all
% function S = ids_list_all
%
% File written automatically using writecell.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
{'amns_data';'barometry';'b_field_non_axisymmetric';'bolometer';'bremsstrahlung_visible';'calorimetry';'camera_ir';'camera_visible';'camera_x_rays';'charge_exchange';'coils_non_axisymmetric';'controllers';'core_instant_changes';'core_profiles';'core_sources';'core_transport';'cryostat';'dataset_description';'dataset_fair';'disruption';'distribution_sources';'distributions';'divertors';'ec_launchers';'ece';'edge_profiles';'edge_sources';'edge_transport';'em_coupling';'equilibrium';'focs';'gas_injection';'gas_pumping';'gyrokinetics';'hard_x_rays';'ic_antennas';'interferometer';'iron_core';'langmuir_probes';'lh_antennas';'magnetics';'mhd';'mhd_linear';'mse';'nbi';'neutron_diagnostic';'ntms';'pellets';'pf_active';'pf_passive';'plasma_initiation';'polarimeter';'pulse_schedule';'radiation';'real_time_data';'reflectometer_profile';'reflectometer_fluctuation';'refractometer';'runaway_electrons';'sawteeth';'soft_x_rays';'spectrometer_mass';'spectrometer_uv';'spectrometer_visible';'spectrometer_x_ray_crystal';'summary';'temporary';'thomson_scattering';'tf';'transport_solver_numerics';'turbulence';'wall';'waves';'workflow'};
