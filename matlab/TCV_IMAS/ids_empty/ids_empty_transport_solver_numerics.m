function S = ids_empty_transport_solver_numerics
% function S = ids_empty_transport_solver_numerics
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'boundary_conditions_1d',{{struct(...
'current',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'electrons',struct(...
'energy',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'particles',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([]))),...
'energy_ion_total',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'ion',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'energy',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'label','',...
'multiple_states_flag',int32(-999999999),...
'particles',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'state',{{struct(...
'electron_configuration','',...
'energy',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'is_neutral',int32(-999999999),...
'label','',...
'neutral_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'particles',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'momentum_tor',struct(...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([])),...
'time',double(-9e+40))
}},...
'boundary_conditions_ggd',{{struct(...
'current',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'electrons',struct(...
'energy',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'particles',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}}),...
'grid',struct(...
'grid_subset',{{struct(...
'base',{{struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([]))
}},...
'dimension',int32(-999999999),...
'element',{{struct(...
'object',{{struct(...
'dimension',int32(-999999999),...
'index',int32(-999999999),...
'space',int32(-999999999))
}})
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'metric',struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([])))
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'path','',...
'space',{{struct(...
'coordinates_type',int32([]),...
'geometry_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'objects_per_dimension',{{struct(...
'geometry_content',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'object',{{struct(...
'boundary',{{struct(...
'index',int32(-999999999),...
'neighbours',int32([]))
}},...
'geometry',double([]),...
'geometry_2d',double([]),...
'geometry_2d_error_index',int32(-999999999),...
'geometry_2d_error_lower',double([]),...
'geometry_2d_error_upper',double([]),...
'geometry_error_index',int32(-999999999),...
'geometry_error_lower',double([]),...
'geometry_error_upper',double([]),...
'measure',double(-9e+40),...
'measure_error_index',int32(-999999999),...
'measure_error_lower',double(-9e+40),...
'measure_error_upper',double(-9e+40),...
'nodes',int32([]))
}})
}})
}}),...
'ion',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'energy',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'label','',...
'multiple_states_flag',int32(-999999999),...
'particles',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'state',{{struct(...
'electron_configuration','',...
'energy',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'is_neutral',int32(-999999999),...
'label','',...
'neutral_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'particles',{{struct(...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'time',double(-9e+40))
}},...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'convergence',struct(...
'equations',{{struct(...
'current',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'electrons',struct(...
'energy',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'particles',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999))),...
'energy_ion_total',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'ion',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'energy',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'label','',...
'multiple_states_flag',int32(-999999999),...
'particles',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'state',{{struct(...
'electron_configuration','',...
'energy',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'is_neutral',int32(-999999999),...
'label','',...
'neutral_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'particles',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'time',double(-9e+40))
}},...
'time_step',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([]))),...
'derivatives_1d',{{struct(...
'd2_drho_tor_norm2',struct(...
'n_i_total_over_n_e',double([]),...
'n_i_total_over_n_e_error_index',int32(-999999999),...
'n_i_total_over_n_e_error_lower',double([]),...
'n_i_total_over_n_e_error_upper',double([]),...
'pressure_ion_total',double([]),...
'pressure_ion_total_error_index',int32(-999999999),...
'pressure_ion_total_error_lower',double([]),...
'pressure_ion_total_error_upper',double([])),...
'd2psi_drho_tor2',double([]),...
'd2psi_drho_tor2_error_index',int32(-999999999),...
'd2psi_drho_tor2_error_lower',double([]),...
'd2psi_drho_tor2_error_upper',double([]),...
'd_drho_tor_norm',struct(...
'n_i_total_over_n_e',double([]),...
'n_i_total_over_n_e_error_index',int32(-999999999),...
'n_i_total_over_n_e_error_lower',double([]),...
'n_i_total_over_n_e_error_upper',double([]),...
'pressure_ion_total',double([]),...
'pressure_ion_total_error_index',int32(-999999999),...
'pressure_ion_total_error_lower',double([]),...
'pressure_ion_total_error_upper',double([])),...
'd_dt',struct(...
'n_i_total_over_n_e',double([]),...
'n_i_total_over_n_e_error_index',int32(-999999999),...
'n_i_total_over_n_e_error_lower',double([]),...
'n_i_total_over_n_e_error_upper',double([]),...
'pressure_ion_total',double([]),...
'pressure_ion_total_error_index',int32(-999999999),...
'pressure_ion_total_error_lower',double([]),...
'pressure_ion_total_error_upper',double([])),...
'd_dvolume_drho_tor_dt',double([]),...
'd_dvolume_drho_tor_dt_error_index',int32(-999999999),...
'd_dvolume_drho_tor_dt_error_lower',double([]),...
'd_dvolume_drho_tor_dt_error_upper',double([]),...
'dpsi_drho_tor',double([]),...
'dpsi_drho_tor_error_index',int32(-999999999),...
'dpsi_drho_tor_error_lower',double([]),...
'dpsi_drho_tor_error_upper',double([]),...
'dpsi_dt',double([]),...
'dpsi_dt_cphi',double([]),...
'dpsi_dt_cphi_error_index',int32(-999999999),...
'dpsi_dt_cphi_error_lower',double([]),...
'dpsi_dt_cphi_error_upper',double([]),...
'dpsi_dt_crho_tor_norm',double([]),...
'dpsi_dt_crho_tor_norm_error_index',int32(-999999999),...
'dpsi_dt_crho_tor_norm_error_lower',double([]),...
'dpsi_dt_crho_tor_norm_error_upper',double([]),...
'dpsi_dt_error_index',int32(-999999999),...
'dpsi_dt_error_lower',double([]),...
'dpsi_dt_error_upper',double([]),...
'drho_tor_dt',double([]),...
'drho_tor_dt_error_index',int32(-999999999),...
'drho_tor_dt_error_lower',double([]),...
'drho_tor_dt_error_upper',double([]),...
'electrons',struct(...
'd2_drho_tor_norm2',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_drho_tor_norm',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_dt',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([]))),...
'grid',struct(...
'area',double([]),...
'area_error_index',int32(-999999999),...
'area_error_lower',double([]),...
'area_error_upper',double([]),...
'psi',double([]),...
'psi_boundary',double(-9e+40),...
'psi_boundary_error_index',int32(-999999999),...
'psi_boundary_error_lower',double(-9e+40),...
'psi_boundary_error_upper',double(-9e+40),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'psi_magnetic_axis',double(-9e+40),...
'psi_magnetic_axis_error_index',int32(-999999999),...
'psi_magnetic_axis_error_lower',double(-9e+40),...
'psi_magnetic_axis_error_upper',double(-9e+40),...
'rho_pol_norm',double([]),...
'rho_pol_norm_error_index',int32(-999999999),...
'rho_pol_norm_error_lower',double([]),...
'rho_pol_norm_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'surface',double([]),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double([]),...
'surface_error_upper',double([]),...
'volume',double([]),...
'volume_error_index',int32(-999999999),...
'volume_error_lower',double([]),...
'volume_error_upper',double([])),...
'ion',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'd2_drho_tor_norm2',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_drho_tor_norm',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_dt',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'label','',...
'multiple_states_flag',int32(-999999999),...
'state',{{struct(...
'd2_drho_tor_norm2',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_drho_tor_norm',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'd_dt',struct(...
'density',double([]),...
'density_error_index',int32(-999999999),...
'density_error_lower',double([]),...
'density_error_upper',double([]),...
'density_fast',double([]),...
'density_fast_error_index',int32(-999999999),...
'density_fast_error_lower',double([]),...
'density_fast_error_upper',double([]),...
'pressure',double([]),...
'pressure_error_index',int32(-999999999),...
'pressure_error_lower',double([]),...
'pressure_error_upper',double([]),...
'pressure_fast_parallel',double([]),...
'pressure_fast_parallel_error_index',int32(-999999999),...
'pressure_fast_parallel_error_lower',double([]),...
'pressure_fast_parallel_error_upper',double([]),...
'pressure_fast_perpendicular',double([]),...
'pressure_fast_perpendicular_error_index',int32(-999999999),...
'pressure_fast_perpendicular_error_lower',double([]),...
'pressure_fast_perpendicular_error_upper',double([]),...
'temperature',double([]),...
'temperature_error_index',int32(-999999999),...
'temperature_error_lower',double([]),...
'temperature_error_upper',double([]),...
'velocity_pol',double([]),...
'velocity_pol_error_index',int32(-999999999),...
'velocity_pol_error_lower',double([]),...
'velocity_pol_error_upper',double([]),...
'velocity_tor',double([]),...
'velocity_tor_error_index',int32(-999999999),...
'velocity_tor_error_lower',double([]),...
'velocity_tor_error_upper',double([])),...
'electron_configuration','',...
'is_neutral',int32(-999999999),...
'label','',...
'neutral_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'vibrational_level',double(-9e+40),...
'vibrational_level_error_index',int32(-999999999),...
'vibrational_level_error_lower',double(-9e+40),...
'vibrational_level_error_upper',double(-9e+40),...
'vibrational_mode','',...
'z_max',double(-9e+40),...
'z_max_error_index',int32(-999999999),...
'z_max_error_lower',double(-9e+40),...
'z_max_error_upper',double(-9e+40),...
'z_min',double(-9e+40),...
'z_min_error_index',int32(-999999999),...
'z_min_error_lower',double(-9e+40),...
'z_min_error_upper',double(-9e+40))
}},...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'time',double(-9e+40))
}},...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'primary_coordinate',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'restart_files',{{struct(...
'descriptions',{{}},...
'names',{{}},...
'time',double(-9e+40))
}},...
'solver',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'solver_1d',{{struct(...
'control_parameters',struct(...
'integer0d',{{struct(...
'value',int32(-999999999))
}},...
'real0d',{{struct(...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40))
}}),...
'd_dvolume_drho_tor_dt',double([]),...
'd_dvolume_drho_tor_dt_error_index',int32(-999999999),...
'd_dvolume_drho_tor_dt_error_lower',double([]),...
'd_dvolume_drho_tor_dt_error_upper',double([]),...
'drho_tor_dt',double([]),...
'drho_tor_dt_error_index',int32(-999999999),...
'drho_tor_dt_error_lower',double([]),...
'drho_tor_dt_error_upper',double([]),...
'equation',{{struct(...
'boundary_condition',{{struct(...
'position',double(-9e+40),...
'position_error_index',int32(-999999999),...
'position_error_lower',double(-9e+40),...
'position_error_upper',double(-9e+40),...
'type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([]))
}},...
'coefficient',{{struct(...
'profile',double([]),...
'profile_error_index',int32(-999999999),...
'profile_error_lower',double([]),...
'profile_error_upper',double([]))
}},...
'computation_mode',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'convergence',struct(...
'delta_relative',struct(...
'expression','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40)),...
'iterations_n',int32(-999999999)),...
'primary_quantity',struct(...
'd2_dr2',double([]),...
'd2_dr2_error_index',int32(-999999999),...
'd2_dr2_error_lower',double([]),...
'd2_dr2_error_upper',double([]),...
'd_dr',double([]),...
'd_dr_error_index',int32(-999999999),...
'd_dr_error_lower',double([]),...
'd_dr_error_upper',double([]),...
'd_dt',double([]),...
'd_dt_cphi',double([]),...
'd_dt_cphi_error_index',int32(-999999999),...
'd_dt_cphi_error_lower',double([]),...
'd_dt_cphi_error_upper',double([]),...
'd_dt_cr',double([]),...
'd_dt_cr_error_index',int32(-999999999),...
'd_dt_cr_error_lower',double([]),...
'd_dt_cr_error_upper',double([]),...
'd_dt_error_index',int32(-999999999),...
'd_dt_error_lower',double([]),...
'd_dt_error_upper',double([]),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'ion_index',int32(-999999999),...
'neutral_index',int32(-999999999),...
'profile',double([]),...
'profile_error_index',int32(-999999999),...
'profile_error_lower',double([]),...
'profile_error_upper',double([]),...
'state_index',int32(-999999999)))
}},...
'grid',struct(...
'area',double([]),...
'area_error_index',int32(-999999999),...
'area_error_lower',double([]),...
'area_error_upper',double([]),...
'psi',double([]),...
'psi_boundary',double(-9e+40),...
'psi_boundary_error_index',int32(-999999999),...
'psi_boundary_error_lower',double(-9e+40),...
'psi_boundary_error_upper',double(-9e+40),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'psi_magnetic_axis',double(-9e+40),...
'psi_magnetic_axis_error_index',int32(-999999999),...
'psi_magnetic_axis_error_lower',double(-9e+40),...
'psi_magnetic_axis_error_upper',double(-9e+40),...
'rho_pol_norm',double([]),...
'rho_pol_norm_error_index',int32(-999999999),...
'rho_pol_norm_error_lower',double([]),...
'rho_pol_norm_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'surface',double([]),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double([]),...
'surface_error_upper',double([]),...
'volume',double([]),...
'volume_error_index',int32(-999999999),...
'volume_error_lower',double([]),...
'volume_error_upper',double([])),...
'time',double(-9e+40))
}},...
'time',double([]),...
'time_step',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'time_step_average',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'time_step_min',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'vacuum_toroidal_field',struct(...
'b0',double([]),...
'b0_error_index',int32(-999999999),...
'b0_error_lower',double([]),...
'b0_error_upper',double([]),...
'r0',double(-9e+40),...
'r0_error_index',int32(-999999999),...
'r0_error_lower',double(-9e+40),...
'r0_error_upper',double(-9e+40)));
