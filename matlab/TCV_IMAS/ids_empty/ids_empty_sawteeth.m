function S = ids_empty_sawteeth
% function S = ids_empty_sawteeth
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'crash_trigger',int32([]),...
'diagnostics',struct(...
'magnetic_shear_q1',double([]),...
'magnetic_shear_q1_error_index',int32(-999999999),...
'magnetic_shear_q1_error_lower',double([]),...
'magnetic_shear_q1_error_upper',double([]),...
'previous_crash_time',double([]),...
'previous_crash_time_error_index',int32(-999999999),...
'previous_crash_time_error_lower',double([]),...
'previous_crash_time_error_upper',double([]),...
'previous_crash_trigger',int32([]),...
'previous_period',double([]),...
'previous_period_error_index',int32(-999999999),...
'previous_period_error_lower',double([]),...
'previous_period_error_upper',double([]),...
'rho_tor_norm_inversion',double([]),...
'rho_tor_norm_inversion_error_index',int32(-999999999),...
'rho_tor_norm_inversion_error_lower',double([]),...
'rho_tor_norm_inversion_error_upper',double([]),...
'rho_tor_norm_mixing',double([]),...
'rho_tor_norm_mixing_error_index',int32(-999999999),...
'rho_tor_norm_mixing_error_lower',double([]),...
'rho_tor_norm_mixing_error_upper',double([]),...
'rho_tor_norm_q1',double([]),...
'rho_tor_norm_q1_error_index',int32(-999999999),...
'rho_tor_norm_q1_error_lower',double([]),...
'rho_tor_norm_q1_error_upper',double([])),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'profiles_1d',{{struct(...
'conductivity_parallel',double([]),...
'conductivity_parallel_error_index',int32(-999999999),...
'conductivity_parallel_error_lower',double([]),...
'conductivity_parallel_error_upper',double([]),...
'e_field_parallel',double([]),...
'e_field_parallel_error_index',int32(-999999999),...
'e_field_parallel_error_lower',double([]),...
'e_field_parallel_error_upper',double([]),...
'grid',struct(...
'area',double([]),...
'area_error_index',int32(-999999999),...
'area_error_lower',double([]),...
'area_error_upper',double([]),...
'psi',double([]),...
'psi_boundary',double(-9e+40),...
'psi_boundary_error_index',int32(-999999999),...
'psi_boundary_error_lower',double(-9e+40),...
'psi_boundary_error_upper',double(-9e+40),...
'psi_error_index',int32(-999999999),...
'psi_error_lower',double([]),...
'psi_error_upper',double([]),...
'psi_magnetic_axis',double(-9e+40),...
'psi_magnetic_axis_error_index',int32(-999999999),...
'psi_magnetic_axis_error_lower',double(-9e+40),...
'psi_magnetic_axis_error_upper',double(-9e+40),...
'rho_pol_norm',double([]),...
'rho_pol_norm_error_index',int32(-999999999),...
'rho_pol_norm_error_lower',double([]),...
'rho_pol_norm_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'surface',double([]),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double([]),...
'surface_error_upper',double([]),...
'volume',double([]),...
'volume_error_index',int32(-999999999),...
'volume_error_lower',double([]),...
'volume_error_upper',double([])),...
'j_bootstrap',double([]),...
'j_bootstrap_error_index',int32(-999999999),...
'j_bootstrap_error_lower',double([]),...
'j_bootstrap_error_upper',double([]),...
'j_non_inductive',double([]),...
'j_non_inductive_error_index',int32(-999999999),...
'j_non_inductive_error_lower',double([]),...
'j_non_inductive_error_upper',double([]),...
'j_ohmic',double([]),...
'j_ohmic_error_index',int32(-999999999),...
'j_ohmic_error_lower',double([]),...
'j_ohmic_error_upper',double([]),...
'j_tor',double([]),...
'j_tor_error_index',int32(-999999999),...
'j_tor_error_lower',double([]),...
'j_tor_error_upper',double([]),...
'j_total',double([]),...
'j_total_error_index',int32(-999999999),...
'j_total_error_lower',double([]),...
'j_total_error_upper',double([]),...
'magnetic_shear',double([]),...
'magnetic_shear_error_index',int32(-999999999),...
'magnetic_shear_error_lower',double([]),...
'magnetic_shear_error_upper',double([]),...
'momentum_tor',double([]),...
'momentum_tor_error_index',int32(-999999999),...
'momentum_tor_error_lower',double([]),...
'momentum_tor_error_upper',double([]),...
'n_e',double([]),...
'n_e_error_index',int32(-999999999),...
'n_e_error_lower',double([]),...
'n_e_error_upper',double([]),...
'n_e_fast',double([]),...
'n_e_fast_error_index',int32(-999999999),...
'n_e_fast_error_lower',double([]),...
'n_e_fast_error_upper',double([]),...
'n_i_total_over_n_e',double([]),...
'n_i_total_over_n_e_error_index',int32(-999999999),...
'n_i_total_over_n_e_error_lower',double([]),...
'n_i_total_over_n_e_error_upper',double([]),...
'p_e',double([]),...
'p_e_error_index',int32(-999999999),...
'p_e_error_lower',double([]),...
'p_e_error_upper',double([]),...
'p_e_fast_parallel',double([]),...
'p_e_fast_parallel_error_index',int32(-999999999),...
'p_e_fast_parallel_error_lower',double([]),...
'p_e_fast_parallel_error_upper',double([]),...
'p_e_fast_perpendicular',double([]),...
'p_e_fast_perpendicular_error_index',int32(-999999999),...
'p_e_fast_perpendicular_error_lower',double([]),...
'p_e_fast_perpendicular_error_upper',double([]),...
'p_i_total',double([]),...
'p_i_total_error_index',int32(-999999999),...
'p_i_total_error_lower',double([]),...
'p_i_total_error_upper',double([]),...
'p_i_total_fast_parallel',double([]),...
'p_i_total_fast_parallel_error_index',int32(-999999999),...
'p_i_total_fast_parallel_error_lower',double([]),...
'p_i_total_fast_parallel_error_upper',double([]),...
'p_i_total_fast_perpendicular',double([]),...
'p_i_total_fast_perpendicular_error_index',int32(-999999999),...
'p_i_total_fast_perpendicular_error_lower',double([]),...
'p_i_total_fast_perpendicular_error_upper',double([]),...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'pressure_parallel',double([]),...
'pressure_parallel_error_index',int32(-999999999),...
'pressure_parallel_error_lower',double([]),...
'pressure_parallel_error_upper',double([]),...
'pressure_perpendicular',double([]),...
'pressure_perpendicular_error_index',int32(-999999999),...
'pressure_perpendicular_error_lower',double([]),...
'pressure_perpendicular_error_upper',double([]),...
'pressure_thermal',double([]),...
'pressure_thermal_error_index',int32(-999999999),...
'pressure_thermal_error_lower',double([]),...
'pressure_thermal_error_upper',double([]),...
'psi_star_post_crash',double([]),...
'psi_star_post_crash_error_index',int32(-999999999),...
'psi_star_post_crash_error_lower',double([]),...
'psi_star_post_crash_error_upper',double([]),...
'psi_star_pre_crash',double([]),...
'psi_star_pre_crash_error_index',int32(-999999999),...
'psi_star_pre_crash_error_lower',double([]),...
'psi_star_pre_crash_error_upper',double([]),...
'q',double([]),...
'q_error_index',int32(-999999999),...
'q_error_lower',double([]),...
'q_error_upper',double([]),...
't_e',double([]),...
't_e_error_index',int32(-999999999),...
't_e_error_lower',double([]),...
't_e_error_upper',double([]),...
't_i_average',double([]),...
't_i_average_error_index',int32(-999999999),...
't_i_average_error_lower',double([]),...
't_i_average_error_upper',double([]),...
'time',double(-9e+40),...
'zeff',double([]),...
'zeff_error_index',int32(-999999999),...
'zeff_error_lower',double([]),...
'zeff_error_upper',double([]))
}},...
'time',double([]),...
'vacuum_toroidal_field',struct(...
'b0',double([]),...
'b0_error_index',int32(-999999999),...
'b0_error_lower',double([]),...
'b0_error_upper',double([]),...
'r0',double(-9e+40),...
'r0_error_index',int32(-999999999),...
'r0_error_lower',double(-9e+40),...
'r0_error_upper',double(-9e+40)));
