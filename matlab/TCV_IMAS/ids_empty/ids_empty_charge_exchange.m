function S = ids_empty_charge_exchange
% function S = ids_empty_charge_exchange
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'aperture',struct(...
'centre',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'geometry_type',int32(-999999999),...
'outline',struct(...
'x1',double([]),...
'x1_error_index',int32(-999999999),...
'x1_error_lower',double([]),...
'x1_error_upper',double([]),...
'x2',double([]),...
'x2_error_index',int32(-999999999),...
'x2_error_lower',double([]),...
'x2_error_upper',double([])),...
'radius',double(-9e+40),...
'radius_error_index',int32(-999999999),...
'radius_error_lower',double(-9e+40),...
'radius_error_upper',double(-9e+40),...
'surface',double(-9e+40),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double(-9e+40),...
'surface_error_upper',double(-9e+40),...
'x1_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x1_width',double(-9e+40),...
'x1_width_error_index',int32(-999999999),...
'x1_width_error_lower',double(-9e+40),...
'x1_width_error_upper',double(-9e+40),...
'x2_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x2_width',double(-9e+40),...
'x2_width_error_index',int32(-999999999),...
'x2_width_error_lower',double(-9e+40),...
'x2_width_error_upper',double(-9e+40),...
'x3_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40))),...
'channel',{{struct(...
'bes',struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'doppler_shift',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'label','',...
'lorentz_shift',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'radiances',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'transition_wavelength',double(-9e+40),...
'transition_wavelength_error_index',int32(-999999999),...
'transition_wavelength_error_lower',double(-9e+40),...
'transition_wavelength_error_upper',double(-9e+40),...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40)),...
'identifier','',...
'ion',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'label','',...
'n_i_over_n_e',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'n_i_over_n_e_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
't_i',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
't_i_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'velocity_pol',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'velocity_pol_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'velocity_tor',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'velocity_tor_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'ion_fast',{{struct(...
'a',double(-9e+40),...
'a_error_index',int32(-999999999),...
'a_error_lower',double(-9e+40),...
'a_error_upper',double(-9e+40),...
'label','',...
'radiance',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'radiance_spectral_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'transition_wavelength',double(-9e+40),...
'transition_wavelength_error_index',int32(-999999999),...
'transition_wavelength_error_lower',double(-9e+40),...
'transition_wavelength_error_upper',double(-9e+40),...
'z_ion',double(-9e+40),...
'z_ion_error_index',int32(-999999999),...
'z_ion_error_lower',double(-9e+40),...
'z_ion_error_upper',double(-9e+40),...
'z_n',double(-9e+40),...
'z_n_error_index',int32(-999999999),...
'z_n_error_lower',double(-9e+40),...
'z_n_error_upper',double(-9e+40))
}},...
'momentum_tor',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'momentum_tor_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'name','',...
'position',struct(...
'phi',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'r',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'z',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([]))),...
'spectrum',{{struct(...
'exposure_time',double(-9e+40),...
'exposure_time_error_index',int32(-999999999),...
'exposure_time_error_lower',double(-9e+40),...
'exposure_time_error_upper',double(-9e+40),...
'grating',double(-9e+40),...
'grating_error_index',int32(-999999999),...
'grating_error_lower',double(-9e+40),...
'grating_error_upper',double(-9e+40),...
'instrument_function',double([]),...
'instrument_function_error_index',int32(-999999999),...
'instrument_function_error_lower',double([]),...
'instrument_function_error_upper',double([]),...
'intensity_spectrum',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'processed_line',{{struct(...
'intensity',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'label','',...
'radiance',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'shift',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'wavelength_central',double(-9e+40),...
'wavelength_central_error_index',int32(-999999999),...
'wavelength_central_error_lower',double(-9e+40),...
'wavelength_central_error_upper',double(-9e+40),...
'width',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])))
}},...
'radiance_calibration',double([]),...
'radiance_calibration_date','',...
'radiance_calibration_error_index',int32(-999999999),...
'radiance_calibration_error_lower',double([]),...
'radiance_calibration_error_upper',double([]),...
'radiance_continuum',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'radiance_spectral',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'slit_width',double(-9e+40),...
'slit_width_error_index',int32(-999999999),...
'slit_width_error_lower',double(-9e+40),...
'slit_width_error_upper',double(-9e+40),...
'wavelength_calibration_date','',...
'wavelengths',double([]),...
'wavelengths_error_index',int32(-999999999),...
'wavelengths_error_lower',double([]),...
'wavelengths_error_upper',double([]))
}},...
't_i_average',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
't_i_average_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'zeff',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'zeff_line_average',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'zeff_line_average_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'zeff_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''))
}},...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'etendue',double(-9e+40),...
'etendue_error_index',int32(-999999999),...
'etendue_error_lower',double(-9e+40),...
'etendue_error_upper',double(-9e+40),...
'etendue_method',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'latency',double(-9e+40),...
'latency_error_index',int32(-999999999),...
'latency_error_lower',double(-9e+40),...
'latency_error_upper',double(-9e+40),...
'time',double([]));
