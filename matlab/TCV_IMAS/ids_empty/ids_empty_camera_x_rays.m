function S = ids_empty_camera_x_rays
% function S = ids_empty_camera_x_rays
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'aperture',struct(...
'centre',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'geometry_type',int32(-999999999),...
'outline',struct(...
'x1',double([]),...
'x1_error_index',int32(-999999999),...
'x1_error_lower',double([]),...
'x1_error_upper',double([]),...
'x2',double([]),...
'x2_error_index',int32(-999999999),...
'x2_error_lower',double([]),...
'x2_error_upper',double([])),...
'radius',double(-9e+40),...
'radius_error_index',int32(-999999999),...
'radius_error_lower',double(-9e+40),...
'radius_error_upper',double(-9e+40),...
'surface',double(-9e+40),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double(-9e+40),...
'surface_error_upper',double(-9e+40),...
'x1_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x1_width',double(-9e+40),...
'x1_width_error_index',int32(-999999999),...
'x1_width_error_lower',double(-9e+40),...
'x1_width_error_upper',double(-9e+40),...
'x2_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x2_width',double(-9e+40),...
'x2_width_error_index',int32(-999999999),...
'x2_width_error_lower',double(-9e+40),...
'x2_width_error_upper',double(-9e+40),...
'x3_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40))),...
'camera',struct(...
'camera_dimensions',double([]),...
'camera_dimensions_error_index',int32(-999999999),...
'camera_dimensions_error_lower',double([]),...
'camera_dimensions_error_upper',double([]),...
'centre',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'identifier','',...
'line_of_sight',struct(...
'first_point',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'second_point',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([]))),...
'pixel_dimensions',double([]),...
'pixel_dimensions_error_index',int32(-999999999),...
'pixel_dimensions_error_lower',double([]),...
'pixel_dimensions_error_upper',double([]),...
'pixel_position',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'pixels_n',int32([]),...
'x1_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x2_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x3_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40))),...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'detector_humidity',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'detector_temperature',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'energy_configuration_name','',...
'energy_threshold_lower',double([]),...
'energy_threshold_lower_error_index',int32(-999999999),...
'energy_threshold_lower_error_lower',double([]),...
'energy_threshold_lower_error_upper',double([]),...
'exposure_time',double(-9e+40),...
'exposure_time_error_index',int32(-999999999),...
'exposure_time_error_lower',double(-9e+40),...
'exposure_time_error_upper',double(-9e+40),...
'filter_window',struct(...
'centre',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'curvature_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'geometry_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'identifier','',...
'material',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'outline',struct(...
'x1',double([]),...
'x1_error_index',int32(-999999999),...
'x1_error_lower',double([]),...
'x1_error_upper',double([]),...
'x2',double([]),...
'x2_error_index',int32(-999999999),...
'x2_error_lower',double([]),...
'x2_error_upper',double([])),...
'photon_absorption',double([]),...
'photon_absorption_error_index',int32(-999999999),...
'photon_absorption_error_lower',double([]),...
'photon_absorption_error_upper',double([]),...
'radius',double(-9e+40),...
'radius_error_index',int32(-999999999),...
'radius_error_lower',double(-9e+40),...
'radius_error_upper',double(-9e+40),...
'surface',double(-9e+40),...
'surface_error_index',int32(-999999999),...
'surface_error_lower',double(-9e+40),...
'surface_error_upper',double(-9e+40),...
'thickness',double(-9e+40),...
'thickness_error_index',int32(-999999999),...
'thickness_error_lower',double(-9e+40),...
'thickness_error_upper',double(-9e+40),...
'wavelength_lower',double(-9e+40),...
'wavelength_lower_error_index',int32(-999999999),...
'wavelength_lower_error_lower',double(-9e+40),...
'wavelength_lower_error_upper',double(-9e+40),...
'wavelength_upper',double(-9e+40),...
'wavelength_upper_error_index',int32(-999999999),...
'wavelength_upper_error_lower',double(-9e+40),...
'wavelength_upper_error_upper',double(-9e+40),...
'wavelengths',double([]),...
'wavelengths_error_index',int32(-999999999),...
'wavelengths_error_lower',double([]),...
'wavelengths_error_upper',double([]),...
'x1_curvature',double(-9e+40),...
'x1_curvature_error_index',int32(-999999999),...
'x1_curvature_error_lower',double(-9e+40),...
'x1_curvature_error_upper',double(-9e+40),...
'x1_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x1_width',double(-9e+40),...
'x1_width_error_index',int32(-999999999),...
'x1_width_error_lower',double(-9e+40),...
'x1_width_error_upper',double(-9e+40),...
'x2_curvature',double(-9e+40),...
'x2_curvature_error_index',int32(-999999999),...
'x2_curvature_error_lower',double(-9e+40),...
'x2_curvature_error_upper',double(-9e+40),...
'x2_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'x2_width',double(-9e+40),...
'x2_width_error_index',int32(-999999999),...
'x2_width_error_lower',double(-9e+40),...
'x2_width_error_upper',double(-9e+40),...
'x3_unit_vector',struct(...
'x',double(-9e+40),...
'x_error_index',int32(-999999999),...
'x_error_lower',double(-9e+40),...
'x_error_upper',double(-9e+40),...
'y',double(-9e+40),...
'y_error_index',int32(-999999999),...
'y_error_lower',double(-9e+40),...
'y_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40))),...
'frame',{{struct(...
'counts_n',int32([]),...
'time',double(-9e+40))
}},...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'latency',double(-9e+40),...
'latency_error_index',int32(-999999999),...
'latency_error_lower',double(-9e+40),...
'latency_error_upper',double(-9e+40),...
'name','',...
'photon_energy',double([]),...
'photon_energy_error_index',int32(-999999999),...
'photon_energy_error_lower',double([]),...
'photon_energy_error_upper',double([]),...
'pixel_status',int32([]),...
'quantum_efficiency',double([]),...
'quantum_efficiency_error_index',int32(-999999999),...
'quantum_efficiency_error_lower',double([]),...
'quantum_efficiency_error_upper',double([]),...
'readout_time',double(-9e+40),...
'readout_time_error_index',int32(-999999999),...
'readout_time_error_lower',double(-9e+40),...
'readout_time_error_upper',double(-9e+40),...
'time',double([]));
