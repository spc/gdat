function S = ids_empty_tf
% function S = ids_empty_tf
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'b_field_tor_vacuum_r',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'coil',{{struct(...
'conductor',{{struct(...
'cross_section',struct(...
'delta_phi',double([]),...
'delta_phi_error_index',int32(-999999999),...
'delta_phi_error_lower',double([]),...
'delta_phi_error_upper',double([]),...
'delta_r',double([]),...
'delta_r_error_index',int32(-999999999),...
'delta_r_error_lower',double([]),...
'delta_r_error_upper',double([]),...
'delta_z',double([]),...
'delta_z_error_index',int32(-999999999),...
'delta_z_error_lower',double([]),...
'delta_z_error_upper',double([])),...
'current',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'elements',struct(...
'centres',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'end_points',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'intermediate_points',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'names',{{}},...
'start_points',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'types',int32([])),...
'resistance',double(-9e+40),...
'resistance_error_index',int32(-999999999),...
'resistance_error_lower',double(-9e+40),...
'resistance_error_upper',double(-9e+40),...
'voltage',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])))
}},...
'current',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'identifier','',...
'name','',...
'resistance',double(-9e+40),...
'resistance_error_index',int32(-999999999),...
'resistance_error_lower',double(-9e+40),...
'resistance_error_upper',double(-9e+40),...
'turns',double(-9e+40),...
'turns_error_index',int32(-999999999),...
'turns_error_lower',double(-9e+40),...
'turns_error_upper',double(-9e+40),...
'voltage',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])))
}},...
'coils_n',int32(-999999999),...
'delta_b_field_tor_vacuum_r',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'field_map',{{struct(...
'a_field_r',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'a_field_tor',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'a_field_z',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'b_field_r',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'b_field_tor',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'b_field_z',{{struct(...
'coefficients',double([]),...
'coefficients_error_index',int32(-999999999),...
'coefficients_error_lower',double([]),...
'coefficients_error_upper',double([]),...
'grid_index',int32(-999999999),...
'grid_subset_index',int32(-999999999),...
'values',double([]),...
'values_error_index',int32(-999999999),...
'values_error_lower',double([]),...
'values_error_upper',double([]))
}},...
'grid',struct(...
'grid_subset',{{struct(...
'base',{{struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([]))
}},...
'dimension',int32(-999999999),...
'element',{{struct(...
'object',{{struct(...
'dimension',int32(-999999999),...
'index',int32(-999999999),...
'space',int32(-999999999))
}})
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'metric',struct(...
'jacobian',double([]),...
'jacobian_error_index',int32(-999999999),...
'jacobian_error_lower',double([]),...
'jacobian_error_upper',double([]),...
'tensor_contravariant',double([]),...
'tensor_contravariant_error_index',int32(-999999999),...
'tensor_contravariant_error_lower',double([]),...
'tensor_contravariant_error_upper',double([]),...
'tensor_covariant',double([]),...
'tensor_covariant_error_index',int32(-999999999),...
'tensor_covariant_error_lower',double([]),...
'tensor_covariant_error_upper',double([])))
}},...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'path','',...
'space',{{struct(...
'coordinates_type',int32([]),...
'geometry_type',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'identifier',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'objects_per_dimension',{{struct(...
'geometry_content',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'object',{{struct(...
'boundary',{{struct(...
'index',int32(-999999999),...
'neighbours',int32([]))
}},...
'geometry',double([]),...
'geometry_2d',double([]),...
'geometry_2d_error_index',int32(-999999999),...
'geometry_2d_error_lower',double([]),...
'geometry_2d_error_upper',double([]),...
'geometry_error_index',int32(-999999999),...
'geometry_error_lower',double([]),...
'geometry_error_upper',double([]),...
'measure',double(-9e+40),...
'measure_error_index',int32(-999999999),...
'measure_error_lower',double(-9e+40),...
'measure_error_upper',double(-9e+40),...
'nodes',int32([]))
}})
}})
}}),...
'time',double(-9e+40))
}},...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'is_periodic',int32(-999999999),...
'latency',double(-9e+40),...
'latency_error_index',int32(-999999999),...
'latency_error_lower',double(-9e+40),...
'latency_error_upper',double(-9e+40),...
'r0',double(-9e+40),...
'r0_error_index',int32(-999999999),...
'r0_error_lower',double(-9e+40),...
'r0_error_upper',double(-9e+40),...
'time',double([]));
