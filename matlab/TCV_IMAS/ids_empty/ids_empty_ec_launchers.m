function S = ids_empty_ec_launchers
% function S = ids_empty_ec_launchers
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'beam',{{struct(...
'frequency',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'identifier','',...
'launching_position',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'r_limit_max',double(-9e+40),...
'r_limit_min',double(-9e+40),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])),...
'mode',int32(-999999999),...
'name','',...
'phase',struct(...
'angle',double([]),...
'angle_error_index',int32(-999999999),...
'angle_error_lower',double([]),...
'angle_error_upper',double([]),...
'curvature',double([]),...
'curvature_error_index',int32(-999999999),...
'curvature_error_lower',double([]),...
'curvature_error_upper',double([])),...
'power_launched',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'spot',struct(...
'angle',double([]),...
'angle_error_index',int32(-999999999),...
'angle_error_lower',double([]),...
'angle_error_upper',double([]),...
'size',double([]),...
'size_error_index',int32(-999999999),...
'size_error_lower',double([]),...
'size_error_upper',double([])),...
'steering_angle_pol',double([]),...
'steering_angle_pol_error_index',int32(-999999999),...
'steering_angle_pol_error_lower',double([]),...
'steering_angle_pol_error_upper',double([]),...
'steering_angle_tor',double([]),...
'steering_angle_tor_error_index',int32(-999999999),...
'steering_angle_tor_error_lower',double([]),...
'steering_angle_tor_error_upper',double([]),...
'time',double([]))
}},...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'latency',double(-9e+40),...
'latency_error_index',int32(-999999999),...
'latency_error_lower',double(-9e+40),...
'latency_error_upper',double(-9e+40),...
'time',double([]));
