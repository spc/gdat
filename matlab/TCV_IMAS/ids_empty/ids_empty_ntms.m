function S = ids_empty_ntms
% function S = ids_empty_ntms
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'time',double([]),...
'time_slice',{{struct(...
'mode',{{struct(...
'calculation_method','',...
'delta_diff',double([]),...
'delta_diff_error_index',int32(-999999999),...
'delta_diff_error_lower',double([]),...
'delta_diff_error_upper',double([]),...
'deltaw',{{struct(...
'name','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40))
}},...
'detailed_evolution',struct(...
'calculation_method','',...
'delta_diff',double([]),...
'delta_diff_error_index',int32(-999999999),...
'delta_diff_error_lower',double([]),...
'delta_diff_error_upper',double([]),...
'deltaw',{{struct(...
'name','',...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([]))
}},...
'dfrequency_dt',double([]),...
'dfrequency_dt_error_index',int32(-999999999),...
'dfrequency_dt_error_lower',double([]),...
'dfrequency_dt_error_upper',double([]),...
'dphase_dt',double([]),...
'dphase_dt_error_index',int32(-999999999),...
'dphase_dt_error_lower',double([]),...
'dphase_dt_error_upper',double([]),...
'dwidth_dt',double([]),...
'dwidth_dt_error_index',int32(-999999999),...
'dwidth_dt_error_lower',double([]),...
'dwidth_dt_error_upper',double([]),...
'frequency',double([]),...
'frequency_error_index',int32(-999999999),...
'frequency_error_lower',double([]),...
'frequency_error_upper',double([]),...
'm_pol',int32(-999999999),...
'n_tor',int32(-999999999),...
'phase',double([]),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double([]),...
'phase_error_upper',double([]),...
'rho_tor',double([]),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double([]),...
'rho_tor_error_upper',double([]),...
'rho_tor_norm',double([]),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double([]),...
'rho_tor_norm_error_upper',double([]),...
'time_detailed',double([]),...
'time_detailed_error_index',int32(-999999999),...
'time_detailed_error_lower',double([]),...
'time_detailed_error_upper',double([]),...
'torque',{{struct(...
'name','',...
'value',double([]),...
'value_error_index',int32(-999999999),...
'value_error_lower',double([]),...
'value_error_upper',double([]))
}},...
'width',double([]),...
'width_error_index',int32(-999999999),...
'width_error_lower',double([]),...
'width_error_upper',double([])),...
'dfrequency_dt',double(-9e+40),...
'dfrequency_dt_error_index',int32(-999999999),...
'dfrequency_dt_error_lower',double(-9e+40),...
'dfrequency_dt_error_upper',double(-9e+40),...
'dphase_dt',double(-9e+40),...
'dphase_dt_error_index',int32(-999999999),...
'dphase_dt_error_lower',double(-9e+40),...
'dphase_dt_error_upper',double(-9e+40),...
'dwidth_dt',double(-9e+40),...
'dwidth_dt_error_index',int32(-999999999),...
'dwidth_dt_error_lower',double(-9e+40),...
'dwidth_dt_error_upper',double(-9e+40),...
'frequency',double(-9e+40),...
'frequency_error_index',int32(-999999999),...
'frequency_error_lower',double(-9e+40),...
'frequency_error_upper',double(-9e+40),...
'm_pol',int32(-999999999),...
'n_tor',int32(-999999999),...
'onset',struct(...
'cause','',...
'm_pol',int32(-999999999),...
'n_tor',int32(-999999999),...
'phase',double(-9e+40),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double(-9e+40),...
'phase_error_upper',double(-9e+40),...
'time_offset',double(-9e+40),...
'time_offset_error_index',int32(-999999999),...
'time_offset_error_lower',double(-9e+40),...
'time_offset_error_upper',double(-9e+40),...
'time_onset',double(-9e+40),...
'time_onset_error_index',int32(-999999999),...
'time_onset_error_lower',double(-9e+40),...
'time_onset_error_upper',double(-9e+40),...
'width',double(-9e+40),...
'width_error_index',int32(-999999999),...
'width_error_lower',double(-9e+40),...
'width_error_upper',double(-9e+40)),...
'phase',double(-9e+40),...
'phase_error_index',int32(-999999999),...
'phase_error_lower',double(-9e+40),...
'phase_error_upper',double(-9e+40),...
'rho_tor',double(-9e+40),...
'rho_tor_error_index',int32(-999999999),...
'rho_tor_error_lower',double(-9e+40),...
'rho_tor_error_upper',double(-9e+40),...
'rho_tor_norm',double(-9e+40),...
'rho_tor_norm_error_index',int32(-999999999),...
'rho_tor_norm_error_lower',double(-9e+40),...
'rho_tor_norm_error_upper',double(-9e+40),...
'torque',{{struct(...
'name','',...
'value',double(-9e+40),...
'value_error_index',int32(-999999999),...
'value_error_lower',double(-9e+40),...
'value_error_upper',double(-9e+40))
}},...
'width',double(-9e+40),...
'width_error_index',int32(-999999999),...
'width_error_lower',double(-9e+40),...
'width_error_upper',double(-9e+40))
}},...
'time',double(-9e+40))
}},...
'vacuum_toroidal_field',struct(...
'b0',double([]),...
'b0_error_index',int32(-999999999),...
'b0_error_lower',double([]),...
'b0_error_upper',double([]),...
'r0',double(-9e+40),...
'r0_error_index',int32(-999999999),...
'r0_error_lower',double(-9e+40),...
'r0_error_upper',double(-9e+40)));
