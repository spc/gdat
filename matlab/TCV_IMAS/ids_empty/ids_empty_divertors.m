function S = ids_empty_divertors
% function S = ids_empty_divertors
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'divertor',{{struct(...
'current_incident',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'identifier','',...
'name','',...
'particle_flux_recycled_total',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_black_body',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_conducted',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_convected',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_currents',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_incident',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_neutrals',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_radiated',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_recombination_neutrals',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_recombination_plasma',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'target',{{struct(...
'current_incident',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'extension_r',double(-9e+40),...
'extension_r_error_index',int32(-999999999),...
'extension_r_error_lower',double(-9e+40),...
'extension_r_error_upper',double(-9e+40),...
'extension_z',double(-9e+40),...
'extension_z_error_index',int32(-999999999),...
'extension_z_error_lower',double(-9e+40),...
'extension_z_error_upper',double(-9e+40),...
'flux_expansion',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'heat_flux_steady_limit_max',double(-9e+40),...
'identifier','',...
'name','',...
'power_black_body',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_conducted',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_convected',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_currents',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_flux_peak',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_incident',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_incident_fraction',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_neutrals',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_radiated',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_recombination_neutrals',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'power_recombination_plasma',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
't_e_target_sputtering_limit_max',double(-9e+40),...
'temperature_limit_max',double(-9e+40),...
'tile',{{struct(...
'current_incident',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'identifier','',...
'name','',...
'shunt_index',int32(-999999999),...
'surface_area',double(-9e+40),...
'surface_area_error_index',int32(-999999999),...
'surface_area_error_lower',double(-9e+40),...
'surface_area_error_upper',double(-9e+40),...
'surface_outline',struct(...
'phi',double([]),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double([]),...
'phi_error_upper',double([]),...
'r',double([]),...
'r_error_index',int32(-999999999),...
'r_error_lower',double([]),...
'r_error_upper',double([]),...
'z',double([]),...
'z_error_index',int32(-999999999),...
'z_error_lower',double([]),...
'z_error_upper',double([])))
}},...
'tilt_angle_pol',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])),...
'two_point_model',{{struct(...
'n_e_target',double(-9e+40),...
'n_e_target_error_index',int32(-999999999),...
'n_e_target_error_lower',double(-9e+40),...
'n_e_target_error_upper',double(-9e+40),...
'sol_heat_decay_length',double(-9e+40),...
'sol_heat_decay_length_error_index',int32(-999999999),...
'sol_heat_decay_length_error_lower',double(-9e+40),...
'sol_heat_decay_length_error_upper',double(-9e+40),...
'sol_heat_spreading_length',double(-9e+40),...
'sol_heat_spreading_length_error_index',int32(-999999999),...
'sol_heat_spreading_length_error_lower',double(-9e+40),...
'sol_heat_spreading_length_error_upper',double(-9e+40),...
't_e_target',double(-9e+40),...
't_e_target_error_index',int32(-999999999),...
't_e_target_error_lower',double(-9e+40),...
't_e_target_error_upper',double(-9e+40),...
'time',double(-9e+40))
}},...
'wetted_area',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])))
}},...
'wetted_area',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([])))
}},...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'midplane',struct(...
'description','',...
'index',int32(-999999999),...
'name',''),...
'time',double([]));
