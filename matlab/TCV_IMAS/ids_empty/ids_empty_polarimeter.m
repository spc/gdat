function S = ids_empty_polarimeter
% function S = ids_empty_polarimeter
%
% File written automatically using writestruct_to_mfile.m
% writestruct_to_mfile with n=15, with ids_gen to generate empty IDS within IMAS version 3.39.0
%

S = ...
struct(...
'channel',{{struct(...
'ellipticity',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([]),...
'validity',int32(-999999999),...
'validity_timed',int32([])),...
'ellipticity_initial',double(-9e+40),...
'ellipticity_initial_error_index',int32(-999999999),...
'ellipticity_initial_error_lower',double(-9e+40),...
'ellipticity_initial_error_upper',double(-9e+40),...
'faraday_angle',struct(...
'data',double([]),...
'data_error_index',int32(-999999999),...
'data_error_lower',double([]),...
'data_error_upper',double([]),...
'time',double([]),...
'validity',int32(-999999999),...
'validity_timed',int32([])),...
'identifier','',...
'line_of_sight',struct(...
'first_point',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'second_point',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40)),...
'third_point',struct(...
'phi',double(-9e+40),...
'phi_error_index',int32(-999999999),...
'phi_error_lower',double(-9e+40),...
'phi_error_upper',double(-9e+40),...
'r',double(-9e+40),...
'r_error_index',int32(-999999999),...
'r_error_lower',double(-9e+40),...
'r_error_upper',double(-9e+40),...
'z',double(-9e+40),...
'z_error_index',int32(-999999999),...
'z_error_lower',double(-9e+40),...
'z_error_upper',double(-9e+40))),...
'name','',...
'polarisation_initial',double(-9e+40),...
'polarisation_initial_error_index',int32(-999999999),...
'polarisation_initial_error_lower',double(-9e+40),...
'polarisation_initial_error_upper',double(-9e+40),...
'wavelength',double(-9e+40),...
'wavelength_error_index',int32(-999999999),...
'wavelength_error_lower',double(-9e+40),...
'wavelength_error_upper',double(-9e+40))
}},...
'code',struct(...
'commit','',...
'description','',...
'library',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'name','',...
'output_flag',int32([]),...
'parameters','',...
'repository','',...
'version',''),...
'ids_properties',struct(...
'comment','',...
'creation_date','',...
'homogeneous_time',int32(-999999999),...
'plugins',struct(...
'infrastructure_get',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'infrastructure_put',struct(...
'commit','',...
'description','',...
'name','',...
'repository','',...
'version',''),...
'node',{{struct(...
'get_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'path','',...
'put_operation',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}},...
'readback',{{struct(...
'commit','',...
'description','',...
'name','',...
'parameters','',...
'repository','',...
'version','')
}})
}}),...
'provenance',struct(...
'node',{{struct(...
'path','',...
'sources',{{}})
}}),...
'provider','',...
'source','',...
'version_put',struct(...
'access_layer','',...
'access_layer_language','',...
'data_dictionary','')),...
'latency',double(-9e+40),...
'latency_error_index',int32(-999999999),...
'latency_error_lower',double(-9e+40),...
'latency_error_upper',double(-9e+40),...
'time',double([]));
