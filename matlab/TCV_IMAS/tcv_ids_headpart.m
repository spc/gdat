function [ids_generic, params_ids_generic] = tcv_ids_headpart(shot,ids_in,ids_name,varargin)
%
% [ids_generic, params_ids_generic] = tcv_ids_headpart(shot,ids_in,ids_name,varargin);
%
% parses inputs and fill in ids_properties
%
% varargin options:
% 'comment':          comment to include in ids_properties, using gdat_params 
%                     for example cocos_in and cocos_out
% 'homogeneous_time': homogeneous_time in ids_properties: 
%                     1 (default) if the whole ids has same time, 0 otherwise
% 'gdat_params':      gdat params structure
%
%
% example:
%   [ids_equilibrium, params_ids_equilibrium] = tcv_ids_headpart(shot,ids_equil_empty,'equilibrium','comment','your comment');
%

% initialize input parser
p = inputParser;
% no required inputs here so one can call with empty args and get defaults parameters
% effectively required inputs should have defaults as empty
p.addOptional('shot', [], @(x) (isnumeric(x) && isscalar(x) && (x == round(x)))); % integer
p.addOptional('ids_in', struct([]), @(x) (isstruct(x)));
p.addOptional('ids_name', '', @(x) (ischar(x))); % char
p.addOptional('gdat_params', [], @(x) (isempty(x) || isstruct(x))); % char
p.addOptional('comment', '', @(x) isempty(x) || ischar(x)); % char
p.addOptional('homogeneous_time', 1, @(x) (isnumeric(x) && isscalar(x) && (x == round(x))));

p.parse(shot,ids_in,ids_name);
defaults_ids_generic = p.Results; % to keep track of defaults
params = p.Results;
if nargin >= 4
  p.parse(shot,'ids_in',ids_in,'ids_name',ids_name,varargin{:});
  params = p.Results;
end

% replace empty inputs with defaults
names = fieldnames(params);
mask = structfun(@isempty,params);
if any(mask),
  params = rmfield(params,unique([names(mask); p.UsingDefaults.']));
  p.parse(params);
  params = p.Results;
end

params_ids_generic = params;

ids_generic = ids_in;

%
% ids_properties
%
subcall='';

ids_generic.ids_properties.comment = params_ids_generic.comment;
if ~isempty(subcall)
  add_to_comment = ['varargin: ' subcall];
  if isempty(ids_generic.ids_properties.comment)
    ids_generic.ids_properties.comment = add_to_comment;
  else
    ids_generic.ids_properties.comment(end+1:end+length(add_to_comment)+2) = ['; ' add_to_comment];
  end
end

%get gdat version
[abcpath,abcfile,abcext]=fileparts(mfilename('fullpath'));
[aa1,aa2]=unix(['cd ' abcpath '; git describe --tags --first-parent --abbrev=11 --long --dirty --always']);
git_release_hash='gdat git hash not found';
if aa1==0; git_release_hash = aa2(1:end-1); end % avoid newline

ids_generic.ids_properties.provider = ['gdat_git: ' git_release_hash '; tcv_get_ids_' ids_name];
if ~isempty(params_ids_generic.gdat_params)
  if isfield(params_ids_generic.gdat_params,'cocos_in') && isfield(params_ids_generic.gdat_params,'cocos_out')
    comment_add = ['from TCV cocos_in=' num2str(params_ids_generic.gdat_params.cocos_in) ' transformed to cocos_out=' num2str(params_ids_generic.gdat_params.cocos_out)];
    if isempty(ids_generic.ids_properties.comment)
      ids_generic.ids_properties.comment = comment_add;
    else
      ids_generic.ids_properties.comment(end+1:end+length(comment_add)+2) = ['; ' comment_add];
    end
  else
    disp('cocos_in and cocos_out expected to be fields of gdat_params')
  end
  if isfield(params_ids_generic.gdat_params,'error_bar')
    provider_add = ['; error_bar option: ' params_ids_generic.gdat_params.error_bar];
    ids_generic.ids_properties.provider(end+1:end+length(provider_add)) = provider_add;
  end
end
ids_generic.ids_properties.homogeneous_time = params_ids_generic.homogeneous_time;
ids_generic.ids_properties.source = ['TCV shot #' num2str(params_ids_generic.shot)];
ids_generic.ids_properties.creation_date = date;
