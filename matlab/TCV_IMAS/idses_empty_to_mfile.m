function [empty_dir_out,ids_empty_structures,okflags] = idses_empty_to_mfile(dirname_for_files,varargin)
%
% [empty_dir_out,ids_empty_structures,okflags] = idses_empty_to_mfile(dirname_for_files,varargin);
%
% dirname_for_files: folder to save files, if not provided or empty, use /tmp/$USER/idses_empty_to_mfile
%
% generate empty ids structures and save them with writestruct_to_mfile as ascii .m files defining the structures
% Add json output when ready
%
% varargin{1}: option for outputs: 1 (default) to mfiles, 2 to mfiles and json files, 3 to json files
%
% The procedure for updating the empty idses available for TCV/lacs is to do:
% [empty_dir_out,ids_empty_structures,okflags] = idses_empty_to_mfile;
% !cp -pr /tmp/$USER/idses_empty_to_mfile/* /.../gdat/matlab/TCV_IMAS/ids_empty
% !ls -alt /.../gdat/matlab/TCV_IMAS/ids_empty
% and rm the old files (not replaced by the cp above, since it means non existent in current version
% add branch to gdat and merge, then deploy
%
% wall = ids_empty_wall; % for example gives the IDS wall structure
% help ids_empty_wall % gives you the IMAS version it used to produce it
%

if nargin < 1 || isempty(dirname_for_files) || ~exist(dirname_for_files,'dir')
  dirname_for_files = sprintf('/tmp/%s/idses_empty_to_mfile',getenv('USER'));
  [dummy1,dummy2]=rmdir(dirname_for_files,'s');
  mkdir(dirname_for_files);
  warning('no folder provided, files will be written to: %s\n',dirname_for_files);
end
empty_dir_out = dirname_for_files;

write_option = 1;
if nargin>=2 && ~isempty(varargin{1})
  write_option = varargin{1};
end

ids_list_to_generate = IDS_list;
imas_version = getenv('IMAS_VERSION');
n_prec = 15; % for 1e40 and -1e9+1
provenance_text = sprintf('writestruct_to_mfile with n=%d, with ids_gen to generate empty IDS within IMAS version %s',n_prec,imas_version);
for i=1:length(ids_list_to_generate)
  ids_empty_structures.(ids_list_to_generate{i}) = ids_gen(ids_list_to_generate{i});
  okflags(i) = writestruct_to_mfile(ids_empty_structures.(ids_list_to_generate{i}), ...
          fullfile(dirname_for_files,['ids_empty_',ids_list_to_generate{i},'.m']),n_prec,provenance_text);
end

okflags(end+1) = writecell(ids_list_to_generate,fullfile(dirname_for_files,'ids_list_all.m'),n_prec,provenance_text);


if any(okflags~=1)
  warning('some flags not ok')
end
