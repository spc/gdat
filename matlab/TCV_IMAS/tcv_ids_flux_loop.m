function [sub_ids_struct_out,sub_ids_struct_out_description] = tcv_ids_flux_loop(shot, ids_structures, gdat_params, varargin)
%
% [sub_ids_struct_out,sub_ids_struct_out_description] = tcv_ids_flux_loop(shot, ids_structures, gdat_params, varargin)
%
% Get ids field magnetics.flux_loop
%
% error_bar: from gdat_params.error_bar
%            'delta' (default): error_bar to be added inserted in "upper" only as mentioned in description
%            'delta_with_lower' : error_bar (abs) inserted in both lower and upper
%            'added': value already added to data: upper/lower = data +/- error_bar
%

error_bar = 'delta';
if exist('gdat_params','var') && isfield(gdat_params,'error_bar') && ~isempty(gdat_params.error_bar)
  error_bar = gdat_params.error_bar;
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% Get data
params_eff = params_eff_ref;
params_eff.data_request = 'tcv_idealloop("FLUX")';
tmp = gdat_tcv(shot,params_eff);
% tmp_desc = params_eff.data_request;
names = tmp.dim{2};
time = tmp.dim{1};
data = tmp.data;

% Preallocate output structure
Nprobes = size(tmp.data,2);
sub_ids_struct_out(1:Nprobes) = ids_structures;
sub_ids_struct_out_description = cell(1,Nprobes);

% Put data on ids structure
for ii=1:Nprobes
    sub_ids_struct_out{ii}.name  = [names{ii}];
    sub_ids_struct_out_description{ii}.name  = [names{ii}];
    sub_ids_struct_out{ii}.position{1}.r  = mdsvalue('STATIC("R_F"  )[$1]',sub_ids_struct_out{ii}.name);
    sub_ids_struct_out_description{ii}.position_r  = ['from ''STATIC("R_F"  )[' sub_ids_struct_out{ii}.name ']'''];
    sub_ids_struct_out{ii}.position{1}.z  = mdsvalue('STATIC("Z_F"  )[$1]',sub_ids_struct_out{ii}.name);
    sub_ids_struct_out_description{ii}.position_z  = ['from ''STATIC("Z_F"  )[' sub_ids_struct_out{ii}.name ']'''];
%     sub_ids_struct_out{ii}.position{1}.phi  = 0.; % TO BE FOUND
%     sub_ids_struct_out_description{ii}.position_phi  = 'assumed 0';
    sub_ids_struct_out{ii}.flux.data  =  data(:,ii);
    sub_ids_struct_out_description{ii}.flux_data  =  'from ''tcv_idealloop("FLUX")''';
    sub_ids_struct_out{ii}.flux.time  =  time;
end

fixed_error = 0.0012*2*pi; % Convert old LIUQE error to Webers
switch error_bar
 case 'delta'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.flux.data_error_upper = fixed_error.*ones(size(sub_ids_struct_out{ii}.flux.data));
    sub_ids_struct_out_description{ii}.flux_data_error_upper = ['from fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.flux_data_error_lower = 'not provided since symmetric';
    %(not filled if symmetric) sub_ids_struct_out{ii}.flux.data_error_lower = 0.0012;%.*ones(size(sub_ids_struct_out{ii}.flux.data));
  end
 case 'delta_with_lower'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.flux.data_error_upper = fixed_error.*ones(size(sub_ids_struct_out{ii}.flux.data));
    sub_ids_struct_out{ii}.flux.data_error_lower = sub_ids_struct_out{ii}.flux.data_error_upper;
    sub_ids_struct_out_description{ii}.flux_data_error_upper = ['from fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.flux_data_error_lower = ['from fixed error value in case ' error_bar];
    %(not filled if symmetric) sub_ids_struct_out{ii}.flux.data_error_lower = 0.0012;%.*ones(size(sub_ids_struct_out{ii}.flux.data));
  end
 case 'added'
  for ii=1:Nprobes
    sub_ids_struct_out{ii}.flux.data_error_upper = sub_ids_struct_out{ii}.flux.data + fixed_error.*ones(size(sub_ids_struct_out{ii}.flux.data));
    sub_ids_struct_out{ii}.flux.data_error_lower = sub_ids_struct_out{ii}.flux.data - fixed_error.*ones(size(sub_ids_struct_out{ii}.flux.data));
    sub_ids_struct_out_description{ii}.flux.data_error_upper = ['from data + fixed error value in case ' error_bar];
    sub_ids_struct_out_description{ii}.flux.data_error_lower = ['from data - fixed error value in case ' error_bar];
  end
 otherwise
  error(['tcv_ids_flux_loop: error_bar option not known: ' error_bar])
end
