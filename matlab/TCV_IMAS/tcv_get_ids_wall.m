function [ids_wall,ids_wall_description,varargout] = tcv_get_ids_wall(shot, ids_wall_empty, gdat_params, varargin)
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call, in particular error_bar options
%

% Input parser
if exist('gdat_params','var')
  [ids_wall, params] = tcv_ids_headpart(shot, ids_wall_empty,'wall','homogeneous_time',0,'gdat_params',gdat_params,varargin{:});
else
  [ids_wall, params] = tcv_ids_headpart(shot, ids_wall_empty,'wall','homogeneous_time',0,varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end

% Get data of outline, only static, no need for gdat_params and time_out parameter for example
[ids_wall.description_2d,ids_wall_description.description_2d]= tcv_ids_wall_description_2d(ids_wall.description_2d(1));

% make arrays not filled in empty: not the case for magnetics
ids_wall.global_quantities.neutral = {}; % wall does not work with Antoine's addpath
ids_wall.description_ggd = {};
ids_wall.description_2d{1}.mobile.unit = {};
ids_wall.description_2d{1}.vessel.unit{1}.element = {};


%% TODO
ids_description = [];

% cocos automatic transform (should have nothing to do for wall)
if exist('ids_generic_cocos_nodes_transformation_symbolic','file')
  [ids_wall,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_wall,'wall',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);
end
