function [empty_dir_out] = ids_structures_mat_to_m(mat_filename,emptydirname)
%%
% auxiliary script to convert .mat containing empty IDS to separate .m files
% in ids_empty_folder
%
% Example:
% [empty_dir_out] = ids_structures_mat_to_m('/tmp/sautero/ids_structures_IMAS331.mat','/tmp/sautero/ids_empty')
%
if nargin < 1 || isempty(mat_filename)
  error('requires mat filename');
  return
end
if nargin < 2 || isempty(emptydirname)
  target_folder = 'ids_empty';
else
  target_folder = emptydirname;
end
empty_dir_out = target_folder;

ids_struct_saved = mat_filename;
ffolder = fullfile(target_folder);
assert(~~exist(ffolder,'dir'),'target folder does not exist')

load(ids_struct_saved);

fields = fieldnames(ids_structures);

%% write file with
okflag = writecell(fields,fullfile(ffolder,'ids_list_all'),15);
assert(okflag,'problem writing cell array');
assert(isequaln(fields,eval('ids_list_all'))); % check
%% write individual files per ids template
addpath(ffolder)
for ii=1:numel(fields)
  myfield = fields{ii};
  fieldval = ids_structures.(myfield);

  %% write to .m
  fname = sprintf('ids_empty_%s',myfield);
  fpath = fullfile(ffolder,fname);

  fprintf('writing %s\n',fpath);
  okflag =writestruct(fieldval,fpath,15);
  assert(okflag,'failed writing %s',fpath);

  try
    S_out = eval(fname);
    assert(isequaln(fieldval,S_out),'fields not equal %s',myfield);
  catch ME
    keyboard
    rethrow(ME)
  end
end
