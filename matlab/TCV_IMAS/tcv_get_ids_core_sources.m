function [ids_core_sources,ids_core_sources_description,varargout] = ...
  tcv_get_ids_core_sources(shot,ids_equil_empty,gdat_params,varargin)
%
% [ids_core_sources,ids_core_sources_description,varargout] = ...
%     tcv_get_ids_core_sources(shot,ids_equil_empty,gdat_params,varargin);
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call
%

machine = 'tcv';
tens_time = -1;
tens_rho = -0.1;

if exist('gdat_params','var')
  [ids_core_sources, params_core_sources] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_sources','gdat_params',gdat_params,...
    'homogeneous_time',0,varargin{:});
else
  [ids_core_sources, params_core_sources] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_sources','homogeneous_time',0,varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% initialize description
ids_core_sources_description = [];

%% fill vacuum_toroidal field
params_eff = params_eff_ref; params_eff.data_request='b0';
b0_gdat=gdat(params_core_sources.shot,params_eff);
ids_core_sources.vacuum_toroidal_field.r0 = b0_gdat.r0;
ids_core_sources.vacuum_toroidal_field.b0 = b0_gdat.data;
ids_core_sources_description.vacuum_toroidal_field = b0_gdat.request_description;
ids_core_sources.time = b0_gdat.t;
%%
% save source template to fill for differnet source options
source_template = ids_core_sources.source{1};
profiles_template = source_template.profiles_1d{1};
globals_template = source_template.global_quantities{1};

last_index = 0; % index to be incremented when sources are added

%% get powers information from gdat
params_eff = params_eff_ref; params_eff.data_request='powers';
powers_gdat = gdat(params_core_sources.shot,params_eff);

%% load liuqe data from nodes for conversions
if params_eff_ref.liuqe == 1
  nodes_liuqe = 'equil';
else
  nodes_liuqe = ['equil',num2str(params_eff_ref.liuqe)];
end
if check_nodes_filled(params_core_sources.shot,nodes_liuqe)
  params_eff = params_eff_ref;
  % normalized sqrt poloidal flux including axis
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:rho';
  rho_pol_norm_liu = gdat(params_core_sources.shot,params_eff);
  % <1/R^2>
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:gpsi0_r2_fsa';
  Rm2_fs_liu = gdat(params_core_sources.shot,params_eff);
  % Volume
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:vol';
  vol_liu= gdat(params_core_sources.shot,params_eff);
  % T(\psi) = R*B_tor
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:rbtor_rho';
  T_liu = gdat(params_core_sources.shot,params_eff);
  % safety factor q = -dPhi/dPsi
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:q';
  q_liu = gdat(params_core_sources.shot,params_eff);
  % I_tor = I_p, total plasma current
  params_eff.data_request='\tcv_shot::top.results.equil_1.results:i_pl';
  Ip_liu = gdat(params_core_sources.shot,params_eff);
  % define liuqe_time from Ip_liu
  liuqe_time = Ip_liu.t;
else
  error('Liuqe nodes %s not filled. Contact O. Sauter.',nodes_liuqe)
end
%% initialize source from template
% ohm
params_eff = params_eff_ref; params_eff.data_request='ohm_data';
ohm_gdat = gdat(params_core_sources.shot,params_eff); ohm_data = ohm_gdat.ohm.ohm_data;
ohm_tgrid = ohm_gdat.ohm.t; ohm_n_t = numel(ohm_tgrid);

% make sure that ohm data is on correct tgrid
ohm_power = interpos(powers_gdat.ohm.t,powers_gdat.ohm.data,ohm_tgrid);

main_desc = 'Source from ohmic heating'; production = 'IBS nodes';
id_ohm.description = sprintf('%s from %s',main_desc,production);
id_ohm.index = 7; id_ohm.name = 'ohmic';
ids_core_sources.source{last_index+1} = source_template;
ids_core_sources.source{last_index+1}.identifier = id_ohm;
ids_core_sources.source{last_index+1}.profiles_1d(1:ohm_n_t) = {profiles_template};
ids_core_sources.source{last_index+1}.global_quantities(1:ohm_n_t) = {globals_template};

% load LIUQE data to convert
% \tilde{j}_// = <jdotB>/(R0<Bphi/R>) to j_//0 = <jdotB>/B0; Bphi=T(psi)/R
% interpolate liuqe outputs on ohm_tgrid
T      = interp1(liuqe_time,T_liu.data.',     ohm_tgrid)';
Rm2_fs = interp1(liuqe_time,Rm2_fs_liu.data.',ohm_tgrid)';
vol    = interp1(liuqe_time,vol_liu.data.',   ohm_tgrid)';
% get vacuum field data from ids
R0 = ids_core_sources.vacuum_toroidal_field.r0;
B0 = interp1(ids_core_sources.time,ids_core_sources.vacuum_toroidal_field.b0,ohm_tgrid);

jpar_tilde_to_jpar_0 = R0*T.*Rm2_fs./B0;

% map volume and conversion factor on rho_pol grid of ohm_data cd_dens
jpar_tilde_to_jpar_0_mapped = ...
  interp1(rho_pol_norm_liu.data,jpar_tilde_to_jpar_0,ohm_data.cd_dens.rhopol_norm);
vol_mapped = interp1(rho_pol_norm_liu.data,vol,ohm_data.cd_dens.rhopol_norm);

for ii = 1:ohm_n_t
  % profiles_1d
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.time = ohm_tgrid(ii);
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.grid.rho_pol_norm = ...
    ohm_data.cd_dens.rhopol_norm';
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.grid.rho_tor_norm = ...
    ohm_data.cd_dens.rhotor_norm(:,ii)';
  jpar_0_tmp = ohm_data.cd_dens.data(:,ii)'.*jpar_tilde_to_jpar_0_mapped(:,ii)';
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.j_parallel = jpar_0_tmp;
  integrated_jpar_0_tmp = cumtrapz(vol_mapped(:,ii),jpar_0_tmp)/2/pi/R0;
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.current_parallel_inside = integrated_jpar_0_tmp;

  % globals
  ids_core_sources.source{last_index+1}.global_quantities{ii}.time = ohm_tgrid(ii);
  ids_core_sources.source{last_index+1}.global_quantities{ii}.power = ohm_power(ii);
  ids_core_sources.source{last_index+1}.global_quantities{ii}.current_parallel = integrated_jpar_0_tmp(end);
end

last_index = last_index+1;  % add if statement to only increment if ohmic source has been added

%% bs
params_eff = params_eff_ref; params_eff.data_request='bs_data';
bs_gdat = gdat(params_core_sources.shot,params_eff); bs_data = bs_gdat.bs.bs_data;
bs_tgrid = bs_gdat.bs.t; bs_n_t = numel(bs_tgrid);

main_desc = 'Bootstrap current'; production = 'IBS nodes';
id_bs.description = sprintf('%s from %s',main_desc,production);
id_bs.index = 13; id_bs.name = 'bootstrap_current';
ids_core_sources.source{last_index+1} = source_template;
ids_core_sources.source{last_index+1}.identifier = id_bs;
ids_core_sources.source{last_index+1}.profiles_1d(1:bs_n_t) = {profiles_template};
ids_core_sources.source{last_index+1}.global_quantities(1:bs_n_t) = {globals_template};

for ii = 1:bs_n_t
  % profiles_1d
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.time = bs_tgrid(ii);
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.grid.rho_pol_norm = ...
    bs_data.cd_dens.rhopol_norm';
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.grid.rho_tor_norm = ...
    bs_data.cd_dens.rhotor_norm(:,ii)';
  % use same conversion as for ohmic data from LIUQE
  jpar_0_tmp = bs_data.cd_dens.data(:,ii)'.*jpar_tilde_to_jpar_0_mapped(:,ii)';
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.j_parallel = jpar_0_tmp;
  integrated_jpar_0_tmp = cumtrapz(vol_mapped(:,ii),jpar_0_tmp)/2/pi/R0;
  ids_core_sources.source{last_index+1}.profiles_1d{ii}.current_parallel_inside = integrated_jpar_0_tmp;

  % globals
  ids_core_sources.source{last_index+1}.global_quantities{ii}.time = bs_tgrid(ii);
  ids_core_sources.source{last_index+1}.global_quantities{ii}.current_parallel = integrated_jpar_0_tmp(end);
end

last_index = last_index+1;  % add if statement to only increment if bs source has been added

%% ec
params_eff = params_eff_ref;
params_eff.data_request='ec_data';
params_eff.ec_inputs = 1; % load EC input information
ec_gdat = gdat(params_core_sources.shot,params_eff);

if ~isempty(ec_gdat.ec.data) % if EC data available, fill sources

  ec_data = ec_gdat.ec.ec_data; ec_inputs = ec_gdat.ec.ec_inputs;
  % get tgrid from gdat ec_data
  ec_tgrid_toray = ec_data.p_dens.t; nt_ec_toray = numel(ec_tgrid_toray);

  % retrieve active launcher information from ec_inputs
  nb_launchers = numel(ec_inputs.launchers_active.data);
  active_launchers = find(ec_inputs.launchers_active.data==1)';
  nb_active= numel(active_launchers);

  % find times of injected EC power to interpolate power & current densities
  % on p_ec_injected tgrid
  ec_powers_tgrid = powers_gdat.ec.t; %nt_ec_powers = numel(ec_powers_tgrid);
  % find times where EC is on to define time grid with extra time slice just
  % before/after  EC power and at start/end of shot
  itime_ec = find(powers_gdat.ec.data(:,end)>0);
  if ec_powers_tgrid(itime_ec(end))>=ohm_tgrid(end)
    i_time_end = iround(ec_powers_tgrid,ohm_tgrid(end));
    ec_tgrid_out = [ohm_tgrid(1),ec_powers_tgrid(itime_ec(1)-1:i_time_end)'];
  else
    ec_tgrid_out = [ohm_tgrid(1),ec_powers_tgrid(itime_ec(1)-1:itime_ec(end)+1)',ohm_tgrid(end)];
  end
  nt_ec_out = numel(ec_tgrid_out);
  p_ec_injected = interpos(ec_powers_tgrid,powers_gdat.ec.data(:,end),ec_tgrid_out);

  % Setup source structs for active launchers from template
  main_desc = 'Source from electron cyclotron heating and current drive';
  production = 'TORAY';
  id_ec.index = 3; id_ec.name = 'ec';
  for i_lau = 1:nb_active
    id_ec.description = sprintf('L%i/G%i, %s from %s double width CD profiles',active_launchers(i_lau),ec_inputs.gyro2launcher.data(active_launchers(i_lau)),main_desc,production);
    ids_core_sources.source{last_index+i_lau} = source_template;
    ids_core_sources.source{last_index+i_lau}.identifier = id_ec;
    ids_core_sources.source{last_index+i_lau}.profiles_1d(1:nt_ec_out) = {profiles_template};
    ids_core_sources.source{last_index+i_lau}.global_quantities(1:nt_ec_out) = {globals_template};
  end

  % get vacuum field data from ids
  R0 = ids_core_sources.vacuum_toroidal_field.r0;
  B0 = interp1(ids_core_sources.time,ids_core_sources.vacuum_toroidal_field.b0,ec_tgrid_toray);

  % do conversion j_{V,TORAY} = dI_\phi/dV = (1/2\pi)<j_\phi/R> to j_//0 = <jdotB>/B0;
  % via j_tilde// = <jdotB>/(R0 <Bphi/R>) = 2*pi/R0/Rm2_fs[j_{V,TORAY}-dT/dV/T*I_{phi,cd}]

  % interpolate liuqe outputs on ec_tgrid_toray
  T      = interp1(liuqe_time,T_liu.data.',     ec_tgrid_toray)';
  Rm2_fs = interp1(liuqe_time,Rm2_fs_liu.data.',ec_tgrid_toray)';
  vol    = interp1(liuqe_time,vol_liu.data.',   ec_tgrid_toray)';

%  % alternative conversion, slightly mismatching with the conversion
%  % above, to be clarified
%  % using j_//,cd = <j_cd/B> B, thus j_// = 2\pi <B^2> / (B_0 <B_\phi/R>)
%   mu0     = 4*pi*10^-7;
%   q      = interp1(liuqe_time,q_liu.data.',     ec_tgrid_toray)';
%   Ip     = interp1(liuqe_time,Ip_liu.data,      ec_tgrid_toray);
%   Vprime = -2*pi*q./T./Rm2_fs;
%   B2_fs   = -mu0*Ip./Vprime + T.^2.*Rm2_fs;
%   jtoray_to_jpar0 = 2*pi./B0./T./Rm2_fs.*B2_fs;
%   jtoray_to_jpar0_mapped = nan(size(ec_data.cd_dens.x));

  % interpolate on ec_data rho_pol grid
  vol_mapped    = nan(size(ec_data.cd_dens.x));
  T_mapped      = nan(size(ec_data.cd_dens.x));
  dTdV_mapped   = nan(size(ec_data.cd_dens.x));
  Rm2_fs_mapped = nan(size(ec_data.cd_dens.x));
  for ii = 1:nt_ec_toray
%     jtoray_to_jpar0_mapped(:,ii) = ...
%       interp1(rho_pol_norm_liu.data,jtoray_to_jpar0(:,ii),ec_data.cd_dens.grids.rho_pol_norm(:,ii));
    vol_mapped(:,ii)      = interpos(...
      rho_pol_norm_liu.data,vol(:,ii),ec_data.cd_dens.grids.rho_pol_norm(:,ii));
    T_mapped(:,ii)        = interpos(...
      rho_pol_norm_liu.data,T(:,ii),ec_data.cd_dens.grids.rho_pol_norm(:,ii));
    [~,dTdV_mapped(:,ii)] = interpos(vol_mapped(:,ii),T_mapped(:,ii));
    Rm2_fs_mapped(:,ii)   = interpos(...
      rho_pol_norm_liu.data,Rm2_fs(:,ii),ec_data.cd_dens.grids.rho_pol_norm(:,ii));
  end

  % load 1d_profiles from ec_data
  p_dens        = ec_data.p_dens.data;
  p_dens(isnan(p_dens)) = 0;
  p_integrated  = ec_data.p_integrated.data;
  p_integrated(isnan(p_integrated)) = 0;
  cd_dens       = ec_data.cd_dens_doublewidth.data; % use double width to have realistic deposition broadening
  cd_dens(isnan(cd_dens)) = 0;
  cd_integrated = ec_data.cd_integrated.data;
  cd_integrated(isnan(cd_integrated)) = 0;

  % initialize variables
  jpar_tilde       = zeros(size(cd_dens));
  jpar0            = zeros(size(cd_dens));
  jpar0_integrated = zeros(size(cd_dens));

  % convert and integrate j_TORAY to j//0
  for i_lau = active_launchers
%     % alternative conversion
%     jpar0(:,i_lau,:) = squeeze(cd_dens(:,i_lau,:)).*jtoray_to_jpar0_mapped;

    jpar_tilde(:,i_lau,:) = 2*pi/R0./Rm2_fs_mapped.*(squeeze(cd_dens(:,i_lau,:))...
      -dTdV_mapped./T_mapped.*squeeze(cd_integrated(:,i_lau,:)));
    jpar0(:,i_lau,:) = squeeze(jpar_tilde(:,i_lau,:)).*R0.*T_mapped.*Rm2_fs_mapped./B0;

    for ii = 1:nt_ec_toray
      % integrate j//0
      [~,~,~,jpar0_integrated(:,i_lau,ii)] = interpos(vol_mapped(:,ii),jpar0(:,i_lau,ii)/(2*pi*R0));
    end
  end

%% interpolating 1d_profiles from 'ec_tgrid_toray' grid (toray tgrid) to 'interp_tgrid' (powers tgrid when powers.data>0)
  ij = iround_os(ec_tgrid_out,ec_tgrid_toray);
  sparse_p_ec_injected = p_ec_injected(ij); % injected ec power vals corresponding to ec_tgrid_toray
  n_rho = size(p_dens, 1);
  %rho grids to be interpolated on power time grid
  rho_pol_norm = ec_data.cd_dens.grids.rho_pol_norm;
  rho_tor_norm = ec_data.cd_dens.grids.rho_tor_norm;

  % calculate normalised profiles on ec_tgrid_toray grid
  norm_p_dens           = zeros(n_rho,nb_launchers+1,nt_ec_toray);
  norm_p_integrated     = zeros(n_rho,nb_launchers+1,nt_ec_toray);
  norm_jpar0            = zeros(n_rho,nb_launchers+1,nt_ec_toray);
  norm_jpar0_integrated = zeros(n_rho,nb_launchers+1,nt_ec_toray);
  for it = 1:nt_ec_toray
    norm_p_dens(:,:,it)           = p_dens(:,:,it)          ./sparse_p_ec_injected(it);
    norm_p_integrated(:,:,it)     = p_integrated(:,:,it)    ./sparse_p_ec_injected(it);
    norm_jpar0(:,:,it)            = jpar0(:,:,it)           ./sparse_p_ec_injected(it);
    norm_jpar0_integrated(:,:,it) = jpar0_integrated(:,:,it)./sparse_p_ec_injected(it);
  end

  % interpolate normalised p_dens profiles on ec_powers_tgrid
  interp_norm_p_dens           = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_norm_p_integrated     = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_norm_jpar0            = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_norm_jpar0_integrated = zeros(n_rho,nb_launchers+1,nt_ec_out);
  for i_lau = active_launchers
    for irho = 1:n_rho
      % get power and current density at each rho
      trace_p_dens        = squeeze(norm_p_dens(irho,i_lau,:));
      trace_p_integrated  = squeeze(norm_p_integrated(irho,i_lau,:));
      trace_jpar0       = squeeze(norm_jpar0(irho,i_lau,:));
      trace_jpar0_integrated = squeeze(norm_jpar0_integrated(irho,i_lau,:));
      % interpolate on gdat powers tgrid, interpos 21 to extrapolate,
      % taking constant value y_edge; needed for time window between start of EC and first TS time
      interp_norm_p_dens(          irho,i_lau,3:end-2) = interpos(21,ec_tgrid_toray,trace_p_dens,          ec_tgrid_out(3:end-2));
      interp_norm_p_integrated(    irho,i_lau,3:end-2) = interpos(21,ec_tgrid_toray,trace_p_integrated,    ec_tgrid_out(3:end-2));
      interp_norm_jpar0(           irho,i_lau,3:end-2) = interpos(21,ec_tgrid_toray,trace_jpar0,           ec_tgrid_out(3:end-2));
      interp_norm_jpar0_integrated(irho,i_lau,3:end-2) = interpos(21,ec_tgrid_toray,trace_jpar0_integrated,ec_tgrid_out(3:end-2));
    end
  end

  % interpolate the rho_pol & rho_tor on ec_powers_tgrid
  interp_rho_pol = zeros(n_rho,nt_ec_out);
  interp_rho_tor = repmat(rho_tor_norm(:,1),1,nt_ec_out);
  for irho = 1:n_rho
    interp_rho_pol(irho,3:end-2) = interpos(21,ec_tgrid_toray,rho_pol_norm(irho,:),ec_tgrid_out(3:end-2));
    %interp_rho_tor(irho,3:end-2) = interpos(21,ec_tgrid_toray,rho_tor_norm(irho,:),ec_tgrid_out(3:end-2));
  end
  % fill rho_pol just outside of TORAY times(zero power) with the known profiles one time slice after/before
  interp_rho_pol(:,2) = interp_rho_pol(:,3); interp_rho_pol(:,end-1) = interp_rho_pol(:,end-2);
  %interp_rho_tor(:,2) = interp_rho_tor(:,3); interp_rho_tor(:,end-1) = interp_rho_tor(:,end-2);
  
  
  % normalised & interpolated profiles * p_ec_injected on interp_tgrid
  interp_p_dens        = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_p_integrated  = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_jpar0         = zeros(n_rho,nb_launchers+1,nt_ec_out);
  interp_jpar0_integrated = zeros(n_rho,nb_launchers+1,nt_ec_out);
  for it = 1:nt_ec_out % fill profiles only for time slices when EC is nonzero, thus it+2 assignment
    interp_p_dens(:,:,it)           = interp_norm_p_dens(:,:,it)          .*p_ec_injected(it);
    interp_p_integrated(:,:,it)     = interp_norm_p_integrated(:,:,it)    .*p_ec_injected(it);
    interp_jpar0(:,:,it)            = interp_norm_jpar0(:,:,it)           .*p_ec_injected(it);
    interp_jpar0_integrated(:,:,it) = interp_norm_jpar0_integrated(:,:,it).*p_ec_injected(it);
  end

  % fill the ids entries
  for i_lau = 1:nb_active
    for ii = 1:nt_ec_out
      % 1d_profiles
      ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.time = ec_tgrid_out(ii);
      ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.grid.rho_tor_norm = interp_rho_tor(:,ii);
      % grids (do not fill for first and last time slice)
      if ii~=1 || ii~=nt_ec_out
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.grid.rho_tor_norm = interp_rho_tor(:,ii);
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.grid.rho_pol_norm = interp_rho_pol(:,ii);
        % power density
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.electrons.energy = ...
          interp_p_dens(:,active_launchers(i_lau),ii);
        % integrated power density
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.electrons.power_inside = ...
          interp_p_integrated(:,active_launchers(i_lau),ii);
        % current density
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.j_parallel = ...
          interp_jpar0(:,active_launchers(i_lau),ii);
        % integrated current density
        ids_core_sources.source{last_index+i_lau}.profiles_1d{ii}.current_parallel_inside = ...
          interp_jpar0_integrated(:,active_launchers(i_lau),ii);
      end

      % globals
      ids_core_sources.source{last_index+i_lau}.global_quantities{ii}.time = ec_tgrid_out(ii);
      ids_core_sources.source{last_index+i_lau}.global_quantities{ii}.power = ...
        interp_p_integrated(end,active_launchers(i_lau),ii);
      ids_core_sources.source{last_index+i_lau}.global_quantities{ii}.current_parallel = ...
        interp_jpar0_integrated(end,active_launchers(i_lau),ii);
    end
  end

  last_index = last_index+nb_active;
end

%% NBI. same tgrid for NBI1 and NBI2
check_nbi = [~isempty(powers_gdat.nbi1.data),~isempty(powers_gdat.nbi2.data)];
active_nbi = find(check_nbi==1); nb_active = numel(active_nbi);
nbi_names = {'nbi1','nbi2'};

% Setup source structs for active nbi from template
main_desc = 'Source from Neutral Beam Injection';
id_nbi.index = 2; id_nbi.name = 'nbi';

disp('Loading of current & power densities from ASTRA not implemented yet.')
disp('Checking if ASTRA run available on partition /Lac8_D:')
[~,hostname] = unix('hostname');
hostname = hostname(~isspace(hostname));
if strcmp(hostname,'lac8.epfl.ch')
  unix(sprintf('ls /Lac8_D/ASTRA/ | grep ''%i'' && echo File for shotnumber exists! || echo File for shotnumber does not exist!',shot));
else
  unix(sprintf('ssh $(whoami)@lac8 "ls /Lac8_D/ASTRA/ | grep ''%i'' && echo File for shotnumber exists! || echo File for shotnumber does not exist! && exit"',shot));
end
rho_test = linspace(0,1,101);
if numel(active_nbi)>0
  for i_nbi = active_nbi
    % get tgrid (same for NBI1 & 2 if both active) from active_nbi(1)
    nbi_powers_tgrid = powers_gdat.(nbi_names{i_nbi}).t;
    % find times where NBI is on to define time grid with extra time slice just
    % before/after  NBI power and at start/end of shot
    itime_nbi = find((powers_gdat.(nbi_names{i_nbi}).data>0));
    if ~isempty(itime_nbi)
      if nbi_powers_tgrid(itime_nbi(end))>=ohm_tgrid(end)
        i_time_end = iround(nbi_powers_tgrid,ohm_tgrid(end));
        nbi_tgrid_out = [ohm_tgrid(1),nbi_powers_tgrid(itime_nbi(1)-1:i_time_end)'];
      else
        nbi_tgrid_out = [ohm_tgrid(1),nbi_powers_tgrid(itime_nbi(1)-1:itime_nbi(end)+1)',ohm_tgrid(end)];
      end
      nt_nbi_out = numel(nbi_tgrid_out);

      % Setup source structs for active nbi from template
      id_nbi.description = sprintf('NBI%i %s',i_nbi,main_desc);
      ids_core_sources.source{last_index+i_nbi} = source_template;
      ids_core_sources.source{last_index+i_nbi}.identifier = id_nbi;
      ids_core_sources.source{last_index+i_nbi}.profiles_1d(1:nt_nbi_out) = {profiles_template};
      ids_core_sources.source{last_index+i_nbi}.global_quantities(1:nt_nbi_out) = {globals_template};

      p_nbi_injected_tmp = interpos(nbi_powers_tgrid,powers_gdat.(nbi_names{i_nbi}).data,nbi_tgrid_out);
      for ii = 1:nt_nbi_out
        ids_core_sources.source{last_index+i_nbi}.profiles_1d{ii}.time = nbi_tgrid_out(ii);
        ids_core_sources.source{last_index+i_nbi}.profiles_1d{ii}.grid.rho_tor_norm = rho_test;
        % globals
        ids_core_sources.source{last_index+i_nbi}.global_quantities{ii}.time = nbi_tgrid_out(ii);
        ids_core_sources.source{last_index+i_nbi}.global_quantities{ii}.power = p_nbi_injected_tmp(ii);
      end
    else
      disp('NBI power is zero for all times, skip source.')
    end
  end

  last_index = numel(ids_core_sources.source); % check for existing sources

end

%% DNBI has it's own time grid
if ~isempty(powers_gdat.dnbi)
  % get tgrid (same for NBI1 & 2 if both active) from active_nbi(1)
  dnbi_powers_tgrid = powers_gdat.dnbi.t;
  % find times where DNBI is on to define time grid with extra time slice just
  % before/after  DNBI power and at start/end of shot
  itime_dnbi = find((powers_gdat.dnbi.data>0));
  rho_test = linspace(0,1,101);
  if ~isempty(itime_dnbi)
    if dnbi_powers_tgrid(itime_dnbi(end))>=ohm_tgrid(end)
      i_time_end = iround(dnbi_powers_tgrid,ohm_tgrid(end));
      dnbi_tgrid_out = [ohm_tgrid(1),dnbi_powers_tgrid(itime_dnbi(1)-1:i_time_end)'];
    else
      dnbi_tgrid_out = [ohm_tgrid(1),dnbi_powers_tgrid(itime_dnbi(1)-1:itime_dnbi(end)+1)',ohm_tgrid(end)];
    end
    nt_dnbi_out = numel(dnbi_tgrid_out);

    id_dnbi.description = 'DNBI Source from Neutral Beam Injection';
    id_dnbi.index = 2; id_dnbi.name = 'nbi';
    ids_core_sources.source{last_index+1} = source_template;
    ids_core_sources.source{last_index+1}.identifier = id_dnbi;
    ids_core_sources.source{last_index+1}.profiles_1d(1:nt_dnbi_out) = {profiles_template};
    ids_core_sources.source{last_index+1}.global_quantities(1:nt_dnbi_out) = {globals_template};

    p_dnbi_injected = interpos(dnbi_powers_tgrid,powers_gdat.dnbi.data,dnbi_tgrid_out);
    for ii = 1:nt_dnbi_out
      ids_core_sources.source{last_index+1}.profiles_1d{ii}.time = dnbi_tgrid_out(ii);
      ids_core_sources.source{last_index+1}.profiles_1d{ii}.grid.rho_tor_norm = rho_test;
      % globals
      ids_core_sources.source{last_index+1}.global_quantities{ii}.time = dnbi_tgrid_out(ii);
      ids_core_sources.source{last_index+1}.global_quantities{ii}.power = p_dnbi_injected(ii);
    end
    last_index = last_index+1;
  else
    disp('DNBI power is zero for all times, skip source.')
  end
end

%% total
% id_total.description = 'Total source; combines all sources';
% id_total.index = 1; id_total.name = 'total';
% ids_core_sources.source{} = source_template;
% ids_core_sources.source{}.identifier = id_total;
% ids_core_sources.source{}.profiles_1d(1:n_t) = {profiles_template};
% ids_core_sources.source{}.global_quantities(1:n_t) = {globals_template};

%% add descriptions for profiles_1d

desc.source = ...
  'available by now {source_index}: ohmic {1}, bootstrap {2}, ec {3+} for individual launchers (if present in shot), nbi {3+|14+} if available, dnbi {3|14|16} if available';
desc.profiles_1d.electrons.energy = 'absorbed power density';
desc.profiles_1d.electrons.power_inside = 'integrated absorbed power density';
desc.profiles_1d.j_parallel = 'parallel current density';
desc.profiles_1d.current_parallel_inside = 'integrated parallel current density';
desc.globals.power = 'total power coupled to the plasma';
desc.globals.current_parallel = 'total parallel current driven';
ids_core_sources_description = desc;

%%
if nargin <= 2
  ids_core_sources.code.name = ['tcv_get_ids_core_sources, within gdat, with shot= ' num2str(params_core_sources.shot) ];
else
  ids_core_sources.code.name = ['tcv_get_ids_core_sources, within gdat, with shot= ' num2str(params_core_sources.shot) '; varargin: ' varargin{:}];
end
ids_core_sources_description.code.name = ids_core_sources.code.name;

ids_core_sources.code.output_flag = zeros(size(ids_core_sources.time));

% cocos automatic transform
if ~isempty(which('ids_generic_cocos_nodes_transformation_symbolic'))
  [ids_core_sources,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_core_sources,'core_sources',gdat_params.cocos_in, ...
    gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
    gdat_params.error_bar,gdat_params.nverbose);

end
