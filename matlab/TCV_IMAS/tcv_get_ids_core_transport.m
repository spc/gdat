function [ids_core_transport,ids_core_transport_description,varargout] = ...
  tcv_get_ids_core_transport(shot,ids_equil_empty,gdat_params,varargin)
%
% [ids_core_transport,ids_core_transport_description,varargout] = ...
%     tcv_get_ids_core_transport(shot,ids_equil_empty,gdat_params,varargin);
%
%
% gdat_params: gdat_data.gdat_params to get all params passed from original call
%

machine = 'tcv';
tens_time = -1;
tens_rho = -0.1;

if exist('gdat_params','var')
  [ids_core_transport, params_core_transport] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_profiles','gdat_params',gdat_params,varargin{:});
else
  [ids_core_transport, params_core_transport] = ...
    tcv_ids_headpart(shot,ids_equil_empty,'cores_profiles',varargin{:});
  aa=gdat_tcv;
  gdat_params = aa.gdat_params; % to get default params
end
params_eff_ref = gdat_params; params_eff_ref.doplot=0;
try params_eff_ref=rmfield(params_eff_ref,'source');catch;end % make sure no source (from ids def)

% initialize description
ids_core_transport_description = [];

%%
last_index = 0;

% fill model [name: transport solver, desc: output from transport solver]
% setup model and profiles_1d
comment = 'Output from a transport solver';
ids_core_transport.model{last_index+1}.comment = comment;
ids_core_transport.model{last_index+1}.name = comment;
ids_core_transport.model{last_index+1}.identifier.index = 2;
ids_core_transport.model{last_index+1}.name = 'transport_solver';

% read data and setup time
params_eff.data_request='\results::conf:chie';
temp_1d.chie = gdat(params_core_transport.shot,params_eff.data_request);
temp_1d_desc.chie = params_eff.data_request;

if isempty(temp_1d.chie.t)
  warning('no data in chie node, might need to rerun anaprofs')
  return
end

ids_core_transport.time = temp_1d.chie.t;
ids_core_transport_description.time = ['from node' params_eff.data_request];

ids_core_transport.model{last_index+1}.profiles_1d(1:length(ids_core_transport.time)) = ...
  ids_core_transport.model{last_index+1}.profiles_1d(1);

% fill profiles_1d
for it=1:length(ids_core_transport.time)
  ids_core_transport.model{last_index+1}.profiles_1d{it}.time = ids_core_transport.time(it);
  ids_core_transport.model{last_index+1}.profiles_1d{it}.electrons.energy.d = temp_1d.chie.data(:,it);
  temp_1d_desc.electrons.energy.d = temp_1d.chie.label;
end

%% add descriptions for profiles_1d
ids_core_transport_description.profiles_1d = temp_1d_desc;

%%
if nargin <= 2
  ids_core_transport.code.name = ['tcv_get_ids_core_transport, within gdat, with shot= ' num2str(params_core_transport.shot) ];
else
  ids_core_transport.code.name = ['tcv_get_ids_core_transport, within gdat, with shot= ' num2str(params_core_transport.shot) '; varargin: ' varargin{:}];
end
ids_core_transport_description.code.name = ids_core_transport.code.name;

ids_core_transport.code.output_flag = zeros(size(ids_core_transport.time));

% cocos automatic transform
if ~isempty(which('ids_generic_cocos_nodes_transformation_symbolic'))
  [ids_core_transport,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_core_transport,'core_transport',gdat_params.cocos_in, ...
          gdat_params.cocos_out,gdat_params.ipsign_out,gdat_params.b0sign_out,gdat_params.ipsign_in,gdat_params.b0sign_in, ...
          gdat_params.error_bar,gdat_params.nverbose);

end
