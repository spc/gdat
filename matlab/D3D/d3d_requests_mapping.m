function mapping = d3d_requests_mapping(data_request)
%
% Information pre-defined for gdat_d3d, you can add cases here to match official cases in gdat_d3d, allowing backward compatibility
%
% Defaults
mapping = struct(...
  'label', '', ...
  'method', '', ...
  'expression','', ...
  'timedim', -1, ...     % dim which is the time is the database, to copy in .t, the other dims are in .x (-1 means last dimension)
  'gdat_timedim',[], ...  % if need to reshape data and dim orders to have timedim as gdat_timedim (shifting time to gdat_timedim)
  'min', -inf, ...
  'max', inf);
% Note that gdat_timedim is set to timedim at end of this function if empty
% gdat_timedim should have effective index of the time dimension in gdat

if ~exist('data_request') || isempty(data_request)
  return
end

% default label: data_request keyword itself
mapping.label = data_request;

% for D3D, following choices are set so far:
% method = 'signal' then expression contains the shotfile, diagnostic and if needed the experiment
%                expression is a cell array
% method = 'expression', then expression is the expression to return gdat_tmp...
% method = 'switchcase', then there will be a specific case within gdat_d3d (usual case when not directly a signal)
%
% label is used for plotting
if iscell(data_request) % || (~ischar(data_request) && length(data_request)>1)
  mapping.label = data_request;
  mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
  if length(data_request)==1 && strcmp(data_request{1}(1),'\')
    mapping.expression = {'d3d',data_request{1}};
  else
    mapping.expression = data_request;
  end
  mapping.gdat_timedim = mapping.timedim;
  return
end

switch lower(data_request)
  case 'a_minor'
    mapping.timedim = 1;
    mapping.label = 'a\_minor';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\aminor'}];
  case 'b0'
    mapping.timedim = 1;
    mapping.label = 'B_0';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\bcentr'}];
  case 'beta'
    mapping.timedim = 1;
    mapping.label = '\beta';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\betat'}];
  case 'betan'
    mapping.timedim = 1;
    mapping.label = '\beta_N';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\betan'}];
  case 'betap'
    mapping.timedim = 1;
    mapping.label = '\beta_p';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\betap'}];
  case {'bolo', 'bolom'}
    mapping.timedim = 1;
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
  case 'delta'
    mapping.timedim = 1;
    mapping.label = 'delta';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''delta_bottom''; ' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);params_eff.data_request=''delta_top'';' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);gdat_tmp.data = 0.5.*(gdat_tmp.data+gdat_tmp2.data);'];
  case 'delta_top'
    mapping.label = 'delta\_top';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\tritop'}];
  case 'delta_bottom'
    mapping.label = 'delta\_bottom';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\tribot'}];
  case 'den_target'
    mapping.timedim = 1;
    mapping.label = {'density_ref','density'};
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''\dstdenp'';' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);params_eff.data_request=''\dssdenest'';' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.data(:,2) = gdat_tmp2.data;' ...
                    'gdat_tmp.dim{2}=[1 2];gdat_tmp.dimunits{1}=''s'';gdat_tmp.dimunits{2}={''dstdenp'',''dssdenest''};' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case {'ece', 'ece_rho'}
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'eqdsk'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'equil'
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
    mapping.expression = '';
  case {'gas','gasa'}
    mapping.timedim = 1;
    mapping.label = 'gasa';
    mapping.method = 'signal';
    mapping.expression = [{'D3D'},{'\gasa'}];
  case 'halpha'
    mapping.timedim = 1;
    mapping.label = 'Halpha';
    mapping.method = 'signal';
    mapping.expression = [{'SPECTROSCOPY'},{'\fs04'}];
  case {'h_scalings', 'h98y2'}
    mapping.label = 'H_{98y2}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'results.confinement.times.scalings:h_thh98y2'}];
    mapping.expression = [{'D3D'},{'\h_thh98y2'}];
  case 'ioh'
    mapping.timedim = 1;
    mapping.label = 'I ohmic transformer';
    mapping.method = 'signal';
    mapping.expression = [{'MBI'},{'IOH'}];
  case 'ip'
    mapping.timedim = 1;
    mapping.label = 'Plasma current';
    mapping.method = 'signal';
    mapping.expression = [{'efit01'},{'\cpasma'}];
  case 'kappa'
    mapping.timedim = 1;
    mapping.label = '\kappa';
    mapping.method = 'signal';
    mapping.expression = [{'efit01'},{'\kappa'}];
% $$$  case 'kappa_top'
% $$$   mapping.timedim = 1;
% $$$   mapping.label = '\kappa^{top}';
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'EFIT01'},{'kappa'}];
% $$$  case 'kappa_bottom'
% $$$   mapping.timedim = 1;
% $$$   mapping.label = '\kappa_{bottom}';
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'FPG'},{'kuntn'}];
  case 'li'
    mapping.timedim = 1;
    mapping.label = 'l_i';
    mapping.method = 'signal';
    mapping.expression = [{'efit01'},{'\li3'}];
  case 'mhd'
    mapping.timedim = 1;
    mapping.label = 'Odd and Even n';
    mapping.method = 'expression';
    table_mhd_from_Strait = { ...
      'MPI66M020D',   19.5   , 'MPI66M200D' ,  199.7,         180.2; ...
      'MPI66M067D',   67.5   , 'MPI66M247D'  , 246.4,         178.9; ...
      'MPI66M097D',   97.4,    'MPI66M277D',   277.5,         180.1; ...
      'MPI66M127D',   127.9,   'MPI66M307D',   307.0,          179.1; ...
      'MPI66M132D',   132.5,   'MPI66M312D',   312.4,          179.9; ...
      'MPI66M137D',   137.4,   'MPI66M322D',   317.4,          180.0; ...
      'MPI66M157D',   157.6,  'MPI66M340D',   339.7,          182.2};

    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''MOD'',''OddN''}; ' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);gdat_tmp.data=reshape(gdat_tmp.data,length(gdat_tmp.data),1 );' ...
                    'gdat_tmp.dim{1}=gdat_tmp.t;gdat_tmp.dim{2}=[1 2];gdat_tmp.x=gdat_tmp.dim{2};' ...
                    'gdat_tmp.n_odd.data = gdat_tmp.data;gdat_tmp.n_odd.data_request=params_eff.data_request;' ...
                    'params_eff.data_request={''MOD'',''EvenN''};' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);gdat_tmp.data(:,2)=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_even.data = gdat_tmp2.data;gdat_tmp.n_even.data_request=params_eff.data_request;gdat_tmp.label={''n\_odd'',''n\_even''};' ...
                    'params_eff.data_request={''MOD'',''OddNAmp''};' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);gdat_tmp.n_odd.amp=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_odd.amp_t=gdat_tmp2.t;' ...
                    'params_eff.data_request={''MOD'',''EvenNAmp''};' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);gdat_tmp.n_even.amp=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_even.amp_t=gdat_tmp2.t;' ...
                    'gdat_tmp.full_path=''MOD/Odd in data and .n_odd; .n_even'';' ...
                    'gdat_tmp.gdat_request=''mhd'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;'];
  case 'nbi'
    mapping.timedim = 1;
    mapping.label = 'Pnbi [W]';
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'nb'},{'pinj'}]; % in kW, use expression to have it in W
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''nb''},{''pinj''}];' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);gdat_tmp.units=''W'';' ...
                    'gdat_tmp.data = 1e3.*gdat_tmp.data;gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';' ...
                    'gdat_tmp.data_smooth=interpos(gdat_tmp.t,gdat_tmp.data,-3e7);' ...
                    'gdat_tmp.data_smooth=max(0.,gdat_tmp.data_smooth);'];
  case {'neint'}
    mapping.timedim = 1;
    mapping.label = 'line integrated el. density';
    mapping.method = 'signal';
    mapping.expression = {'d3d','\d3d::top.electrons.bci:denr0'};
  case {'neints'}
    mapping.timedim = 1;
    mapping.label = {'denr0','denv1','denv2','denv3'};
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''\d3d::top.electrons.bci:denr0'';' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);gdat_tmp.denv1=gdat_tmp;' ...
                    'params_eff.data_request=''\d3d::top.electrons.bci:denv1'';gdat_denv1=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.denv1=gdat_denv1;gdat_tmp.data(:,2)=gdat_denv1.data;' ...
                    'params_eff.data_request=''\d3d::top.electrons.bci:denv2'';gdat_denv2=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.denv2=gdat_denv2;gdat_tmp.data(:,3)=gdat_denv2.data;' ...
                    'params_eff.data_request=''\d3d::top.electrons.bci:denv3'';gdat_denv3=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.denv3=gdat_denv3;gdat_tmp.data(:,4)=gdat_denv3.data;' ...
                    'gdat_tmp.dim{2} = [1:4];gdat_tmp.dimunits{2} = {''denr0'',''denv1'',''denv2'',''denv3''};' ...
                    ';gdat_tmp.dimunits{1}=''s'';'];
  case {'neint_ts'}
    % assumes ne_rho in source input
    mapping.timedim = 1;
    mapping.label = 'neint from TS';
    mapping.method = 'switchcase';
  case 'nel'
    mapping.timedim = 1;
    mapping.label = 'NEBAR\_R0';
    mapping.method = 'signal';
    mapping.expression = [{'efit01'},{'\NEBAR_R0'}];
  case 'nel_v1'
    mapping.timedim = 1;
    mapping.label = 'NEBAR\_V1';
    mapping.method = 'signal';
    mapping.expression = [{'efit01'},{'\NEBAR_V1'}];
  case {'ne', 'ne_rho'}
    mapping.timedim = 2;
    mapping.label = 'ne';
    mapping.method = 'switchcase';
  case 'nete_rho'
    mapping.timedim = 2;
    mapping.label = 'ne and Te';
    mapping.method = 'switchcase';
  case 'ni'
    mapping.method = 'switchcase'; % especially since might have option fit, etc
  case {'ng','ngreenwald','n_greenwald'}
    mapping.timedim = 1;
    mapping.label = 'nG=Ip[MA]/(\pi a^2)*1e20 on nel times';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);params_eff.data_request=''ip'';' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'params_eff.data_request=''a_minor'';' ...
                    'gdat_tmp3=gdat_d3d(shot,params_eff);ij=find(gdat_tmp3.data==0);gdat_tmp3.data(ij)=NaN;' ...
                    'tmp_data3=interp1(gdat_tmp3.t,gdat_tmp3.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = abs(tmp_data2*1e-6./pi./(tmp_data3.^2+1e-5));' ...
                    'ij=find(gdat_tmp.data<0 | gdat_tmp.data>4);gdat_tmp.data(ij)=NaN;gdat_tmp.data = gdat_tmp.data * 1e20;'];
  case {'ngf','greenwald_fraction','f_greenwald','ng_fraction'}
    mapping.timedim = 1;
    mapping.label = 'Greenwald\_fraction=n\_el/n\_G';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);params_eff.data_request=''n_greenwald'';' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'if numel(gdat_tmp2.t)>1;tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'else;tmp_data2=gdat_tmp2.data;end;' ...
                    'gdat_tmp.ng=gdat_tmp2;gdat_tmp.ng.data_t_nel=tmp_data2;' ...
                    'gdat_tmp.data = gdat_tmp.data*1e6./(tmp_data2+1e-5);' ...
                    'gdat_tmp.units='''';gdat_tmp.dimunits{1}=''s'';'];
  case {'f2b', 'pcf2b'}
    mapping.label = {'pcf2b','f2b'}';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''d3d''},{''ptdata("pcf2b")''}];' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);params_eff = gdat_data.gdat_params;' ...
                    'params_eff.data_request=[{''d3d''},{''ptdata("f2b")''}];gdat_tmp2=gdat_d3d(shot,params_eff);' ...
                    'if ~isempty(gdat_tmp2.t); gdat_tmp.data(:,2) = interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t);else;' ...
                    'gdat_tmp.data(:,2) = NaN(size(gdat_tmp.t)); end;' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'pgyro'
    mapping.timedim = 1;
    mapping.label = 'EC gyros';
    mapping.method = 'switchcase';
  case 'powers'
    mapping.timedim = 1;
    mapping.label = 'various powers';
    mapping.method = 'switchcase';
  case {'prad','prad_tot','rad','rad_tot'}
    mapping.timedim = 1;
    mapping.label = {'PRAD_divl','PRAD_divu','prad_core','tot-divL-divU','PRAD_TOT'};
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''bolom'',''PRAD_01.PRAD:PRAD_divl''};' ...
                    'gdat_tmp=gdat_d3d(shot,params_eff);gdat_tmp.divl=gdat_tmp;' ...
                    'params_eff.data_request{2}=''PRAD_01.PRAD:PRAD_divu'';gdat_tmpU=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.divu=gdat_tmpU;gdat_tmp.data(:,2)=gdat_tmpU.data;' ...
                    'params_eff.data_request{2}=''\PRAD_CORE'';gdat_tmpcore=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.core=gdat_tmpcore;gdat_tmp.data(:,3)=interp1(gdat_tmpcore.t,gdat_tmpcore.data,gdat_tmp.t);' ...
                    'params_eff.data_request{2}=''PRAD_01.PRAD:PRAD_tot'';gdat_tmptot=gdat_d3d(shot,params_eff);' ...
                    'gdat_tmp.tot=gdat_tmptot;gdat_tmp.data(:,5)=gdat_tmptot.data;' ...
                    'gdat_tmp.data(:,4) = gdat_tmp.data(:,5)-gdat_tmp.data(:,1)-gdat_tmp.data(:,2);' ...
                    'gdat_tmp.dim{2} = [1:5];gdat_tmp.dimunits{2} = {''divl'',''divu'',''core'',''tot-divl-divu'',''prad_tot''};' ...
                    'gdat_tmp.units=''W'';gdat_tmp.dimunits{1}=''s'';'];
  case 'psi_axis'
    mapping.timedim = 1;
    mapping.label ='psi_\axis' ;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\ssimag'}];
  case 'psi_edge'
    mapping.timedim = 1;
    mapping.label = 'psi\_edge';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\ssibry'}];
  case 'q0'
    mapping.timedim = 1;
    mapping.label = 'q_0';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\q0'}];
  case 'qmin'
    mapping.timedim = 1;
    mapping.label = 'q_min';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\qmin'}];
  case 'q95'
    mapping.timedim = 1;
    mapping.label = 'q_{95}';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\q95'}];
  case 'q_edge'
    mapping.timedim = 1;
    mapping.label = 'q_{edge}}';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\ql'}];
  case 'q_rho'
    mapping.timedim = 2;
    mapping.gdat_timedim = 2;
    mapping.label = 'q';
    mapping.method = 'switchcase';
  case {'rot', 'cerqrot'}
    mapping.timedim = 2;
    mapping.label = 'cerqrot';
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'r0'
    mapping.timedim = 1;
    mapping.label = 'R_0';
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\rzero'}];
  case 'rgeom'
    mapping.label = 'Rgeom';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''EFIT01''},{''results.geqdsk:rbbbs''}];' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);params_eff = gdat_data.gdat_params;' ...
                    'params_eff.data_request=[{''EFIT01''},{''\zmaxis''}];gdat_tmp=gdat_d3d(shot,params_eff);' ...
                    'ij=find(gdat_tmp2.data==0);if (~isempty(ij));gdat_tmp2.data(ij)=NaN;end;' ...
                    'gdat_tmp.data = 0.5.*(nanmax(gdat_tmp2.data'',[],2)+nanmin(gdat_tmp2.data'',[],2));gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'r_inboard'
    mapping.label = 'R\_inboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Rin'}];
  case 'r_outboard'
    mapping.label = 'R\_outboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Raus'},{'D3DD'}];
  case 'rhotor'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor';
  case 'rhotor_edge'
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_edge';
  case 'rhotor_norm'
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_norm';
  case 'rhovol'
    mapping.timedim = 2;
    mapping.label = 'rhovol\_norm';
    mapping.method = 'switchcase';
  case {'rmag', 'r_mag', 'r_axis'}
    mapping.label = 'R\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\rmaxis'}];
  case 'sxr'
    mapping.timedim = 1;
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
  case 'tau_tot'
    mapping.label = '\tau_{tot}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'TOT'},{'tau_tot'},{'D3DD'}];
  case {'te', 'te_rho'}
    mapping.timedim = 2;
    mapping.label = 'Te';
    mapping.method = 'switchcase';
  case 'ti'
    mapping.label = 'Ti';
    mapping.method = 'switchcase';
  case 'vloop'
    mapping.label = 'Vloop';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\vloopmhd'}];
  case 'volume'
    mapping.label = 'Volume';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\volume'}];
  case 'volume_rho'
    mapping.timedim = 2;
    mapping.label = 'volume\_norm';
    mapping.method = 'switchcase';
  case 'wmhd'
    mapping.label = 'Wmhd';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\Wmhd'}];
  case 'zeff'
    mapping.label = 'zeff from cxrs';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'ZES'},{'Zeff'},{'D3DD'}];
  case 'zeff_cerq'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
  case 'zgeom'
    mapping.label = 'Zgeom';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''EFIT01''},{''results.geqdsk:zbbbs''}];' ...
                    'gdat_tmp2=gdat_d3d(shot,params_eff);params_eff = gdat_data.gdat_params;' ...
                    'params_eff.data_request=[{''EFIT01''},{''\zmaxis''}];gdat_tmp=gdat_d3d(shot,params_eff);' ...
                    'ij=find(gdat_tmp2.data==0);if (~isempty(ij));gdat_tmp2.data(ij)=NaN;end;' ...
                    'gdat_tmp.data = 0.5.*(nanmax(gdat_tmp2.data'',[],2)+nanmin(gdat_tmp2.data'',[],2));gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'zmag'
    mapping.label = 'Z\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\zmaxis'}];
    %
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % extra D3D cases (not necessarily in official data_request name list)
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
  case {'zxpt1', 'xpt'}
    mapping.label = 'zxpt1';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'EFIT01'},{'\zxpt1'}];
  case 'transp'
    mapping.label = 'transp output';
    mapping.method = 'switchcase';


  otherwise
    mapping.label = data_request;
    mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
    % pseudo point name: '\ptdata' given, or {'\ptdata'} add D3D as tree
    if ischar(data_request) && strcmp(data_request(1),'\')
      mapping.expression = {'d3d',data_request};
    else
      mapping.expression = data_request;
    end

end

if isempty(mapping.gdat_timedim)
  mapping.gdat_timedim = mapping.timedim;
end
