function [eqdskd3d, equil_all_t, equil_t_index]=get_eqdsk_d3d(shot,time,zshift,varargin);
%
% called by:
%
% eqdsk = gdat(shot/equil,'eqdsk','time',time[,'source','efit02',...]);
%
% [eqdskd3d, equil_all_t, equil_t_index]=geteqdskd3d(shot,time,zshift,varargin);
%
% if shot is a structure assumes obtained from gdat(shot,'equil',...);
%
% varargin{1:2}: 'source','efit03' (default) or 'source','efit01' (case insensitive)
%
% [eqdskd3d, equil_all_t, equil_t_index]=geteqdskd3d(shot,time);
% [eqdskd3d, equil_all_t, equil_t_index]=geteqdskd3d(shot,time,[],'source','efit02');
%
% you can then do:
%     write_eqdsk('fname',eqdskd3d,2); % EFIT is COCOS=2 by default (except abs(q) is set, so now q has a sign)
%     write_eqdsk('fname',eqdskd3d,[2 11]); % if you want an ITER version with COCOS=11
%

if ~exist('shot') || isempty(shot); 
  disp('requires a shot or equil structure from gdat(shot,''equil'',...) in geteqdskd3d')
  return
end

if ~exist('time'); 
  time_eff = 2.0*1e3;
else
  time_eff = time;
end

if ~exist('zshift') || isempty(zshift) || ~isnumeric(zshift)
  zshift_eff = 0.; % no shift
else
  zshift_eff = zshift;
end

if nargin >= 5 && ~isempty(varargin{1})
  if ~isempty(varargin{2})
    equil_source = varargin{2};
  else
    disp(['Warning source for equil in get_eqdsk_d3d not defined']);
    return;
  end
else
  equil_source = 'efit03';
end

if isnumeric(shot)
  % check read_mds_eq_func is available
  equil_in = gdat(shot,'equil','equil',equil_source);
else
  equil_in = shot;
  shot = equil_in.shot;
end

equil_all_t = equil_in.equil_all_t;
equil = equil_all_t;
[zz it]=min(abs(equil.time-time_eff));

equil_t_index = it;

eqdsk.cocos=2;
eqdsk.nr = equil.gdata(it).nw;
eqdsk.nz = equil.gdata(it).nh;
eqdsk.rboxlen = equil.gdata(it).xdim ;
eqdsk.rboxleft = equil.gdata(it).rgrid1;
eqdsk.zboxlen = equil.gdata(it).zdim ;
eqdsk.zmid = equil.gdata(it).zmid ;
eqdsk.rmesh = linspace(eqdsk.rboxleft,eqdsk.rboxleft+eqdsk.rboxlen,eqdsk.nr)';
eqdsk.zmesh = linspace(eqdsk.zmid-eqdsk.zboxlen/2,eqdsk.zmid+eqdsk.zboxlen/2,eqdsk.nz)' - zshift_eff;
eqdsk.zshift = zshift_eff;
eqdsk.psi = equil.gdata(it).psirz;
eqdsk.psirz = reshape(eqdsk.psi,prod(size(eqdsk.psi)),1);
eqdsk.psiaxis = equil.gdata(it).ssimag;
eqdsk.psiedge = equil.gdata(it).ssibry;
% need psimesh ot be equidistant and same nb of points as nr
eqdsk.psimesh=linspace(0,1,eqdsk.nr)';
eqdsk.rhopsi = sqrt(eqdsk.psimesh);
% psimesh assumed from psi_axis on axis  to have correct d/dpsi 
eqdsk.psimesh = (eqdsk.psiedge-eqdsk.psiaxis) .* eqdsk.psimesh + eqdsk.psiaxis;
eqdsk.p = equil.gdata(it).pres;
eqdsk.pprime = - equil.gdata(it).pprime; % pprime has wrong sign
eqdsk.FFprime = - equil.gdata(it).ffprim; % ffprime has wrong sign
eqdsk.F = equil.gdata(it).fpol;
eqdsk.q = equil.gdata(it).qpsi.*sign(equil.gdata(it).bzero).*sign(equil.gdata(it).cpasma); % EFIT uses always abs(q) so recover sign

eqdsk.b0 = equil.gdata(it).bzero;
eqdsk.r0 = equil.gdata(it).rzero;
eqdsk.ip = equil.gdata(it).cpasma;

eqdsk.raxis = equil.gdata(it).rmaxis;
eqdsk.zaxis = equil.gdata(it).zmaxis - eqdsk.zshift;
eqdsk.nbbound = equil.gdata(it).nbbbs;
eqdsk.rplas = equil.gdata(it).rbbbs(1:equil.gdata(it).nbbbs);
eqdsk.zplas = equil.gdata(it).zbbbs(1:equil.gdata(it).nbbbs);

eqdsk.nblim = equil.gdata(it).limitr;
eqdsk.rlim = equil.gdata(it).xlim(1:equil.gdata(it).limitr);
eqdsk.zlim = equil.gdata(it).ylim(1:equil.gdata(it).limitr);

eqdsk.stitle=['#' num2str(shot) ' t=' num2str(time_eff) ' from ' equil_in.gdat_params.exp_name '/' equil_in.gdat_params.equil];
eqdsk.ind1=3;

psisign = sign(eqdsk.psimesh(end)-eqdsk.psimesh(1));
[dum1,dum2,dum3,F2_05]=interpos(psisign.*eqdsk.psimesh,eqdsk.FFprime,-0.1);
fedge=eqdsk.r0.*eqdsk.b0;
F2 = psisign.*2.*F2_05 + fedge.^2;
if abs(eqdsk.F(end)-fedge)./abs(fedge)>1e-6
  disp('problem with cocos?')
  disp(['eqdsk.F(end) = ' num2str(eqdsk.F)])
  disp(['r0*b0 = ' num2str(eqdsk.r0) '*' num2str(eqdsk.b0) ' = ' num2str(fedge)]);
end

% get plasma boundary
figure
contour(eqdsk.rmesh,eqdsk.zmesh,eqdsk.psi',100)
hold all
plot(eqdsk.rplas,eqdsk.zplas)
psiedge_eff = 0.001*eqdsk.psiaxis + 0.999*eqdsk.psiedge;
[hh1 hh2]=contour(eqdsk.rmesh,eqdsk.zmesh,eqdsk.psi',[psiedge_eff psiedge_eff],'k--');
axis equal

plot(eqdsk.rlim,eqdsk.zlim,'k-')

title(eqdsk.stitle)

eqdskd3d = eqdsk;
