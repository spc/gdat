function [gdat_data,error_status] = get_signal_d3d(signal,gdat_data);

if strcmp(lower(gdat_data.gdat_params.tree),'d3d') && strcmp(signal(1),'\')
  % try also ptdata
  gdat_data.data = mdsvalue(['_sOS=' signal]);
  if ischar(gdat_data.data) % could check if "not found" present, but not all cases
    gdat_data.data = mdsvalue(['_sOS=ptdata(''' signal ''',' num2str(gdat_data.shot) ')']);
  end
else
  gdat_data.data = mdsvalue(['_sOS=' signal]);
end
error_status = 1;

gdat_data.dim = [];
gdat_data.dimunits = [];
gdat_data.t = [];
gdat_data.x = [];
nbdims=numel(setdiff(size(gdat_data.data),1));
if ~isempty(gdat_data.data) && ~ischar(gdat_data.data)
  gdat_data.units = mdsvalue(['units_of(' '_sOS' ')']);
  if prod(size(gdat_data.data)) > 1
    for i=1:nbdims
      gdat_data.dim{i} = mdsvalue(['dim_of(' '_sOS' ',' num2str(i-1) ')']);
      % gdat_data.dimunits{i} = mdsvalue(['dimunits_of(' '_sOS' ',' num2str(i-1) ')']);
      gdat_data.dimunits{i} = 'to get';
    end
    if nbdims==1
      gdat_data.t = gdat_data.dim{1};
      gdat_data.x = [];
    else
      gdat_data.t = gdat_data.dim{1};
      gdat_data.x = gdat_data.dim{2};
    end
    error_status = 0;
  end
end

