function [gdat_data,gdat_params,error_status,varargout] = gdat_d3d(shot,data_request,varargin)
%
% function [gdat_data,gdat_params,error_status,varargout] = gdat(shot,data_request,varargin)
%
% Aim: get data from a given machine using full path or keywords.
%      data_request are and should be case independent (transformed in lower case in the function and outputs)
%
% If no inputs are provided, return the list of available pre-defined data_request in gdat_data and default parameters gdat_params
%
% Inputs:
%
%    no inputs: return default parameters in a structure form in gdat_params
%    shot: shot number
%    data_request: keyword (like 'ip') or trace name or structure containing all parameters but shot number
%    varargin{i},varargin{i+1},i=1:nargin-2: additional optional parameters given in pairs: param_name, param_value
%                                            The optional parameters list might depend on the data_request
%              examples of optional parameters:
%                               'plot',1 (plot is set by default to 0)
%                               'machine','D3D' (the default machine is the local machine)
%
%
% Outputs:
%
% gdat_data: structure containing the data, the time trace if known and other useful information
% gdat_data.t : time trace
% gdat_data.data: requested data values
% gdat_data.dim : values of the various coordinates related to the dimensions of .data(:,:,...)
%                     note that one of the dim is the time, replicated in .t for clarity
% gdat_data.dimunits : units of the various dimensions, 'dimensionless' if dimensionless
% gdat_data.error_bar : if provided
% gdat_data.gdat_call : list of parameters provided in the gdat call (so can be reproduced)
% gdat_data.shot: shot number
% gdat_data.machine: machine providing the data
% gdat_data.gdat_request: keyword for gdat if relevant
% gdat_data.data_fullpath: full path to the data node if known and relevant, or relevant expression called if relevant
% gdat_data.gdat_params: copy gdat_params for completeness (gdat_params contains a .help structure detailing each parameter)
% gdat_data.xxx: any other relevant information
%
% gdat_params contains the options relevant for the called data_request. It also contains a help structure for each option
% eg.: param1 in gdat_params.param1 and help in gdat_params.help.param1
%
% Examples:
% (should add working examples for various machines (provides working shot numbers for each machine...))
%
%    [a1,a2]=gdat;
%    a2.data_request = 'Ip';
%    a3=gdat(32827,a2);  % gives input parameters as a structure, allows to call the same for many shots
%    a4=gdat('opt1',123,'opt2',[1 2 3],'shot',48832,'data_request','Ip','opt3','aAdB'); % all in pairs
%    a5=gdat(32827,'ip'); % standard call
%    a6=gdat(32827,'ip','Opt1',123,'Doplot',1,'opt2','Abc'); % standard call with a few options (note all lowercase in output)
%
% From local: mdsconnect('atlas.gat.com'); mdsvalue('1+3')
% From remote:
% ssh -p 2039 -C -l sautero -L 8002:atlas.gat.com:8000 cybele.gat.com
% And in another session in matlab:
% mdsconnect('localhost:8002'); mdsvalue('1+3') % to test connection to server
% sh=mdsopen('efit01',193400); (instead of no connect and sh=mdsopen('atlas.gat.com::efit01',193400); from iris
%

%
% Comments for local developer:
% This gdat is just a "header routine" calling the gdat for the specific machine gdat_`machine`.m which can be called
% directly, thus which should be able to treat the same type of input arguments
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare some variables, etc

varargout{1}=cell(1,1);
error_status=1;

% construct main default parameters structure
gdat_params.data_request = '';
default_machine = 'd3d';

gdat_params.machine=default_machine;
gdat_params.doplot = 0;
gdat_params.exp_name = 'D3D';
gdat_params.nverbose = 1;

% construct list of keywords from global set of keywords and specific D3D set
% get data_request names from centralized table
%%% data_request_names = get_data_request_names; % do not use xlsx anymore but scan respective machine_requests_mapping.m files
data_request_names = get_data_request_names_from_gdat_xxx(default_machine);
% add D3D specific to all:
if ~isempty(data_request_names.d3d)
  d3d_names = fieldnames(data_request_names.d3d);
  for i=1:length(d3d_names)
    data_request_names.all.(d3d_names{i}) = data_request_names.d3d.(d3d_names{i});
  end
end
data_request_names_all = sort(fieldnames(data_request_names.all));

% construct default output structure
gdat_data.data = [];
gdat_data.units = [];
gdat_data.dim = [];
gdat_data.dimunits = [];
gdat_data.t = [];
gdat_data.x = [];
gdat_data.shot = [];
gdat_data.gdat_request = [];
gdat_data.gdat_params = gdat_params;
gdat_data.data_fullpath = [];


% Treat inputs:
ivarargin_first_char = 3;
data_request_eff = '';
if nargin>=2 && ischar(data_request); data_request = lower(data_request); end

gdat_data.gdat_request = data_request_names_all; % so if return early gives list of possible request names
gdat_data.gdat_params.help = d3d_help_parameters(fieldnames(gdat_data.gdat_params));
gdat_params = gdat_data.gdat_params;

% no inputs
if nargin==0
  % return defaults and list of keywords
  return
end

do_mdsopen_mdsclose = 1;
% treat 1st arg
if nargin>=1
  if isempty(shot)
    % means mdsopen(shot) already performed
    shot=-999; % means not defined
    do_mdsopen_mdsclose = 0;
  elseif isnumeric(shot)
    gdat_data.shot = shot;
  elseif ischar(shot)
    ivarargin_first_char = 1;
  else
    if gdat_params.nverbose>=1; warning('type of 1st argument unexpected, should be numeric or char'); end
    error_status=2;
    return
  end
  if nargin==1
    % Only shot number given. If there is a default data_request set it and continue, otherwise return
    return
  end
end
% 2nd input argument if not part of pairs
if nargin>=2 && ivarargin_first_char~=1
  if isempty(data_request)
    return
  end
  % 2nd arg can be a structure with all options except shot_number, or a string for the pathname or keyword, or the start of pairs string/value for the parameters
  if isstruct(data_request)
    if ~isfield(data_request,'data_request')
      if gdat_params.nverbose>=1; warning('expects field data_request in input parameters structure'); end
      error_status=3;
      return
    end
    %data_request.data_request = lower(data_request.data_request);
    data_request_eff = data_request.data_request;
    gdat_params = data_request;
  else
    % since data_request is char check from nb of args if it is data_request or a start of pairs
    if mod(nargin-1,2)==0
      ivarargin_first_char = 2;
    else
      ivarargin_first_char = 3;
      data_request_eff = data_request;
    end
  end
end
if ~isstruct(data_request)
  gdat_params.data_request = data_request_eff;
end

% if start pairs from shot or data_request, shift varargin
if ivarargin_first_char==1
  varargin_eff{1} = shot;
  varargin_eff{2} = data_request;
  varargin_eff(3:nargin) = varargin(:);
elseif ivarargin_first_char==2
  varargin_eff{1} = data_request;
  varargin_eff(2:nargin-1) = varargin(:);
else
  varargin_eff(1:nargin-2) = varargin(:);
end

% extract parameters from pairs of varargin:
if (nargin>=ivarargin_first_char)
  if mod(nargin-ivarargin_first_char+1,2)==0
    for i=1:2:nargin-ivarargin_first_char+1
      if ischar(varargin_eff{i})
        % enforce lower case for any character driven input
        if ischar(varargin_eff{i+1})
          gdat_params.(lower(varargin_eff{i})) = lower(varargin_eff{i+1});
        else
          gdat_params.(lower(varargin_eff{i})) = varargin_eff{i+1};
        end
      else
        if gdat_params.nverbose>=1; warning(['input argument nb: ' num2str(i) ' is incorrect, expects a character string']); end
        error_status=401;
        return
      end
    end
  else
    if gdat_params.nverbose>=1; warning('number of input arguments incorrect, cannot make pairs of parameters'); end
    error_status=402;
    return
  end
end
data_request_eff = gdat_params.data_request; % in case was defined in pairs
% default equil:
if ~isfield(gdat_params,'equil'); gdat_params.equil = 'EFIT01'; end

% if it is a request_keyword can obtain description:
if ischar(data_request_eff) || length(data_request_eff)==1
  ij=strmatch(data_request_eff,data_request_names_all,'exact');
else
  ij=[];
end

if ~isempty(ij);
  gdat_data.gdat_request = data_request_names_all{ij};
  if isfield(data_request_names.all.(data_request_names_all{ij}),'description') && ~isempty(data_request_names.all.(data_request_names_all{ij}).description)
    % copy description of keyword
    gdat_data.request_description = data_request_names.all.(data_request_names_all{ij}).description;
  end
else
  if ~isempty(data_request_eff); gdat_data.gdat_request = data_request_eff; end
end

% special treatment if shot and data_request given within pairs
if isfield(gdat_params,'shot')
  shot = gdat_params.shot; % should use only gdat_params.shot but change shot to make sure
  gdat_data.shot = gdat_params.shot;
  gdat_params=rmfield(gdat_params,'shot');
end

if ~isfield(gdat_params,'data_request') || isempty(gdat_params.data_request)
  % warning('input for ''data_request'' missing from input arguments') % might be correct, asking for list of requests
  error_status=5;
  return
end
gdat_data.gdat_params = gdat_params;

% re-assign main variables to make sure use the one in gdat_data structure
shot = gdat_data.shot;
data_request_eff = gdat_data.gdat_params.data_request;
error_status = 6; % at least reached this level

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specifications on how to get the data provided in d3d_requests_mapping
mapping_for_d3d = d3d_requests_mapping(data_request_eff);
gdat_data.label = mapping_for_d3d.label;

% fill again at end to have full case, but here to have present status in case of early return
gdat_data.gdat_params.help = d3d_help_parameters(fieldnames(gdat_data.gdat_params));
gdat_data.mapping_for.d3d = mapping_for_d3d;
gdat_params = gdat_data.gdat_params;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1st treat the simplest method: "signal"
if strcmp(mapping_for_d3d.method,'signal')
  % allow changing main tree with simple signals, 'tree' parameter relates to mapping_for_d3d.expression{1}
  if ~isfield(gdat_data.gdat_params,'tree') || isempty(gdat_data.gdat_params.tree) || ~ischar(gdat_data.gdat_params.tree)
    gdat_data.gdat_params.tree = mapping_for_d3d.expression{1};
  else
    mapping_for_d3d.expression{1} = gdat_data.gdat_params.tree;
  end
  shoteff = [];
  if exist('mdscurrent') > 0
    aa=mdscurrent;
  else
    aa='will connect';
  end
  if ~isempty(shot)
    if isempty(aa) && mdsremotelist==0 && any(findstr('epfl',getenv('HOSTNAME')))
      mdsconnect('localhost:8002');
      aa=mdscurrent;
    end
    if strmatch('localhost',aa)
      [shoteff,stat] = mdsopen(mapping_for_d3d.expression{1},shot);
    else
      [shoteff,stat] = mdsopen(['atlas.gat.com::' mapping_for_d3d.expression{1}],shot);
    end
  else
    shot = mdsvalue('$shot');
  end
  if ~isempty(shoteff) && any(shoteff~=shot)
    disp('cannot open shot');
    return
  end
  [gdat_data,error_status]=get_signal_d3d(mapping_for_d3d.expression{2},gdat_data);
  if mapping_for_d3d.timedim==-1
    mapping_for_d3d.timedim = 1;
    mapping_for_d3d.gdat_timedim = 1;
  end
  if error_status~=0
    if gdat_params.nverbose>=3; disp(['error after get_signal_d3d in signal with data_request_eff= ' data_request_eff]); end
    return
  end
  if iscell(gdat_data.label) && length(gdat_data.label)==2;
    gdat_data.label =  [gdat_data.label{1} ' ' gdat_data.label{2}];
  end
  gdat_data.data_fullpath=mapping_for_d3d.expression;
  % end of method "signal"

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(mapping_for_d3d.method,'expression')
  % 2nd: method="expression"
  % assume expression contains an expression to evaluate and which creates a local structure into variable gdat_tmp
  % we copy the structure, to make sure default nodes are defined and to avoid if return is an closed object like tdi
  % eval_expr = [mapping_for_d3d.expression ';'];
  gdata_data_orig = gdat_data;
  fields_to_copy = {'gdat_params', 'gdat_request', 'label', 'data_fullpath', 'request_description', 'mapping_for'};
  eval([mapping_for_d3d.expression ';']);
  for i=1:length(fields_to_copy)
    if isfield(gdata_data_orig,fields_to_copy{i})
      gdat_tmp.(fields_to_copy{i}) = gdata_data_orig.(fields_to_copy{i});
    end
  end
  if isempty(gdat_tmp) || (~isstruct(gdat_tmp) & ~isobject(gdat_tmp))
    if (gdat_params.nverbose>=1); warning(['expression does not create a gdat_tmp structure: ' mapping_for_d3d.expression]); end
    error_status=801;
    return
  end
  tmp_fieldnames = fieldnames(gdat_tmp);
  if sum(strcmp(tmp_fieldnames,'data'))==0 % note: cannot do isfield since gdat_tmp might be an object
    if (gdat_params.nverbose>=1); warning(['expression does not return a child name ''data'' for ' data_request_eff]); end
  end
  for i=1:length(tmp_fieldnames)
    gdat_data.(tmp_fieldnames{i}) = gdat_tmp.(tmp_fieldnames{i});
  end
  % add .t and .x in case only dim is provided
  % do not allow shifting of timedim since should be treated in the relevant expression
  ijdim=find(strcmp(tmp_fieldnames,'dim')==1);
  if ~isempty(ijdim)
    nbdims = length(gdat_data.dim);
    if mapping_for_d3d.timedim==-1;
      mapping_for_d3d.timedim = nbdims;
      if (size(gdat_data.data,nbdims)==1 && nbdims>1); mapping_for_d3d.timedim = nbdims-1; end
    end
    dim_nontim = setdiff([1:nbdims],mapping_for_d3d.timedim);
    ijt=find(strcmp(tmp_fieldnames,'t')==1);
    if isempty(ijt)
      gdat_data.t = gdat_data.dim{mapping_for_d3d.timedim};
    end
    ijx=find(strcmp(tmp_fieldnames,'x')==1);
    if isempty(ijx)
      if ~isempty(dim_nontim)
        % since most cases have at most 2d, copy as array if data is 2D and as cell if 3D or more
        if length(dim_nontim)==1
          gdat_data.x = gdat_data.dim{dim_nontim(1)};
        else
          gdat_data.x = gdat_data.dim(dim_nontim);
        end
      end
    end
    gdat_data.data_fullpath=mapping_for_d3d.expression;
  end
  % end of method "expression"

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(mapping_for_d3d.method,'switchcase')
  switch data_request_eff % not lower(...) since data_request_eff should be lower case already at this stage
    case {'cxrs', 'cxrs_rho'}
      % if 'fit' option is added: 'fit',1, then the fitted profiles are returned which means "_rho" is effectively called
      if ~isfield(gdat_data.gdat_params,'fit') || isempty(gdat_data.gdat_params.fit) || ~isnumeric(gdat_data.gdat_params.fit)
        gdat_data.gdat_params.fit = 0;
      elseif gdat_data.gdat_params.fit==1
        data_request_eff = 'cxrs_rho';
      end
      exp_name_eff = gdat_data.gdat_params.exp_name;
      if ~isfield(gdat_data.gdat_params,'source') || isempty(gdat_data.gdat_params.source) || strcmp(upper(gdat_data.gdat_params.source),'CEZ')
        gdat_data.gdat_params.source = 'CEZ';
        r_node = 'R_time';
        z_node = 'z_time';
      elseif strcmp(upper(gdat_data.gdat_params.source),'CMZ')
        gdat_data.gdat_params.source = 'CMZ';
        r_node = 'R';
        z_node = 'z';
      else
        warning(['source = ' gdat_data.gdat_params.source ' not expected with data_request= ' data_request_eff]);
        return
      end
      diag_name = gdat_data.gdat_params.source;
      % R, Z positions of measurements
      try
        [r_time]=sf2ab(diag_name,shot,r_node,'-exp',exp_name_eff);
      catch ME_R_time
        % assume no shotfile
        getReport(ME_R_time,'basic');
        return
      end
      gdat_data.r = r_time.value{1};
      inotok=find(gdat_data.r<=0);
      gdat_data.r(inotok) = NaN;
      [z_time]=sf2ab(diag_name,shot,z_node,'-exp',exp_name_eff);
      gdat_data.z = z_time.value{1};
      inotok=find(gdat_data.z<=0);
      gdat_data.z(inotok) = NaN;
      [time]=sf2tb(diag_name,shot,'time','-exp',exp_name_eff);
      gdat_data.t = time.value;
      gdat_data.dim{1} = {gdat_data.r , gdat_data.z};
      gdat_data.dimunits{1} = 'R, Z [m]';
      gdat_data.dim{2} = gdat_data.t;
      gdat_data.dimunits{2} = 't [s]';
      gdat_data.x = gdat_data.dim{1};
      % vrot
      [a,error_status]=rdaD3D_eff(shot,diag_name,'vrot',exp_name_eff);
      if isempty(a.data) || isempty(a.t) || error_status>0
        if gdat_params.nverbose>=3;
          a
          disp(['with data_request= ' data_request_eff])
        end
      end
      gdat_data.vrot.data = a.data;
      if any(strcmp(fieldnames(a),'units')); gdat_data.vrot.units=a.units; end
      if any(strcmp(fieldnames(a),'name')); gdat_data.vrot.name=a.name; end
      gdat_data.vrot.label = 'vrot_tor';
      [aerr,e]=rdaD3D_eff(shot,diag_name,'err_vrot',exp_name_eff);
      gdat_data.vrot.error_bar = aerr.data;
      % Ti
      %     [a,e]=rdaD3D_eff(shot,diag_name,'Ti',exp_name_eff);
      %     [aerr,e]=rdaD3D_eff(shot,diag_name,'err_Ti',exp_name_eff);
      [a,e]=rdaD3D_eff(shot,diag_name,'Ti_c',exp_name_eff);
      [aerr,e]=rdaD3D_eff(shot,diag_name,'err_Ti_c',exp_name_eff);
      gdat_data.ti.data = a.data;
      gdat_data.data = a.data;
      gdat_data.label = [gdat_data.label '/Ti'];
      if any(strcmp(fieldnames(a),'units')); gdat_data.ti.units=a.units; end
      if any(strcmp(fieldnames(a),'units')); gdat_data.units=a.units; end
      if any(strcmp(fieldnames(a),'name')); gdat_data.ti.name=a.name; end
      gdat_data.ti.label = 'Ti_c';
      gdat_data.ti.error_bar = aerr.data;
      gdat_data.error_bar = aerr.data;
      gdat_data.data_fullpath=[exp_name_eff '/' diag_name '/' a.name ';vrot, Ti in data, {r;z} in .x'];
      %
      if strcmp(data_request_eff,'cxrs_rho')
        params_equil = gdat_data.gdat_params;
        params_equil.data_request = 'equil';
        [equil,params_equil,error_status] = gdat_d3d(shot,params_equil);
        if error_status>0
          if gdat_params.nverbose>=3; disp(['problems with ' params_equil.data_request]); end
          return
        end
        gdat_data.gdat_params.equil = params_equil.equil;
        gdat_data.equil = equil;
        inb_chord_cxrs=size(gdat_data.data,1);
        inb_time_cxrs=size(gdat_data.data,2);
        psi_out = NaN*ones(inb_chord_cxrs,inb_time_cxrs);
        rhopolnorm_out = NaN*ones(inb_chord_cxrs,inb_time_cxrs);
        rhotornorm_out = NaN*ones(inb_chord_cxrs,inb_time_cxrs);
        rhovolnorm_out = NaN*ones(inb_chord_cxrs,inb_time_cxrs);
        % constructs intervals within which a given equil is used: [time_equil(i),time_equil(i+1)]
        time_equil=[min(gdat_data.t(1)-0.1,equil.t(1)-0.1) 0.5.*(equil.t(1:end-1)+equil.t(2:end)) max(equil.t(end)+0.1,gdat_data.t(end)+0.1)];
        iok=find(~isnan(gdat_data.r(:,1)));
        for itequil=1:length(time_equil)-1
          rr=equil.Rmesh(:,itequil);
          zz=equil.Zmesh(:,itequil);
          psirz_in = equil.psi2D(:,:,itequil);
          it_cxrs_inequil = find(gdat_data.t>time_equil(itequil) & gdat_data.t<=time_equil(itequil+1));
          if ~isempty(it_cxrs_inequil)
            if strcmp(upper(gdat_data.gdat_params.source),'CEZ')
              rout=gdat_data.r(iok,it_cxrs_inequil);
              zout=gdat_data.z(iok,it_cxrs_inequil);
            else
              rout=gdat_data.r(iok);
              zout=gdat_data.z(iok);
            end
            psi_at_routzout = interpos2Dcartesian(rr,zz,psirz_in,rout,zout);
            if strcmp(upper(gdat_data.gdat_params.source),'CEZ')
              psi_out(iok,it_cxrs_inequil) = reshape(psi_at_routzout,length(iok),length(it_cxrs_inequil));
            else
              psi_out(iok,it_cxrs_inequil) = repmat(psi_at_routzout,[1,length(it_cxrs_inequil)]);
            end
            rhopolnorm_out(iok,it_cxrs_inequil) = sqrt((psi_out(iok,it_cxrs_inequil)-equil.psi_axis(itequil))./(equil.psi_lcfs(itequil)-equil.psi_axis(itequil)));
            for it_cx=1:length(it_cxrs_inequil)
              rhotornorm_out(iok,it_cxrs_inequil(it_cx)) = interpos(equil.rhopolnorm(:,itequil),equil.rhotornorm(:,itequil),rhopolnorm_out(iok,it_cxrs_inequil(it_cx)),-3,[2 2],[0 1]);
              rhovolnorm_out(iok,it_cxrs_inequil(it_cx)) = interpos(equil.rhopolnorm(:,itequil),equil.rhovolnorm(:,itequil),rhopolnorm_out(iok,it_cxrs_inequil(it_cx)),-3,[2 2],[0 1]);
            end
          end
        end
        gdat_data.psi = psi_out;
        gdat_data.rhopolnorm = rhopolnorm_out;
        gdat_data.rhotornorm = rhotornorm_out;
        gdat_data.rhovolnorm = rhovolnorm_out;
        % defaults for fits, so user always gets std structure
        gdat_data.fit.rhotornorm = []; % same for both ti and vrot
        gdat_data.fit.rhopolnorm = [];
        gdat_data.fit.t = [];
        gdat_data.fit.ti.data = [];
        gdat_data.fit.ti.drhotornorm = [];
        gdat_data.fit.vrot.data = [];
        gdat_data.fit.vrot.drhotornorm = [];
        gdat_data.fit.raw.rhotornorm = [];
        gdat_data.fit.raw.ti.data = [];
        gdat_data.fit.raw.vrot.data = [];
        fit_tension_default = -1;
        if isfield(gdat_data.gdat_params,'fit_tension')
          fit_tension = gdat_data.gdat_params.fit_tension;
        else
          fit_tension = fit_tension_default;
        end
        if ~isstruct(fit_tension)
          fit_tension_eff.ti = fit_tension;
          fit_tension_eff.vrot = fit_tension;
          fit_tension = fit_tension_eff;
        else
          if ~isfield(fit_tension,'ti'); fit_tension.ti = fit_tension_default; end
          if ~isfield(fit_tension,'vrot '); fit_tension.vrot = fit_tension_default; end
        end
        gdat_data.gdat_params.fit_tension = fit_tension;
        if isfield(gdat_data.gdat_params,'fit_nb_rho_points')
          fit_nb_rho_points = gdat_data.gdat_params.fit_nb_rho_points;
        else
          fit_nb_rho_points = 201;
        end
        gdat_data.gdat_params.fit_nb_rho_points = fit_nb_rho_points;
        %
        if gdat_data.gdat_params.fit==1
          % add fits
          gdat_data.fit.raw.rhotornorm = NaN*ones(size(gdat_data.ti.data));
          gdat_data.fit.raw.ti.data = NaN*ones(size(gdat_data.ti.data));
          gdat_data.fit.raw.vrot.data = NaN*ones(size(gdat_data.vrot.data));
          rhotornormfit = linspace(0,1,fit_nb_rho_points)';
          gdat_data.fit.rhotornorm = rhotornormfit;
          gdat_data.fit.t = gdat_data.t;
          for it=1:length(gdat_data.t)
            % make rhotor->rhopol transformation for each time since equilibrium might have changed
            irhook=find(gdat_data.rhotornorm(:,it)>0 & gdat_data.rhotornorm(:,it)<1); % no need for ~isnan
            [rhoeff isort]=sort(gdat_data.rhotornorm(irhook,it));
            gdat_data.fit.rhopolnorm(:,it)=interpos([0; rhoeff; 1],[0; gdat_data.rhopolnorm(irhook(isort),it); 1],rhotornormfit,-0.1,[2 2],[0 1]);
            idata = find(gdat_data.ti.data(:,it)>0 & gdat_data.rhotornorm(:,it)<1.01);
            if length(idata)>0
              gdat_data.fit.ti.raw.rhotornorm(idata,it) = gdat_data.rhotornorm(idata,it);
              gdat_data.fit.ti.raw.data(idata,it) = gdat_data.ti.data(idata,it);
              gdat_data.fit.ti.raw.error_bar(idata,it) = gdat_data.ti.error_bar(idata,it);
              gdat_data.fit.vrot.raw.rhotornorm(idata,it) = gdat_data.rhotornorm(idata,it);
              gdat_data.fit.vrot.raw.data(idata,it) = gdat_data.vrot.data(idata,it);
              gdat_data.fit.vrot.raw.error_bar(idata,it) = gdat_data.vrot.error_bar(idata,it);
              [rhoeff,irhoeff] = sort(gdat_data.rhotornorm(idata,it));
              rhoeff = [0; rhoeff];
              % they are some strange low error_bars, so remove these by max(Ti/error_bar)<=10; and changing it to large error bar
              tieff = gdat_data.ti.data(idata(irhoeff),it);
              ti_err_eff = gdat_data.ti.error_bar(idata(irhoeff),it);
              ij=find(tieff./ti_err_eff>10.);
              if ~isempty(ij); ti_err_eff(ij) = tieff(ij)./0.1; end
              vroteff = gdat_data.vrot.data(idata(irhoeff),it);
              vrot_err_eff = gdat_data.vrot.error_bar(idata(irhoeff),it);
              ij=find(vroteff./vrot_err_eff>10.);
              if ~isempty(ij); vrot_err_eff(ij) = vroteff(ij)./0.1; end
              %
              tieff =  [tieff(1); tieff];
              ti_err_eff =  [1e4; ti_err_eff];
              vroteff =  [vroteff(1); vroteff];
              vrot_err_eff =  [1e5; vrot_err_eff];
              [gdat_data.fit.ti.data(:,it), gdat_data.fit.ti.drhotornorm(:,it)] = interpos(rhoeff,tieff,rhotornormfit,fit_tension.ti,[1 0],[0 0],ti_err_eff);
              [gdat_data.fit.vrot.data(:,it), gdat_data.fit.vrot.drhotornorm(:,it)] = interpos(rhoeff,vroteff,rhotornormfit,fit_tension.vrot,[1 0],[0 0],vrot_err_eff);
            end
          end
        end
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'ece', 'ece_rho'}
      nth_points = 13;
      if isfield(gdat_data.gdat_params,'nth_points') && ~isempty(gdat_data.gdat_params.nth_points)
        nth_points = gdat_data.gdat_params.nth_points;
      else
        gdat_data.gdat_params.nth_points = nth_points;
      end
      channels = -1;
      if isfield(gdat_data.gdat_params,'channels') && ~isempty(gdat_data.gdat_params.channels)
        channels = gdat_data.gdat_params.channels;
      end
      diag_name = 'ece';
      if nth_points>=10
        match_rz_to_time = 1;
      else
        match_rz_to_time = 0;
      end
      if isfield(gdat_data.gdat_params,'match_rz_to_time') && ~isempty(gdat_data.gdat_params.match_rz_to_time)
        match_rz_to_time = gdat_data.gdat_params.match_rz_to_time;
      else
        gdat_data.gdat_params.match_rz_to_time = match_rz_to_time;
      end
      time_interval = [-Inf +Inf];
      if isfield(gdat_data.gdat_params,'time_interval') && ~isempty(gdat_data.gdat_params.time_interval)
        time_interval = gdat_data.gdat_params.time_interval;
      else
        gdat_data.gdat_params.time_interval = time_interval;
      end
      %
      if channels(1)<=0
        channels = [1:40];
      end
      chanelles=sort(channels);
      gdat_data.dim{1} = channels;
      gdat_data.gdat_params.channels = channels;
      for i=channels
        a = gdat_d3d(shot,{'ece',['\tece' num2str(i,'%.2d')]});
        if nth_points>1
          gdat_data.data(i,:) = a.data(1:nth_points:end);
          gdat_data.dim{2} = a.t(1:nth_points:end);
        else
          gdat_data.data(i,:) = a.data;
          gdat_data.dim{2} = a.t;
        end
      end
      gdat_data.x = gdat_data.dim{1};
      gdat_data.t = gdat_data.dim{2};
      gdat_data.dimunits=[{'channels'} ; {'time [s]'}];
      if any(strcmp(fieldnames(a),'units')); gdat_data.units=a.units; end
% $$$     if match_rz_to_time
% $$$       % interpolate R structure on ece data time array, to ease plot vs R
% $$$       for i=1:length(channels)
% $$$         radius.data(i,:) = interp1(aR.t,aR.data(channels(i),:),gdat_data.t);
% $$$         zheight.data(i,:) = interp1(aZ.t,aZ.data(channels(i),:),gdat_data.t);
% $$$       end
% $$$       radiuszheight.t=gdat_data.t;
% $$$     else
% $$$       radius.data = aR.data(channels,:);
% $$$       radiuszheight.t=aR.t;
% $$$       zheight.data = aZ.data(channels,:);
% $$$     end
      ij=find(gdat_data.data<=0);
      gdat_data.data(ij)=NaN;
% $$$     gdat_data.rz.r=radius.data;
% $$$     gdat_data.rz.z=zheight.data;
% $$$     gdat_data.rz.t = radiuszheight.t;
      gdat_data.data_fullpath = [diag_name '\tece' num2str(channels(1)) '...' num2str(channels(end))];

% $$$       gdat_data.rhos.psi = psi_out;
% $$$       gdat_data.rhos.rhopolnorm = rhopolnorm_out;
% $$$       gdat_data.rhos.rhotornorm = rhotornorm_out;
% $$$       gdat_data.rhos.rhovolnorm = rhovolnorm_out;
% $$$       gdat_data.rhos.t = gdat_data.rz.t;
% $$$     end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'eqdsk'}
      %
      time_eqdsks=2000.; % default time
      if isfield(gdat_data.gdat_params,'time') && ~isempty(gdat_data.gdat_params.time)
        time_eqdsks = gdat_data.gdat_params.time;
      else
        gdat_data.gdat_params.time = time_eqdsks;
        disp(['"time" is expected as an option, choose default time = ' num2str(time_eqdsks)]);
      end
      if time_eqdsks < 10;
        % assume given in seconds, d3d time is in ms
        time_eqdsks = time_eqdsks * 1e3;
      end
      gdat_data.gdat_params.time = time_eqdsks;
      gdat_data.t = time_eqdsks;
      zshift = 0.;
      if isfield(gdat_data.gdat_params,'zshift') && ~isempty(gdat_data.gdat_params.zshift)
        zshift = gdat_data.gdat_params.zshift;
      else
        gdat_data.gdat_params.zshift = zshift;
      end
      gdat_data.gdat_params.zshift = zshift;
      params_equil = gdat_data.gdat_params;
      params_equil.data_request = 'equil';
      [equil,params_equil,error_status] = gdat_d3d(shot,params_equil);
      if error_status>0
        if gdat_params.nverbose>=3; disp(['problems with ' params_equil.data_request]); end
        return
      end
      gdat_data.gdat_params = params_equil;
      gdat_data.equil = equil;
      for itime=1:length(time_eqdsks)
        time_eff = time_eqdsks(itime);
        % use read_results updated to effectively obtain an eqdsk with sign correct with COCOS=2
        [eqdskD3D, equil_all_t, equil_t_index]=get_eqdsk_d3d(equil,time_eff,gdat_data.gdat_params.zshift,'source',gdat_data.gdat_params.equil);
        eqdskD3D.fnamefull = fullfile(['/tmp/' getenv('USER')],['EQDSK_' num2str(shot) 't' num2str(time_eff,'%.4f')]);
        cocos_out = equil.cocos;
        if isfield(gdat_data.gdat_params,'cocos') && ~isempty(gdat_data.gdat_params.cocos)
          cocos_out = gdat_data.gdat_params.cocos;
        else
          gdat_data.gdat_params.cocos = cocos_out;
        end
        if equil.cocos ~= cocos_out
          [eqdsk_cocosout, eqdsk_cocosout_IpB0pos,cocos_inout]=eqdsk_cocos_transform(eqdskD3D,[equil.cocos cocos_out]);
        else
          eqdsk_cocosout = eqdskD3D;
        end
        % for several times, use array of structure for eqdsks,
        % cannot use it for psi(R,Z) in .data and .x since R, Z might be different at different times,
        % so project psi(R,Z) on Rmesh, Zmesh of 1st time
        if length(time_eqdsks) > 1
          gdat_data.eqdsk{itime} = write_eqdsk(eqdskD3D.fnamefull,eqdsk_cocosout);
          if itime==1
            gdat_data.data(:,:,itime) = gdat_data.eqdsk{itime}.psi;
            gdat_data.dim{1} = gdat_data.eqdsk{itime}.rmesh;
            gdat_data.dim{2} = gdat_data.eqdsk{itime}.zmesh;
          else
            xx=repmat(reshape(gdat_data.dim{1},length(gdat_data.dim{1}),1),1,size(gdat_data.eqdsk{itime}.psi,2));
            yy=repmat(reshape(gdat_data.dim{2},1,length(gdat_data.dim{2})),size(gdat_data.eqdsk{itime}.psi,1),1);
            aa = interpos2Dcartesian(gdat_data.eqdsk{itime}.rmesh,gdat_data.eqdsk{itime}.zmesh ...
          ,gdat_data.eqdsk{itime}.psi,xx,yy,-1,-1);
            gdat_data.data(:,:,itime) = aa;
          end
        else
          gdat_data.eqdsk = write_eqdsk(eqdskD3D.fnamefull,eqdsk_cocosout);
          gdat_data.data = gdat_data.eqdsk.psi;
          gdat_data.dim{1} = gdat_data.eqdsk.rmesh;
          gdat_data.dim{2} = gdat_data.eqdsk.zmesh;
        end
      end
      gdat_data.dim{3} = gdat_data.t;
      gdat_data.x = gdat_data.dim(1:2);
      gdat_data.data_fullpath=['psi(R,Z) and eqdsk from geteqdskD3D with ' gdat_data.gdat_params.equil ' ;zshift=' num2str(zshift)];
      gdat_data.units = 'T m^2';
      gdat_data.dimunits = {'m','m','s'};
      gdat_data.request_description = ['data=psi, x=(R,Z), eqdsk contains eqdsk structure with which ' ...
                    'plot_eqdsk, write_eqdsk, read_eqdsk can be used'];

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'equil'}
      if isfield(gdat_data.gdat_params,'source') && ~isempty(gdat_data.gdat_params.source) && ischar(gdat_data.gdat_params.source) && strcmp(lower(gdat_data.gdat_params.equil),'efit01')
        % assume source provided instead of equil for choice of equil, since equil=default and source provided
        warning('choice of equil should be provided in ''equil'' not in ''source'', moved entry')
        gdat_data.gdat_params.equil = gdat_data.gdat_params.source;
        gdat_data.gdat_params = rmfield(gdat_data.gdat_params,'source');
      end
      if ~isfield(gdat_data.gdat_params,'equil') || isempty(gdat_data.gdat_params.equil) || ~ischar(gdat_data.gdat_params.equil)
        gdat_data.gdat_params.equil = 'efit03';
      else
      end
      if exist('read_mds_eq_func')<=0
        if gdat_params.nverbose>=3; disp(['addpath(''/m/GAtools/matlab/efit'',''end'') since needs function read_mds_eq_func']); end
        addpath('/m/GAtools/matlab/efit','end');
      end
      [equil_all_t,neq,eq,ier]=read_mds_eq_func(shot,gdat_data.gdat_params.equil,'d3d');
      gdat_data.rhovolnorm = eq.results.geqdsk.rhovn;
      gdat_data.equil_all_t = equil_all_t;
      gdat_data.eq_raw = eq;
      % extract main parameters, can add more later
      gdat_data.phi = [];
      gdat_data.rhotornorm = [];
      gdat_data.t = equil_all_t.time;
      for it=1:length(equil_all_t.time)
        gdat_data.qvalue(:,it) = equil_all_t.gdata(it).qpsi;
        gdat_data.psi(:,it) = (equil_all_t.gdata(it).ssibry - equil_all_t.gdata(it).ssimag) ...
            .* linspace(0,1,equil_all_t.gdata(it).nw)' + equil_all_t.gdata(it).ssimag;
        gdat_data.psi_axis(it)= equil_all_t.gdata(it).ssimag;
        gdat_data.psi_lcfs(it)= equil_all_t.gdata(it).ssibry;
        gdat_data.rhopolnorm(:,it) = sqrt(abs((gdat_data.psi(:,it)-gdat_data.psi_axis(it)) ./(gdat_data.psi_lcfs(it)-gdat_data.psi_axis(it))));
        gdat_data.rhopolnorm(1,it) = 0.;
        gdat_data.rhopolnorm(end,it) = 1.;
        [~,~,~,int_q]=interpos(gdat_data.psi(:,it),gdat_data.qvalue(:,it));
        gdat_data.rhotornorm(:,it) = sqrt(int_q./int_q(end));
        gdat_data.rhotornorm(1,it) = 0.;
        gdat_data.rhotornorm(end,it) = 1.;
        gdat_data.phi_intq(it) = int_q(end);
        gdat_data.vol(:,it)=gdat_data.rhovolnorm(:,it).^2 .* eq.results.aeqdsk.volume(it);
        gdat_data.Rmesh(:,it) = eq.results.geqdsk.r;
        gdat_data.Zmesh(:,it) = eq.results.geqdsk.z;
        gdat_data.psi2D(:,:,it) = equil_all_t.gdata(it).psirz;
        gdat_data.pressure(:,it) = equil_all_t.gdata(it).pres;
        gdat_data.dpressuredpsi(:,it) = equil_all_t.gdata(it).pprime; % 2nd index are dV/dpsi
        gdat_data.ffprime(:,it) = equil_all_t.gdata(it).ffprim;
        gdat_data.rwall(:,it) = equil_all_t.gdata(it).xlim;
        gdat_data.zwall(:,it) = equil_all_t.gdata(it).ylim;
        if equil_all_t.adata(it).rxpt1 < -9.5
          gdat_data.R_Xpoints(:,it,1) = NaN;
        else
          gdat_data.R_Xpoints(:,it,1) = equil_all_t.adata(it).rxpt1;
        end
        if equil_all_t.adata(it).rxpt1 < -9.5
          gdat_data.Z_Xpoints(:,it,1) = NaN;
        else
          gdat_data.Z_Xpoints(:,it,1) = equil_all_t.adata(it).zxpt1;
        end
        if equil_all_t.adata(it).rxpt2 < -9.5
          gdat_data.R_Xpoints(:,it,2) = NaN;
        else
          gdat_data.R_Xpoints(:,it,2) = equil_all_t.adata(it).rxpt2;
        end
        if equil_all_t.adata(it).rxpt2 < -9.5
          gdat_data.Z_Xpoints(:,it,2) = NaN;
        else
          gdat_data.Z_Xpoints(:,it,2) = equil_all_t.adata(it).zxpt2;
        end
        %
      end
      gdat_data.x = gdat_data.rhopolnorm;
      %
      gdat_data.ip = [equil_all_t.gdata(:).cpasma];
      %
      gdat_data.data = gdat_data.qvalue; % put q in data
      gdat_data.units='q'; % not applicable
      gdat_data.data_fullpath = ...
          ['q(:,time) in .data, all from [equil_all_t,neq,eq,ier]=read_mds_eq_func(shot,gdat_data.gdat_params.equil,''d3d'');'];
      gdat_data.cocos = 2;
      gdat_data.dim{1} = gdat_data.x;
      gdat_data.dim{2} = gdat_data.t;
      gdat_data.dimunits{1} = 'rhopolnorm';
      gdat_data.dimunits{2} = 'time [s]';

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'ne','te'}
      % Tree: ELECTRONS
      %
      % TOP.TS.BLESSED.CORE
      % TOP.TS.BLESSED.DIVERTOR
      % TOP.TS.BLESSED.TANGENTIAL
      % TEMP
      % DENSITY
      % TEMP_E
      % DENSITY_E
      % Z
      % R
      % DCDATA
      % PLDATA
      % PLERROR
      exp_name_eff = gdat_data.gdat_params.exp_name;
      % ne or Te from Thomson data on raw z mesh vs (z,t)
      if strcmp(data_request_eff,'te')
        node_leaf = 'TEMP';
      else
        node_leaf = 'DENSITY';
      end
      %
      extra_source = {'core', 'tangential', 'divertor'};
      gdat_data.with_data = {};
      a_ip = gdat_d3d(shot,'ip');
      if isempty(a_ip.t)
        warning('no Ip?');
        return
      else
        t_range = [a_ip.t(1) a_ip.t(end)];
      end
      for i=1:numel(extra_source)
        main_source = ['TS.BLESSED.' extra_source{i} ':'];
        nodenameeff = [main_source node_leaf];
        a = gdat_d3d(shot,{'electrons',nodenameeff});
        if isempty(a.data) || isempty(a.t)
          if gdat_params.nverbose>=3;
            a
            disp(['with data_request= ' data_request_eff])
          end
          gdat_data.(extra_source{i}) = a;
        else
          ij=[a.t<t_range(1) | a.t>t_range(2)];
          a.data(ij,:) = NaN; % not yet transposed
          gdat_data.with_data{end+1} = extra_source{i};
          gdat_data.(extra_source{i}).data = a.data';
          gdat_data.(extra_source{i}).t = a.t;
          gdat_data.(extra_source{i}).x = a.x;
          gdat_data.(extra_source{i}).gdat_params.tree = a.gdat_params.tree;
          if any(strcmp(fieldnames(a),'units'))
            gdat_data.(extra_source{i}).units=a.units;
          end
          gdat_data.(extra_source{i}).r = mdsvalue([main_source 'r']);
          gdat_data.(extra_source{i}).z = mdsvalue([main_source 'z']);
          gdat_data.(extra_source{i}).error_bar = mdsvalue([nodenameeff '_e'])';
          gdat_data.(extra_source{i}).data_fullpath=[data_request_eff 'from electrons ' nodenameeff];
          gdat_data.(extra_source{i}).dim=[{gdat_data.x};{gdat_data.t}];
          if strcmp(extra_source{i},'tangential')
            gdat_data.(extra_source{i}).dimunits=[{'R [m]'}; {'time [s]'}];
          else
            gdat_data.(extra_source{i}).dimunits=[{'Z [m]'}; {'time [s]'}];
          end
        end
      end
      %
      % put core in main
      i = 1;
      gdat_data.data = gdat_data.(extra_source{i}).data;
      gdat_data.error_bar = gdat_data.(extra_source{i}).error_bar;
      gdat_data.t = gdat_data.(extra_source{i}).t;
      gdat_data.x = gdat_data.(extra_source{i}).x;
      gdat_data.r = gdat_data.(extra_source{i}).r;
      gdat_data.z = gdat_data.(extra_source{i}).z;
      gdat_data.dim = gdat_data.(extra_source{i}).dim;
      gdat_data.dimunits = gdat_data.(extra_source{i}).dimunits;
      gdat_data.units = gdat_data.(extra_source{i}).units;
      gdat_data.data_fullpath = gdat_data.(extra_source{i}).data_fullpath;
      gdat_data.(data_request_eff) = gdat_data;
      %

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'neint_ts'}
      if ~isfield(gdat_data.gdat_params,'source') || isempty(gdat_data.gdat_params.source) || ~isstruct(gdat_data.gdat_params.source)
        warning('requires ''ne_rho'' gdat structure as input in ''source'' option. Loading it...');
        params_eff = gdat_data.gdat_params;
        params_eff.data_request = 'ne_rho';
        [gdat_data.gdat_params.source,params_nerho,error_status]=gdat_d3d(shot,params_eff);
      end
      ne_rho = gdat_data.gdat_params.source;
      % Computes R0 and V1 line integrated signal based on ne fit (so inside LCFS), provide also R(psi_ts) and Z(psi_ts) on these lines
      z_at_r0line = 0.;
      r_at_v1line = 1.48;
      % finds xx 1d dependence for each line within d3d wall for each time in equil
      nb_xx_points = 257;
      for it_equil=1:numel(ne_rho.equil.t)
        % R0 line
        gdat_data.r0.y = z_at_r0line;
        gdat_data.r0.x(:,it_equil) = linspace(ne_rho.equil.Rmesh(1,it_equil),ne_rho.equil.Rmesh(end,it_equil),nb_xx_points);
        [psi_xx_out]=interpos2Dcartesian(ne_rho.equil.Rmesh(:,it_equil),ne_rho.equil.Zmesh(:,it_equil), ...
          ne_rho.equil.psi2D(:,:,it_equil),gdat_data.r0.x(:,it_equil),z_at_r0line.*ones(size(gdat_data.r0.x(:,it_equil))));
        gdat_data.r0.psi(:,it_equil) = psi_xx_out;
        gdat_data.r0.rhopolnorm(:,it_equil) = sqrt((psi_xx_out-ne_rho.equil.psi_axis(it_equil)) / ...
          (ne_rho.equil.psi_lcfs(it_equil)-ne_rho.equil.psi_axis(it_equil)));
        % find line within lcfs
        zdist = (psi_xx_out(1:end-1)-ne_rho.equil.psi_lcfs(it_equil)).*(psi_xx_out(2:end)-ne_rho.equil.psi_lcfs(it_equil));
        ij=find(zdist<=0);
        % assume 2 crossing not near ends
        for ii=1:2
          r_cross_lcfs(ii) = interp1(psi_xx_out(ij(ii):ij(ii)+1),gdat_data.r0.x(ij(ii):ij(ii)+1,it_equil), ...
          ne_rho.equil.psi_lcfs(it_equil));
        end
        gdat_data.r0.x_cross_lcfs(1:2,it_equil) = r_cross_lcfs;
        gdat_data.r0.x_len_plasma(it_equil) = abs(diff(gdat_data.r0.x_cross_lcfs(:,it_equil)));
        % find line within walls
        zdist=(ne_rho.equil.zwall(1:end-1,it_equil)-z_at_r0line).*(ne_rho.equil.zwall(2:end,it_equil)-z_at_r0line);
        ij=find(zdist<=0);
        % assume 2 crossing
        for ii=1:min(2,numel(ij))
          if ij(ii)~=size(ne_rho.equil.zwall,1)
            rcrosswall(ii) = interp1(ne_rho.equil.zwall(ij(ii):ij(ii)+1,it_equil),ne_rho.equil.rwall(ij(ii):ij(ii)+1,it_equil), ...
          z_at_r0line);
          else
            rcrosswall(ii) = interp1(ne_rho.equil.zwall(ij(ii)-1:ij(ii),it_equil),ne_rho.equil.rwall(ij(ii)-1:ij(ii),it_equil), ...
          z_at_r0line);
          end
        end
        if numel(ij)==1
          rcrosswall(2) = NaN;
        end
        gdat_data.r0.x_crosswall(1:2,it_equil) = rcrosswall;
        gdat_data.r0.x_len_crosswall(it_equil) = abs(diff(gdat_data.r0.x_crosswall(:,it_equil)));
        % V1 line
        gdat_data.v1.y = r_at_v1line;
        gdat_data.v1.x(:,it_equil) = linspace(ne_rho.equil.Zmesh(1,it_equil),ne_rho.equil.Zmesh(end,it_equil),nb_xx_points);
        [psi_xx_out]=interpos2Dcartesian(ne_rho.equil.Rmesh(:,it_equil),ne_rho.equil.Zmesh(:,it_equil), ...
          ne_rho.equil.psi2D(:,:,it_equil),r_at_v1line.*ones(size(gdat_data.v1.x(:,it_equil))),gdat_data.v1.x(:,it_equil));
        gdat_data.v1.psi(:,it_equil) = psi_xx_out;
        gdat_data.v1.rhopolnorm(:,it_equil) = sqrt((psi_xx_out-ne_rho.equil.psi_axis(it_equil)) / ...
          (ne_rho.equil.psi_lcfs(it_equil)-ne_rho.equil.psi_axis(it_equil)));
        % find line within lcfs
        zdist = (psi_xx_out(1:end-1)-ne_rho.equil.psi_lcfs(it_equil)).*(psi_xx_out(2:end)-ne_rho.equil.psi_lcfs(it_equil));
        ij=find(zdist<=0);
        % may have more than 2 crossing if private flux
        if numel(ij)>2
          zz_ij=abs(gdat_data.v1.x(ij,it_equil));
          [dok,ijok] = sort(zz_ij);
        else
          ijok = [1:2];
        end
        for ii=1:2
          r_cross_lcfs(ii) = interp1(psi_xx_out(ij(ijok(ii)):ij(ijok(ii))+1),gdat_data.v1.x(ij(ijok(ii)):ij(ijok(ii))+1,it_equil), ...
          ne_rho.equil.psi_lcfs(it_equil));
        end
        gdat_data.v1.x_cross_lcfs(1:2,it_equil) = r_cross_lcfs;
        gdat_data.v1.x_len_plasma(it_equil) = abs(diff(gdat_data.v1.x_cross_lcfs(:,it_equil)));
        % walls
        zdist=(ne_rho.equil.rwall(1:end-1,it_equil)-r_at_v1line).*(ne_rho.equil.rwall(2:end,it_equil)-r_at_v1line);
        ij=find(zdist<=0 & ne_rho.equil.zwall(1:end-1,it_equil)<=0);
        if numel(ij)>1
          rho_ij=sqrt((ne_rho.equil.rwall(ij,it_equil)-r_at_v1line).^2 + (ne_rho.equil.zwall(ij,it_equil)-0.).^2);
          [dok,ijok] = sort(rho_ij);
          zcrosswall(1) = interp1(ne_rho.equil.rwall(ij(ijok):ij(ijok)+1,it_equil),ne_rho.equil.zwall(ij(ijok):ij(ijok)+1,it_equil), ...
          r_at_v1line);
        elseif numel(ij)==1
          zcrosswall(1) = interp1(ne_rho.equil.rwall(ij:ij+1,it_equil),ne_rho.equil.zwall(ij:ij+1,it_equil), ...
          r_at_v1line);
        else
          zcrosswall(1) = NaN;
        end
        ij=find(zdist<=0 & ne_rho.equil.zwall(1:end-1,it_equil)>=0);
        if numel(ij)>1
          rho_ij=sqrt((ne_rho.equil.rwall(ij,it_equil)-r_at_v1line).^2 + (ne_rho.equil.zwall(ij,it_equil)-0.).^2);
          [dok,ijok] = sort(rho_ij);
          zcrosswall(2) = interp1(ne_rho.equil.rwall(ij(ijok):ij(ijok)+1,it_equil),ne_rho.equil.zwall(ij(ijok):ij(ijok)+1,it_equil), ...
          r_at_v1line);
        elseif numel(ij)==1
          zcrosswall(2) = interp1(ne_rho.equil.rwall(ij:ij+1,it_equil),ne_rho.equil.zwall(ij:ij+1,it_equil), ...
          r_at_v1line);
        else
          zcrosswall(2) = NaN;
        end
        gdat_data.v1.x_crosswall(1:2,it_equil) = zcrosswall;
        gdat_data.v1.x_len_crosswall(it_equil) = abs(diff(gdat_data.v1.x_crosswall(:,it_equil)));
      end
      for isub={'r0','v1'}
        gdat_data.(isub{1}).t = ne_rho.fit.t;
        gdat_data.(isub{1}).dimunits{1} = 'time [s]';
        gdat_data.(isub{1}).dim{1} = gdat_data.(isub{1}).t;
        gdat_data.(isub{1}).units = 'm^{-2}';
        gdat_data.(isub{1}).label = ['ne int one line ' upper(isub{1})];
        gdat_data.(isub{1}).data_fullpath = ['from ne_rho fit to Thomson line-integral along line ' upper(isub{1})];
      end
      for it=1:numel(ne_rho.fit.t)
        it_equil = iround_os(ne_rho.equil.t,ne_rho.fit.t(it));
        % r0
        gdat_data.r0.it_equil(it) = it_equil;
        gdat_data.r0.nefit(:,it) = interpos(-63,ne_rho.fit.rhopolnorm(:,it),ne_rho.fit.ne.data(:,it),gdat_data.r0.rhopolnorm(:,it_equil), ...
          -0.1,[1 0],[0 0]);
        ij = find((gdat_data.r0.x(:,it_equil)-gdat_data.r0.x_crosswall(1,it_equil)).*(gdat_data.r0.x(:,it_equil)-gdat_data.r0.x_crosswall(2,it_equil)) > 0);
        gdat_data.r0.nefit(ij,it) = NaN;
        ij=find(~isnan(gdat_data.r0.nefit(:,it)));
        gdat_data.r0.data(it) = trapz(gdat_data.r0.x(ij,it_equil),gdat_data.r0.nefit(ij,it));
        non_vacuum = gdat_data.r0.nefit(:,it);
        non_vacuum_min = min(non_vacuum(non_vacuum>0));
        non_vacuum(non_vacuum==0) = non_vacuum_min;
        gdat_data.r0.neint_ts_vacuum_min(it) = trapz(gdat_data.r0.x(ij,it_equil),non_vacuum(ij));
        non_vacuum_110 = gdat_data.r0.nefit(:,it);
        ij = find(gdat_data.r0.rhopolnorm(:,it_equil)>1 & gdat_data.r0.rhopolnorm(:,it_equil)<=1.10 & ~isnan(non_vacuum_110));
        non_vacuum_110(ij) = non_vacuum_min;
        ij=find(~isnan(non_vacuum_110));
        gdat_data.r0.neint_ts_vacuum_rhopol110(it) = trapz(gdat_data.r0.x(ij,it_equil),non_vacuum_110(ij));
        % v1
        gdat_data.v1.it_equil(it) = it_equil;
        gdat_data.v1.nefit(:,it) = interpos(-63,ne_rho.fit.rhopolnorm(:,it),ne_rho.fit.ne.data(:,it),gdat_data.v1.rhopolnorm(:,it_equil), ...
          -0.1,[1 0],[0 0]);
        ij = find((gdat_data.v1.x(:,it_equil)-gdat_data.v1.x_crosswall(1,it_equil)).*(gdat_data.v1.x(:,it_equil)-gdat_data.v1.x_crosswall(2,it_equil)) > 0);
        gdat_data.v1.nefit(ij,it) = NaN;
        ij=find(~isnan(gdat_data.v1.nefit(:,it)));
        gdat_data.v1.data(it) = trapz(gdat_data.v1.x(ij,it_equil),gdat_data.v1.nefit(ij,it));
        non_vacuum = gdat_data.v1.nefit(:,it);
        non_vacuum_min = min(non_vacuum(non_vacuum>0));
        non_vacuum(non_vacuum==0) = non_vacuum_min;
        gdat_data.v1.neint_ts_vacuum_min(it) = trapz(gdat_data.v1.x(ij,it_equil),non_vacuum(ij));
        non_vacuum_110 = gdat_data.v1.nefit(:,it);
        ij = find(gdat_data.v1.rhopolnorm(:,it_equil)>1 & gdat_data.v1.rhopolnorm(:,it_equil)<=1.1 & ~isnan(non_vacuum_110));
        non_vacuum_110(ij) = non_vacuum_min;
        ij=find(~isnan(non_vacuum_110));
        gdat_data.v1.neint_ts_vacuum_rhopol110(it) = trapz(gdat_data.v1.x(ij,it_equil),non_vacuum_110(ij));
      end
      gdat_data.r0.nel = gdat_data.r0.data./gdat_data.r0.x_len_plasma(gdat_data.r0.it_equil);
      gdat_data.v1.nel = gdat_data.v1.data./gdat_data.v1.x_len_plasma(gdat_data.v1.it_equil);

      fields_to_copy = {'data','units','dim','dimunits','t','x','data_fullpath','label','help'};
      for i=1:length(fields_to_copy)
        if isfield(gdat_data.r0,fields_to_copy{i})
          gdat_data.(fields_to_copy{i}) = gdat_data.r0.(fields_to_copy{i});
        end
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'ne_rho', 'te_rho', 'nete_rho'}
      if ~isfield(gdat_data.gdat_params,'fit') || isempty(gdat_data.gdat_params.fit) || ~isnumeric(gdat_data.gdat_params.fit)
        gdat_data.gdat_params.fit = 1; % default do fit
      end
      params_eff = gdat_data.gdat_params;
      params_eff.data_request=data_request_eff(1:2); % start with ne if nete_rho
      % get raw data
      [gdat_data,params_kin,error_status]=gdat_d3d(shot,params_eff);
      gdat_data.gdat_params.data_request=data_request_eff;
      gdat_data.gdat_request=data_request_eff;
      if strcmp(lower(data_request_eff(1:4)),'nete')
        params_eff.data_request=data_request_eff(3:4); % start with ne if nete_rho
        [aate,params_kin,error_status]=gdat_d3d(shot,params_eff);
        gdat_data.te = aate.te;
      end
      if error_status>0
        if gdat_params.nverbose>=3; disp(['problems with ' params_eff.data_request]); end
        return
      end
      % add rho coordinates
      if ~isfield(gdat_data.gdat_params,'equil') || isempty(gdat_data.gdat_params.equil) || ~isstruct(gdat_data.gdat_params.equil)
        params_eff.data_request='equil';
        [equil,params_equil,error_status]=gdat_d3d(shot,params_eff);
        if error_status>0
          if gdat_params.nverbose>=3; disp(['problems with ' params_eff.data_request]); end
          return
        end
      else
        params_equil.equil = gdat_data.gdat_params.equil.gdat_params.equil;
        equil = gdat_data.gdat_params.equil;
      end
      gdat_data.gdat_params.equil = params_equil.equil;
      gdat_data.equil = equil;

      % sources
      % assume, time and grid same for ne and Te for each extra_source
      extra_source = {'core', 'tangential', 'divertor'};
      for i=1:numel(extra_source)
        node_child_nameeff = extra_source{i};
        if ~isempty(gdat_data.(extra_source{i}).data) && ~ischar(gdat_data.(extra_source{i}).data)
          inb_chord_source=size(gdat_data.(extra_source{i}).data,1);
          inb_time_source=size(gdat_data.(extra_source{i}).data,2);
          psi_out_source = NaN*ones(inb_chord_source,inb_time_source);
          rhopolnorm_out_source = NaN*ones(inb_chord_source,inb_time_source);
          rhotornorm_out_source = NaN*ones(inb_chord_source,inb_time_source);
          rhovolnorm_out_source = NaN*ones(inb_chord_source,inb_time_source);
          % constructs intervals within which a given equil is used: [time_equil(i),time_equil(i+1)]
          if ~isempty(gdat_data.(extra_source{i}).t)
            time_equil=[min(gdat_data.(extra_source{i}).t(1)-0.1,equil.t(1)-0.1); 0.5.*(equil.t(1:end-1)+equil.t(2:end)) ;max(equil.t(end)+0.1,gdat_data.(extra_source{i}).t(end)+0.1)];
            clear psi_out_core rhopolnorm_out_core rhotornorm_out_core rhovolnorm_out_core
            for itequil=1:numel(time_equil)-1
              rr=equil.Rmesh(:,itequil);
              zz=equil.Zmesh(:,itequil);
              psirz_in = equil.psi2D(:,:,itequil);
              it_core_inequil = find(gdat_data.(extra_source{i}).t>time_equil(itequil) & gdat_data.(extra_source{i}).t<=time_equil(itequil+1));
              if ~isempty(it_core_inequil)
                rout_core=gdat_data.(extra_source{i}).r;
                zout_core=gdat_data.(extra_source{i}).z;
                psi_at_routzout = interpos2Dcartesian(rr,zz,psirz_in,rout_core,zout_core);
                psi_out_core(:,it_core_inequil) = repmat(psi_at_routzout,1,numel(it_core_inequil));
                rhopolnorm_out_core(:,it_core_inequil) = sqrt((psi_out_core(:,it_core_inequil)-equil.psi_axis(itequil))./(equil.psi_lcfs(itequil)-equil.psi_axis(itequil)));
                for it_cx=1:length(it_core_inequil)
                  rhotornorm_out_core(:,it_core_inequil(it_cx)) = ...
                      interpos(equil.rhopolnorm(:,itequil),equil.rhotornorm(:,itequil),rhopolnorm_out_core(:,it_core_inequil(it_cx)),-3,[2 2],[0 1]);
                  rhovolnorm_out_core(:,it_core_inequil(it_cx)) = ...
                      interpos(equil.rhopolnorm(:,itequil),equil.rhovolnorm(:,itequil),rhopolnorm_out_core(:,it_core_inequil(it_cx)),-3,[2 2],[0 1]);
                end
              end
            end
          end
        end
        gdat_data.(extra_source{i}).psi = psi_out_core;
        gdat_data.(extra_source{i}).rhopolnorm = rhopolnorm_out_core;
        gdat_data.(extra_source{i}).rhotornorm = rhotornorm_out_core;
        gdat_data.(extra_source{i}).rhovolnorm = rhovolnorm_out_core;
        % put values of rhopolnorm for dim{1} by default, all radial mesh for combined core, edge in grids_1d
        if i==1
          gdat_data.x = gdat_data.(extra_source{i}).rhopolnorm;
          gdat_data.dim{1} = gdat_data.x;
          gdat_data.dimunits{1} = 'rhopolnorm';
          gdat_data.grids_1d.rhopolnorm = gdat_data.x;
          gdat_data.grids_1d.psi = gdat_data.(extra_source{i}).psi;
          gdat_data.grids_1d.rhotornorm = gdat_data.(extra_source{i}).rhotornorm;
          gdat_data.grids_1d.rhovolnorm = gdat_data.(extra_source{i}).rhovolnorm;

          gdat_data.data_fullpath = [gdat_data.data_fullpath ' projected on equilibrium ' gdat_data.gdat_params.equil];
        end
      end
      %
      % defaults for fits, so user always gets std structure
      gdat_data.fit.rhotornorm = []; % same for both ne and te
      gdat_data.fit.rhopolnorm = [];
      gdat_data.fit.t = [];
      gdat_data.fit.te.data = [];
      gdat_data.fit.te.drhotornorm = [];
      gdat_data.fit.ne.data = [];
      gdat_data.fit.ne.drhotornorm = [];
      gdat_data.fit.raw.te.data = [];
      gdat_data.fit.raw.te.rhotornorm = [];
      gdat_data.fit.raw.ne.data = [];
      gdat_data.fit.raw.ne.rhotornorm = [];
      fit_tension_default = -0.1;
      if isfield(gdat_data.gdat_params,'fit_tension')
        fit_tension = gdat_data.gdat_params.fit_tension;
      else
        fit_tension = fit_tension_default;
      end
      if ~isstruct(fit_tension)
        fit_tension_eff.te = fit_tension;
        fit_tension_eff.ne = fit_tension;
        fit_tension = fit_tension_eff;
      else
        if ~isfield(fit_tension,'te'); fit_tension.te = fit_tension_default; end
        if ~isfield(fit_tension,'ne '); fit_tension.ne = fit_tension_default; end
      end
      gdat_data.gdat_params.fit_tension = fit_tension;
      if isfield(gdat_data.gdat_params,'fit_nb_rho_points')
        fit_nb_rho_points = gdat_data.gdat_params.fit_nb_rho_points;
      else
        fit_nb_rho_points = 201;
      end
      gdat_data.gdat_params.fit_nb_rho_points = fit_nb_rho_points;
      %
      if gdat_data.gdat_params.fit==1
        % add fits
        gdat_data.fit.t = gdat_data.t;
        rhotornormfit = linspace(0,1,fit_nb_rho_points)';
        gdat_data.fit.rhotornorm = rhotornormfit;
        gdat_data.fit.rhopolnorm = NaN*ones(length(rhotornormfit),length(gdat_data.fit.t));
        if any(strfind(data_request_eff,'te'))
          gdat_data.fit.raw.te.data = NaN*ones(size(gdat_data.te.data));
          gdat_data.fit.raw.te.error_bar = NaN*ones(size(gdat_data.te.data));
          gdat_data.fit.raw.te.rhotornorm = NaN*ones(size(gdat_data.te.data));
          gdat_data.fit.te.data = gdat_data.fit.rhopolnorm;
          gdat_data.fit.te.drhotornorm = gdat_data.fit.rhopolnorm;
        end
        if any(strfind(data_request_eff,'ne'))
          gdat_data.fit.raw.ne.data = NaN*ones(size(gdat_data.ne.data));
          gdat_data.fit.raw.ne.error_bar = NaN*ones(size(gdat_data.ne.data));
          gdat_data.fit.raw.ne.rhotornorm = NaN*ones(size(gdat_data.ne.data));
          gdat_data.fit.ne.data =gdat_data.fit.rhopolnorm;
          gdat_data.fit.ne.drhotornorm = gdat_data.fit.rhopolnorm;
        end
        for it=1:length(gdat_data.t)
          % make rhotor->rhopol transformation for each time since equilibrium might have changed
          irhook=find(gdat_data.grids_1d.rhotornorm(:,it)>0 & gdat_data.grids_1d.rhotornorm(:,it)<1); % no need for ~isnan
          [rhoeff isort]=sort(gdat_data.grids_1d.rhotornorm(irhook,it));
          gdat_data.fit.rhopolnorm(:,it)=interpos([0; rhoeff; 1],[0; gdat_data.grids_1d.rhopolnorm(irhook(isort),it); 1],rhotornormfit,-0.1,[2 2],[0 1]);
          if any(strfind(data_request_eff,'te'))
            idatate = find(gdat_data.te.data(:,it)>0 & gdat_data.grids_1d.rhotornorm(:,it)<=1.05);
            if numel(idatate) >= 1
              gdat_data.fit.raw.te.rhotornorm(idatate,it) = gdat_data.grids_1d.rhotornorm(idatate,it);
              gdat_data.fit.raw.te.data(idatate,it) = gdat_data.te.data(idatate,it);
              gdat_data.fit.raw.te.error_bar(idatate,it) = gdat_data.te.error_bar(idatate,it);
              [rhoeff,irhoeff] = sort(gdat_data.grids_1d.rhotornorm(idatate,it));
              rhoeff = [0; rhoeff];
              teeff = gdat_data.te.data(idatate(irhoeff),it);
              te_err_eff = gdat_data.te.error_bar(idatate(irhoeff),it);
              % they are some strange low error_bars, so remove these by max(Te/error_bar)<=10; and changing it to large error bar
              ij=find(teeff./te_err_eff>10.);
              if ~isempty(ij); te_err_eff(ij) = teeff(ij)./0.1; end
              %
              teeff =  [teeff(1); teeff];
              te_err_eff =  [1e4; te_err_eff];
              [gdat_data.fit.te.data(:,it), gdat_data.fit.te.drhotornorm(:,it)] = interpos(rhoeff,teeff,rhotornormfit,fit_tension.te,[1 0],[0 0],te_err_eff);
            end
          end
          if any(strfind(data_request_eff,'ne'))
            idatane = find(gdat_data.ne.data(:,it)>0 & gdat_data.grids_1d.rhotornorm(:,it)<=1.05);
            if numel(idatane)>= 1
              gdat_data.fit.raw.ne.rhotornorm(idatane,it) = gdat_data.grids_1d.rhotornorm(idatane,it);
              gdat_data.fit.raw.ne.data(idatane,it) = gdat_data.ne.data(idatane,it);
              gdat_data.fit.raw.ne.error_bar(idatane,it) = gdat_data.ne.error_bar(idatane,it);
              [rhoeff,irhoeff] = sort(gdat_data.grids_1d.rhotornorm(idatane,it));
              rhoeff = [0; rhoeff];
              % they are some strange low error_bars, so remove these by max(Te/error_bar)<=10; and changing it to large error bar
              neeff = gdat_data.ne.data(idatane(irhoeff),it);
              ne_err_eff = gdat_data.ne.error_bar(idatane(irhoeff),it);
              ij=find(neeff./ne_err_eff>10.);
              if ~isempty(ij); ne_err_eff(ij) = neeff(ij)./0.1; end
              %
              neeff =  [neeff(1); neeff];
              ne_err_eff =  [1e21; ne_err_eff];
              [gdat_data.fit.ne.data(:,it), gdat_data.fit.ne.drhotornorm(:,it)] = interpos(rhoeff,neeff,rhotornormfit,fit_tension.ne,[1 0],[0 0],ne_err_eff);
            end
          end
        end
      end


      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'pgyro'}
      %  LOAD MULTI CHANNEL DATA ECS
      %  powers, frequencies, etc
      params_eff = gdat_data.gdat_params;
      params_eff.data_request={'rf'  '\echpwrc'};
% $$$     gyro_names={'leia','luke','scarecrow','tinman','chewbacca','nasa'};
% $$$     power_names={'ecleifpwrc','eclukfpwrc','ecscafpwrc','ectinfpwrc','ecchefpwrc','ecnasfpwrc'};
      gyro_names={'leia','luke','r2d2','yoda','han'};
      power_names={'ecleifpwrc','eclukfpwrc','ecr2dfpwrc','ecyodfpwrc','echanfpwrc'};
      % pgyro tot in index=length(gyro_names)+1
      try
        gdat_data=gdat_d3d(shot,params_eff);
        gdat_data.data_request = data_request_eff;
        gdat_data.gdat_params.data_request = data_request_eff;
      catch
        if gdat_params.nverbose>=3; disp(['problems with ' params_eff.data_request]); end
        gdat_data.data_request = data_request_eff;
        return
      end
      nb_timepoints = length(gdat_data.t);
      pgyro = NaN*ones(nb_timepoints,length(gyro_names)+1);
      pgyro(:,length(gyro_names)+1) = reshape(gdat_data.data,nb_timepoints,1);
      gdat_data.data = pgyro;
      labels{length(gyro_names)+1} = 'ECtot';
      for i=1:length(gyro_names)
        % "old" ECRH1 gyrotrons: gyro 1 to 4 in pgyro
        params_eff.data_request={'rf',['ech.' gyro_names{i} ':' power_names{i}]};
        gdat_data_i=gdat_d3d(shot,params_eff);
        if isempty(gdat_data_i.data) || isempty(gdat_data_i.dim)
        else
          gdat_data.ec{i} = gdat_data_i;
          gdat_data.data(:,i) = reshape(gdat_data_i.data,nb_timepoints,1);
          gdat_data.dim = [{gdat_data_i.t} {[1:length(gyro_names)+1]}];
          if max(gdat_data_i.data) > 0.
            % labels{i} = [gyro_names{i} ':' power_names{i}];
            labels{i} = [gyro_names{i}];
          end
        end
        try
          params_eff.data_request={'rf',['ech.' gyro_names{i} ':ec' gyro_names{i}(1:3) 'polang']};
          a=gdat_d3d(shot,params_eff);
        catch
          % polang not present
          a=[];
        end
        if isempty(a)
        else
          gdat_data.ec{i}.polang_t = a.t;
          gdat_data.ec{i}.polang = a.data;
          gdat_data.polang_ec{i} = a;
        end
        try
          params_eff.data_request={'rf',['ech.' gyro_names{i} ':ec' gyro_names{i}(1:3) 'polcnt']};
          a=gdat_d3d(shot,params_eff);
        catch
          a=[];
        end
        if isempty(a)
        else
          gdat_data.ec{i}.polcnt = a.data;
          gdat_data.polcnt_ec{i} = a;
        end
        try
          params_eff.data_request={'rf',['ech.' gyro_names{i} ':ec' gyro_names{i}(1:3) 'torcnt']};
          a=gdat_d3d(shot,params_eff);
        catch
          a=[];
        end
        if isempty(a)
        else
          gdat_data.ec{i}.torcnt = a.data;
          gdat_data.torcnt_ec{i} = a;
        end
      end
      gdat_data.gyro_names = gyro_names;
      gdat_data.power_names = power_names;
      % add ech on time_interval from total power>3% of max;
      ij=find(gdat_data.data(:,end)>0.03.*max(gdat_data.data(:,end)));
      if ~isempty(ij)
        gdat_data.ec_t_on = [gdat_data.t(ij(1)) gdat_data.t(ij(end))];
      else
        gdat_data.ec_t_on = [0 0];
      end
      if ~isempty(gdat_data.dim)
        gdat_data.t = gdat_data.dim{1};
        gdat_data.x = gdat_data.dim{2};
        gdat_data.dimunits=[{'time [ms]'} gyro_names{:} {'ECtot'}];
        gdat_data.units='W';
        gdat_data.data_fullpath=['rf::ech.gyro_names:ec..pwrc'];
        icount=0;
        for i=1:length(labels)
          if ~isempty(labels{i})
            icount=icount+1;
            if icount==1 & ~iscell(gdat_data.label)
              gdat_data = rmfield(gdat_data,'label');
            end
            gdat_data.label{icount} = labels{i};
          end
        end
      else
        gdat_data.freq_ech_units =[]';
        gdat_data.ec = [];
        gdat_data.polpos_ec = [];
        gdat_data.torpos_ec = [];
      end


      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'powers'}
      sources_avail = {'ohm','ec','nbi','ic'}; % note should allow ech, nbh, ohmic in parameter sources
      sources_avail = {'ohm','ec','nbi'}; % note should allow ech, nbh, ohmic in parameter sources
      for i=1:length(sources_avail)
        gdat_data.(sources_avail{i}).data = [];
        gdat_data.(sources_avail{i}).units = [];
        gdat_data.(sources_avail{i}).dim=[];
        gdat_data.(sources_avail{i}).dimunits=[];
        gdat_data.(sources_avail{i}).t=[];
        gdat_data.(sources_avail{i}).x=[];
        gdat_data.(sources_avail{i}).data_fullpath=[];
        gdat_data.(sources_avail{i}).label=[];
      end
      if ~isfield(gdat_data.gdat_params,'source') || isempty(gdat_data.gdat_params.source)
        gdat_data.gdat_params.source = sources_avail;
      elseif ~iscell(gdat_data.gdat_params.source)
        if ischar(gdat_data.gdat_params.source)
          gdat_data.gdat_params.source = lower(gdat_data.gdat_params.source);
          if ~any(strmatch(gdat_data.gdat_params.source,lower(sources_avail)))
            if (gdat_params.nverbose>=1)
              warning(['source= ' gdat_data.gdat_params.source ' is not part of the available sources: ' sprintf('''%s'' ',sources_avail{:})]);
            end
            return
          else
            gdat_data.gdat_params.source = {gdat_data.gdat_params.source};
          end
        else
          if (gdat_params.nverbose>=1); warning([' source parameter not compatible with: ' sprintf('''%s'' ',sources_avail{:})]); end
          return
        end
      else
        for i=1:length(gdat_data.gdat_params.source)
          gdat_data.gdat_params.source{i} = lower(gdat_data.gdat_params.source{i});
          if ~any(strmatch(gdat_data.gdat_params.source{i},lower(sources_avail)))
            if gdat_data.gdat_params.nverbose>=1
              warning(['source = ' gdat_data.gdat_params.source{i} ' not expected with data_request= ' data_request_eff])
            end
          end
        end
      end
      % always start from ohmic so can use this time as base time since should yield full shot

      fields_to_copy = {'data','units','dim','dimunits','t','x','data_fullpath','label','help','gdat_params'};
      fields_to_not_copy = {'shot','gdat_request'};
      % total of each source in .data, but full data in subfield like pgyro in .ec, to check for nbi
      params_eff = gdat_data.gdat_params;
      % ohmic, use its time-base
      params_eff.data_request={'EFIT01','\vloopmhd'}; %poh too noisy even if vloop*ip is not quite correct
      try
        ohm=gdat_d3d(shot,params_eff);
      catch
        ohm.data = [];
        ohm.dim = [];
      end
      if ~isempty(ohm.data) && ~isempty(ohm.dim)
        ip=gdat_d3d(shot,'ip');
        ip_ohm=interp1(ip.t,ip.data,ohm.t);
        ohm.data = ohm.data .* ip_ohm;
        for i=1:length(fields_to_copy)
          if isfield(ohm,fields_to_copy{i})
            gdat_data.ohm.(fields_to_copy{i}) = ohm.(fields_to_copy{i});
          end
        end
        gdat_data.ohm.raw_data = gdat_data.ohm.data;
      else
        if gdat_params.nverbose>=3; disp(['problems with ' params_eff.data_request]); end
        return
      end
      mapping_for_d3d.timedim = 1; mapping_for_d3d.gdat_timedim = 1;
      taus = -10;
      %
      % add each source in main.data, on ohm time array
      gdat_data.t = gdat_data.ohm.t;
      gdat_data.dim{1} = gdat_data.t;
      gdat_data.dimunits{1} = 's';
      gdat_data.ohm.data = interpos(gdat_data.t,gdat_data.ohm.raw_data,5.*taus);
      gdat_data.data = reshape(gdat_data.ohm.data,length(gdat_data.t),1);
      gdat_data.ohm.tension = 5.*taus;
      gdat_data.x =[1];
      gdat_data.label={'P_{ohm}'};
      gdat_data.units = 'W';
      %
      if any(strmatch('ec',gdat_data.gdat_params.source))
        % ec
        params_eff.data_request='pgyro';
        try
          ec=gdat_d3d(shot,params_eff);
        catch
        end
        if ~isempty(ec.data) && ~isempty(ec.dim)
          for i=1:length(fields_to_copy)
            % if has pgyro, use not_copy
            if isfield(ec,fields_to_copy{i}) && ~any(strmatch(fields_to_not_copy,fields_to_copy{i}))
              gdat_data.ec.(fields_to_copy{i}) = ec.(fields_to_copy{i});
            end
          end
          gdat_data.data(:,end+1) = interpos(-21,gdat_data.ec.t,gdat_data.ec.data(:,end),gdat_data.t);
          gdat_data.x(end+1) =gdat_data.x(end)+1;
          gdat_data.label{end+1}='P_{ec}';
        end
      end
      %
      if any(strmatch('nb',gdat_data.gdat_params.source))
        % nbi
        params_eff.data_request='nbi';
        try
          nbi=gdat_d3d(shot,params_eff);
        catch
        end
        if ~isempty(nbi.data) && ~isempty(nbi.dim)
          for i=1:length(fields_to_copy)
            if isfield(nbi,fields_to_copy{i})
              gdat_data.nbi.(fields_to_copy{i}) = nbi.(fields_to_copy{i});
            end
          end
          % add to main with linear interpolation and 0 for extrapolated values
          gdat_data.data(:,end+1) = interpos(-21,gdat_data.nbi.t,gdat_data.nbi.data(:,end),gdat_data.t);
          gdat_data.x(end+1) =gdat_data.x(end)+1;
          gdat_data.label{end+1}='P_{nbi}';
        end
      end
      %
      if any(strmatch('ic',gdat_data.gdat_params.source))
        % ic
        params_eff.data_request={'ICP','PICRN'};
        try
          ic=gdat_d3d(shot,params_eff);
        catch
        end
        if ~isempty(ic.data) && ~isempty(ic.dim)
          for i=1:length(fields_to_copy)
            if isfield(ic,fields_to_copy{i})
              gdat_data.ic.(fields_to_copy{i}) = ic.(fields_to_copy{i});
            end
          end
          gdat_data.data(:,end+1) = interpos(-21,gdat_data.ic.t,gdat_data.ic.data,gdat_data.t);
          gdat_data.x(end+1) =gdat_data.x(end)+1;
          gdat_data.label{end+1}='P_{ic}';
        end
      end
      % add tot power
      gdat_data.data(:,end+1) = sum(gdat_data.data,2);
      gdat_data.label{end+1}='P_{tot}';
      gdat_data.x(end+1) =gdat_data.x(end)+1;
      gdat_data.dim{2} = gdat_data.x; gdat_data.dimunits{2} = '';
      gdat_data.data_fullpath = 'tot powers from each sources, and total power in .data(:,end)';

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'q_rho'}
      [gdat_data,exp_name_eff,DIAG,NTIME_Lpf,NTIME,Lpf1_t,Lpf_SOL] = get_EQ_params(gdat_data);
      % since Lpf depends on time, need to load all first and then loop over time for easier mapping
      [qpsi,e]=rdaD3D_eff(shot,DIAG,'Qpsi',exp_name_eff);
      ndimrho = size(qpsi.data,2);
      if ndimrho==NTIME_Lpf
        % data seems to be transposed
        ndimrho = size(qpsi.data,1);
        itotransposeback = 1; % seems x,time inverted so transpose and exchange .x and .t
      else
        itotransposeback = 0;
      end
      qpsi=adapt_rda(qpsi,NTIME,ndimrho,itotransposeback);
      ijnan=find(isnan(qpsi.value));
      qpsi.value(ijnan)=0;
      [psi_tree,e]=rdaD3D_eff(shot,DIAG,'PFL',exp_name_eff);
      psi_tree=adapt_rda(psi_tree,NTIME,ndimrho,itotransposeback);
      [phi_tree,e]=rdaD3D_eff(shot,DIAG,'TFLx',exp_name_eff);
      phi_tree=adapt_rda(phi_tree,NTIME,ndimrho,itotransposeback);
      [Vol,e]=rdaD3D_eff(shot,DIAG,'Vol',exp_name_eff);
      Vol=adapt_rda(Vol,NTIME,2*ndimrho,itotransposeback);
      % seems "LCFS" q-value is far too large, limit to some max (when diverted)
      max_qValue = 30.; % Note could just put a NaN on LCFS value since ill-defined when diverted
      for it=1:NTIME
        Lpf1 = Lpf1_t(it);
        % Qpsi and similar data is on (time,radius) with radius being: LCFS..Lpf_points dummy LCFS..SOL part
        % change it to (radial,time) and use only Lpf+1 points up to LCFS
        ijok=find(qpsi.value(:,1)); % note: eqr fills in only odd points radially
        % set NaNs to zeroes
        if qpsi.value(ijok(1),1)<0
          gdat_data.qvalue(:,it) = max(qpsi.value(it,Lpf1:-1:1)',-max_qValue);
        else
          gdat_data.qvalue(:,it) = min(qpsi.value(it,Lpf1:-1:1)',max_qValue);
        end
        % get x values
        psi_it=psi_tree.value(it,Lpf1:-1:1)';
        gdat_data.psi_axis(it)= psi_it(1);
        gdat_data.psi_lcfs(it)= psi_it(end);
        gdat_data.rhopolnorm(:,it) = sqrt(abs((psi_it-gdat_data.psi_axis(it)) ./(gdat_data.psi_lcfs(it)-gdat_data.psi_axis(it))));
        if strcmp(DIAG,'EQR');
          % q value has only a few values and from center to edge, assume they are from central rhopol values on
          % But they are every other point starting from 3rd
          ijk=find(gdat_data.qvalue(:,it)~=0);
          if length(ijk)>2
            % now shots have non-zero axis values in eqr
            rhoeff=gdat_data.rhopolnorm(ijk,it);
            qeff=gdat_data.qvalue(ijk,it); % radial order was already inverted above
            if ijk(1)>1
              rhoeff = [0.; rhoeff];
              qeff = [qeff(1) ;qeff];
            end
            ij_nonan=find(~isnan(gdat_data.rhopolnorm(:,it)));
            qfit = zeros(size(gdat_data.rhopolnorm(:,it)));
            qfit(ij_nonan)=interpos(rhoeff,qeff,gdat_data.rhopolnorm(ij_nonan,it),-0.01,[1 0],[0 0],[300; ones(size(qeff(1:end-1)))]);
          else
            qfit = zeros(size(gdat_data.rhopolnorm(:,it)));
          end
          gdat_data.qvalue(:,it) = qfit;
        end
        % get rhotor values
        phi_it = phi_tree.value(it,Lpf1:-1:1)';
        gdat_data.rhotornorm(:,it) = sqrt(abs(phi_it ./ phi_it(end)));
        % get rhovol values
        vol_it=Vol.value(it,2*Lpf1-1:-2:1)';
        gdat_data.rhovolnorm(:,it) = sqrt(abs(vol_it ./ vol_it(end)));
        % Qpsi and similar data is on (time,radius) with radius being: LCFS..Lpf_points dummy LCFS..SOL part
        % change it to (radial,time) and use only Lpf+1 points up to LCFS
        ijok=find(qpsi.value(:,1)); % note: eqr fills in only odd points radially
        % max value 1e6, change to 2*extrapolation (was max_qValue before)
        q_edge=interpos(gdat_data.rhotornorm(1:end-1,it),qpsi.value(it,Lpf1:-1:2),1,-0.1);
        gdat_data.qvalue(:,it) = qpsi.value(it,Lpf1:-1:1)';
        if abs(gdat_data.qvalue(end,it)) > 1e3
          % assume diverted
          gdat_data.qvalue(end,it) = 2. * q_edge;
        end
% $$$       if qpsi.value(ijok(1),1)<0
% $$$         gdat_data.qvalue(:,it) = max(qpsi.value(it,Lpf1:-1:1)',-max_qValue);
% $$$       else
% $$$         gdat_data.qvalue(:,it) = min(qpsi.value(it,Lpf1:-1:1)',max_qValue);
% $$$       end
      end
      gdat_data.x = gdat_data.rhopolnorm;
      % get time values
      gdat_data.data = gdat_data.qvalue; % put q in data
      gdat_data.units=[]; % not applicable
      gdat_data.data_fullpath = [DIAG ' from expname: ' gdat_data.gdat_params.exp_name '; q(rhopolnorm,t) in .data(.x,.t)'];
      gdat_data.cocos = 17; % should check FPP
      gdat_data.dim{1} = gdat_data.x;
      gdat_data.dim{2} = gdat_data.t;
      gdat_data.dimunits{1} = 'rhopolnorm';
      gdat_data.dimunits{2} = 'time [s]';

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'psi_axis', 'psi_edge'}
      if strcmp(upper(gdat_data.gdat_params.equil),'FPG')
        gdat_params_prev = gdat_data.gdat_params; gdat_params_eff = gdat_params_prev;
        gdat_params_eff.data_request = [{'FPG'},{'fax-bnd'},{'D3DD'}];
        gdat_data = gdat_d3d(gdat_data.shot,gdat_params_eff);
        gdat_data.gdat_params = gdat_params_prev;
        gdat_data.label = 'psi\_axis-psi\_edge';
        gdat_data.data_fullpath = [gdat_data.data_fullpath ' yields only psi\_axis-psi\_edge from FPG/fax-bnd'];
      else
        [gdat_data,exp_name_eff,DIAG,NTIME_Lpf,NTIME,Lpf1_t,Lpf_SOL] = get_EQ_params(gdat_data);
        % since Lpf depends on time, need to load all first and then loop over time for easier mapping
        [psi_tree,e]=rdaD3D_eff(shot,DIAG,'PFL',exp_name_eff);
        ndimrho = size(psi_tree.data,2);
        if ndimrho==NTIME_Lpf
          % data seems to be transposed
          ndimrho = size(psi_tree.data,1);
          itotransposeback = 1; % seems x,time inverted so transpose and exchange .x and .t
        else
          itotransposeback = 0;
        end
        psi_tree=adapt_rda(psi_tree,NTIME,ndimrho,itotransposeback);
        ijnan=find(isnan(psi_tree.value));
        psi_tree.value(ijnan)=0;
        gdat_data.dim{1} = gdat_data.t;
        gdat_data.dimunits{1} = 'time [s]';
        for it=1:NTIME
          Lpf1 = Lpf1_t(it);
          psi_it=psi_tree.value(it,Lpf1:-1:1)';
          if strcmp(data_request_eff,'psi_axis')
            gdat_data.data(it)= psi_it(1);
          elseif strcmp(data_request_eff,'psi_edge')
            gdat_data.data(it)= psi_it(end);
          else
          end
        end
        gdat_data.units = psi_tree.units;
        gdat_data.data_fullpath = [DIAG '/PFL extract ' data_request_eff];
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'rot', 'cerqrot'}
      nth_points = 13;
      if isfield(gdat_data.gdat_params,'nth_points') && ~isempty(gdat_data.gdat_params.nth_points)
        nth_points = gdat_data.gdat_params.nth_points;
      else
        gdat_data.gdat_params.nth_points = nth_points;
      end
      channels = -1;
      if isfield(gdat_data.gdat_params,'channels') && ~isempty(gdat_data.gdat_params.channels)
        channels = gdat_data.gdat_params.channels;
      end
      diag_name = 'cerqrot';
      if nth_points>=10
        match_rz_to_time = 1;
      else
        match_rz_to_time = 0;
      end
      if isfield(gdat_data.gdat_params,'match_rz_to_time') && ~isempty(gdat_data.gdat_params.match_rz_to_time)
        match_rz_to_time = gdat_data.gdat_params.match_rz_to_time;
      else
        gdat_data.gdat_params.match_rz_to_time = match_rz_to_time;
      end
      time_interval = [-Inf +Inf];
      if isfield(gdat_data.gdat_params,'time_interval') && ~isempty(gdat_data.gdat_params.time_interval)
        time_interval = gdat_data.gdat_params.time_interval;
      else
        gdat_data.gdat_params.time_interval = time_interval;
      end
      %
      if channels(1)<=0
        channels = [1:24];
      end
      chanelles=sort(channels);
      gdat_data.dim{1} = channels;
      gdat_data.gdat_params.channels = channels;
      max_points = 0;
      for i=channels
        aall{i} = gdat_d3d(shot,['\cerqrott' num2str(i)]);
        if isnumeric(aall{i}.data)
          gdat_data.channels_ok(i) = 1;
          if numel(aall{i}.data) > max_points
            max_points = numel(aall{i}.data);
            gdat_data.t = aall{i}.t;
          end
        else
          gdat_data.channels_ok(i) = 0;
        end
      end
      gdat_data.dim{2} = gdat_data.t;
      gdat_data.data = NaN(max(channels),max_points);
      for i=channels
        if gdat_data.channels_ok(i)
          if nth_points>1
            ieff = [1:nth_points:numel(aall{i}.data)];
            iteff = iround_os(gdat_data.t,aall{i}.t(ieff));
            gdat_data.data(i,iteff) = interp1(aall{i}.t,aall{i}.data,gdat_data.t(iteff));
          else
            iteff = iround_os(gdat_data.t,aall{i}.t);
            gdat_data.data(i,iteff) = interp1(aall{i}.t,aall{i}.data,gdat_data.t(iteff));
          end
        end
      end
      gdat_data.x = gdat_data.dim{1};
      gdat_data.dimunits=[{'channels'} ; {'time [s]'}];
      %if any(strcmp(fieldnames(a),'units')); gdat_data.units=a.units; end
      gdat_data.data_fullpath = [diag_name '\tcerqrot' num2str(channels(1)) '...' num2str(channels(end))];
      gdat_data.allchannels = aall;

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'rhotor', 'rhotor_edge', 'rhotor_norm', 'rhovol', 'volume_rho'}
      if strcmp(upper(gdat_data.gdat_params.equil),'FPG')
        gdat_params_prev = gdat_data.gdat_params; gdat_params_eff = gdat_params_prev;
        gdat_params_eff.data_request = [{'FPG'},{'Vol'},{'D3DD'}];
        gdat_data = gdat_d3d(gdat_data.shot,gdat_params_eff);
        gdat_data.gdat_params = gdat_params_prev;
        gdat_data.label = 'Vol';
        gdat_data.data_fullpath = [gdat_data.data_fullpath ' yields only edge Volume from FPG/Vol'];
        return
      end
      [gdat_data,exp_name_eff,DIAG,NTIME_Lpf,NTIME,Lpf1_t,Lpf_SOL] = get_EQ_params(gdat_data);
      % since Lpf depends on time, need to load all first and then loop over time for easier mapping
      [phi_tree,e]=rdaD3D_eff(shot,DIAG,'TFLx',exp_name_eff);
      ndimrho = size(phi_tree.data,2);
      if ndimrho==NTIME_Lpf
        % data seems to be transposed
        ndimrho = size(phi_tree.data,1);
        itotransposeback = 1; % seems x,time inverted so transpose and exchange .x and .t
      else
        itotransposeback = 0;
      end
      phi_tree=adapt_rda(phi_tree,NTIME,ndimrho,itotransposeback);
      ijnan=find(isnan(phi_tree.value));
      phi_tree.value(ijnan)=0;
      [Vol,e]=rdaD3D_eff(shot,DIAG,'Vol',exp_name_eff);
      Vol=adapt_rda(Vol,NTIME,2*ndimrho,itotransposeback);
      [psi_tree,e]=rdaD3D_eff(shot,DIAG,'PFL',exp_name_eff);
      psi_tree=adapt_rda(psi_tree,NTIME,ndimrho,itotransposeback);
      %
      switch data_request_eff
        case {'volume_rho', 'rhovol'}
          for it=1:NTIME
            Lpf1 = Lpf1_t(it);
            psi_it=psi_tree.value(it,Lpf1:-1:1)';
            psi_axis(it)= psi_it(1);
            psi_lcfs(it)= psi_it(end);
            gdat_data.x(:,it) = sqrt(abs((psi_it-psi_axis(it)) ./(psi_lcfs(it)-psi_axis(it))));
            gdat_data.vol(:,it)=Vol.value(it,2*Lpf1-1:-2:1)';
            % gdat_data.dvoldpsi(:,it)=Vol.value(it,2*Lpf1:-2:2)'; % 2nd index are dV/dpsi
            gdat_data.rhovolnorm(:,it) = sqrt(abs(gdat_data.vol(:,it) ./ gdat_data.vol(end,it)));
          end
          gdat_data.dim{1} = gdat_data.x;
          gdat_data.dim{2} = gdat_data.t;
          gdat_data.dimunits{1} = 'rhopolnorm';
          gdat_data.dimunits{2} = 'time [s]';
          if strcmp(data_request_eff,'volume_rho')
            gdat_data.data = gdat_data.vol;
            gdat_data.units = Vol.units;
          else strcmp(data_request_eff,'rhovol')
            gdat_data.data = gdat_data.rhovolnorm;
            gdat_data.units = ' ';
          end
        case {'rhotor', 'rhotor_edge', 'rhotor_norm'}
          b0=gdat(shot,'b0');
          gdat_data.b0 = interpos(b0.t,b0.data,gdat_data.t,-1);
          for it=1:NTIME
            Lpf1 = Lpf1_t(it);
            gdat_data.phi(:,it) = phi_tree.value(it,Lpf1:-1:1)';
            gdat_data.rhotornorm(:,it) = sqrt(abs(gdat_data.phi(:,it) ./ gdat_data.phi(end,it)));
            gdat_data.rhotor(:,it) = sqrt(abs(gdat_data.phi(:,it) ./ gdat_data.b0(it) ./ pi));
            psi_it=psi_tree.value(it,Lpf1:-1:1)';
            psi_axis(it)= psi_it(1);
            psi_lcfs(it)= psi_it(end);
            gdat_data.x(:,it) = sqrt(abs((psi_it-psi_axis(it)) ./(psi_lcfs(it)-psi_axis(it))));
          end
          if strcmp(data_request_eff,'rhotor')
            gdat_data.data = gdat_data.rhotor;
            gdat_data.units = 'm';
            gdat_data.dim{1} = gdat_data.x;
            gdat_data.dim{2} = gdat_data.t;
            gdat_data.dimunits{1} = 'rhopolnorm';
            gdat_data.dimunits{2} = 'time [s]';
          elseif strcmp(data_request_eff,'rhotor_edge')
            gdat_data.data = gdat_data.rhotor(end,:);
            gdat_data.units = 'm';
            gdat_data.dim{1} = gdat_data.t;
            gdat_data.dimunits{1} = 'time [s]';
          elseif strcmp(data_request_eff,'rhotor_norm')
            gdat_data.data = gdat_data.rhotornorm;
            gdat_data.units = ' ';
            gdat_data.dim{1} = gdat_data.x;
            gdat_data.dim{2} = gdat_data.t;
            gdat_data.dimunits{1} = 'rhopolnorm';
            gdat_data.dimunits{2} = 'time [s]';
          end
      end
      gdat_data.data_fullpath = [DIAG '/PFL extract in .data: ' data_request_eff];

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'sxr'}
      % sxr from sx90rm1s by default or else if 'source' is provided
      if ~isfield(gdat_data.gdat_params,'freq')|| isempty(gdat_data.gdat_params.freq)
        gdat_data.gdat_params.freq = 1;
      end
      if ~isfield(gdat_data.gdat_params,'source') || isempty(gdat_data.gdat_params.source)
        gdat_data.gdat_params.source = 'sx90rm1s';
      end
      if ~isfield(gdat_data.gdat_params,'camera') || isempty(gdat_data.gdat_params.camera)
        gdat_data.gdat_params.camera = [1:28];
      end
      gdat_data.gdat_params.source = upper(gdat_data.gdat_params.source);
      %
      if ~isfield(gdat_data.gdat_params,'time_interval')
        gdat_data.gdat_params.time_interval = [];
      end
      exp_name_eff = 'D3D';
      icount = 0;
      nnth = 1;
      if isnumeric(gdat_data.gdat_params.freq) && gdat_data.gdat_params.freq>1;
        nnth = floor(gdat_data.gdat_params.freq+0.5);
        gdat_data.gdat_params.freq = nnth;
      end
      for i=1:length(gdat_data.gdat_params.camera)
        tree = 'spectroscopy';
        ichord = gdat_data.gdat_params.camera(i);
        diagname = ['sxr:' gdat_data.gdat_params.source ':' gdat_data.gdat_params.source num2str(ichord,'%.2d')];
        a = gdat_d3d(shot,{tree,diagname});
        if ~isempty(a.data)
          icount = icount + 1;
          if icount == 1
            % first time has data
            gdat_data.t = a.t(1:nnth:end);
            gdat_data.units = a.units;
            gdat_data.data = NaN*ones(max(gdat_data.gdat_params.camera),length(gdat_data.t));
            gdat_data.x = [1:max(gdat_data.gdat_params.camera)]; % simpler to have index corresponding to chord number, except for central
            gdat_data.dim{1} = gdat_data.x;
            gdat_data.dim{2} = gdat_data.t;
            gdat_data.dimunits = [{'chord nb'}; {'s'}];
            gdat_data.data_fullpath = ['sxr from source=''' gdat_data.gdat_params.source ''''];
            gdat_data.label = ['SXR/' upper(gdat_data.gdat_params.source)];
          end
          try
            gdat_data.data(ichord,:) = a.data(1:nnth:end);
          catch
            if (gdat_params.nverbose>=1); disp(['problem with ' gdat_data.gdat_params.source '...' num2str(ichord)]); end
          end
        else
          % add fields not yet defined in case all cases have empty data
        end
      end
      gdat_data.chords = gdat_data.gdat_params.camera;

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'bolo', 'bolom'}
      % sxr from sx90rm1s by default or else if 'source' is provided
      if ~isfield(gdat_data.gdat_params,'freq')|| isempty(gdat_data.gdat_params.freq)
        gdat_data.gdat_params.freq = 1;
      end
      if ~isfield(gdat_data.gdat_params,'source') || isempty(gdat_data.gdat_params.source)
        gdat_data.gdat_params.source = 'BOL_L';
      end
      if ~isfield(gdat_data.gdat_params,'camera') || isempty(gdat_data.gdat_params.camera)
        gdat_data.gdat_params.camera = [1:24];
      end
      gdat_data.gdat_params.source = upper(gdat_data.gdat_params.source);
      %
      if ~isfield(gdat_data.gdat_params,'time_interval')
        gdat_data.gdat_params.time_interval = [];
      end
      exp_name_eff = 'D3D';
      icount = 0;
      nnth = 1;
      if isnumeric(gdat_data.gdat_params.freq) && gdat_data.gdat_params.freq>1;
        nnth = floor(gdat_data.gdat_params.freq+0.5);
        gdat_data.gdat_params.freq = nnth;
      end
      tree = 'spectroscopy';
      gdat_data.gdat_params.tree = tree;
      for i=1:length(gdat_data.gdat_params.camera)
        ichord = gdat_data.gdat_params.camera(i);
        diagname = ['PRAD.BOLOM.PRAD_01.POWER:' gdat_data.gdat_params.source num2str(ichord,'%.2d') '_P'];
        a = gdat_d3d(shot,{tree,diagname});
        if ~isempty(a.data)
          icount = icount + 1;
          if icount == 1
            % first time has data
            gdat_data.t = a.t(1:nnth:end);
            gdat_data.units = a.units;
            gdat_data.data = NaN*ones(max(gdat_data.gdat_params.camera),length(gdat_data.t));
            gdat_data.x = [1:max(gdat_data.gdat_params.camera)]; % simpler to have index corresponding to chord number, except for central
            gdat_data.dim{1} = gdat_data.x;
            gdat_data.dim{2} = gdat_data.t;
            gdat_data.dimunits = [{'chord nb'}; {'s'}];
            gdat_data.data_fullpath = ['bolo from source=''' gdat_data.gdat_params.source ''''];
            gdat_data.label = ['PRAD.BOLOM.PRAD\_01.POWER:' upper(gdat_data.gdat_params.source)];
          end
          try
            gdat_data.data(ichord,:) = a.data(1:nnth:end);
          catch
            if (gdat_params.nverbose>=1); disp(['problem with ' gdat_data.gdat_params.source '...' num2str(ichord)]); end
          end
        else
          % add fields not yet defined in case all cases have empty data
        end
      end
      gdat_data.chords = gdat_data.gdat_params.camera;

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'transp'}
      % most of the times the exp for the shotfile should be provided
      shotfile_exp_eff = gdat_params.exp_name;
      diagname='TRA';
      TRANSP_signals;
      for i=1:size(transp_sig,1)
        if strcmp(lower(transp_sig{i,2}),'signal') || strcmp(lower(transp_sig{i,2}),'signal-group')
          try
            eval(['[gdat_data.' transp_sig{i,1} ',e]=rdaD3D_eff(shot,diagname,''' transp_sig{i,1} ''',shotfile_exp_eff);']);
          catch
            eval(['gdat_data.' transp_sig{i,1} '=[];']);
          end
        elseif strcmp(lower(transp_sig{i,2}),'area-base')
          clear adata_area
          try
            [adata_area]=sf2ab(diagname,shot,transp_sig{i,1},'-exp',shotfile_exp_eff);
          catch
            adata_area.value = cell(0);
          end
          eval(['gdat_data.' transp_sig{i,1} '=adata_area;']);
        elseif strcmp(lower(transp_sig{i,2}),'time-base')
          clear adata_time
          try
            [adata_time]=sf2tb(diagname,shot,transp_sig{i,1},'-exp',shotfile_exp_eff);
          catch
            adata_time.value = cell(0);
          end
          eval(['gdat_data.' transp_sig{i,1} '=adata_time;']);
        end
      end
      % copy TIME to .t
      if isfield(gdat_data,'TIME') && isfield(gdat_data.TIME,'value')
        gdat_data.t = gdat_data.TIME.value;
        gdat_data.dim{1} = gdat_data.t,
        gdat_data.dimunits{1} = gdat_data.TIME.unit;
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'zeff_cerq', 'zeff_cer'}
      % zeff from specific cer lines, C, N, ...
      if ~isfield(gdat_data.gdat_params,'camera') || isempty(gdat_data.gdat_params.camera)
        gdat_data.gdat_params.camera = [9 18 20 41];
      end
      gdat_data.label = {};
      for i=1:numel(gdat_data.gdat_params.camera)
        gdat_data.label{i} = ['cerqzefft' num2str(gdat_data.gdat_params.camera(i))];
      end
      %
      if ~isfield(gdat_data.gdat_params,'time_interval')
        gdat_data.gdat_params.time_interval = [];
      end
      icount = 0;
      for i=1:length(gdat_data.gdat_params.camera)
        ichord = gdat_data.gdat_params.camera(i);
        diagname = ['\' gdat_data.label{i}];
        a = gdat_d3d(shot,diagname);
        if ~isempty(a.data) && isnumeric(a.data)
          icount = icount + 1;
          if icount == 1
            % first time has data
            gdat_data.t = a.t;
            gdat_data.units = a.units;
            gdat_data.data = NaN*ones(numel(gdat_data.gdat_params.camera),length(gdat_data.t));
            gdat_data.data(icount,:) = a.data;
            gdat_data.x = gdat_data.gdat_params.camera;
            gdat_data.dim{1} = gdat_data.x;
            gdat_data.dim{2} = gdat_data.t;
            gdat_data.dimunits = [{'t nb'}; {'s'}];
            gdat_data.data_fullpath = '\cerqzefftii';
          else
            try
              gdat_data.data(icount,:) = interp1(a.t,a.data,gdat_data.t);
            catch
              if (gdat_params.nverbose>=1); disp(['problem with ichord = ' num2str(ichord)]); end
            end
          end
        else
          % add fields not yet defined in case all cases have empty data
        end
      end
      gdat_data.chords = gdat_data.gdat_params.camera;

    otherwise
      if (gdat_params.nverbose>=1); warning(['switchcase= ' data_request_eff ' not known in gdat_d3d']); end
      error_status=901;
      return
  end

else
  if (gdat_params.nverbose>=1); warning(['D3D method=' mapping_for_D3D.method ' not known yet, contact Olivier.Sauter@epfl.ch']); end
  error_status=602;
  return
end

gdat_data.gdat_params.help = d3d_help_parameters(fieldnames(gdat_data.gdat_params));
gdat_data.mapping_for.d3d = mapping_for_d3d;
gdat_params = gdat_data.gdat_params;
error_status=0;

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% functions for portions called several times
