function [sig,t]=get_xtomo_data(shot,t1,t2,dt,fans,angfact,tag);

%                                                                  -
%[sig,t]=get_xtomo_data_m5(shot,t1,t2,dt,fans,angfact,tag);
%
%    INPUT:
%       shot:       TCV shot
%       t1:      start time
%       t2:      stop time
%       dt:         timestep
%       fans:    camera switch, e.g. [0 0 0 0 0 0 1 0 1 0];
%       angfact: relative etendue, size:    [20 x 10]
%                tag    'full' or 'brief', indicates if it is
%                a slow or a fast Pentland acquisition
%
% OUTPUTS:
%       sig:        xtomo signals, size:    [sum(fans) x length(t)]
%       t:       times
%
%    ATTENTION: length(time) may be shorter than foreseen !!
%
% This routine works on Matlab5.
% Original routine for Matlab4 by Anton Mathias.
%
% Last update: 25-08-1999
%
%-------------MAC:[FURNO.MATLAB5.XTOMO]----------------------------------


%---- get  data and offsets ---------------------------------------------------

% set a flag if flattenign of noise
% for channels with low signal


iflat=0;
minsiglevel=0.01;
satlevel=9.9;


st1=sprintf('%6.4f',t1);
st2=sprintf('%6.4f',t2);
sdt=num2str(dt);

%    sdt=sprintf('%6.4f',dt); % modified from old version


if shot >= 13836 & shot <= 13848
  tstart=-0.02;tstop=-0.01;        %  this  one is to be used only for
else                        %  shot=13836 to shot=13848
  tstart=-0.04;tstop=-0.01;
end

ststart=num2str(tstart);
ststop=num2str(tstop);

if nargin <=6

  shot=mdsopen(shot);
  t=mdsdata(['dim_of(\base::xtomo:array_001[',st1,':',st2,':',sdt,',*])']);
  S1=[];
  S2=[];
  S3=[];
  S4=[];
  S5=[];
  S6=[];
  S7=[];
  S8=[];
  S9=[];
  S10=[];

  if isempty(t)
    disp('get_xtomo_data: sorry, nothing to be found for this shot ...')
    return
  else
    disp('*-------------------------------------------*')
    disp('| get_xtomo_data: getting data from MDS     |')
    disp('*-------------------------------------------*')
  end

  if shot>6768

    if fans(1)
      S=mdsdata(['\base::xtomo:array_001[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_001[',ststart,':',ststop,',0:19]']);
      S1=S-repmat(mean(offset),length(t),1)';
    end

    if fans(2)
      S=mdsdata(['\base::xtomo:array_002[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_002[',ststart,':',ststop,',0:19]']);
      S2=S-repmat(mean(offset),length(t),1)';
    end

    if fans(3)
      S=mdsdata(['\base::xtomo:array_003[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_003[',ststart,':',ststop,',0:19]']);
      S3=S-repmat(mean(offset),length(t),1)';
    end

    if fans(4)
      S=mdsdata(['\base::xtomo:array_004[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_004[',ststart,':',ststop,',0:19]']);
      S4=S-repmat(mean(offset),length(t),1)';
    end

    if fans(5)
      S=mdsdata(['\base::xtomo:array_005[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_005[',ststart,':',ststop,',0:19]']);
      S5=S-repmat(mean(offset),length(t),1)';
    end

    if fans(6)
      S=mdsdata(['\base::xtomo:array_006[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_006[',ststart,':',ststop,',0:19]']);
      S6=S-repmat(mean(offset),length(t),1)';
    end

    if fans(7)
      S=mdsdata(['\base::xtomo:array_007[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_007[',ststart,':',ststop,',0:19]']);
      S7=S-repmat(mean(offset),length(t),1)';
    end

    if fans(8)
      S=mdsdata(['\base::xtomo:array_008[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_008[',ststart,':',ststop,',0:19]']);
      S8=S-repmat(mean(offset),length(t),1)';
    end

    if fans(9)
      S=mdsdata(['\base::xtomo:array_009[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_009[',ststart,':',ststop,',0:19]']);
      S9=S-repmat(mean(offset),length(t),1)';
    end

    if fans(10)
      S=mdsdata(['\base::xtomo:array_010[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_010[',ststart,':',ststop,',0:19]']);
      S10=S-repmat(mean(offset),length(t),1)';
    end


    sig=-[S1;S2;S3;S4;S5;S6;S7;S8;S9;S10];
    [satrow,satcol]=find(abs(sig)>satlevel);

    if ~isempty(satcol)
      i_tlimit=min(satcol(:));
      if i_tlimit>1
        sig=sig(:,1:i_tlimit-1);
        t=t(1:i_tlimit-1);
        disp(['get_xtomo_data WARNING: some channels saturated',......
              ', t2 changed to ',sprintf('%6.4f',max(t))]);
      else
        sig=[];
        t=[];
        disp('get_xtomo_data WARNING: saturations, no data returned');
        return
      end
    end


    angfact=angfact(:,find(fans));
    angfact=angfact(:);
    gains=get_xtomo_gains(shot);
    % earlier than shot xxx:
    perm=[1:180,[180+[2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15,18,17,20,19]]];
    gains=gains(perm);
    iact=find(fans);
    nact=length(iact);
    idetec=[];
    for k=1:nact
      idetec=[idetec,(iact(k)-1)*20+1:iact(k)*20];
    end
    gains=gains(idetec);
    gains=gains(:);
    fac=ones(size(gains))./gains;


    sig=-sig.*(repmat(fac,1,length(t)));

    if iflat
      for ii=1:length(t)
        imini=find( sig(:,ii) < minsiglevel*max(sig(:,ii)));
        sig(imini,ii)=zeros(size(imini));
      end
    end

    t=t';
    sig=sig.*(repmat(angfact,1,length(t)));


  else

    if fans(9)
      S=mdsdata(['\base::xtomo:array_001[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_001[',ststart,':',ststop,',0:19]']);
      V=S-repmat(mean(offset),length(t),1)';
    end
    if fans(7)
      S=mdsdata(['\base::xtomo:array_002[',st1,':',st2,':',sdt,',0:19]'])';
      offset=mdsdata(['\base::xtomo:array_002[',ststart,':',ststop,',0:19]']);
      H=S-repmat(mean(offset),length(t),1)';
    end

    %---- read gains of the current shot ---------------------------------------

    gains=get_xtomo_gains(shot);
    g1=gains(1:20)';
    g2=gains(21:40)';

    %------ solid angle factors -----------------------

    aom1=angfact(:,9);
    aom2=angfact(:,7);


    %-----  calculate correcting factors ---------------------------------------

    fac1=(aom1./g1);
    fac2=(aom2./g2);

    %--- result: offset-, calibration- and angular-factor corrected data -------

    V=-V.*(repmat(fac1,1,length(t))); % vertical camera:
    H=-H.*(repmat(fac2,1,length(t))); % horizontal camera:


    sig=[H;V];

    t=t';

  end

else

  if (~strcmp(tag,'brief') & ~strcmp(tag,'full'))
    disp('Only full or brief accepted as tag')
    return
  end


  trace_tree=['\atlas::t_rex3_' tag ':'];
  trace=[trace_tree 'signal_inp:p'];
  %shot=mdsopen('eltca1::tcv_shot',shot);
  shot=mdsopen(shot);
  tref=mdsdata(['_tref=dim_of(' trace,'001)']);  %trace :p001
  dtref=mdsdata('_dtref = _tref[1]-_tref[0]');


  S1=[];
  S2=[];
  S3=[];
  S4=[];
  S5=[];
  S6=[];
  S7=[];
  S8=[];
  S9=[];
  S10=[];

  t_1=mdsdata('_t = dim_of(\atlas::t_rex3_full:signal_inp:p001)');
  dt1=mdsdata('_dt = _t[1]-_t[0]');
  npts=floor(mdsdata(['_npts=(',ststop,'-(',ststart,'))/_dt']));



  if isempty(t_1)
    disp('get_xtomo_data: sorry, nothing to be found for this shot ...')
    return
  else
    disp('*--------------------------------------------*')
    disp('| getting Pentland acquisition: getting      *')
    if strcmp(tag,'brief')
      disp('|         BRIEF data from MDS                *');
    else
      disp('|         FULL   data from MDS               *');
    end
    disp('*--------------------------------------------*');
  end



  if ((dt - dtref) < 1e-7 | floor(dt/dtref) ==0)
    step='1';
  else
    step =num2str(dt/dtref);
  end


  ind1=find( abs(tref-t1) == min(abs(tref-t1)));
  npts1=floor(mdsdata(['_npts1=(',st2,'-(',st1,'))/_dtref']));
  sind1=num2str(ind1);
  sind2=num2str(npts1+ind1);

  t=mdsdata(['data(_tref)[',sind1,':',sind2,':', step, ']']);




  if fans(1)
    channel=['001';'002';'003';'004';'005';'006';'007';'008';'009';'010';...
             '011';'012';'013';'014';'015';'016';'017';'018';'019';'020'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':' , step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S1(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end



  if fans(2)
    channel=['021';'022';'023';'024';'025';'026';'027';'028';'029';'030';...
             '031';'032';'033';'034';'035';'036';'037';'038';'039';'040'];
    for i=1:20

      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step, ']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S2(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(3)

    channel=['041';'042';'043';'044';'045';'046';'047';'048';'049';'050';...
             '051';'052';'053';'054';'055';'056';'057';'058';'059';'060'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step, ']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S3(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end

  if fans(4)
    channel=['061';'062';'063';'064';'065';'066';'067';'068';'069';'070';...
             '071';'072';'073';'074';'075';'076';'077';'078';'079';'080'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step, ']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S4(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(5)
    channel=['081';'082';'083';'084';'085';'086';'087';'088';'089';'090';...
             '091';'092';'093';'094';'095';'096';'097';'098';'099';'100'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':',step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S5(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end

  if fans(6)
    channel=['101';'102';'103';'104';'105';'106';'107';'108';'109';'110';...
             '111';'112';'113';'114';'115';'116';'117';'118';'119';'120'];
    for i=1:20
      %            S(i,:)=mdsdata([trace,channel(i,:),'[',st1,':',st2,':',sdt,':',step,']'])'
      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step, ']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S6(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(7)
    channel=['121';'122';'123';'124';'125';'126';'127';'128';'129';'130';...
             '131';'132';'133';'134';'135';'136';'137';'138';'139';'140'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':',step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S7(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(8)
    channel=['141';'142';'143';'144';'145';'146';'147';'148';'149';'150';...
             '151';'152';'153';'154';'155';'156';'157';'158';'159';'160'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S8(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(9)
    channel=['161';'162';'163';'164';'165';'166';'167';'168';'169';'170';...
             '171';'172';'173';'174';'175';'176';'177';'178';'179';'180'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S9(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end
  if fans(10)
    %        channel=['181';'182';'183';'184';'185';'186';'187';'188';'189';'190';...
    %              '191';'192';'193';'194';'195';'196';'197';'198';'199';'200'];

    % this permutation takes into account some not well defined (not yet)
    % hardware corrections. Are the gains to be permutated ?


    channel=['182';'181';'184';'183';'186';'185';'188';'187';'190';'189';...
             '192';'191';'194';'193';'196';'195';'198';'197';'200';'199'];
    for i=1:20
      %            S(i,:)=mdsdata([trace, channel(i,:),'[',st1,':',st2,':',sdt,']'])';


      S(i,:)=mdsdata(['data(',trace,channel(i,:),')[',sind1,':',sind2,':', step,']'])';
      offset=mdsdata(['data(\atlas::t_rex3_full:signal_inp:p',channel(i,:),')[0:',num2str(npts),']' ]);
      S10(i,:)=S(i,:)-repmat(mean(offset),length(t),1)';
    end
  end

  sig=[S1;S2;S3;S4;S5;S6;S7;S8;S9;S10];


  [satrow,satcol]=find(abs(sig)>satlevel);

  if ~isempty(satcol)
    i_tlimit=min(satcol(:));
    if i_tlimit>1
      sig=sig(:,1:i_tlimit-1);
      t=t(1:i_tlimit-1);
      disp(['get_xtomo_data WARNING: some channels saturated',......
            ', t2 changed to ',sprintf('%6.4f',max(t))]);
    else
      sig=[];
      t=[];
      disp('get_xtomo_data WARNING: saturations, no data returned');
      return
    end
  end


  angfact=angfact(:,find(fans));
  angfact=angfact(:);
  gains=get_xtomo_gains(shot);
  % earlier than shot xxx:
  perm=[1:180,[180+[2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15,18,17,20,19]]];
  gains=gains(perm);
  iact=find(fans);
  nact=length(iact);
  idetec=[];
  for k=1:nact
    idetec=[idetec,(iact(k)-1)*20+1:iact(k)*20];
  end
  gains=gains(idetec);
  gains=gains(:);
  fac=ones(size(gains))./gains;


  sig=sig.*(repmat(fac,1,length(t)));

  if iflat
    for ii=1:length(t)
      imini=find( sig(:,ii) < minsiglevel*max(sig(:,ii)));
      sig(imini,ii)=zeros(size(imini));
    end
  end

  t=t';
  sig=sig.*(repmat(angfact,1,length(t)));


end

mdsclose


return
