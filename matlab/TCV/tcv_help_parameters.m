function help_struct = tcv_help_parameters(parameter_list)
%
% retrieve from present table the relevant help lines for the parameter_list{:}
% should come from sqlite database at some point...
%
% return the whole help structure if parameter_list empty or not provided
%
% do:
%      help_struct = tcv_help_parameters(fieldnames(gdat_data.gdat_params));
%
% to get relevant help description
%

% Defaults
help_struct_all = struct(...
    'data_request', ['automatically filled in by gdat, name of request used in gdat call.' char(10) ...
                    'contains current list of keywords if gdat called with no arguments: aa=gdat;' char(10) ...
                    'Note shot value should not be in params so params can be used to load same data from another shot']  ...
    ,'machine', 'machine name like ''TCV'', ''AUG'', case insensitive' ...
    ,'doplot', '0 (default), if 1 calls gdat_plot for a new figure, -1 plot over current figure with hold all, see gdat_plot for details' ...
    ,'liuqe','liuqe version 1 (default), 2, 3 for LIUQE1, 2, 3 resp. or -1 for model/FBTE values (11, 12, 13 for liuqe_fortran)' ...
    ,'nverbose','1 (default) displays warnings, 0: only errors, >=3: displays all extra information' ...
    );

% TCV related
help_struct_all.cxrs_plot = '0 (default) no plots, 1 get plots from CXRS_get_profiles see ''help CXRS_get_profiles'' for k_plot values';
help_struct_all.cxrs_xout = 'xout array for rhopol fits, set [] to get CXRS_get_profiles default';
help_struct_all.cxrs_time_interval = ['cxrs: (time_interval can have several nbs) take data and average over time interval(s) only, plots from CXRS_get_profiles are then provided' ...
                    ' as well'];
help_struct_all.fit_tension = ['smoothing value used in interpos fitting routine, -30 means ''30 times default value'', thus -1 often a' ...
                    ' good value' char(10) ...
                    'cxrs: if numeric, default for all cases, if structure, default for non given fields' char(10) ...
                    'radcam: tension for interpos smoothing of data to have lower time samples'];
help_struct_all.time = 'eqdsk: time(s) value(s) requested, by default time=1.0s (see time_out for other requests)';
help_struct_all.time_out = ['requested time for output: data points within interval if time_out=[t1 t2], otherwise assumes series of points, uses linear interpolation in that case (default [-Inf Inf])'...
                   char(10) 'for sxr, mpx: only time interval provided in time_out is relevant'];
help_struct_all.zshift = 'vertical shift of equilibrium, either for eqdsk (1 to shift to zaxis=0) or for mapping measurements on to rho surfaces [m]';
help_struct_all.cocos = ['cocos value desired in output, uses eqdsk_cocos_transform. Note should use latter if a specific Ip and/or B0 sign' ...
                    'is wanted. See O. Sauter et al Comput. Phys. Commun. 184 (2013) 293'];
help_struct_all.nrz_out = 'Nb of radial and vertical points in output eqdsk, default is [129,129], set to [-1,-1] to get original meshes';
help_struct_all.edge = '0 (default), 1 to get edge Thomson values';
help_struct_all.ec_inputs = '0 (default), 1 to get characterics of gyrotrons connected to each launcher: active, freq, power';
help_struct_all.fit = '0, no fit profiles, 1 (default) if fit profiles desired as well, relevant for _rho profiles. See also fit_type';
help_struct_all.fit_type = 'provenance of fitted profiles ''conf'' (default) from conf nodes, ''avg'' or ''local'' for resp. proffit: nodes';
help_struct_all.trialindx = 'value of trialindx desired to get a specific one when relevant, otherwise gets ok_trialindx, that is 1 usually';
help_struct_all.source = sprintf('%s\n','cxrs: [1 2 3] (default systems);', ...
          'sxr: main source: ''MPX'' (default) or ''XTOMO'', case insensitive', ...
          'mhd request: ''23'':23 LFS/HFS (default), ''23full'': 23cm sector 3 and 11, ''0'':z=0 LFS/HFS, ''0full'': 0cm sector 3 and 11', ...
          'powers: ohmic in any case + ''ec'', ''nbi'', ''rad'' ', ...
          'rtc: defines which pattern(s) to search for (default: {''Actuator_state'',''plasma_state'',''samone_out''}, empty to have all', ...
          'transp: source provides the netcdf file', ...
          'icds: ''ec'', ''nbi'' ', ...
          'ids_names: for request ''ids'' like magnetics, equilibrium, etc', ...
          'nel: ''fir'' (default) or ''fit'' to compute nel from fit');
help_struct_all.source_ec = sprintf('%s\n','toray (for toray nodes), no other source for eccd yet implemented');
help_struct_all.source_nbi = sprintf('%s\n','nbi_output_matfilename containing out structure, to be added astra nodes');
help_struct_all.error_bar = sprintf('%s\n','for ids: choice of nodes fill in and how:', ...
          '''delta'' (default): only upper fill in  with the abs(value) to add or subtract to data to get upper and lower values (symmetric)', ...
          '''delta_with_lower'': same as delta but fill in lower node as well (with delta as well, same as upper)', ...
          '''added'': add the delta values (old cpo style), so upper=data+error_bar and lower=data+error_bar');
help_struct_all.camera = sprintf('%s\n%s\n%s', ...
          'sxr: for radcam: 1 to 4 or ''top'' (default), ''upper'', ''equatorial'', ''bottom'' array or cell array ok as well;', ...
          ' for MPX: ''central'', ''top'' (default), ''bottom'' or ''both'' ;', ...
          ' for XTOMO: ''central'' (a central chord only), defaults if empty, [1 3 5] if only camera 1, 3 and 5 are desired');
help_struct_all.channel = sprintf('%s\n%s\n%s', ...
          'radcam: chord to choose within camera interval, or simply chords, then it is re-distributed to correct camera');
help_struct_all.freq = '''slow'', default, lower sampling (for radcam smoothing on dt=0.1ms); ''fast'' full samples for radcam, mpx and xtomo';
help_struct_all.max_adcs = 'rtc: source=''adcs'' maximum nb of adc channels loaded for each board in each active node';
help_struct_all.nfft = '512 (default) changes time resolution in spectrogram in gdat_plot for ''mhd'' request';
help_struct_all.map_eqdsk_psirz = 'eqdsk: if time array provided, maps all psi(R,Z,t) on same R,Zmesh in .data (1) or not (0, default)';
help_struct_all.write = 'eqdsk: write eqdsk while loading data (1, default) or not (0)';
help_struct_all.rtc = 'load data from mems saved by SCD system (if on for that shot), source defines what to search for';
%help_struct_all. = '';

if ~exist('parameter_list') || isempty(parameter_list)
  help_struct = help_struct_all;
  return
end

if iscell(parameter_list)
  parameter_list_eff = parameter_list;
elseif ischar(parameter_list)
  % assume only one parameter provided as char string
  parameter_list_eff{1} = parameter_list;
else
  disp(['unknown type of parameter_list in tcv_help_parameters.m'])
  parameter_list
  return
end

fieldnames_all = fieldnames(help_struct_all);
for i=1:length(parameter_list_eff)
  if any(strcmp(fieldnames_all,parameter_list_eff{i}))
    help_struct.(parameter_list_eff{i}) = help_struct_all.(parameter_list_eff{i});
  end
end
