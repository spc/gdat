function [t,i1,i2,i12,t1no,t2no,i1no,i2no]=common_ece(t1,t2,tol,mode,bounds)
% [t,i1,i2,i12,t1no,t2no,i1no,i2no]=common(t1,t2,[tol,mode,bounds])
% extract elements that are common to t1 and t2 within tolerance tol
% t:  elements of t1 which are within tol of an element of t2
% i1: index to elements of t1 which are within tol of an element of t2
% i2: index to elements of t2 which are within tol of (and closest to if 'n') an element of t1
% i12:index in t2 for elements from t1 corresponding to (first) match in t2
% t1(2)no: elements of t1(2) which are not matched in t2(1)
% i1(2)no: index to elements in t1(2) which are not matched in t2(1)
% mode: 'a' all or 'n' nearest matches
% bounds: [bmin,bmax] limits search interval for t1

if(nargin<3 || ~exist('tol','var'))
    tol=eps;
end
if(nargin<4 || ~exist('bounds','var'))
    bounds=[min([min(t1),min(t2)])-tol;
    max([max(t2),max(t1)])+tol];
end
if(nargin<5 || ~exist('mode','var'))
    mode='n';
end

i1=zeros(size(t1));
i2=zeros(size(t2));
t=[];
i12=zeros(size(t1));

t1no=[];
t2no=[];

match=0;

for j=1:length(t1)
    if(t1(j)>=bounds(1) && t1(j)<=bounds(2))
        tt1=t1(j)*ones(size(t2));
        ii2=find(abs(t2-tt1)<=tol);
        if(strcmp(mode,'n') && ~isempty(ii2))
            if (length(ii2)>1)
                [~,ii2]=min(abs(t2-tt1));
            end
            match=match+1;
            i2(ii2)=ii2;
            i1(j)=ii2;
            i12(j)=ii2;
        end
        if(strcmp(mode,'a') && ~isempty(ii2))
            match=match+1;
            i1(j)=j;
            i12=ii2(1);
            i2(ii2)=ii2;
        end	
    end
end

%disp(['matches found: ',int2str(match)])
%keyboard

i1no=find(i1==0);
i2no=find(i2==0);
i1=find(i1~=0);
i2=find(i2~=0);
if(~isempty(i1))
    t=t1(i1);
end
if(~isempty(i1no))
    t1no=t1(i1no);
end
if(~isempty(i2no))
    t2no=t2(i2no);
end

%keyboard

end
