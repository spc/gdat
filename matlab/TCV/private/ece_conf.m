function [K,L]=ece_conf(shot,mode)

%	function [K,L]=ece_conf(shot,mode)
%	[K,L]=ece_conf(shot,0);
%	if mode=0 => display some informations
%	
%	Give the configuration of the ECE radiometer for the specified shot.
%	K=1 => high edge resolution (A)
%	K=2 => high center resolution (B)
%	K=3 => low resolution Lo1->IF2; Lo2->IF1 (C)
% 	K=4 => low resolution Lo1->IF1; Lo2->IF2 (D)
%
%	L=0  	=> calibration
%	L=1	=> Z=0
%	L=2	=> Z=0.21 cm

mdsopen('tcv_shot',shot)
Cntrl1= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D0"]');
Cntrl2= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D1"]');
Sw1= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D2"]');
Sw2= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D3"]');
Att= mdsdata('\vsystem::tcv_publicdb_i["ECE:PIO_GAIN_2_IN"]');
mdsclose

if mode==0
i=[1,2];

if strcmp(Cntrl1(i),'ON')
L=2;
disp('ligne de visee Z = 21.3 cm')
else
if strcmp(Cntrl2(i),'ON')
L=1;
disp('ligne de visee Z=0')
else
L=0;
disp('Calibration')
end
end

if strcmp(Sw1(i),'ON') 
 if strcmp(Sw2(i),'ON') 
  K=2;
  disp('Configuration B')
 else
  K=3;
  disp('Configuration C')
 end
else
 if strcmp(Sw2(i),'ON') 
  K=4;
  disp('Configuration D')
 else
  K=1;
  disp('Configuration A')
 end
end
disp(['Attenuation level = ',num2str(Att),' '])
else
i=[1,2];
if strcmp(Cntrl1(i),'ON')
L=2;
else
if strcmp(Cntrl2(i),'ON')
L=1;
else
L=0;
end
end

if strcmp(Sw1(i),'ON') 
 if strcmp(Sw2(i),'ON') 
  K=2;
 else
  K=3;
 end
else
 if strcmp(Sw2(i),'ON') 
  K=4;
 else
  K=1;
 end
end

end
