function [CALIBRATION,Fcentral]=ece_calib(shot,Tc)

%	function [CALIBRATION,Fcentral]=ece_calib(shot,Tc)
%	[CALIBRATION,Fcentral]=ece_calib(shot,[0.1 0.29])
%
%	Calculate the CALIBRATION matrix of ECE signals on Thomson temperature
%	on times bounded by Tc=[a b] for the specified shot#.
%	Fcentral is the central Frequency of the ECE channels
%	
%	Blanchard  09.06.2001

%-----------------------------------------------------------------------
%	Recherche de la configuration de l'ECE
%-----------------------------------------------------------------------
[W]=writeece;

warning off
mdsopen('tcv_shot',shot)
L=mdsdata('GETNCI("\\results::ece:calibration","length")');
LL=mdsdata('GETNCI("\\results::psi_axis:foo","length")');
mdsclose
if LL==0
 disp('----------------------------------')
 disp('\results::psi_axis not calculated')
 disp('----------------------------------')
 return 
end

if L==0 | L==28 |W==1
  disp(' La trace \results::ece:calibration est vide ');
  mdsopen('tcv_shot',shot)
  Cntrl1= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D0"]');
  Cntrl2= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D1"]');
  Sw1= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D2"]');
  Sw2= mdsdata('\vsystem::tcv_publicdb_b["ECE:PIO_D3"]');
  mdsclose
  i=[1,2];
  if strcmp(Cntrl1(i),'ON')
     L=2;
  else
     if strcmp(Cntrl2(i),'ON')
	L=1;
     else
	L=0;
     end
  end

  if strcmp(Sw1(i),'ON') 
   if strcmp(Sw2(i),'ON') 
    K=2;
   else
    K=3;
   end
  else
   if strcmp(Sw2(i),'ON') 
    K=4;
   else
    K=1;
   end
  end
  clear Cntrl1 Cntrl2 Sw1 Sw2

  %-----------------------------------------------------------------------
  %	Appel des principales donnees
  %-----------------------------------------------------------------------
  [RHOece,Rece,Zece,Tece, Fcentralrho]=ece_rho(shot,Tc);Tece=Tece';
  [TEthom,NEthom,TEerr,NEerr,Tthom,RHOthom]=thom_rho(shot,5); %(Tthom,Nbre de pts selon rho)
  [A,B]=size(TEthom);
  KK=0;
  if exist('TEthom')==0|length(TEthom) ==0
	KK=1;
	disp('Le profil Thomson proffit n''existe pas. On essaye le profil direct') %(Tthom,Nbre de pts selon rho)
	[TEthom,NEthom,TEerr,NEerr,Tthom,RHOthom]=thom_rho(shot,3);
	[A,B]=size(TEthom);
	if mean(TEthom)==-1| isstr(TEthom)==1
	 	return
	end
  end
  if A==0|B==0|B==10
	disp('--------------------------')
	disp('No Thomson acquisition')
	disp('--------------------------')
	TEthom
	return
  end
  [t,i1,i2]=common(Tece,Tthom(:,1),0.005);
  [ECE,TECE,Fcentral]=ece_raw_signals(shot,Tc,1);Fcentral=Fcentral';
  [t2,i3,i4]=common(Fcentralrho,Fcentral);
  Rece=Rece(i3,i1);Zece=Zece(i3,i1);RHOece=RHOece(i3,i1);
  TEthom=TEthom(i2,:);TEerr=TEerr(i2,:);RHOthom=RHOthom(i2,:);
  T1=Tece(i1);
  clear NEthom Tthom Tece Zece Fcentralrho
  disp(['Pour la calibration, nous allons considerer ', num2str(length(t)),' profils Thomson '])
  disp(['Tcal = ',num2str(t')])              
  %-----------------------------------------------------------------------
  %	Mise a jour des signaux sur les memes bases temporelles
  %	et RHO
  %-----------------------------------------------------------------------
  [T1,Fcentral]=meshgrid(T1,Fcentral);[T2,Fcentral2]=meshgrid(TECE,Fcentral(:,1));
  RHOcalib=interp2(T1,Fcentral,RHOece,T2',Fcentral2');
  TEthom=griddata(RHOthom,repmat(T1(1,:)',1,B),TEthom,RHOcalib,T2'); 
  TEerr=griddata(RHOthom,repmat(T1(1,:)',1,B),TEerr,RHOcalib,T2');
  %-----------------------------------------------------------------------
  %	Calibration sur mesure Thomson
  %-----------------------------------------------------------------------
  if isempty(find(isnan(TEthom)))==0
    NN=find(isnan(mean(TEthom))==1);   
    for j=1:length(NN)
      nj=find(isnan(TEthom(:,NN(j)))==1);
      nNN=find(isnan(TEthom(:,NN(j)))==0); 
      disp(['On a remplace ',num2str(length(nj)),' NaN dans la colonne ',num2str(NN(j))]); 
      if min(nNN)<min(nj)
        TEthom(nj,NN(j))=TEthom(min(nj)-1,NN(j));
      else
        disp(['probleme avec NaN : toute la colonne ',num2str(NN(j)),' vaut 0']);
        TEthom(nj,NN(j))=0;
      end
    end
  end  
 
  CAL(1,:)=Fcentral(:,1)';
  c=TEthom./ECE;
  if  max(size(find(c==0)))>1
	  disp('On corrige les points valant 0')
	  jj=min(find(mean(TEthom)>0));
	  mm=min(find(c(:,3)>0));
	  MM= max(find(c(:,3)>0));
	  CAL(2,:)=mean(TEthom([mm:MM],:)./ECE([mm:MM],:));
	  CAL(3,:)=std(TEthom([mm:MM],:)./ECE([mm:MM],:));CAL(4,:)=mean(TEerr([mm:MM],:)./ECE([mm:MM],:));
	  CALIBRATION=CAL;CALIBRATION(5,[1:length(T1(1,:))])=T1(1,:);  
  else
	  CAL(2,:)=mean(TEthom./ECE);
	  CAL(3,:)=std(TEthom./ECE);CAL(4,:)=mean(TEerr./ECE);
	  CALIBRATION=CAL;CALIBRATION(5,[1:length(T1(1,:))])=T1(1,:);   
  end
  if W==2

  else 
	  mdsopen(shot) 
	  mdsput('\results::ece:calibration',CALIBRATION,'f');
	  mdsput('\results::ece:f_central',Fcentral(:,1),'f');
	  mdsclose
	  disp('On a enregistre la matrice de calibration et Fcentral dans MDS')
	  clear RHOcalib TEthom T2 ECE Fcentral2 TECE 
  end
 
else
	disp('On va chercher la matrice \results::ece:CALIBRATION dans MDS!')
	mdsopen('tcv_shot',shot)
	CALIBRATION=mdsdata('\results::ece:calibration');
	Fcentral=mdsdata('\results::ece:f_central');
	mdsclose
end

warning on
