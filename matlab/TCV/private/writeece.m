function [W]=writeece

%Si W=2 Si MDS est rempli, on va chercher dans MDS sinon
%	on calcule tout ce qui faut sans ecrire dans MDS
%Si W=1 on force a tout recalculer et on ecrit dans MDS
%Si W=0 Si MDS est rempli, on va chercher dans MDS sinon
%	on calcule tout ce qui faut et on ecrit dans MDS
W=0;
