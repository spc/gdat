function gains = get_xtomo_gains(shot);

%     [gains] = get_xtomo_gains(shot);
%
%  emerged from MJD's  XTOMOSEQ/XTOMO_LOAD_GAINS 
%  purpose: load gains of a certain shot nr. from the database
%  usage:   'gains' is a row vector containing the gains of 
%           [array_001(det.1..020), array_002, det. 001...020 etc]
%
%
% This routine works on Matlab5.
% Original routine for Matlab4 by Anton Mathias.  
%
% Last update: 25-08-1999
%
%-------------MAC:[FURNO.MATLAB5.XTOMO]----------------------------------



if nargin~=1
   shot=0;
end


MAX_DET = 20; 

if isempty(mdsopen('vsystem',shot));
	  mdsclose
   info = sprintf('Shot #%.0f not found',shot);
   return
end

if shot<=6768
	% narray =2 for the prototype system only
	narray=2;
else
	narray=10;
end


temp = isempty(mdsdata('_v=\diag2db_i'));
if temp
     info = sprintf('Vista database not available for shot #%.0f',shot);
     mdsclose;
     return
end

mdsdata(['_p="_"//translate(text(build_range(1,' int2str(MAX_DET) '),3),"0"," ");']);

G=[];
info=[];
for sig=1:narray
     G=[G, mdsdata(['_s="XTOMO_AMP:' sprintf('%03.0f',sig) '"//_p;_v[_s]'])'];
end

if isempty(info)
  info = sprintf('Gains loaded from shot #%.0f',shot);
else
  info(1) = '';
end

disp(info)
        	
% G contains log10 of the real gains, so...

gains=(10*ones(size(G))).^G;

% This is the modification after the TCV HSUT down in 1998
% due to the incorrect insatllation of the camera #2
if shot>13884  
	permute=20:-1:1;
	dummy=gains(21:40);
	gains(21:40)=dummy(permute);
end


mdsclose
return

permute=[2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15,18,17,20,19];
dummy=gains(181:200);
gains(181:200)=dummy(permute);


	


