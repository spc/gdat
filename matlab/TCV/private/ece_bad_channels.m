function [N]=ece_bad_channels(shot)
%	function [N]=ece_bad_channels(shot)
%
%	Fonction donnant la liste des cannaux  ne fonctionnant pas
%	La numerotation est celle de la notice de l'ECE => comme dans MDS

load bad_channels;
if isempty(find(A(:,1)==shot))==1
  if shot <=18628 
    N=[1 23];
  elseif shot > 18628 & shot<20000
    N=[1 15 21 23];
   elseif shot>20000
    N=[15 21 23];
  end
else
  N=A(find(A(:,1)==shot),2:20);
  N=N(find(N>0));
end
