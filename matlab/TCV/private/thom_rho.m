function [TEthom,NEthom,TEbar,NEbar,Tthom,RHOthom]=thom_rho(shot,mode)

%	[TEthom,NEthom,TEbar,NEbar,Tthom,RHOthom]=thom_rho(shot,mode);
% 	 	
%	Fonction calculant le profil de Te et ne provenant de Thomson
%	en fonction de rho(psi)
%	mode =	1 => profil fitte \results::th_prof_te
%		2 => profil brut 2 demi-profil
%		3 => profil brut 1 demi-profil superieur
%		4 => profil brut 1 demi-profil inferieur
%		5 => profil proffit local_time
%		6 => profil proffit avg_time
%		7 => profil brut corrige selon z
%		8 => profil TS_FITDATA
%		9 => profil TS_RAWDATA

%

if mode ==1
	mdsopen('tcv_shot',shot)
	L=mdsdata('GETNCI("\\results::th_prof_te","length")');
	LL=mdsdata('GETNCI("\\results::thomson:te","length")');
	if L==0
	  mdsclose
	  disp('No Thomson \results::th_prof_te node acquisition')
	  if LL~=0
	    disp('but Thomson results::thomson:te node exist!')
	  end
	  return 
	end
	TEthom=mdsdata('\results::th_prof_te');
	NEthom=mdsdata('\results::th_prof_ne');
	Tt=mdsdata('dim_of(\results::th_prof_te)');
	Tn=mdsdata('dim_of(\results::th_prof_ne)');
	TEbar=mdsdata('\results::th_prof_te:std_error');
	NEbar=mdsdata('\results::th_prof_ne:std_error');
	mdsclose
	if length(Tt)~=length(Tn)
	 [t,i1,i2]=common(Tt,Tn);
	 Tthom=t;TEthom=TEthom(i1,:);NEthom=NEthom(i1,:);
	 TEbar=TEbar(i1,:);NEbar=NEbar(i1,:);
	else
	 Tthom=Tn;
	end
	RHOthom=linspace(0,1,41);
	RHOthom=repmat(RHOthom,length(Tthom),1);

elseif mode >=2 & mode <=4

	mdsopen('tcv_shot',shot)
	L=mdsdata('GETNCI("\\results::thomson:te","length")');
	if L==0
	  mdsclose
	  disp('No Thomson acquisition')
	  return 
	end
	TEthom=mdsdata('\results::thomson:te');
	if isstr(TEthom)==1
	 disp('---------------------------------------')
	 disp('No data with Thomson')
	 disp('---------------------------------------')
	 return
	end	
	TEbar=mdsdata('\results::thomson:te:error_bar');
	NEthom=mdsdata('\results::thomson:ne');
	NEbar=mdsdata('\results::thomson:ne:error_bar');
	Tthom=mdsdata('\results::thomson:times');
	Zthom=mdsdata('\diagz::thomson_set_up:vertical_pos')';
	Rthom=mdsdata('\diagz::thomson_set_up:radial_pos')'; 
	Psithom=mdsdata('\results::thomson:psiscatvol');   
	Psithommax=mdsdata('\results::thomson:psi_max'); 
	mdsclose
	Ste=size(TEthom);Spsi=size(Psithom);
	if Ste(2)~=Spsi(2)
	 	 disp('ATTENTION: on doit corriger des longueurs car')
		 disp('TEthom ~= Psithom')
		 ms=min([Ste(2) Spsi(2)]);
		 TEbar=TEbar(:,1:ms);NEbar=NEbar(:,1:ms);
		 TEthom=TEthom(:,1:ms);NEthom=NEthom(:,1:ms);
		 Rthom=Rthom(1:ms);
		 Psithom=Psithom(:,1:ms);
	end
	
	LTE=length(find(TEthom==-1));LNE=length(find(NEthom==-1));
	if LTE>0 | LNE >0
	  disp(['ATTENTION ',num2str(LTE),' points de mdsdata(''\results::thomson:te'') valent -1']) 
	  disp(['ATTENTION ',num2str(LNE),' points de mdsdata(''\results::thomson:ne'') valent -1']) 
	end
	J=0;
	if isempty(NEthom)
	  disp('----------------------')
	  disp('No Thomson acquisition')
	  disp('----------------------')
	  J=1;
	end
	if isstr(Psithommax)
	  disp('----------------------')
	  disp(Psithommax) 
	  disp('----------------------')
	  J=1;
	end
	if J==1
	  return
	end

	RHOTHOM=sqrt(1-Psithom./repmat(Psithommax,1,length(Zthom)));
	RHOTHOM(isnan(RHOTHOM))=0; 

	if mode ==2
	  [RHO,I] = sort(RHOTHOM');
	  NN=size(RHO);
	  TEthom=TEthom';TEbar=TEbar';
	  NEthom=NEthom';NEbar=NEbar';
	  for j = 1:NN(2)
	    TE(:,j) = TEthom(I(:,j),j); NE(:,j) = NEthom(I(:,j),j);
	    TE_bar(:,j) = TEbar(I(:,j),j); NE_bar(:,j) = NEbar(I(:,j),j);
	  end
	  TEthom=TE';TEbar=TE_bar';
	  NEthom=NE';NEbar=NE_bar';
	  RHOthom=RHO';
	elseif mode==3
	  nn=round(mean(mod(find(diff(sign(diff(RHOTHOM')))>0),length(Rthom)-2)))+1;
	  RHOthom=RHOTHOM(:,[1:nn]);NEthom=NEthom(:,[1:nn]);TEthom=TEthom(:,[1:nn]);
	  NEbar=NEbar(:,[1:nn]);TEbar=TEbar(:,[1:nn]);
	  Zthom=Zthom([1:nn]);
	elseif mode==4
	  nn=round(mean(mod(find(diff(sign(diff(RHOTHOM')))>0),length(Rthom)-2)))+1;
	  L=length(RHOTHOM(1,:));
	  RHOthom=RHOTHOM(:,[nn:L]);NEthom=NEthom(:,[nn:L]);TEthom=TEthom(:,[nn:L]);
	  NEbar=NEbar(:,[nn:L]);TEbar=TEbar(:,[nn:L]);
	  Zthom=Zthom([nn:L]);
	end

elseif mode==5
	mdsopen('tcv_shot',shot)
	L=mdsdata('GETNCI("\\results::proffit.local_time:teft","length")');
	LL=mdsdata('GETNCI("\\results::thomson:te","length")');

	if L==0
		  mdsclose
		  disp('No Thomson \results::proffit.local_time:teft node acquisition')
		  if LL~=0
	    		disp('but Thomson results::thomson:te node exist!')
		  end
		  return 
	end
	TEthom=mdsdata('\results::proffit.local_time:teft')';
	NEthom=mdsdata('\results::proffit.local_time:neft')';
	Tthom=mdsdata('dim_of(\results::proffit.local_time:teft,1)');
	RHOthom=mdsdata('dim_of(\results::proffit.local_time:teft,0)')';
	TEbar=mdsdata('\results::proffit.local_time:teft_std')';
	NEbar=mdsdata('\results::proffit.local_time:neft_std')';
	mdsclose
	RHOthom=repmat(RHOthom,length(Tthom),1);
elseif mode==6
	mdsopen('tcv_shot',shot)
	L=mdsdata('GETNCI("\\results::proffit.avg_time:teft","length")');
	LL=mdsdata('GETNCI("\\results::thomson:te","length")');
	if L==0
	  mdsclose
	  disp('No Thomson \results::th_prof_te node acquisition')
	  if LL~=0
	    disp('but Thomson results::thomson:te node exist!')
	  end
	  return 
	end
	TEthom=mdsdata('\results::proffit.avg_time:teft')';
	NEthom=mdsdata('\results::proffit.avg_time:neft')';	
	Tthom=mdsdata('dim_of(\results::proffit.avg_time:teft,1)');
	RHOthom=mdsdata('dim_of(\results::proffit.avg_time:teft,0)')';
	TEbar=mdsdata('\results::proffit.avg_time:teft_std')';
	NEbar=mdsdata('\results::proffit.avg_time:neft_std')';
	mdsclose
	RHOthom=repmat(RHOthom,length(Tthom),1);
elseif mode==7
        del=0.03;
	J=0;
	mdsopen('tcv_shot',shot)
	L=mdsdata('GETNCI("\\results::psi_axis:foo","length")');
	if L==0
	  disp('----------------------------------')
	  disp('\results::psi_axis not calculated')
	  disp('----------------------------------')
	  mdsclose
	  return 
	end
	TEthom=mdsdata('\results::thomson:te');
	TEbar=mdsdata('\results::thomson:te:error_bar');
	NEthom=mdsdata('\results::thomson:ne');
	NEbar=mdsdata('\results::thomson:ne:error_bar');
	Tthom=mdsdata('\results::thomson:times');
	Zthom=mdsdata('\diagz::thomson_set_up:vertical_pos')'+del;
	Rthom=mdsdata('\diagz::thomson_set_up:radial_pos')'; 
	PSIMAG = mdsdata('\results::psi_axis');
	Tpsimag=mdsdata('dim_of(\results::psi_axis)');
	mdsclose
	PSI=psitbxtcv(shot);
	if  exist('PSI')
	  X=PSI.psitbxfun.x;
	  T=PSI.psitbxfun.t;
	  RG=PSI.psitbxfun.grid.x{1}';
	  ZG=PSI.psitbxfun.grid.x{2}';
	else
	  disp('No psitbxtcv ')
	  return
	end
	[t,i1,i2]=common(T,Tthom,0.005);        
	T=T(i1);X=X(:,:,i1);
	TEbar=TEbar(i2,:);NEbar=NEbar(i2,:);NEthom=NEthom(i2,:);TEthom=TEthom(i2,:);
	[t,i1,i2]=common(Tthom(i2),Tpsimag,0.005); 
	PSIMAG=PSIMAG(i2);
	%-------------------------------------------------------------
	%	Psi...= profil de psi pour tous les temps T
	%-------------------------------------------------------------
	PSIMAG1=repmat(PSIMAG,1,length(ZG));
	Psi=X(min(find(RG>=mean(Rthom))),:,:);
	Psi=reshape(Psi,length(ZG),length(T));
	rho=sqrt(1-(Psi)./PSIMAG1');
	[RHOthom]=griddata(repmat(ZG,1,length(T)),repmat(T,length(ZG),1),rho,...
			repmat(Zthom',1,length(T)),repmat(T,length(Zthom),1));
	RHOthom=RHOthom';Tthom=T';
elseif mode==8
	mdsopen('tcv_shot',shot)
	ncode='TS_FITDATA("ne","psi",1,1)';
	tcode='TS_FITDATA("te","psi",1,1)';
	NEthom=mdsdata(ncode);
	TEthom=mdsdata(tcode);
	Tthom=mdsdata(['dim_of(' tcode ',0)']);
	RHOthom=mdsdata(['dim_of(' tcode ',1)']);
	mdsclose
	elseif mode==9
	mdsopen('tcv_shot',shot)
	ncode='TS_RAWDATA("ne","psi",1,1)';
	tcode='TS_RAWDATA("te","psi",1,1)';
	NEthom=mdsdata(ncode);
	TEthom=mdsdata(tcode);
	Tthom=mdsdata(['dim_of(' tcode ',0)']);
	RHOthom=mdsdata(['dim_of(' tcode ',1)']);
	mdsclose
end

[A,B]=size(TEthom);

if mean(TEthom)==-1
	disp('---------------------------------------------')
	disp('Toutes les valeurs de Thomson valent -1')
	disp('---------------------------------------------')
elseif A==0|B==0
	disp('---------------------------------------------')
	disp('No Thomson acquisition') 
	disp('---------------------------------------------')
end



