function [Fcentral, Rcentral,No]=ece_mode(K,B)

%	function [Fcentral, Rcentral,No]=ece_mode(K,B)
%
%	Programme calculant les frequences centrales, et la disposition 
%	spatiale relative a ces frequences pour chaques cannaux dans les quatres 
%	configurations possibles du radiometre ECE
%
%	K=1 => haute resol bord (8a)
%	K=2 => haute resol centre (8b)
%	K=3 => basse resolution Lo1->IF2; Lo2->IF1 (8c)
%	K=4 => basse resolution Lo1->IF1; Lo2->IF2 (8d)
%	B=champs magnetique
%
%	Fcentral = frequence centrale de chaque cannaux
%	Rcentral = position selon R de l'emission a la frequence Fcentral
%	No	 = correspondance Fcentral -> cannaux de l'ECE
%
%	Voir ece_conf.m

%[Lo1,Lo2,cann1,cann2,cann,del,No1,No2]=ece_lo;
Lo1=76.475;
Lo2=94.475;
cann1=[2.375:1.5:18.875]';
No1=fliplr([1:12])';
cann2=[3.125:1.5:19.625]';
No2=([13:24]');
cann=[2.375:0.75:19.625]'; 
del=0.75;
% introduce const constants from /mac/blanchard/matlab5/ece/public/const.m

%	CONST.M
%
%	Constante physique						
%									
%	q, e, k, me, mp, h, hbar, c, epsilon0, mu0, G, Na
%
%	Unites SI
%	Blanchard 07.97
q=1.6022e-19;				% [C]
e=1.6022e-19;				% [C]
k=1.3807e-23;				% [J/K]
me=9.1094e-31;				% [kg]
mp=1.6726e-27;				% [kg]
h=6.6261e-34;				% [Js]
hbar=h/2/pi;				% [Js]
c=2.9979e8;				% [m/s]
epsilon0=8.8542e-12;			% [F/m]
mu0=4e-7*pi;				% [H/m]
G=6.6726e-11;				% [m^3/kg/s^2]
Na=6.0221e23;				% [1/mol]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
%       Programme donnant les parametres importants de TCV
%       R0=Grand rayon
%       eptuile = distance paroi-bord tuile cote chambre
%       a=distance centre-bord tuile
R0=0.88;
eptuile=0.024;
a=0.56/2-eptuile;

if K == 1
	F=cann;
	Fcentral=cann+Lo2;
	Rcentral=2*e*R0/me*(1./Fcentral)*B/1e9/2/pi;
	%Wce_central=2*e*R0*(1./Rcentral)*B/me;
	No=[cann2+Lo2,No2];
	No(13:24,:)=[cann1+Lo2,No1];
	[No]=sortrows(No,1); 
elseif K == 2
	F=cann;
	Fcentral=cann+Lo1;
	Rcentral=2*e*R0/me*(1./Fcentral)*B/1e9/2/pi;
	%Wce_central=2*e*R0.*(1./Rcentral)*B/me;
	No=[cann2+Lo1,No2];
	No(13:24,:)=[cann1+Lo1,No1];
	[No]=sortrows(No,1); 
elseif K == 3
	F=cann1;
	F(length(cann1)+1:length(cann1)+length(cann2))=cann2;
	Fcentral=cann2+Lo1;
	Fcentral(length(cann1)+1:length(cann1)+length(cann2))=cann1+Lo2;
	Rcentral=2*e*R0/me*(1./Fcentral)*B/1e9/2/pi;
	%Wce_central=2*e*R0.*(1./Rcentral)*B/me;
	No=[cann1+Lo2,No1];
	No(13:24,:)=[cann2+Lo1,No2];
	[No]=sortrows(No,1); 
elseif K == 4
	F=cann1;
	F(length(cann1)+1:length(cann1)+length(cann2))=cann2;
	Fcentral=cann1+Lo1;
	Fcentral(length(cann1)+1:length(cann1)+length(cann2))=cann2+Lo2;
	Rcentral=2*e*R0/me*(1./Fcentral)*B/1e9/2/pi;
	%Wce_central=2*e*R0.*(1./Rcentral)*B/me;
	No=[cann1+Lo1,No1];
	No(13:24,:)=[cann2+Lo2,No2];
	[No]=sortrows(No,1); 
end
