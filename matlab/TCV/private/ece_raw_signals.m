function [ECE,Tece,Fcentral]=ece_raw_signals(shot,T,SS);

%	function [ECE,Tece,Fcentral]=ece_raw_signals(shot,T,SS);
%	
%	Programme donnant les signaux ECE [V] non corriges par les 
%	calibrations, pour le vecteur temporel T ou pour un sous 
%	echantillonage SS
%
%	shot 	= no de tir a analyser
%	T	= soit 
%		-vecteur temporel croissant
%		-si T=10 => tout les temps seront cherches
%		-si T=[a b];=> a et b sont les bornes temporelles
%	SS > 1 	=> sous echantillonage de facteur SS si T=10 ou T=[a b]
%	Les 24 lignes de ECE correspondent aux frequences d'emissions
%	classees par ordre croissant de frequence d'emission.
%
%	Blanchard  10.02.2000

%------------------------------------------------------------------------------
%	INFORMATION
%
%	Pour toutes les matrices, on aura la convention suivante:
%	Les signaux temporels des 24 cannaux seront les 24 lignes,
%------------------------------------------------------------------------------
if shot>=20055

	[K,L]=ece_conf(shot,1);
	ece_d;
	[Fcentral, Rcentral,No]=ece_mode(K,1.5);
	[t,i1,i2]=common_ece(No(:,1),D(:,1));
	D=D(i2,:);[D]=sortrows(D,1);
	E=D(:,2);E(:,2)=[1:24]';[E]=sortrows(E,1);

	%------------------------------------------------------------------------------
	%	Appel des valeurs dans MDS
	%	La matrice ECE a 24 lignes correspondant aux 24 cannaux
	%	
	%	La matrice est classee par ordre croissant des frequences cesntrales
	%	des divers cannaux
	%------------------------------------------------------------------------------
	if length(T)==2
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('dim_of(\atlas::dt100_northwest_001:channel_012)');   

	   X=[1:SS:length(Tece)]';
	   Tece=Tece(X);clear X

	   del=Tece(2)-Tece(1);
	   NN=Tece(1:max(find(Tece<0))); 
	   N=find(Tece>=T(1) & Tece<=T(2));
	   Tece=Tece(N); 
	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_northwest_001:channel_012[$]',Tece);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_northwest_001:channel_011[$]',Tece);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_northwest_001:channel_010[$]',Tece);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_northwest_001:channel_009[$]',Tece);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_northwest_001:channel_008[$]',Tece);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_northwest_001:channel_007[$]',Tece);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_northwest_001:channel_006[$]',Tece);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_northwest_001:channel_005[$]',Tece);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_northwest_001:channel_004[$]',Tece);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_northwest_001:channel_003[$]',Tece);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_northwest_001:channel_002[$]',Tece);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_northwest_001:channel_001[$]',Tece);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_northwest_001:channel_017[$]',Tece);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_northwest_001:channel_018[$]',Tece);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_northwest_001:channel_019[$]',Tece);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_northwest_001:channel_020[$]',Tece);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_northwest_001:channel_021[$]',Tece);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_northwest_001:channel_022[$]',Tece);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_northwest_001:channel_023[$]',Tece);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_northwest_001:channel_024[$]',Tece);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_northwest_001:channel_025[$]',Tece);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_northwest_001:channel_026[$]',Tece);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_northwest_001:channel_027[$]',Tece);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_northwest_001:channel_028[$]',Tece);
	   M=zeros(length(NN),24);
	   M(:,E(1,2))=mdsdata('\atlas::dt100_northwest_001:channel_012[$]',NN);
	   M(:,E(2,2))=mdsdata('\atlas::dt100_northwest_001:channel_011[$]',NN);
	   M(:,E(3,2))=mdsdata('\atlas::dt100_northwest_001:channel_010[$]',NN);
	   M(:,E(4,2))=mdsdata('\atlas::dt100_northwest_001:channel_009[$]',NN);
	   M(:,E(5,2))=mdsdata('\atlas::dt100_northwest_001:channel_008[$]',NN);
	   M(:,E(6,2))=mdsdata('\atlas::dt100_northwest_001:channel_007[$]',NN);
	   M(:,E(7,2))=mdsdata('\atlas::dt100_northwest_001:channel_006[$]',NN);
	   M(:,E(8,2))=mdsdata('\atlas::dt100_northwest_001:channel_005[$]',NN);
	   M(:,E(9,2))=mdsdata('\atlas::dt100_northwest_001:channel_004[$]',NN);
	   M(:,E(10,2))=mdsdata('\atlas::dt100_northwest_001:channel_003[$]',NN);
	   M(:,E(11,2))=mdsdata('\atlas::dt100_northwest_001:channel_002[$]',NN);
	   M(:,E(12,2))=mdsdata('\atlas::dt100_northwest_001:channel_001[$]',NN);
	   M(:,E(13,2))=mdsdata('\atlas::dt100_northwest_001:channel_017[$]',NN);
	   M(:,E(14,2))=mdsdata('\atlas::dt100_northwest_001:channel_018[$]',NN);
	   M(:,E(15,2))=mdsdata('\atlas::dt100_northwest_001:channel_019[$]',NN);
	   M(:,E(16,2))=mdsdata('\atlas::dt100_northwest_001:channel_020[$]',NN);
	   M(:,E(17,2))=mdsdata('\atlas::dt100_northwest_001:channel_021[$]',NN);
	   M(:,E(18,2))=mdsdata('\atlas::dt100_northwest_001:channel_022[$]',NN);
	   M(:,E(19,2))=mdsdata('\atlas::dt100_northwest_001:channel_023[$]',NN);
	   M(:,E(20,2))=mdsdata('\atlas::dt100_northwest_001:channel_024[$]',NN);
	   M(:,E(21,2))=mdsdata('\atlas::dt100_northwest_001:channel_025[$]',NN);
	   M(:,E(22,2))=mdsdata('\atlas::dt100_northwest_001:channel_026[$]',NN);
	   M(:,E(23,2))=mdsdata('\atlas::dt100_northwest_001:channel_027[$]',NN);
	   M(:,E(24,2))=mdsdata('\atlas::dt100_northwest_001:channel_028[$]',NN);
	   mdsclose
	   N=min(find(Tece>0-del&Tece<0+del));
	   M=mean(M);
	   ECE=ECE-repmat(M,length(Tece),1);

	elseif T==10
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('dim_of(\atlas::dt100_northwest_001:channel_012)');   

	   X=[1:SS:length(Tece)]';
	   Tece=Tece(X);clear X

	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_northwest_001:channel_012[$]',Tece);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_northwest_001:channel_011[$]',Tece);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_northwest_001:channel_010[$]',Tece);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_northwest_001:channel_009[$]',Tece);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_northwest_001:channel_008[$]',Tece);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_northwest_001:channel_007[$]',Tece);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_northwest_001:channel_006[$]',Tece);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_northwest_001:channel_005[$]',Tece);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_northwest_001:channel_004[$]',Tece);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_northwest_001:channel_003[$]',Tece);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_northwest_001:channel_002[$]',Tece);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_northwest_001:channel_001[$]',Tece);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_northwest_001:channel_017[$]',Tece);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_northwest_001:channel_018[$]',Tece);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_northwest_001:channel_019[$]',Tece);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_northwest_001:channel_020[$]',Tece);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_northwest_001:channel_021[$]',Tece);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_northwest_001:channel_022[$]',Tece);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_northwest_001:channel_023[$]',Tece);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_northwest_001:channel_024[$]',Tece);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_northwest_001:channel_025[$]',Tece);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_northwest_001:channel_026[$]',Tece);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_northwest_001:channel_027[$]',Tece);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_northwest_001:channel_028[$]',Tece);
	   mdsclose
	   del=Tece(2)-Tece(1);
	   N=min(find(Tece>0-del&Tece<0+del));
	   M=mean(ECE([1:N],:));
	   ECE=ECE-repmat(M,length(Tece),1);

	elseif T~=10 | length(T)>2
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('dim_of(\atlas::dt100_northwest_001:channel_012)');   
	   
	   NN=Tece(1:max(find(Tece<0))); 
	   Tece=mdsdata('dim_of(\atlas::dt100_northwest_001:channel_012[$])',T);
	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_northwest_001:channel_012[$]',T);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_northwest_001:channel_011[$]',T);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_northwest_001:channel_010[$]',T);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_northwest_001:channel_009[$]',T);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_northwest_001:channel_008[$]',T);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_northwest_001:channel_007[$]',T);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_northwest_001:channel_006[$]',T);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_northwest_001:channel_005[$]',T);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_northwest_001:channel_004[$]',T);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_northwest_001:channel_003[$]',T);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_northwest_001:channel_002[$]',T);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_northwest_001:channel_001[$]',T);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_northwest_001:channel_017[$]',T);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_northwest_001:channel_018[$]',T);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_northwest_001:channel_019[$]',T);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_northwest_001:channel_020[$]',T);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_northwest_001:channel_021[$]',T);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_northwest_001:channel_022[$]',T);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_northwest_001:channel_023[$]',T);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_northwest_001:channel_024[$]',T);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_northwest_001:channel_025[$]',T);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_northwest_001:channel_026[$]',T);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_northwest_001:channel_027[$]',T);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_northwest_001:channel_028[$]',T);
	   M=zeros(length(NN),24);
	   M(:,E(1,2))=mdsdata('\atlas::dt100_northwest_001:channel_012[$]',NN);
	   M(:,E(2,2))=mdsdata('\atlas::dt100_northwest_001:channel_011[$]',NN);
	   M(:,E(3,2))=mdsdata('\atlas::dt100_northwest_001:channel_010[$]',NN);
	   M(:,E(4,2))=mdsdata('\atlas::dt100_northwest_001:channel_009[$]',NN);
	   M(:,E(5,2))=mdsdata('\atlas::dt100_northwest_001:channel_008[$]',NN);
	   M(:,E(6,2))=mdsdata('\atlas::dt100_northwest_001:channel_007[$]',NN);
	   M(:,E(7,2))=mdsdata('\atlas::dt100_northwest_001:channel_006[$]',NN);
	   M(:,E(8,2))=mdsdata('\atlas::dt100_northwest_001:channel_005[$]',NN);
	   M(:,E(9,2))=mdsdata('\atlas::dt100_northwest_001:channel_004[$]',NN);
	   M(:,E(10,2))=mdsdata('\atlas::dt100_northwest_001:channel_003[$]',NN);
	   M(:,E(11,2))=mdsdata('\atlas::dt100_northwest_001:channel_002[$]',NN);
	   M(:,E(12,2))=mdsdata('\atlas::dt100_northwest_001:channel_001[$]',NN);
	   M(:,E(13,2))=mdsdata('\atlas::dt100_northwest_001:channel_017[$]',NN);
	   M(:,E(14,2))=mdsdata('\atlas::dt100_northwest_001:channel_018[$]',NN);
	   M(:,E(15,2))=mdsdata('\atlas::dt100_northwest_001:channel_019[$]',NN);
	   M(:,E(16,2))=mdsdata('\atlas::dt100_northwest_001:channel_020[$]',NN);
	   M(:,E(17,2))=mdsdata('\atlas::dt100_northwest_001:channel_021[$]',NN);
	   M(:,E(18,2))=mdsdata('\atlas::dt100_northwest_001:channel_022[$]',NN);
	   M(:,E(19,2))=mdsdata('\atlas::dt100_northwest_001:channel_023[$]',NN);
	   M(:,E(20,2))=mdsdata('\atlas::dt100_northwest_001:channel_024[$]',NN);
	   M(:,E(21,2))=mdsdata('\atlas::dt100_northwest_001:channel_025[$]',NN);
	   M(:,E(22,2))=mdsdata('\atlas::dt100_northwest_001:channel_026[$]',NN);
	   M(:,E(23,2))=mdsdata('\atlas::dt100_northwest_001:channel_027[$]',NN);
	   M(:,E(24,2))=mdsdata('\atlas::dt100_northwest_001:channel_028[$]',NN);
	   mdsclose
	   M=mean(M);
	   ECE=ECE-repmat(M,length(Tece),1);
	% SS=0;
	end
	%------------------------------------------------------------------------------
	%	Correction des mauvais cannaux : met les cannaux non-existant a NaN
	%------------------------------------------------------------------------------
	[N]=ece_bad_channels(shot);
	disp(['Pour le shot # ',num2str(shot),' les cannaux ECE qui ne vont pas sont: ',num2str(N)])
	%N=input('Introduire les cannaux qui ne vont pas')
	[t,i1,i2,i3,i4,i5,i6]=common_ece(E(N,2),[1:24]);
	ECE=ECE(:,i5);Fcentral=Fcentral(i5);

else

	[K,L]=ece_conf(shot,1);
	ece_d;
	[Fcentral, Rcentral,No]=ece_mode(K,1.5);
	[t,i1,i2]=common_ece(No(:,1),D(:,1));
	D=D(i2,:);[D]=sortrows(D,1);
	E=D(:,2);E(:,2)=[1:24]';[E]=sortrows(E,1);

	%------------------------------------------------------------------------------
	%	Appel des valeurs dans MDS
	%	La matrice ECE a 24 lignes correspondant aux 24 cannaux
	%	
	%	La matrice est classee par ordre croissant des frequences cesntrales
	%	des divers cannaux
	%------------------------------------------------------------------------------
	if length(T)==2
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('\atlas::dt100_003:fast:time');
	   
	   X=[1:SS:length(Tece)]';
	   Tece=Tece(X);clear X

	   del=Tece(2)-Tece(1);
	   NN=Tece(1:max(find(Tece<0))); 
	   N=find(Tece>=T(1) & Tece<=T(2));
	   Tece=Tece(N); 
	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_003:fast:channel_012[$]',Tece);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_003:fast:channel_011[$]',Tece);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_003:fast:channel_010[$]',Tece);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_003:fast:channel_009[$]',Tece);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_003:fast:channel_008[$]',Tece);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_003:fast:channel_007[$]',Tece);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_003:fast:channel_006[$]',Tece);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_003:fast:channel_005[$]',Tece);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_003:fast:channel_004[$]',Tece);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_003:fast:channel_003[$]',Tece);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_003:fast:channel_002[$]',Tece);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_003:fast:channel_001[$]',Tece);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_003:fast:channel_017[$]',Tece);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_003:fast:channel_018[$]',Tece);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_003:fast:channel_019[$]',Tece);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_003:fast:channel_020[$]',Tece);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_003:fast:channel_021[$]',Tece);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_003:fast:channel_022[$]',Tece);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_003:fast:channel_023[$]',Tece);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_003:fast:channel_024[$]',Tece);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_003:fast:channel_025[$]',Tece);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_003:fast:channel_026[$]',Tece);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_003:fast:channel_027[$]',Tece);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_003:fast:channel_028[$]',Tece);
	   M=zeros(length(NN),24);
	   M(:,E(1,2))=mdsdata('\atlas::dt100_003:fast:channel_012[$]',NN);
	   M(:,E(2,2))=mdsdata('\atlas::dt100_003:fast:channel_011[$]',NN);
	   M(:,E(3,2))=mdsdata('\atlas::dt100_003:fast:channel_010[$]',NN);
	   M(:,E(4,2))=mdsdata('\atlas::dt100_003:fast:channel_009[$]',NN);
	   M(:,E(5,2))=mdsdata('\atlas::dt100_003:fast:channel_008[$]',NN);
	   M(:,E(6,2))=mdsdata('\atlas::dt100_003:fast:channel_007[$]',NN);
	   M(:,E(7,2))=mdsdata('\atlas::dt100_003:fast:channel_006[$]',NN);
	   M(:,E(8,2))=mdsdata('\atlas::dt100_003:fast:channel_005[$]',NN);
	   M(:,E(9,2))=mdsdata('\atlas::dt100_003:fast:channel_004[$]',NN);
	   M(:,E(10,2))=mdsdata('\atlas::dt100_003:fast:channel_003[$]',NN);
	   M(:,E(11,2))=mdsdata('\atlas::dt100_003:fast:channel_002[$]',NN);
	   M(:,E(12,2))=mdsdata('\atlas::dt100_003:fast:channel_001[$]',NN);
	   M(:,E(13,2))=mdsdata('\atlas::dt100_003:fast:channel_017[$]',NN);
	   M(:,E(14,2))=mdsdata('\atlas::dt100_003:fast:channel_018[$]',NN);
	   M(:,E(15,2))=mdsdata('\atlas::dt100_003:fast:channel_019[$]',NN);
	   M(:,E(16,2))=mdsdata('\atlas::dt100_003:fast:channel_020[$]',NN);
	   M(:,E(17,2))=mdsdata('\atlas::dt100_003:fast:channel_021[$]',NN);
	   M(:,E(18,2))=mdsdata('\atlas::dt100_003:fast:channel_022[$]',NN);
	   M(:,E(19,2))=mdsdata('\atlas::dt100_003:fast:channel_023[$]',NN);
	   M(:,E(20,2))=mdsdata('\atlas::dt100_003:fast:channel_024[$]',NN);
	   M(:,E(21,2))=mdsdata('\atlas::dt100_003:fast:channel_025[$]',NN);
	   M(:,E(22,2))=mdsdata('\atlas::dt100_003:fast:channel_026[$]',NN);
	   M(:,E(23,2))=mdsdata('\atlas::dt100_003:fast:channel_027[$]',NN);
	   M(:,E(24,2))=mdsdata('\atlas::dt100_003:fast:channel_028[$]',NN);
	   mdsclose
	   N=min(find(Tece>0-del&Tece<0+del));
	   M=mean(M);
	   ECE=ECE-repmat(M,length(Tece),1);

	elseif T==10
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('\atlas::dt100_003:fast:time');

	   X=[1:SS:length(Tece)]';
	   Tece=Tece(X);clear X

	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_003:fast:channel_012[$]',Tece);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_003:fast:channel_011[$]',Tece);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_003:fast:channel_010[$]',Tece);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_003:fast:channel_009[$]',Tece);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_003:fast:channel_008[$]',Tece);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_003:fast:channel_007[$]',Tece);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_003:fast:channel_006[$]',Tece);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_003:fast:channel_005[$]',Tece);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_003:fast:channel_004[$]',Tece);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_003:fast:channel_003[$]',Tece);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_003:fast:channel_002[$]',Tece);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_003:fast:channel_001[$]',Tece);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_003:fast:channel_017[$]',Tece);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_003:fast:channel_018[$]',Tece);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_003:fast:channel_019[$]',Tece);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_003:fast:channel_020[$]',Tece);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_003:fast:channel_021[$]',Tece);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_003:fast:channel_022[$]',Tece);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_003:fast:channel_023[$]',Tece);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_003:fast:channel_024[$]',Tece);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_003:fast:channel_025[$]',Tece);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_003:fast:channel_026[$]',Tece);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_003:fast:channel_027[$]',Tece);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_003:fast:channel_028[$]',Tece);
	   mdsclose
	   del=Tece(2)-Tece(1);
	   N=min(find(Tece>0-del&Tece<0+del));
	   M=mean(ECE([1:N],:));
	   ECE=ECE-repmat(M,length(Tece),1);

	elseif T~=10 | length(T)>2
	   mdsopen('tcv_shot',shot)
	   Tece=mdsdata('\atlas::dt100_003:fast:time');
	   NN=Tece(1:max(find(Tece<0))); 
	   Tece=mdsdata('dim_of(\atlas::dt100_003:fast:channel_012[$])',T);
	   ECE=zeros(length(Tece),24);
	   ECE(:,E(1,2))=mdsdata('\atlas::dt100_003:fast:channel_012[$]',T);
	   ECE(:,E(2,2))=mdsdata('\atlas::dt100_003:fast:channel_011[$]',T);
	   ECE(:,E(3,2))=mdsdata('\atlas::dt100_003:fast:channel_010[$]',T);
	   ECE(:,E(4,2))=mdsdata('\atlas::dt100_003:fast:channel_009[$]',T);
	   ECE(:,E(5,2))=mdsdata('\atlas::dt100_003:fast:channel_008[$]',T);
	   ECE(:,E(6,2))=mdsdata('\atlas::dt100_003:fast:channel_007[$]',T);
	   ECE(:,E(7,2))=mdsdata('\atlas::dt100_003:fast:channel_006[$]',T);
	   ECE(:,E(8,2))=mdsdata('\atlas::dt100_003:fast:channel_005[$]',T);
	   ECE(:,E(9,2))=mdsdata('\atlas::dt100_003:fast:channel_004[$]',T);
	   ECE(:,E(10,2))=mdsdata('\atlas::dt100_003:fast:channel_003[$]',T);
	   ECE(:,E(11,2))=mdsdata('\atlas::dt100_003:fast:channel_002[$]',T);
	   ECE(:,E(12,2))=mdsdata('\atlas::dt100_003:fast:channel_001[$]',T);
	   ECE(:,E(13,2))=mdsdata('\atlas::dt100_003:fast:channel_017[$]',T);
	   ECE(:,E(14,2))=mdsdata('\atlas::dt100_003:fast:channel_018[$]',T);
	   ECE(:,E(15,2))=mdsdata('\atlas::dt100_003:fast:channel_019[$]',T);
	   ECE(:,E(16,2))=mdsdata('\atlas::dt100_003:fast:channel_020[$]',T);
	   ECE(:,E(17,2))=mdsdata('\atlas::dt100_003:fast:channel_021[$]',T);
	   ECE(:,E(18,2))=mdsdata('\atlas::dt100_003:fast:channel_022[$]',T);
	   ECE(:,E(19,2))=mdsdata('\atlas::dt100_003:fast:channel_023[$]',T);
	   ECE(:,E(20,2))=mdsdata('\atlas::dt100_003:fast:channel_024[$]',T);
	   ECE(:,E(21,2))=mdsdata('\atlas::dt100_003:fast:channel_025[$]',T);
	   ECE(:,E(22,2))=mdsdata('\atlas::dt100_003:fast:channel_026[$]',T);
	   ECE(:,E(23,2))=mdsdata('\atlas::dt100_003:fast:channel_027[$]',T);
	   ECE(:,E(24,2))=mdsdata('\atlas::dt100_003:fast:channel_028[$]',T);
	   M=zeros(length(NN),24);
	   M(:,E(1,2))=mdsdata('\atlas::dt100_003:fast:channel_012[$]',NN);
	   M(:,E(2,2))=mdsdata('\atlas::dt100_003:fast:channel_011[$]',NN);
	   M(:,E(3,2))=mdsdata('\atlas::dt100_003:fast:channel_010[$]',NN);
	   M(:,E(4,2))=mdsdata('\atlas::dt100_003:fast:channel_009[$]',NN);
	   M(:,E(5,2))=mdsdata('\atlas::dt100_003:fast:channel_008[$]',NN);
	   M(:,E(6,2))=mdsdata('\atlas::dt100_003:fast:channel_007[$]',NN);
	   M(:,E(7,2))=mdsdata('\atlas::dt100_003:fast:channel_006[$]',NN);
	   M(:,E(8,2))=mdsdata('\atlas::dt100_003:fast:channel_005[$]',NN);
	   M(:,E(9,2))=mdsdata('\atlas::dt100_003:fast:channel_004[$]',NN);
	   M(:,E(10,2))=mdsdata('\atlas::dt100_003:fast:channel_003[$]',NN);
	   M(:,E(11,2))=mdsdata('\atlas::dt100_003:fast:channel_002[$]',NN);
	   M(:,E(12,2))=mdsdata('\atlas::dt100_003:fast:channel_001[$]',NN);
	   M(:,E(13,2))=mdsdata('\atlas::dt100_003:fast:channel_017[$]',NN);
	   M(:,E(14,2))=mdsdata('\atlas::dt100_003:fast:channel_018[$]',NN);
	   M(:,E(15,2))=mdsdata('\atlas::dt100_003:fast:channel_019[$]',NN);
	   M(:,E(16,2))=mdsdata('\atlas::dt100_003:fast:channel_020[$]',NN);
	   M(:,E(17,2))=mdsdata('\atlas::dt100_003:fast:channel_021[$]',NN);
	   M(:,E(18,2))=mdsdata('\atlas::dt100_003:fast:channel_022[$]',NN);
	   M(:,E(19,2))=mdsdata('\atlas::dt100_003:fast:channel_023[$]',NN);
	   M(:,E(20,2))=mdsdata('\atlas::dt100_003:fast:channel_024[$]',NN);
	   M(:,E(21,2))=mdsdata('\atlas::dt100_003:fast:channel_025[$]',NN);
	   M(:,E(22,2))=mdsdata('\atlas::dt100_003:fast:channel_026[$]',NN);
	   M(:,E(23,2))=mdsdata('\atlas::dt100_003:fast:channel_027[$]',NN);
	   M(:,E(24,2))=mdsdata('\atlas::dt100_003:fast:channel_028[$]',NN);
	   mdsclose
	   M=mean(M);
	   ECE=ECE-repmat(M,length(Tece),1);
	% SS=0;
	end
	%------------------------------------------------------------------------------
	%	Correction des mauvais cannaux : met les cannaux non-existant a NaN
	%------------------------------------------------------------------------------
	[N]=ece_bad_channels(shot);
	disp(['Pour le shot # ',num2str(shot),' les cannaux ECE qui ne vont pas sont: ',num2str(N)])
	%N=input('Introduire les cannaux qui ne vont pas')
	[t,i1,i2,i3,i4,i5,i6]=common_ece(E(N,2),[1:24]);
	ECE=ECE(:,i5);Fcentral=Fcentral(i5);



end

