function [RHO,R,Z,tpsi,Fcentral]=ece_rho(shot,t)

%	[RHO,R,Z,tbtot,Fcentral]=ece_rho(shot,t)
%
%	Fonction calculant les rayons normalise rho pour l'ECE
%	pour tous les temps tpsi de psitbx appartenant a t=[a b]
%	Si t=10 => tous les temps seront recherche.
%	Pour le calcul de Btot, on utilise une routine de O.Sauter
%	intitulee BandBres_allt.m
%
%	rho = rayon normalise pour la ligne de visee de l'ECE
%	R   = Grand rayon du tore correspondant a RHO
%		

mdsopen('tcv_shot',shot)
L=mdsdata('GETNCI("\\results::ece:rho","length")');
LL=mdsdata('GETNCI("\\results::psi_axis:foo","length")');
mdsclose
if LL==0
 disp('----------------------------------')
 disp('\results::psi_axis not calculated')
 disp('----------------------------------')
 return 
end

[W]=writeece;
if L==0 | L==28 |W==1
	disp(' La trace \results::ece:rho est vide ');

	[K,L]=ece_conf(shot,1);
	if L==1
	 	Z=0;
	elseif L==2
	 	Z=0.21;
	end
	[Fcentral,Rcentral,No]=ece_mode(K,1.45); 
	tt=[0:0.05:3];
	psi=psitbxtcv(shot,tt,'01');
	tpsi=psi.psitbxfun.t;
	if length(t)==2
		 if min(tpsi)>t(2)
			  disp('-------------------------------')
			  disp('No Psitbx for these times')
			  disp('-------------------------------')
			  return
		 end
	end
	
	if length(tpsi)==0
		 disp('psitbxtcv(shot,tt,''01'') est vide');
		 return
	end
	
	[Rbtot,Zbtot,BTOT,ttt]=bandbres_allt(shot,tpsi,Fcentral*1e9,2,[]);
	
	%-----------------------------------------------------------------
	%	definition de la ligne de visee en r,z,phi de longueur 50
	j=50;
	r=linspace(min(Rcentral)*0.9,max(Rcentral)*1.1,j);
	rzphiline={r,Z,NaN};
	GridEceCyl=psitbxgrid('Cylindrical','Grid',rzphiline);%clear R Z rzphiline K L No
	%-----------------------------------------------------------------
	%	definition de la grille de psi en rho,theta,phi
	%psi=psitbxtcv(shot,tbtot,'01');
	%tpsi=psi.psitbxfun.t; 
	%GridFluxPsi=psitbxgrid('Flux','Grid','Default',psi);  
	GridBtotCyl=psitbxgrid('Cylindrical','Grid',{Rbtot,Zbtot,NaN});
	%-----------------------------------------------------------------
	%	definition de la ligne de visee en rho,theta,phi
	GridEcePsi=psitbxg2g(GridEceCyl,'Flux',psi);  
	%-----------------------------------------------------------------
	%	Calcul de Btot sur la grille GridFluxPsi
	%BTOT=reshape(BTOT,41,129,length(tbtot));
	FTOT=2*1.6022e-19*BTOT/9.1094e-31/2/pi/1e9;clear BTOT Rbtot Zbtot
	FtotFun=psitbxfun(FTOT,GridBtotCyl,tpsi);clear FTOT
	%-----------------------------------------------------------------
	%	Calcul de Ftot sur la grille GridEcePsi
	FtotLineEcePsi=psitbxf2f(FtotFun,GridEceCyl);   
	FtotLineEce=FtotLineEcePsi.x; 
	FtotLineEce(find(isnan(FtotLineEce)))=0;
	T=repmat(tpsi,1,j)';r=repmat(r,length(tpsi),1)';
	rho=GridEcePsi.x{1};
	%error('err')
	for i=1:length(tpsi)
	 	jj=find(FtotLineEce(:,i)~=0);
		R(:,i)=interp1(FtotLineEce(jj,i),r(jj,i),Fcentral);
		RHO(:,i)=interp1(r(jj,i),rho(jj,1,i),R(:,i));
        end
        Z=Z*ones(size(R));
	if W==2
		
       	else
	       mdsopen('results',shot)   
	       mdsput('\results::ece:rho',RHO,'f');
	       mdsput('\results::ece:r',R,'f');
	       mdsput('\results::ece:z',Z,'f');
	       mdsput('\results::ece:times',tpsi,'f');
	       mdsclose
	       disp('On a remplis les matrices \results::ece:rho,r,z,times')
	       disp(['Size(RHO) = ',num2str(size(RHO))])
	end
       
else
      disp('On vas chercher les traces \results::ece:rho,z,r,times dans MDS')
      mdsopen('results',shot)   
      RHO=mdsdata('\results::ece:rho');
      R=mdsdata('\results::ece:r');
      Z=mdsdata('\results::ece:z');
      tbtot=mdsdata('\results::ece:times');
      mdsclose
      [K,L]=ece_conf(shot,1);
      [Fcentral, Rcentral,No]=ece_mode(K,1.5);
      Z=mean(mean(Z))*ones(size(R));
      tpsi=tbtot;
end

if t~=10
	NT=find(tpsi>t(1)-0.005  & tpsi <t(2)+0.005);
	if isempty(NT)
		NT=max(find(tpsi<t(2)));
		disp('Nous n''avons aucun points de T_psi compris dans le vecteur t')
		disp('Nous avons pris le temps le plus proche')
	elseif NT(1)==1 & length(NT)~=length(tpsi)
		NT=[1:max(NT)];
	elseif length(NT)~=length(tpsi)
		NT=[min(NT):max(NT)];
	end
	RHO=RHO(:,NT);R=R(:,NT);tpsi=tpsi(NT);Z=Z(:,NT);
end
