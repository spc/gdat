function mapping = tcv_requests_mapping(data_request,shot)
%
% Information pre-defined for gdat_tcv, you can add cases here to match official cases in gdat_tcv, allowing backward compatibility
%
% give the shot number in case data origin depends on the shot number, allows to adapt easily
%

% Defaults
mapping = struct(...
    'label', '', ...
    'method', '', ...
    'expression','', ...
    'timedim', -1, ...     % dim which is the time is the database, to copy in .t, the other dims are in .x (-1 means last dimension)
    'gdat_timedim',[], ...  % if need to reshape data and dim orders to have timedim as gdat_timedim (shifting time to gdat_timedim)
    'min', -inf, ...
    'max', inf);
% Note that gdat_timedim is set to timedim at end of this function if empty
% gdat_timedim should have effective index of the time dimension in gdat

if ~exist('data_request') || isempty(data_request)
  return
end

% default label: data_request keyword itself
mapping.label = data_request;

% for TCV, following choices are set so far:
% method = 'tdi' and then expression is the string within tdi (usual case when there is a direct link to an existing signal)
%                with tdi, if expression cell array, call tdi(cell{1},cell{2},...)
% method = 'tdiliuqe': same as tdi but adds "_2" or "_3" if 'liuqe',2 or 3 is asked for in options
% method = 'expression', then expression is executed and it should provide the structure gdat_tmp, which fields are copied to gdat_data
% method = 'switchcase', then there will be a specific case within gdat_tcv (usual case when not directly a signal)
%
% label is used for plotting
if iscell(data_request)
  mapping.label = data_request;
  mapping.method = 'tdi'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
  mapping.expression = data_request;
  return
end
switch lower(data_request)
 case 'a_minor'
  mapping.timedim = 1;
  mapping.label = 'a(LCFS)';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'a_minor_rho'
  mapping.timedim = 2;
  mapping.label = 'a(rho,t)';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'area'
  mapping.timedim = 1;
  mapping.label = 'area';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("area","LIUQE.M")';
 case 'area_rho'
  mapping.timedim = 2;
  mapping.label = 'area_rho';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("area_rho","LIUQE.M")';
 case 'area_edge'
  mapping.timedim = 1;
  mapping.label = 'area\_lcfs';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("area_edge","LIUQE.M")';
 case 'b0'
  mapping.timedim = 1;
  mapping.label = 'B_0';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'bfields'
  mapping.timedim = 1;
  mapping.label = 'bfields';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case {'beta', 'beta_tor'}
  mapping.timedim = 1;
  mapping.label = '\beta';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::beta_tor';
  mapping.expression = 'tcv_eq("beta_tor","LIUQE.M")';
 case {'betan', 'beta_tor_norm'}
  mapping.timedim = 1;
  mapping.label = '\beta_N';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case {'beta_pol', 'betap'}
  mapping.timedim = 1;
  mapping.label = '\beta_p';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::beta_pol';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:beta_pol';
  mapping.expression = 'tcv_eq("beta_pol","LIUQE.M")';
 case 'cxrs'
  mapping.timedim = 2;
  mapping.label = 'cxrs';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'cxrs_rho'
  mapping.timedim = 2;
  mapping.label = 'cxrs';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'delta'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::delta_edge';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:delta_edge';
  mapping.expression = 'tcv_eq("delta_edge","LIUQE.M")';
  % mapping.method = 'expression';
  % mapping.expression = ['tdi(''\results::q_psi'');']; % for tests
 case 'delta_bottom'
  mapping.timedim = 1;
  mapping.label = 'delta\_bottom';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::delta_ed_bot';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:delta_bot';
  mapping.expression = 'tcv_eq("delta_ed_bot","LIUQE.M")';
 case 'delta_rho'
  mapping.timedim = 2;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:delta';
  mapping.expression = 'tcv_eq("delta","LIUQE.M")';
 case 'delta_top'
  mapping.timedim = 1;
  mapping.label = 'delta\_top';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::delta_ed_top';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:delta_top';
  mapping.expression = 'tcv_eq("delta_ed_top","LIUQE.M")';
 case 'ece'
  mapping.timedim = 2;
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'eqdsk'
  mapping.timedim = 0;
  mapping.method = 'switchcase'; % could use function make_eqdsk directly?
  mapping.expression = '';
 case 'expcode'
  mapping.timedim = 0;
  mapping.method = 'tdi';
  mapping.expression = '\rtc::node03.params.cfg_file';
 case {'gaps', 'gap_inout', 'gap_out', 'gap_in', 'gap_lfs','gap_hfs'}
  mapping.timedim = 2;
  mapping.label = 'gap hfs/lfs';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("r_contour","LIUQE.M")'';' ...
                   'gdat_tmp=gdat_tcv([],params_eff);aa_data(1,:)=min(gdat_tmp.data,[],1)-0.624;aa_data(1,[aa_data(1,:)<0]) = 0.;' ...
                    'aa_data(2,:)=1.1376-max(gdat_tmp.data,[],1);aa_data(2,[aa_data(2,:)<0]) = 0.;gdat_tmp.data=aa_data;gdat_tmp.x=[1:2];gdat_tmp.dim{1}=gdat_tmp.x;' ...
                   'gdat_tmp.dimunits{1}={''HFS'',''LFS''};gdat_tmp.units=''m'';'];
 case {'gas', 'gas_flux', 'gas_request', 'gas_feedforward','gas_valve', 'gas_fluxes'}
  mapping.timedim = 1;
  mapping.label = 'gas flux';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'halpha'
  mapping.timedim = 1;
  mapping.label = 'Halpha';
  mapping.method = 'tdi';
  mapping.expression = '\base::pd:pd_001';
 case 'halphas'
  mapping.timedim = 1;
  mapping.label = 'Halpha';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'h89p'
  mapping.timedim = 1;
  mapping.label = 'H89P';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''\tcv_shot::top.results.conf:tau'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''\tcv_shot::top.results.conf:taue'';' ...
                    'gdat_tmp2=gdat_tcv(shot,params_eff);'...
                    'params_eff.data_request=''\tcv_shot::top.results.conf:h_scal'';' ...
                    'gdat_tmp3=gdat_tcv(shot,params_eff);ih89p=4;' ...
                    'ih89p=5;if size(gdat_tmp3.data,1)>=ih89p;gdat_tmp.data = gdat_tmp.data./gdat_tmp2.data .* gdat_tmp3.data(ih89p,:)'';end;' ...
                    'gdat_tmp.units='''';gdat_tmp.dimunits{1}=''s'';'];
 case 'h98y2'
  mapping.timedim = 1;
  mapping.label = 'H98y2';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''\tcv_shot::top.results.conf:tau'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''\tcv_shot::top.results.conf:taue'';' ...
                    'gdat_tmp2=gdat_tcv(shot,params_eff);'...
                    'params_eff.data_request=''\tcv_shot::top.results.conf:h_scal'';' ...
                    'gdat_tmp3=gdat_tcv(shot,params_eff);ih98y2=5;' ...
                    'ih98y2=5;if size(gdat_tmp3.data,1)>=ih98y2;gdat_tmp.data = gdat_tmp.data./gdat_tmp2.data .* gdat_tmp3.data(ih98y2,:)'';end;' ...
                    'gdat_tmp.units='''';gdat_tmp.dimunits{1}=''s'';'];
% $$$  case 'icds'
% $$$   mapping.timedim = 1;
% $$$   mapping.label = 'various driven current';
% $$$   mapping.method = 'switchcase';
 case 'ids'
  mapping.timedim = 1;
  mapping.label = 'ids ala imas';
  mapping.method = 'switchcase';
 case 'ioh'
  mapping.timedim = 1;
  mapping.label = 'I ohmic transformer';
  mapping.method = 'tdi';
  mapping.expression = [{'\magnetics::ipol[*,$1]'} {'OH_001'}];
 case 'ip_trapeze'
  mapping.timedim = 1;
  mapping.label = 'Plasma current';
  mapping.method = 'tdi';
  mapping.expression = '\magnetics::iplasma:trapeze';
 case 'ip'
  mapping.timedim = 1;
  mapping.label = 'Plasma current';
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("i_pl","LIUQE.M")'; % to be able to get ip consistent with relevant LIUQE value
 case 'i_pol'
  disp('use ''ipol'' to get values from magnetics');
  mapping.timedim = 2;
  mapping.method = 'expression';
  mapping.label = 'ipol from liuqe';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("i_pol","LIUQE.M")''; ' ...
                    'gdat_tmp=gdat_tcv([],params_eff);coilsname=mdsvalue(''dim_of(\magnetics::ipol,1)''); ' ...
                    'gdat_tmp.dimunits{1}=coilsname([3:18 20 1:2]);'];
 case 'ipol'
  disp('use ''i_pol'' to get values from liuqe');
  mapping.timedim = 2; % changed to match usual time as last one and match liuqe i_pol
  mapping.method = 'expression';
  mapping.label = 'ipol from magnetics';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''\magnetics::ipol''; ' ...
                    'gdat_tmp=gdat_tcv([],params_eff);gdat_tmp.dimunits{2}=''s'';gdat_tmp.dimunits{1}=gdat_tmp.dim{2};' ...
                    'gdat_tmp.dim{2}=gdat_tmp.dim{1};gdat_tmp.dim{1}=[0:numel(gdat_tmp.dimunits{1})-1]'';gdat_tmp.data=gdat_tmp.data'''];
 case 'kappa'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::kappa_edge';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:kappa_edge';
  mapping.expression = 'tcv_eq("kappa_edge","LIUQE.M")';
 case 'kappa_rho'
  mapping.timedim = 2;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:kappa';
  mapping.expression = 'tcv_eq("kappa","LIUQE.M")';
 case 'li'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::l_i';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:l_i_3';
  mapping.expression = 'tcv_eq("l_i_3","LIUQE.M")';
 case 'mhd'
  mapping.timedim = 1;
  mapping.label = 'n=1,2, etc';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'ne'
  mapping.timedim = 2;
  mapping.method = 'switchcase';
 case 'neint'
  mapping.timedim = 1;
  mapping.label = 'line integrated el. density';
  mapping.method = 'tdi';
  mapping.expression = '\results::fir:lin_int_dens';
 case 'neints'
  mapping.timedim = 1;
  mapping.label = 'Array line integrated el. density';
  mapping.method = 'tdi';
  mapping.expression = '\tcv_shot::top.results.fir.fir_array:lin_int_dens';
 case 'nel'
  mapping.timedim = 1;
  mapping.label = 'line-averaged el. density';
  mapping.method = 'switchcase';
  mapping.expression = '\results::fir:n_average';
 case 'nels'
  mapping.timedim = 1;
  mapping.label = 'Array line-averaged el. density';
  mapping.method = 'tdi';
  mapping.expression = '\tcv_shot::top.results.fir.fir_array:n_average';
 case 'ne_rho'
  mapping.timedim = 2;
  mapping.label = 'ne';
  mapping.method = 'switchcase';
 case 'neft'
  mapping.timedim = 2;
  mapping.label = 'ne';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''ne_rho''; ' ...
                    'params_eff.fit=1;params_eff.fit_type=''avg'';gdat_tmp=gdat_tcv([],params_eff);'];
 case 'nete_rho'
  mapping.timedim = 2;
  mapping.label = 'ne and Te';
  mapping.method = 'switchcase';
 case 'ni'
  mapping.timedim = 2;
  mapping.method = 'switchcase'; % especially since might have option fit, etc
 case {'ng','ngreenwald','n_greenwald'}
  mapping.timedim = 1;
  mapping.label = 'nG=Ip[MA]/(\pi a^2)*1e20 on nel times';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''ip'';' ...
                    'gdat_tmp2=gdat_tcv(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'if numel(gdat_tmp2.t)>1;tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'else;tmp_data2=gdat_tmp2.data;end;' ...
                    'params_eff.data_request=''a_minor'';' ...
                    'gdat_tmp3=gdat_tcv(shot,params_eff);ij=find(gdat_tmp3.data==0);gdat_tmp3.data(ij)=NaN;' ...
                    'if numel(gdat_tmp3.t)>1;tmp_data3=interp1(gdat_tmp3.t,gdat_tmp3.data,gdat_tmp.t,[],NaN);' ...
                    'else;tmp_data3=gdat_tmp3.data;end;' ...
                    'gdat_tmp.data = abs(tmp_data2*1e-6./pi./(tmp_data3.^2+1e-5));' ...
                    'ij=find(gdat_tmp.data<0 | gdat_tmp.data>4);gdat_tmp.data(ij)=NaN;gdat_tmp.data = gdat_tmp.data * 1e20;'];
 case {'ngf','greenwald_fraction','f_greenwald','ng_fraction'}
  mapping.timedim = 1;
  mapping.label = 'Greenwald\_fraction=n\_el/n\_G';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''n_greenwald'';' ...
                    'gdat_tmp2=gdat_tcv(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'if numel(gdat_tmp2.t)>1;tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'else;tmp_data2=gdat_tmp2.data;end;' ...
                    'gdat_tmp.ng=gdat_tmp2;gdat_tmp.ng.data_t_nel=tmp_data2;' ...
                    'gdat_tmp.data = gdat_tmp.data./(tmp_data2+1e-5);' ...
                    'gdat_tmp.units='''';gdat_tmp.dimunits{1}=''s'';'];
 case {'ec_data', 'aux', 'h_cd', 'nbi_data', 'ic_data', 'lh_data', 'ohm_data', 'bs_data'}
  mapping.timedim = 2;
  mapping.label = 'various Pdens, Icd, jcd';
  mapping.method = 'switchcase';
 case {'phi_tor', 'phitor', 'toroidal_flux'}
  mapping.timedim = 2;
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("tor_flux_tot","LIUQE.M")';
  % node not filled in and in any case need special case for
  mapping.label = 'toroidal_flux';
  mapping.method = 'switchcase';
 case {'p_lh', 'plh', 'plh_s'}
  mapping.label = 'P\_LH threshold with S';
  mapping.timedim = 1;
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''b0'';gdat_tmpb0=gdat_tcv(shot,params_eff);' ...
                    'params_eff.data_request=''surface_edge'';gdat_tmp_s=gdat_tcv(shot,params_eff);' ...
                     's_nel=interpos(gdat_tmp_s.t,gdat_tmp_s.data,gdat_tmp.t,-1.);b0_nel=interpos(gdat_tmpb0.t,gdat_tmpb0.data,gdat_tmp.t,-1.);' ...
                    'gdat_tmp.data = 0.0488e6.*(gdat_tmp.data/1e20).^0.717.*abs(b0_nel).^0.803.*s_nel.^0.941;' ...
                    'gdat_tmp.units=''W'';gdat_tmp.dimunits{1}=''s'';' ...
                    'gdat_tmp.help=''Uses Eq(2) from Y R Martin et al 2008 J. Phys.: Conf. Ser. 123 012033 with toroidal Surface. Request "plh_a_r" to get Eq(3) with a and R0 instead'';'];
 case {'p_lh_a_r', 'plh_a_r'}
  mapping.label = 'P\_LH threshold with a, R0';
  mapping.timedim = 1;
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff);params_eff.data_request=''b0'';gdat_tmpb0=gdat_tcv(shot,params_eff);' ...
                    'params_eff.data_request=''a_minor'';gdat_tmp_a=gdat_tcv(shot,params_eff);' ...
                     'a_nel=interpos(gdat_tmp_a.t,gdat_tmp_a.data,gdat_tmp.t,-1.);b0_nel=interpos(gdat_tmpb0.t,gdat_tmpb0.data,gdat_tmp.t,-1.);' ...
                    'gdat_tmp.data = 2.15e6.*(gdat_tmp.data/1e20).^0.782.*abs(b0_nel).^0.772.*a_nel.^0.975.*gdat_tmpb0.r0.^0.999;' ...
                    'gdat_tmp.units=''W'';gdat_tmp.dimunits{1}=''s'';' ...
                    'gdat_tmp.help=''Uses Eq(3) from Y R Martin et al 2008 J. Phys.: Conf. Ser. 123 012033 with a and R0. Request "plh" to get Eq(2) with toroidal Surface instead'';'];

 case 'psi'
  mapping.timedim = 1;
  mapping.label = 'psi(R,Z)';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("psi","LIUQE.M")';
 case 'powers'
  mapping.timedim = 1;
  mapping.label = 'various powers';
  mapping.method = 'switchcase';
 case {'pprime', 'pprime_rho'}
  mapping.timedim = 2;
  mapping.label = 'pprime';
  mapping.method = 'switchcase';
 case {'pressure', 'p_rho', 'pressure_rho'} % note: not pressure from liuqe fortran which is 2D
  mapping.timedim = 2;
  mapping.label = 'pressure';
  mapping.method = 'switchcase';
 case {'psi_axis', 'psi_mag'}
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::psi_axis';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:psi_axis';
  mapping.expression = 'tcv_eq("psi_axis","LIUQE.M")';
  mapping.label = 'psi\_axis with psi_edge=0';
 case 'psi_edge'
  mapping.timedim = 1;
  mapping.method = 'switchcase'; % should be psi_edge(t) so to add to 0 of standard LCFS in LIUQE
  mapping.label = 'psi\_edge';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::surface_flux';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:psi_surf';
  mapping.expression = 'tcv_eq("psi_surf","LIUQE.M")';
 case 'q0'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::q_zero';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:q_axis';
  mapping.expression = 'tcv_eq("q_axis","LIUQE.M")';
 case 'q95'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  % mapping.expression = '\results::q_95';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:q_95';
  mapping.expression = 'tcv_eq("q_95","LIUQE.M")';
 case 'qedge'
  mapping.timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::q_edge';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:q_edge';
  mapping.expression = 'tcv_eq("q_edge","LIUQE.M")';
 case 'q_rho'
  mapping.timedim = 2;
  mapping.label = 'q';
  mapping.method = 'switchcase';
 case {'rbphi_rho', 'rbtor_rho'}
  mapping.timedim = 2;
  mapping.label = 'R B_\phi';
  mapping.method = 'switchcase';
% $$$  case 'r_contour'   % r_rho not yet implemented
% $$$   mapping.timedim = 2;
% $$$   mapping.method = 'tdiliuqe';
% $$$   mapping.expression = '\results::r_contour';
% $$$   mapping.expression = '\tcv_shot::top.results.equil_1.results:r_rho'; % several flux surfaces R coordinates (irho,itheta,t)
% $$$   mapping.expression = 'tcv_eq("r_rho","LIUQE.M")';
 case 'r_contour_edge'
  mapping.timedim = 2;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::r_contour';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:r_surf'; % LCFS R coordinates (r,t)
  mapping.expression = 'tcv_eq("r_edge","LIUQE.M")';

  mapping.label = 'R\_lcfs';
  mapping.method = 'switchcase';

% $$$   mapping.method = 'expression';
% $$$   mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("r_edge","LIUQE.M")''; ' ...
% $$$                     'gdat_tmp=gdat_tcv(shot,params_eff);gdat_tmp.data=gdat_tmp.data(:,:,end);' ...
% $$$                     'gdat_tmp.dim=gdat_tmp.dim(1:2);gdat_tmp.dimunits=gdat_tmp.dimunits(1:2);'];
 case {'rgeom', 'r_geom'}
  mapping.timedim = 1;
  mapping.label = 'Rgeom';
  mapping.method = 'switchcase';
 case {'rgeom_rho', 'r_geom_rho'}
  mapping.timedim = 2;
  mapping.label = 'Rgeom';
  mapping.method = 'switchcase';
 case 'rhotor_edge'
  mapping.timedim = 1;
  mapping.label = 'rhotor\_edge=sqrt(Phi/pi/B0)';
  mapping.method = 'switchcase'; % from conf if exist otherwise computes it
 case 'rhotor_norm'
  mapping.timedim = 2;
  mapping.label = 'rhotor\_norm';
  mapping.method = 'switchcase'; % from conf if exist otherwise computes it
 case 'rhotor'
  mapping.timedim = 2;
  mapping.label = 'rhotor';
  mapping.method = 'switchcase'; % from conf if exist otherwise computes it
 case {'rhovol', 'rho_vol'}
  mapping.timedim = 2;
  mapping.label = 'rhovol\_norm';
  mapping.method = 'switchcase'; % from conf if exist otherwise computes it
 case {'rmag', 'r_axis', 'r_mag'}
  mapping.timedim = 1;
  mapping.label = 'R\_magaxis';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::r_axis';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:r_axis';
  mapping.expression = 'tcv_eq("r_axis","LIUQE.M")';
 case {'r_xpts', 'rxpts', 'r_xpt'}
  mapping.timedim = 2;
  mapping.label = 'R\_Xpts';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("r_xpts","LIUQE.M")';
 case {'rtc','scd'}
  if strcmp('scd',lower(data_request)); error('********* should call with request****** ''rtc'''); end
  mapping.timedim = 1;
  mapping.label = 'scd\_mems\_signals';
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'surface_rho'
  mapping.timedim = 1;
  mapping.label = 'surface\_tor(rho)';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("surf","LIUQE.M")';
  mapping.help = 'toroidal surface'; % works for method tdi..., in method expression, gdat_tmp.help should be defined
 case {'surface', 'surface_edge'}
  mapping.timedim = 1;
  mapping.label = 'surface';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("surf","LIUQE.M")'';' ...
                    'gdat_tmp=gdat_tcv(shot,params_eff); gdat_tmp.dim = {gdat_tmp.t}; gdat_tmp.x=[]; gdat_tmp.data= gdat_tmp.data(end,:);' ...
                    'gdat_tmp.dimunits{1}=''s'';gdat_tmp.help=''toroidal surface of LCFS'';'];
 case {'sxr', 'mpx', 'radcam'}
  mapping.timedim = 1;
  mapping.gdat_timedim = 2;
  mapping.method = 'switchcase';
 case 'tau_r'
  mapping.timedim = 1;
  mapping.gdat_timedim = 1;
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''a_minor''; ' ...
                    'gdat_tmp=gdat_tcv([],params_eff); ' ...
                    'params_eff = gdat_data.gdat_params;params_eff.data_request=''\results::ibs:signeo''; ' ...
                    'aa=gdat_tcv([],params_eff);ix=iround_os(aa.x,0.4); ' ...
                    'bb=interpos(aa.t,aa.data(ix,:),gdat_tmp.t,-100);' ...
                    'gdat_tmp.data = 4e-7*pi.*gdat_tmp.data.^2.*bb./1.22; gdat_tmp.label=''\tau_R'';gdat_tmp.gdat_request=''tau_r'';' ...
                    'gdat_tmp.units=''s'';gdat_tmp.help=''tauR=mu0 a^2 / 1.22 etaneo(rhopol=0.4)'''];
 case 'tau_tot'
  mapping.timedim = 1;
  mapping.label = 'total energy confinment time';
  mapping.method = 'tdi';
  mapping.expression = '\results::conf:tau';
 case 'te'
  mapping.timedim = 2;
  mapping.label = 'Te';
  mapping.method = 'switchcase';
 case 'te_rho'
  mapping.timedim = 2;
  mapping.label = 'Te';
  mapping.method = 'switchcase';
 case 'teft'
  mapping.timedim = 2;
  mapping.label = 'te fit';
  mapping.method = 'expression';
  mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''te_rho''; ' ...
                    'params_eff.fit=1;params_eff.fit_type=''avg'';gdat_tmp=gdat_tcv([],params_eff);'];
 case 'ti'
  mapping.timedim = 2;
  mapping.label = 'Ti';
  mapping.method = 'switchcase';
 case 'transp'
  mapping.label = 'transp output from netcdf file';
  mapping.method = 'switchcase';
 case {'ttprime', 'ttprime_rho'}
  mapping.timedim = 2;
  mapping.label = 'ttprime';
  mapping.method = 'switchcase';
 case 'vloop'
  mapping.timedim = 1;
  mapping.label = 'Vloop';
  mapping.method = 'tdi';
  mapping.expression = [{'\magnetics::vloop[*,$1]'} {'001'}];
 case 'volume'
  mapping.timedim = 1;
  mapping.label = 'Volume\_LCFS';
  mapping.method = 'switchcase';
 case 'volume_rho'
  mapping.timedim = 2;
  mapping.label = 'Volume(\rho)';
  mapping.method = 'switchcase';
  % mapping.expression = '\results::psitbx:vol'; (if exists for liuqe2 and 3 as well)
 case {'wmhd', 'w_mhd'}
  mapping.timedim = 1;
% $$$   if shot==57716 || shot==57732
% $$$     time_for_corr = [0.5,1.03];
% $$$     corr = [4e3,4.5e3];
% $$$     if shot==57732; corr=[4650,4650]; end
% $$$     mapping.method = 'expression';
% $$$     mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("w_mhd","LIUQE.M")'';' ...
% $$$                     'gdat_tmp=gdat_tcv(shot,params_eff);ij=find(gdat_tmp.t>0.5&gdat_tmp.t<1.03);' ...
% $$$                     'aa=interp1([' num2str(time_for_corr(1)) ' ' num2str(time_for_corr(2)) ...
% $$$                     '],[' num2str(corr(1)) ' ' num2str(corr(2)) '],gdat_tmp.t(ij));' ...
% $$$           'gdat_tmp.data(ij)=max(gdat_tmp.data(ij),aa);'];
% $$$   else
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::total_energy';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:w_mhd';
  mapping.expression = 'tcv_eq("w_mhd","LIUQE.M")';
% $$$   end
% $$$  case 'z_contour'   % z_rho not yet implemented
% $$$   mapping.timedim = 2;
% $$$   mapping.method = 'tdiliuqe';
% $$$   mapping.expression = '\results::z_contour';
% $$$   mapping.expression = '\tcv_shot::top.results.equil_1.results:z_rho'; % several flux surfaces Z coordinates (irho,itheta,t)
% $$$   mapping.expression = 'tcv_eq("z_rho","LIUQE.M")';
 case 'z_contour_edge'
  mapping.timedim = 2;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::z_contour';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:z_surf'; % LCFS Z coordinates (r,t)
  mapping.expression = 'tcv_eq("z_edge","LIUQE.M")';

  mapping.label = 'Z\_lcfs';
  mapping.method = 'switchcase';

% $$$   mapping.method = 'expression';
% $$$   mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''tcv_eq("z_edge","LIUQE.M")''; ' ...
% $$$                     'gdat_tmp=gdat_tcv(shot,params_eff);gdat_tmp.data=gdat_tmp.data(:,:,end);' ...
% $$$                     'gdat_tmp.dim=gdat_tmp.dim(1:2);gdat_tmp.dimunits=gdat_tmp.dimunits(1:2);'];
 case 'zeff'
  mapping.timedim = 1;
  mapping.label = 'zeff from Ip-Ibs';
  mapping.method = 'tdi';
  mapping.expression = '\results::ibs:z_eff';
 case {'zgeom', 'z_geom'}
  mapping.timedim = 1;
  mapping.label = 'Zgeom';
  mapping.method = 'switchcase';
 case {'zmag', 'z_mag', 'z_axis'}
  mapping.timedim = 1;
  mapping.label = 'Zmagaxis';
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::z_axis';
  mapping.expression = '\tcv_shot::top.results.equil_1.results:z_axis';
  mapping.expression = 'tcv_eq("z_axis","LIUQE.M")';
 case {'z_xpts', 'zxpts', 'z_xpt'}
  mapping.timedim = 2;
  mapping.label = 'Z\_Xpts';
  mapping.method = 'tdiliuqe';
  mapping.expression = 'tcv_eq("z_xpts","LIUQE.M")';
  %
  % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % extra TCV cases (not necessarily in official data_request name list)
  % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
% $$$  case '\results::total_energy'
% $$$   mapping.timedim = 1;
% $$$   mapping.gdat_timedim = 1;
% $$$   mapping.method = 'tdiliuqe';
% $$$   % mapping.expression = '\results::total_energy:foo';
% $$$   mapping.expression = '\results::total_energy';
% $$$  case '\results::thomson:psiscatvol'
% $$$   mapping.timedim = 1;
% $$$   mapping.gdat_timedim = 1;
% $$$   mapping.method = 'tdiliuqe';
% $$$   % mapping.expression = '\results::thomson:psiscatvol:foo';
% $$$   mapping.expression = '\results::thomson:psiscatvol';
 case {'profnerho','profterho'}
  mapping.timedim = 1;
  mapping.label = data_request;
  mapping.method = 'switchcase';
  mapping.expression = '';
 case 'xte'
  mapping.timedim = 1;
  mapping.gdat_timedim = 1;
  mapping.method = 'tdiliuqe';
  mapping.expression = '\results::te_x_a[*,12]';
  mapping.method = 'expression';
  mapping.expression = ['if shot > 78662, disp(''no XTE anymore'');gdat_tmp.data=[];return;end;params_eff = gdat_data.gdat_params;params_eff.data_request=''\results::te_x_a[*,12]''; ' ...
                    'gdat_tmp=gdat_tcv([],params_eff); ' ...
                    'params_eff = gdat_data.gdat_params;params_eff.data_request=''\magnetics::iplasma:trapeze''; ' ...
                    'aa=gdat_tcv([],params_eff);it=find(abs(aa.data)<10e3);it2=iround_os(gdat_tmp.t,aa.t(it));gdat_tmp.data(it2)=NaN;' ...
                    'gdat_tmp.units=''eV'';gdat_tmp.dimunits{1}=''s'';'];

% $$$  case ''
% $$$   mapping.timedim = 1;
% $$$   mapping.label = data_request;
% $$$   mapping.method = 'tdi';
% $$$   mapping.expression = '';
 otherwise
  mapping.label = data_request;
  mapping.method = 'tdi'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
  mapping.expression = data_request;

end

if isempty(mapping.gdat_timedim)
  mapping.gdat_timedim = mapping.timedim;
end
