function [TE_ECE,TE_ECE_ERR,RHO,R,T,TE_THOM,TE_THOM_ERR,Fcentral,CAL,NE_THOM]=ece_te(shot,Tc,T,SS)

%	[TE_ECE,TE_ECE_ERR,RHO,R,T,TE_THOM,TE_THOM_ERR,Fcentral,CAL,NE_THOM]=ece_te(shot,Tc,T,SS)
%	[TE_ECE,TE_ECE_ERR,RHO,R,T,TE_THOM,TE_THOM_ERR,Fcentral,CAL,NE_THOM]=ece_te(19314,[0.1 0.29],[0 1.5],10);
%	
%	Program that compute TE_ECE and TE_THOM profile over R or RHO
%	for all the times T with the error matrices TE_ECE_ERR and TE_THOM_ERR.
%	The calibration matrice CAL comes from the ece_calib.m program.
%	Fcentral is the central frequency of the ECE working channels.		
%	
%	shot 	= shot number
%	Tc	= [a b] temporal boundary on which we want the calibration on 
%		thomson temperature. If you have no idea, put Tc=[0.1 0.29]
%	T	= either
%		-temporal vector
%		-if T=10 => takes all the ECE times
%		-if T=[c d];=> c and d are the temporal boundary for the output
%	SS >= 1 	=> sampling rate SS if T=10 or T=[c d]
%
%	B est calcule avec la routine BandBres_allt de O.Sauter
%	
%	Blanchard  25.11.2000


%-----------------------------------------------------------------------
%	Recherche de la configuration de l'ECE
%-----------------------------------------------------------------------
[RHOece,Rece,Zece,Tece,Fcentralrho]=ece_rho(shot,T);
[CAL,Fcentral]=ece_calib(shot,Tc);

%-----------------------------------------------------------------------
%	Recherches des signaux voulus
%-----------------------------------------------------------------------
[ECE,TECE,Fcentral]=ece_raw_signals(shot,T,SS);
%[RHOece,Rece,Zece,Tece, Fcentralrho]=ece_rho(shot,T);
%Zece=Zece*ones(size(Rece));
[TEthom,NEthom,TEerr,NEerr,Tthom,RHOthom]=thom_rho(shot,6);
if exist('TEthom')==0|length(TEthom) ==0
   disp('Le profil Thomson proffit n''existe pas. On prend le profil direct') %(Tthom,Nbre de pts selon rho)

  [TEthom,NEthom,TEerr,NEerr,Tthom,RHOthom]=thom_rho(shot,3); %(Tthom,Nbre de pts selon rho)
end
[t2,i3,i4]=common_ece(Fcentralrho,Fcentral);
Rece=Rece(i3,:);Zece=Zece(i3,:);RHOece=RHOece(i3,:);
TE_ECE=ECE.*repmat(CAL(2,:),length(TECE),1);
T=repmat(TECE,1,length(Fcentral));
if length(Tece)==1
  RHO=repmat(RHOece,1,length(TECE));RHO=RHO';	
  R=repmat(Rece,1,length(TECE));R=R';
  X=size(RHOthom);
else
  %RHO=interp2(repmat(Fcentral,1,length(Tece))',repmat(Tece,length(Fcentral),1)', ...
  %	    RHOece',repmat(Fcentral',length(TECE),1)',T');
   RHO=interp2(repmat(Fcentral,1,length(Tece))',repmat(Tece',length(Fcentral),1)', ...
	    RHOece',repmat(Fcentral',length(TECE),1)',T');
  RHO=RHO';		
  R=interp2(repmat(Fcentral,1,length(Tece))',repmat(Tece',length(Fcentral),1)', ...
	    Rece',repmat(Fcentral',length(TECE),1),T);
end
 
X=size(RHOthom);
TE_THOM=griddata(repmat(Tthom,1,X(2)),RHOthom,TEthom,T,RHO);
NE_THOM=griddata(repmat(Tthom,1,X(2)),RHOthom,NEthom,T,RHO);
TE_THOM_ERR=griddata(repmat(Tthom,1,X(2)),RHOthom,TEerr,T,RHO);
TE_ECE_ERR=CAL(4,:);T=T(:,1);


