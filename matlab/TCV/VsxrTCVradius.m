function  radius=VsxrTCVradius(ymag,xchord,ychord)

% get intersection of magnetics axis with chords
% input:
% Vsawtooth.shot : shot number
% Vsawtooth.expdata.zmag : positions of the magnetic a 
%        xchord:  two x-coordinates.
%        ychord:  two y-coordinates. 
%                 For each line (2xnl), they specify start + end pointsxis

% Output :
% Vsxr.radius : intersectionof magnetics axis with chords

%parameter: start and end of each of the camera number 2


% calculation intersections
xchord=xchord/100;
ychord=ychord/100;
for i=1:size(xchord,2)
  a=(ychord(1,i)-ychord(2,i))/(xchord(1,i)-xchord(2,i));
  b=ychord(2,i)-a*xchord(2,i);
  radius(:,i)= (ymag-b)/(a+eps);
end
