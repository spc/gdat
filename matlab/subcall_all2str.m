function subcall_string = subcall_all2str(varargin)
%
% subcall_string = subcall_all2str(varargin)
%
% create a string from varargin allowing following types:
%
% char: varargin is just copied
% numeric: if scalar use num2str, if 1 or 2-D array add relevant '[' and ']', else mark a string multi-D
% structure: explode structure as field,value
%

% to be taken from above and include this call in above

if nargin==0
  subcall_string = '';
  return
end

subcall = '';
for i_in=1:length(varargin)
  var_to_treat = varargin{i_in};
  if isempty(var_to_treat)
    subcall = [subcall ',[]'];
  elseif isstruct(var_to_treat)
    % explode structure into 'childname',childvalues
    param_names = fieldnames(var_to_treat);
    for i=1:length(param_names)
      subcall = [subcall ',''' param_names{i} ''''];
      if ischar(var_to_treat.(param_names{i}))
        subcall = [subcall ',''' var_to_treat.(param_names{i}) ''''];
      elseif isnumeric(var_to_treat.(param_names{i}))
        aa_values = var_to_treat.(param_names{i});
        if prod(size(aa_values))~= length(aa_values)
          % multi-D input, do not treat it yet
          subcall = [subcall ',''multi-D input'''];
        elseif numel(aa_values) > 1
          % array
          if numel(aa_values) <=30
            subcall = [subcall ',[' num2str(reshape(aa_values,1,numel(aa_values))) ']'];
          else
            subcall = [subcall ',[' num2str(reshape(aa_values(1:4),1,4)) ' ... ' num2str(reshape(aa_values(end-3:end),1,4)) ']'];
          end
        else
          subcall = [subcall ',' num2str(aa_values) ''];
        end
      else
        % to treat extra cases
      end
    end
  elseif isnumeric(var_to_treat)
    if prod(size(var_to_treat))~= length(var_to_treat)
      % multi-D input, do not treat it yet
      subcall = [subcall ',''multi-D input'''];
    elseif numel(var_to_treat) > 1
      % array
      if numel(var_to_treat) <=30
        subcall = [subcall ',[' num2str(reshape(var_to_treat,1,numel(var_to_treat))) ']'];
      else
        subcall = [subcall ',[' num2str(reshape(var_to_treat(1:4),1,4)) ' ... ' num2str(reshape(var_to_treat(end-3:end),1,4)) ']'];
      end
    else
      subcall = [subcall ',' num2str(var_to_treat) ''];
    end
  elseif ischar(var_to_treat)
    subcall = [subcall ',''' var_to_treat ''''];
  elseif iscell(var_to_treat)
    subcall = [subcall ',' cell2str(var_to_treat,3) ''];
  else
    warning('in subcall_all2str: case not foreseen');
  end
end

subcall_string = subcall;
