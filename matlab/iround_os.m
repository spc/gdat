function  [is,sigi]=iround_os(sig,val)
%
% Given two arrays sig and val, for each element in val
% returns the index and value of the nearest element in sig.
%
% sig and/or val can be non-monotonic (contrary to TCV iround which requires sig to be monotonic)
%
% Example:
%
% >> [i,sigi]=iround_os(sig,val);
%
%
%
is = zeros(size(val));
for j=1:numel(val)
  [~,is(j)]=min(abs(sig-val(j)));
end
sigi=sig(is);
