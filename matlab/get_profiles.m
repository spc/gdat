function [profile_struct,varargout] = get_profiles(shot,data_request_in,varargin);
%
% data_request_in: 1 or several of the optional keywords related to the machine options for profiles
%               for example: 'te_thomson', or {'te_thomson','te_ece'}, or {'ne_thomson','te_thomson'}
%               (case insensitive)
%
% varargin in pairs:
%      'equil',equil_source: equilibrium source: EQI(default), EQH, etc for AUG or LIUQE1 (default), LIUQE2, etc for TCV
%      'machine', machine_name: 'TCV', 'AUG' (default is the default in gdat, thus local machine)
%
% output profile_struct is an array of structure for each requested keyword:
%
% profile_struct{i}.data, .t, .error_bar, .label, .shot, .keyword, .provenance, .comment, .q (q profile)
% profile_struct{i}.grid.rhotornorm, .rhopolnorm, .rhovolnorm, .rovera, .psi, .phi, .vol, .provenance
%
% profile_struct = get_profiles; % returns the list of possible keywords in profile_struct{i}.keyword
%

error_status = -1;
% default output structure
i_init=1;
profile_struct{i_init}.data = [];
profile_struct{i_init}.t = [];
profile_struct{i_init}.error_bar = [];
profile_struct{i_init}.label = [];
profile_struct{i_init}.shot = [];
profile_struct{i_init}.keyword = [];
profile_struct{i_init}.provenance = [];
profile_struct{i_init}.comment = [];
profile_struct{i_init}.q = [];
profile_struct{i_init}.grid.rhotornorm = [];
profile_struct{i_init}.grid.rhopolnorm = [];
profile_struct{i_init}.grid.rhovolnorm = [];
profile_struct{i_init}.grid.rovera = [];
profile_struct{i_init}.grid.psi = [];
profile_struct{i_init}.grid.phi = [];
profile_struct{i_init}.grid.vol = [];
profile_struct{i_init}.grid.provenance = [];
% default param
get_profiles_params.machine = [];
get_profiles_params.equil = [];
get_profiles_params.nverbose = [];
get_profiles_params.help.machine = 'machine to get data, aug or tcv at this stage';
get_profiles_params.help.equil = 'equilibrium source, use default of gdat as default';
get_profiles_params.help.nverbose = ...
    'level of information written during the call to get_profiles, default=1, up to warnings but no details';
profile_struct{i_init}.profiles_params = get_profiles_params;


% defaults:
shot_eff = [];
if exist('shot') && ~isempty(shot); shot_eff = shot; end
data_request_eff = [];
if exist('data_request_in') && ~isempty(data_request_in)
  if ischar(data_request_in)
    data_request_eff{1} = data_request_in;
  else
    data_request_eff = data_request_in;
  end
  if ~iscell(data_request_eff)
    disp('problem with data_request_in, expects string or cell of strings')
    error_status=-2;
    return
  end
end

% get default machine
aa=gdat;
get_profiles_params.machine = aa.gdat_params.machine;
get_profiles_params.equil = aa.gdat_params.equil;
get_profiles_params.nverbose=1;

% extract parameters from pairs of varargin:
if nargin>=3
  if mod(nargin-2,2)==0
    for i=1:2:length(varargin)-1
      if ischar(varargin{i})
        % enforce lower case for any character driven input
        if ischar(varargin{i+1})
          get_profiles_params.(lower(varargin{i})) = lower(varargin{i+1});
        else
          get_profiles_params.(lower(varargin{i})) = varargin{i+1};
        end
      else
        if get_profiles_params.nverbose>=1; warning(['varargin input argument nb: ' num2str(i) ...
                    ' (nargin=' num2str(i+2) ') is incorrect, expects a character string']);
        end
        error_status=-11;
        return
      end
    end
  else
    if get_profiles_params.nverbose>=1; ...
          warning('number of varargin input arguments incorrect, cannot make pairs of parameters');
    end
    error_status=-12;
    return
  end
end
profile_struct{i_init}.profiles_params = get_profiles_params;

% list of keywords
keywords={'te_thomson','ne_thomson','vrot_cxrs','ni_cxrs','ti_cxrs','vpol_cxrs'};
keywords_tcv_only = [];
keywords_aug_only = {'te_ece','te_ida','ne_ida'};
if strcmp(get_profiles_params.machine,'tcv')
  if ~isempty(keywords_tcv_only); keywords(end+1:end+length(keywords_tcv_only)) = keywords_tcv_only; end
elseif strcmp(get_profiles_params.machine,'aug')
  if ~isempty(keywords_aug_only); keywords(end+1:end+length(keywords_aug_only)) = keywords_aug_only; end
else
  if get_profiles_params.nverbose>=1
    disp(['warning machine = ' get_profiles_params.machine ' is not known'])
    return
  end
end
keywords = sort(keywords);

if isempty(data_request_eff)
  profile_struct{i_init}.keyword = keywords;
  return
end

% fill in default structure for each data_requested keyword, ignore unexpected keywords
ieff = 0;
data_request_eff = sort(data_request_eff);
for i=1:length(data_request_eff)
  if ~isempty(strmatch(data_request_eff{i},keywords,'exact'))
    ieff = ieff + 1;
    if ieff>1
      profile_struct{ieff} = profile_struct{1};
    end
    profile_struct{ieff}.keyword = data_request_eff{i};
    profile_struct{ieff}.shot = shot_eff;
  else
    if get_profiles_params.nverbose>=1
      disp(['warning data_request = ' data_request_eff{i} ' is not available and ignored, ask O. Sauter'])
    end
  end
end

if ieff==0
  profile_struct{i_init}.keyword = keywords;
  return
end

% Now can fill in data using gdat calls (meaning that gdat calls should be more and more standardized as well to this structure)
% should use only profile_struct{ieff} to determine what to get, in order to make sure it is self-consistent and fully described

for i=1:length(profile_struct)
  try
    profile_struct{i} = get_profile_structure(profile_struct{i});
  catch
    if get_profiles_params.nverbose>=1
      disp(['could not get_profile_structure for ' profile_struct{i}.keyword ', shot = ' num2str(profile_struct{i}.shot)]);
    end
  end
end
