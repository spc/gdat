function tests_matlab(test_case)
try
   fprintf('\n Running test file: %s\n',mfilename('fullpath'));
   fprintf('     Time: %s\n',datestr(now));
   passed = run_gdat_tests(test_case); % call to your test script here, with optional test_case input
   exit_code = int32(~passed); % convert to bash shell convention
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
