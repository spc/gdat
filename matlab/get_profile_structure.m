function [profile_structs_out,error_status] = get_profile_structure(profile_struct_in);
%
% get data from self-description in single profile_struct_in following description in get_profiles.m
%
% profile_struct_in, input single structure, and profile_structs_out, output array of structure
%

error_status=-1001;

profile_struct_def = profile_struct_in;

data_request = profile_struct_def.keyword;
shot = profile_struct_def.shot;

i_structs_out = 0;
switch lower(data_request)
 case {'ne_thomson','te_thomson','nete_thomson'}
  i_underscore = findstr(data_request,'_');
  data_request_gdat = [data_request(1:i_underscore-1) '_rho'];
  aa=gdat(shot,data_request_gdat,'fit',1,'equil',profile_struct_def.profiles_params.equil);
  if strcmp(data_request(1:i_underscore-1),'nete')
    % extract ne and te parts into several profile_struct
    to_extract = {'ne','te'};
  else
    to_extract = {data_request(1:i_underscore-1)};
  end
  for i=1:length(to_extract)
    i_structs_out = i_structs_out + 1;
    profile_structs_out{i_structs_out} = profile_struct_def;
    if strcmp(profile_struct_def.profiles_params.machine,'tcv')
      profile_structs_out{i_structs_out}.keyword = [to_extract{i} '_thomson'];
      profile_structs_out{i_structs_out}.provenance = aa.data_fullpath;
      profile_structs_out{i_structs_out}.gdat_params = aa.gdat_params;
      profile_structs_out{i_structs_out}.data = aa.(to_extract{i}).data;
      profile_structs_out{i_structs_out}.t = aa.(to_extract{i}).t;
      profile_structs_out{i_structs_out}.error_bar = aa.(to_extract{i}).error_bar;
      profile_structs_out{i_structs_out}.units = aa.(to_extract{i}).units;
      profile_structs_out{i_structs_out}.label = [to_extract{i} '\_thomson'];
      profile_structs_out{i_structs_out}.grid.rhotornorm = aa.grids_1d.rhotornorm;
      profile_structs_out{i_structs_out}.grid.rhopolnorm = aa.grids_1d.rhopolnorm;
      profile_structs_out{i_structs_out}.grid.rhovolnorm = aa.grids_1d.rhovolnorm;
      profile_structs_out{i_structs_out}.grid.psi = aa.grids_1d.psi;
    elseif strcmp(profile_struct_def.profiles_params.machine,'aug')
    % for AUG, extract core and edge parts into separate structures
    % core
      profile_structs_out{i_structs_out}.keyword = [to_extract{i} '_thomson_core'];
      profile_structs_out{i_structs_out}.provenance = aa.data_fullpath;
      profile_structs_out{i_structs_out}.gdat_params = aa.gdat_params;
      profile_structs_out{i_structs_out}.data = aa.(to_extract{i}).core.data;
      profile_structs_out{i_structs_out}.t = aa.(to_extract{i}).core.t;
      profile_structs_out{i_structs_out}.error_bar = aa.(to_extract{i}).core.error_bar;
      profile_structs_out{i_structs_out}.units = aa.(to_extract{i}).units;
      profile_structs_out{i_structs_out}.label = [to_extract{i} '\_thomson\_core'];
      profile_structs_out{i_structs_out}.grid.rhotornorm = aa.(to_extract{i}).core.rhotornorm;
      profile_structs_out{i_structs_out}.grid.rhopolnorm = aa.(to_extract{i}).core.rhopolnorm;
      profile_structs_out{i_structs_out}.grid.rhovolnorm = aa.(to_extract{i}).core.rhovolnorm;
      profile_structs_out{i_structs_out}.grid.psi = aa.(to_extract{i}).core.psi;
      % edge
      i_structs_out = i_structs_out + 1;
      profile_structs_out{i_structs_out} = profile_struct_def;
      profile_structs_out{i_structs_out}.keyword = [to_extract{i} '_thomson_edge'];
      profile_structs_out{i_structs_out}.provenance = aa.data_fullpath;
      profile_structs_out{i_structs_out}.gdat_params = aa.gdat_params;
      profile_structs_out{i_structs_out}.data = aa.(to_extract{i}).edge.data;
      profile_structs_out{i_structs_out}.t = aa.(to_extract{i}).edge.t;
      profile_structs_out{i_structs_out}.error_bar = aa.(to_extract{i}).edge.error_bar;
      profile_structs_out{i_structs_out}.units = aa.(to_extract{i}).units;
      profile_structs_out{i_structs_out}.label = [to_extract{i} '\_thomson\_edge'];
      profile_structs_out{i_structs_out}.grid.rhotornorm = aa.(to_extract{i}).edge.rhotornorm;
      profile_structs_out{i_structs_out}.grid.rhopolnorm = aa.(to_extract{i}).edge.rhopolnorm;
      profile_structs_out{i_structs_out}.grid.rhovolnorm = aa.(to_extract{i}).edge.rhovolnorm;
      profile_structs_out{i_structs_out}.grid.psi = aa.(to_extract{i}).edge.psi;
    else
      disp(['In get_profile_structures: not ready yet for machine = ' profile_struct_def.profiles_params.machine]);
    end
  end
 
 case {'ne_ida', 'te_ida'}

 otherwise
  disp(['case = ' data_request ' not defined yet in get_profile_structure, ask O. Sauter']);
  return
end

