classdef check_mds < matlab.unittest.fixtures.Fixture
  methods
    function setup(fixture)
      fixture.assumeFalse(~exist('mdsconnect','file'),'mdsconnect not found - is mds on path?');
      % other environment checks here
    end
  end
end
