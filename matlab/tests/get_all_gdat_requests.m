function requests = get_all_gdat_requests(machine,testcase)
  [gd0,~] = gdat('machine',machine);
  requests = gd0.gdat_request;
  
switch machine
  % list of calls that take some time, to be skipped for fast tests
  case 'TCV'
      slowlist = {'rtc','mpx','sxr','psi'};
      excludelist = {'scd'}; % cases to skip for good reason
  case 'AUG'
      slowlist = {'sxr','cxrs','transp','te_rho','ne_rho','nete_rho',...
        'ece_rho','eced_rho','cxrs_rho','eqdsk','equil'};
      excludelist = {};
end

% filter requests
requests = requests(~ismember(requests,excludelist));

switch testcase
  case 'fast'
    requests = requests(~ismember(requests,slowlist));
    % choose tests not on the slowlist
  case 'slow'
    requests = requests(ismember(requests,slowlist));
    % choose only tests on the slowlist
  otherwise
    error('unknown test_case %s',testcase);
end

end