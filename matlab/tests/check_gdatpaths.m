classdef check_gdatpaths < matlab.unittest.fixtures.Fixture
  methods
    function setup(fixture)
      % caller
      oldpwd = pwd;
      testfolder = fileparts(mfilename('fullpath')); % relative path w.r.t. this file
      cd(testfolder); % go into test folder so you might not see local gdat.m
      gdatpath = fileparts(which('gdat'));
      cd(oldpwd);
      fixture.assertTrue(startsWith(testfolder,gdatpath),...
          sprintf('test folder path does not contain path of gdat: %s\n This file is in: %s\n which(''gdat''):\n Please run setpaths_gdat() to set the paths',...
          testfolder,which('gdat')));
    end
  end
end
