classdef (TestTags={'tcv'})test_requestnames_tcv < test_requestnames
  % everything is implemented in superclass!

  properties
    Machine = 'TCV';
  end

  properties(TestParameter)
    % parameters that will vary during tests
    shot  = {'82913'};
    requests_fast = get_all_gdat_requests('TCV','fast');
    requests_slow = get_all_gdat_requests('TCV','slow');
  end

  methods(Test,TestTags = {'fast'})
    function test_gdat_call_fast(testCase,shot,requests_fast)
      testCase.test_gdat_call(testCase,shot,requests_fast);
    end
  end

  methods(Test,TestTags = {'slow'})
    function test_gdat_call_slow(testCase,shot,requests_slow)
      testCase.test_gdat_call(testCase,shot,requests_slow);
    end
  end

end
