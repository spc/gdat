classdef (TestTags={'tcv_imas'},SharedTestFixtures={...
    check_mds,check_gdatpaths})...
    test_tcv_get_ids < matlab.unittest.TestCase
    % Tests link between TCV data to matlab ids structure and  ids empty structure
    % The link between ids data to saving on IMAS database, or reading from an IMAS database should be performed
    %   in another function like test_tcv_put_ids with only 'imas' as TestTags, performed where IMAS is installed

  properties(TestParameter)
    shot = {-1,40000,61400};
    ids_name = {'pf_active','wall'};
  end

  methods(Test)
    function test_get_ids_list(testCase,shot)
      gg = testCase.assertWarning(@() gdat(shot,'ids'),'gdat:EmptyIDSName');

      testCase.assertTrue(iscell(gg.gdat_params.sources_available));
    end

    function test_tcv2ids(testCase,shot,ids_name)
      if shot==-1 && strcmp(ids_name,'pf_active')
        ids = testCase.assertWarning(@() tcv2ids(shot,'ids_name',{ids_name}),'TCV:IDS:NoTimeData');
      else
        ids = testCase.assertWarningFree(@() tcv2ids(shot,'ids_name',{ids_name}));
      end
      testCase.assertTrue(~isempty(ids))
    end
  end

end
