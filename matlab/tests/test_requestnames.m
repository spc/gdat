classdef (SharedTestFixtures={...
    check_mds,check_gdatpaths}) ...
    test_requestnames < matlab.unittest.TestCase

  properties (Abstract)
    Machine;
  end

  properties(TestParameter,Abstract)
    % parameters that will vary during tests
    shot;
    requests_fast; % placeholders
    requests_slow;
  end

  methods(Static)
    function test_gdat_call(testCase,shot,request)
      % actual function to test gdat call
      import matlab.unittest.fixtures.WorkingFolderFixture;

      testCase.assertTrue(isnumeric(str2double(shot)));
      testCase.assertTrue(ischar(request));
      % move to a /tmp/xx folder so if fort.0 files or others are created, it can
      f=matlab.unittest.fixtures.WorkingFolderFixture('withsuffix','_gdat');
      testCase.applyFixture(f);

      % gdat call
      gdat_call = sprintf(['gdat_' lower(testCase.Machine) '(%s,''%s'')'],shot,request);
      do_gdat_call = 1;

      switch request
        case 'eqdsk'
          % avoid writing files in /tmp, may not be allowed
          gdat_call = sprintf(['gdat_%s(%s,''%s'',''write'',0)'],lower(testCase.Machine),shot,request);
        case 'gas_fluxes'
          % test new gas flow entries
          shot = 83173; % (GV1=GV2 case)
          gdat_call = sprintf(['gdat_' lower(testCase.Machine) '(%s,''%s'')'],shot,request);
        case 'radcam'
          % need a newer shot for tcv radcam
          shot = 81102;
          gdat_call = sprintf(['gdat_' lower(testCase.Machine) '(%s,''%s'')'],shot,request);
        case {'mpx', 'xte'}
          % need an older shot
          shot = 65555;
          gdat_call = sprintf(['gdat_' lower(testCase.Machine) '(%s,''%s'')'],shot,request);
      end

      % logging
      fprintf('Testing gdat call: %s\n',gdat_call);

      if do_gdat_call
        gdat_out = eval(gdat_call); %#ok<NASGU>
      else
        gdat_out = struct([]);
      end

      % in some future: check for warnings
      %gdat_out = verifyWarningFree(testCase,eval(gdat_call),...
      %  'Warning issued from gdat call:\n   %s\n',gdat_call);

      % (add optional sanity checks of gdat_out here)
      gdat_out
      switch request
        case {'ece', 'expcode', 'ids', 'ni', 'ti', 'transp'}
          % tests not yet fully implemented and empty
        case 'gas_fluxes'
          testCase.assertTrue(iscell(gdat_out.data) && ~isempty(gdat_out.data{1}))
        case 'rtc'
          % in this case .data is empty, all in .scd_mems
          testCase.assertTrue(isfield(gdat_out,'scd_mems') && isstruct(gdat_out.scd_mems));
          % testCase.assertTrue(isstruct(gdat_out.scd_mems));
        otherwise
          testCase.assertTrue(isnumeric(gdat_out.data) & numel(gdat_out.data)>0);
      end
    end
  end
end
