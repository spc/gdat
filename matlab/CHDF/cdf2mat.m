function cdf2mat_out = cdf2mat(pfname,varargin)
% cdf2mat_out = cdf2mat(pfname,varargin)
%
% reads all variables and coordinates from netcdf
% some trials with netcdf, using 50725c01.cdf as test
%
% Based on TANSP netcdf file, but should work with other if simple group structure probably
%
% Uses matlab netcdf.open, ncinfo, netcdf.inqVarIDs, netcd.getVar, etc
%
% varargin{1}: shot number to add (in particular when cdf file does not have it defined
%              shot from file: allinfo=ncinfo(pfname);if ~isempty(allinfo.Attributes),top_attr_names = {allinfo.Attributes(:).Name};allinfo.Attributes(strmatch('shot',top_attr_names,'exact')).Value, else, disp('no Attributes'),end
%

%
if ~exist(pfname,'file')
  error([pfname ' is not a valid filename for a netcdf file']);
end

try
  funnetcdf=netcdf.open(pfname,'nowrite');
catch ME
  warning(['problems with netcdf.open(' pfname ',''nowrite''); May be not a netcdf file?']);
  rethrow(ME)
end

% all data of all vars

allinfo=ncinfo(pfname);

allvarids=netcdf.inqVarIDs(funnetcdf);
allvarnames={allinfo.Variables(:).Name};
if length(allvarnames) ~= length(allvarids)
  allinfo;
  error('problem with Variables, may be several groups')
  return
end

[varnames_sorted,~]=sort(allvarnames);
cdf2mat_out.allvarnames_sorted = varnames_sorted;

% to find a variable:
% strmatch('GFUN',allvarnames,'exact')

% construct relevant data strtucture

coordnames=strtrim({allinfo.Dimensions(:).Name});
[coordnames_sorted,~]=sort(coordnames);
fields_variables_to_copy = {'Name', 'Dimensions', 'Size', 'Datatype'};
for i=1:length(coordnames_sorted)
  matcdf.coords(i).name = coordnames_sorted{i};
  matcdf.coords(i).index_allvarnames = strmatch(matcdf.coords(i).name,allvarnames,'exact');
  matcdf.coords(i).index_varnames_sorted = strmatch(matcdf.coords(i).name,varnames_sorted,'exact');
  matcdf.coords(i).varid = allvarids(matcdf.coords(i).index_allvarnames);
  if ~isempty(matcdf.coords(i).index_allvarnames)
    if strcmp(allinfo.Variables(matcdf.coords(i).index_allvarnames).Datatype,'single')
      matcdf.coords(i).data = netcdf.getVar(funnetcdf,matcdf.coords(i).varid,'double');
    else
      matcdf.coords(i).data = netcdf.getVar(funnetcdf,matcdf.coords(i).varid);
    end
    for ij=1:length(fields_variables_to_copy)
      matcdf.coords(i).(fields_variables_to_copy{ij}) = allinfo.Variables(matcdf.coords(i).index_allvarnames).(fields_variables_to_copy{ij});
    end
    % define defaults before fetching the values in Attributes (if provided)
    matcdf.coords(i).units = '';
    matcdf.coords(i).long_name = [matcdf.coords(i).name ' empty'];
    for jj=1:numel(allinfo.Variables(matcdf.coords(i).index_allvarnames).Attributes)
      if strcmp(lower(allinfo.Variables(matcdf.coords(i).index_allvarnames).Attributes(jj).Name),'units')
        matcdf.coords(i).units = strtrim(allinfo.Variables(matcdf.coords(i).index_allvarnames).Attributes(jj).Value);
      end
      if strcmp(lower(allinfo.Variables(matcdf.coords(i).index_allvarnames).Attributes(jj).Name),'long_name')
        matcdf.coords(i).long_name = strtrim(allinfo.Variables(matcdf.coords(i).index_allvarnames).Attributes(jj).Value);
      end
    end
  else
    matcdf.coords(i).data = [];
    for ij=1:length(fields_variables_to_copy)
      matcdf.coords(i).(fields_variables_to_copy{ij}) = [];
    end
    matcdf.coords(i).units = '';
    matcdf.coords(i).long_name = [matcdf.coords(i).name ' empty'];
  end
  matcdf.coords(i).label = [matcdf.coords(i).name ' ' num2str(matcdf.coords(i).Size) ': ' matcdf.coords(i).long_name];
  cdf2mat_out.coords.(matcdf.coords(i).name) = matcdf.coords(i);
end

for i=1:length(varnames_sorted)
  matcdf.vars(i).name = varnames_sorted{i};
  matcdf.vars(i).index_allvarnames = strmatch(matcdf.vars(i).name,allvarnames,'exact');
  matcdf.vars(i).varid = allvarids(matcdf.vars(i).index_allvarnames);
  % if i==strmatch('ZEFFC',varnames_sorted,'exact'), keyboard; end
  if ~isempty(matcdf.vars(i).index_allvarnames)
    if strcmp(allinfo.Variables(matcdf.vars(i).index_allvarnames).Datatype,'single')
      matcdf.vars(i).data = netcdf.getVar(funnetcdf,matcdf.vars(i).varid,'double');
    else
      matcdf.vars(i).data = netcdf.getVar(funnetcdf,matcdf.vars(i).varid);
    end
    for ij=1:length(fields_variables_to_copy)
      matcdf.vars(i).(fields_variables_to_copy{ij}) = allinfo.Variables(matcdf.vars(i).index_allvarnames).(fields_variables_to_copy{ij});
    end
    % define defaults before fetching the values in Attributes (if provided)
    matcdf.vars(i).units = '';
    matcdf.vars(i).long_name = [matcdf.vars(i).name ' empty'];
    for jj=1:numel(allinfo.Variables(matcdf.vars(i).index_allvarnames).Attributes)
      if strcmp(lower(allinfo.Variables(matcdf.vars(i).index_allvarnames).Attributes(jj).Name),'units')
        matcdf.vars(i).units = strtrim(allinfo.Variables(matcdf.vars(i).index_allvarnames).Attributes(jj).Value);
      end
      if strcmp(lower(allinfo.Variables(matcdf.vars(i).index_allvarnames).Attributes(jj).Name),'long_name')
        matcdf.vars(i).long_name = strtrim(allinfo.Variables(matcdf.vars(i).index_allvarnames).Attributes(jj).Value);
      end
    end
  else
    matcdf.vars(i).data = [];
    for ij=1:length(fields_variables_to_copy)
      matcdf.vars(i).(fields_variables_to_copy{ij}) = [];
    end
    matcdf.vars(i).units = '';
    matcdf.vars(i).long_name = [matcdf.vars(i).name ' empty'];
  end
  matcdf.vars(i).label = matcdf.vars(i).name;
  for j=1:length(matcdf.vars(i).Dimensions)
    ij = strmatch(matcdf.vars(i).Dimensions(j).Name,coordnames_sorted,'exact');
    matcdf.vars(i).dim{j} = matcdf.coords(ij).data;
    matcdf.vars(i).dimunits{j} = matcdf.coords(ij).units;
    matcdf.vars(i).dimname{j} = matcdf.coords(ij).name;
    if j==1
      matcdf.vars(i).label = [matcdf.vars(i).label '(' matcdf.coords(ij).name];
    else
      matcdf.vars(i).label = [matcdf.vars(i).label ',' matcdf.coords(ij).name];
    end
    if j==length(matcdf.vars(i).Dimensions)
      matcdf.vars(i).label = [matcdf.vars(i).label ')'];
    end
  end
  matcdf.vars(i).label = [matcdf.vars(i).label ': ' matcdf.vars(i).long_name ' [' matcdf.vars(i).units ']'];
  cdf2mat_out.allvars.(matcdf.vars(i).name) = matcdf.vars(i);
end
if ~isempty(allinfo.Attributes)
  top_attr_names = {allinfo.Attributes(:).Name};
  ij = strmatch('shot',top_attr_names,'exact');
else
  ij = [];
end
if ~isempty(ij)
  cdf2mat_out.shot = allinfo.Attributes(ij).Value;
else
  if nargin>1 && isnumeric(varargin{1})
    cdf2mat_out.shot = varargin{1};
  else
    cdf2mat_out.shot = NaN;
  end
end
cdf2mat_out.fname = pfname;
[a1,a2,a3]=fileparts(pfname);
cdf2mat_out.id = a2;

  netcdf.close(funnetcdf);

clear matcdf
return
