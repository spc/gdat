function G = G_ITER_generator()
G = struct();

tmp = data_Bm();

G.rm = tmp.r;
G.zm = tmp.z;
G.am = tmp.am;
G.nm = numel(G.zm);

tmp = data_Ff();
G.rf = tmp.r;
G.zf = tmp.z;
G.nf = numel(G.zf);

tmp = data_limiter();
G.rl  = tmp.r;
G.zl = tmp.z;

% TODO vessel is ready but will be added in a second moment
G.rv = [];
G.zv = [];

% Point for the gaps
tmp = data_rW();
G.rW = tmp.r;
G.zW = tmp.z;
G.oW = tmp.oW;
G.aW = tmp.aW;

%% Coils
tmp = data_coils();
R = tmp.R;
Z = tmp.Z;
dR = tmp.dR;
dZ = tmp.dZ;
N = tmp.N;
dima= tmp.names;

%% Sort by CORSICA ordering for the structure liux
% Not all che coils are used in this 
% Matrix to be given to eval_trap
vvtmp = [R';Z';dR';dZ'];
% Sort for Corsica convention
vv = zeros(size(vvtmp));
sort_list = [3,4,2,5,1,6,7,8,9,10,11,12, 13, 14];
for ii=1:numel(sort_list)
    vv(:,ii) = vvtmp(:,sort_list(ii));
end
N = abs(N(sort_list));
% Compute the windings and connection matrix
[rw,zw,Twa] = gen_filament(vv,N,2);
G.dima = dima(sort_list);

G.rw = rw;
G.zw = zw;
G.Twa = Twa;
G.na = numel(R);
G.Tvs = [];

end
