function h5_struct = read_hdf5_simple(ids_h5_file,varargin)
%
% h5_struct = read_hdf5_simple(ids_h5_file,varargin)
%
% varargin{1}: 'imas' (default), 'futils' (for ogyropsi files e.g.) type of hdf5
%
% on spcimas need to do:
%    export LD_PRELOAD_previous=$LD_PRELOAD
%    unset LD_PRELOAD
%    matlab
%    >> h5disp('/tmp/yildiz//imasdb/tcv/3/80000/999/summary.h5')
%    (to reset: export LD_PRELOAD=$LD_PRELOAD_previous )

try
  aa=hdf5info(ids_h5_file);
catch
  error('could not hdf5info(file)')
end

file_type = 'imas',
if nargin>=2 && ~isempty(varargin{1})
  file_type = varargin{1}
end

switch file_type
  case {'futils', 'standard'}
    keyboard
  otherwise
    [a1,ids_name,a3]=fileparts(aa.Filename);
    h5_struct.ids_name = ids_name;
    h5_struct.Filename = aa.GroupHierarchy.Filename;

    if isfield(aa.GroupHierarchy.Groups,'Datasets')
      for i=1:numel(aa.GroupHierarchy.Groups.Datasets)
        if strcmp(aa.GroupHierarchy.Groups.Datasets(i).Name(1),'/')
          istart = 2;
        else
          istart = 1;
        end
        struct_flatname = regexprep(lower(regexprep(aa.GroupHierarchy.Groups.Datasets(i).Name(istart:end),'[/&[]]','.')),'\.+','_');
        struct_flatname = regexprep(struct_flatname,[h5_struct.ids_name '_'],'');
        h5_struct.(struct_flatname) = h5read(aa.GroupHierarchy.Groups.Datasets(i).Filename,aa.GroupHierarchy.Groups.Datasets(i).Name);
      end
    else
      disp(sprintf('\n*************\nno Datasets\n*************\n'));
    end
end
