function [data_struct] = data_coils()
% Data BASED on ITER_D_24JQWZ V1.0
% VSU and VSL are not correctly described at the moment since they are taken as rectangular but they are rounded

names = {'CS3U';'CS2U';'CS1U';'CS1L';'CS2L';'CS3L';'PF1';...
        'PF2';'PF3';'PF4';'PF5';'PF6'; 'VSU'; 'VSL'};

R = [1.6870 ; 1.6870; 1.6870; 1.6870; 1.6870; ...
    1.6870; 3.9431; 8.2851; 11.9919; 11.9630; 8.3908; 4.3340; 5.8261;7.5222];
Z = [ 5.4640; 3.2780; 1.0920; -1.0720; -3.2580; -5.4440;...
    7.5741; 6.5398; 3.2752; -2.2336; -6.7269; -7.4665; 4.9249;-2.4912];
dR = [0.7400; 0.7400;0.7400;0.7400;0.7400;0.7400;0.9590;0.5801;0.6963;0.6382;0.8125;1.5590;0.132;0.132];
dZ = [2.0930; 2.093; 2.093; 2.093; 2.093; 2.093; 0.9841; 0.7146; 0.9538; 0.9538; 0.9538; 1.1075; 0.132;0.132];

N = [554;554;554;554;554;554;248.6;115.2;185.9;169.9;216.8;459.4; 4; -4];

N = round(N);

data_struct = struct();
data_struct.R = R;
data_struct.Z = Z;
data_struct.dR = dR;
data_struct.dZ = dZ;
data_struct.N = N;
data_struct.names = names; 

end
