function mapping = imas_requests_mapping(data_request,shot)
%
% Information pre-defined for gdat_imas, you can add cases here to match official cases in gdat_imas, allowing backward compatibility
%
% give the shot number in case data origin depends on the shot number, allows to adapt easily
%

% Defaults
mapping = struct(...
    'label', '', ...
    'method', '', ...
    'expression','', ...
    'timedim', -1, ...     % dim which is the time is the database, to copy in .t, the other dims are in .x (-1 means last dimension)
    'gdat_timedim',[], ...  % if need to reshape data and dim orders to have timedim as gdat_timedim (shifting time to gdat_timedim)
    'min', -inf, ...
    'max', inf);
% Note that gdat_timedim is set to timedim at end of this function if empty
% gdat_timedim should have effective index of the time dimension in gdat

if ~exist('data_request') || isempty(data_request)
  return
end

% default label: data_request keyword itself
mapping.label = data_request;

%
% label is used for plotting
switch lower(data_request)
 case 'ids'
  mapping.timedim = 1;
  mapping.label = 'ids ala imas';
  mapping.method = 'switchcase';
 otherwise
  mapping.label = data_request;
  mapping.method = 'switchcase';
  mapping.expression = data_request;

end

if isempty(mapping.gdat_timedim)
  mapping.gdat_timedim = mapping.timedim;
end
