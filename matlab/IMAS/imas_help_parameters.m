function help_struct = imas_help_parameters(parameter_list)
%
% retrieve from present table the relevant help lines for the parameter_list{:}
% should come from sqlite database at some point...
%
% return the whole help structure if parameter_list empty or not provided
%
% do:
%      help_struct = imas_help_parameters(fieldnames(gdat_data.gdat_params));
%
% to get relevant help description
%

% Defaults
help_struct_all = struct(...
    'data_request', ['automatically filled in by gdat, name of request used in gdat call.' char(10) ...
                    'contains current list of keywords if gdat called with no arguments: aa=gdat;' char(10) ...
                    'Note shot value should not be in params so params can be used to load same data from another shot']  ...
    ,'machine', 'machine name like ''TCV'', ''AUG'', ''IMAS'' case insensitive' ...
    ,'doplot', '0 (default), if 1 calls gdat_plot for a new figure, -1 plot over current figure with hold all, see gdat_plot for details' ...
    ,'nverbose','1 (default) displays warnings, 0: only errors, >=3: displays all extra information' ...
    );

% IMAS related
help_struct_all.time = 'eqdsk: time(s) value(s) requested, by default time=1.0s (see time_out for other requests)';
help_struct_all.time_out = ['requested time for output: data points within interval if time_out=[t1 t2], otherwise assumes series of points, uses linear interpolation in that case (default [-Inf Inf])'...
                   char(10) 'for sxr, mpx: only time interval provided in time_out is relevant'];
help_struct_all.cocos_in_out = ['cocos_in or cocos_out value input or desired in output, uses eqdsk_cocos_transform. Note should use latter if a specific Ip and/or B0 sign' ...
                    'is wanted. See O. Sauter et al Comput. Phys. Commun. 184 (2013) 293'];
help_struct_all.source = sprintf('%s\n',...
          'ids_names for request ''ids'' like magnetics, equilibrium, etc');
help_struct_all.error_bar = sprintf('%s\n','for ids: choice of nodes fill in and how:', ...
          '''delta'' (default): only upper fill in  with the abs(value) to add or subtract to data to get upper and lower values (symmetric)', ...
          '''delta_with_lower'': same as delta but fill in lower node as well (with delta as well, same as upper)', ...
          '''added'': add the delta values (old cpo style), so upper=data+error_bar and lower=data+error_bar');
help_struct_all.write = 'eqdsk: write eqdsk while loading data (1, default) or not (0)';
%help_struct_all. = '';

if ~exist('parameter_list') || isempty(parameter_list)
  help_struct = help_struct_all;
  return
end

if iscell(parameter_list)
  parameter_list_eff = parameter_list;
elseif ischar(parameter_list)
  % assume only one parameter provided as char string
  parameter_list_eff{1} = parameter_list;
else
  disp(['unknown type of parameter_list in imas_help_parameters.m'])
  parameter_list
  return
end

fieldnames_all = fieldnames(help_struct_all);
for i=1:length(parameter_list_eff)
  if any(strcmp(fieldnames_all,parameter_list_eff{i}))
    help_struct.(parameter_list_eff{i}) = help_struct_all.(parameter_list_eff{i});
  end
end
