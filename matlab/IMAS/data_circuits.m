function data_struct = data_circuits()
% Describe which coils belongs to which circuit
% Information not available in the xls file. 
% CREATE data consistent with this 

% data structure is { {name circuit} , {coils in the circuit}}

data_struct = { 
  {'CS1'},  {'CS1U','CS1L'}, ; 
  {'CS2U'}, {'CS2U'}; 
  {'CS2L'}, {'CS2L'}; 
  {'CS3U'}, {'CS3U'}; 
  {'CS3L'}, {'CS3L'}; 
  {'PF1'},  {'PF1'}; 
  {'PF2'},  {'PF2'};
  {'PF3'},  {'PF3'}; 
  {'PF4'},  {'PF4'}; 
  {'PF5'},  {'PF5'};
  {'PF6'},  {'PF6'}; 
  {'VS'},   {'VSU', 'VSL'}; 
      };
         

end