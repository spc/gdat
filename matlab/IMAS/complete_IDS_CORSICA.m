function IDS_out = complete_IDS_CORSICA(IDS_in,varargin)
%
%

%
doread = 0;
dosave = 0;


if doread
% $$$   clear all
% $$$   close all
% $$$   clc

  % This script will complete the IDS of CORSICA with the missing fields recomputing the missing quantities from available information

  data_path_in = '/NoTivoli/carpanes/LIU_RAP_ITER/CORSICA_ids/CORSICA_130510.mat';
  %path_IDS = '/NoTivoli/carpanes/LIU_RAP_ITER/CORSICA_ids/CORSICA_130506.mat';

  data_path_out = '/NoTivoli/carpanes/LIU_RAP_ITER/CORSICA_ids/CORSICA_130510_extended_COCOS17_trial.mat';

  % Load the IDS_in file
  IDS_out = load(data_path_in);
else
  IDS_out = IDS_in;
end

%% Correct COCOS convention on original data

% Put homogenous = 0 on magnetic but 1 on the coils.
% Sign of the angle of the magnetic probes.

%%
G = G_ITER_generator;
% Get the structure from generating files

Pliuqe = liupiter(1000,'uuu',ones(G.na,1),'vvv',ones(G.nm,1), 'www', ones(G.nf,1)); % Some default setting that could be removed probably to use the liupiter directly
G = liug(G,Pliuqe);

L = liuc(Pliuqe,G);

% Extract information from CORSICA IDS_out
[LC,LXC,LYC] = LLXLY_IDS_corsica(L, IDS_out);

%%  -------------------- ADD the missing information on the existing IDS_out CORSICA -----------------------

% Make sure default substructure maintained if insert new indices
pf_active_default = gdat([],'ids','source','pf_active');
pf_active_default = pf_active_default.pf_active;
%% Consider each circuit to be composed by only one coil
tmp = data_coils();
if isempty(IDS_out.pf_active.coil); IDS_out.pf_active.coil(1:numel(tmp.names)) = pf_active_default.coil(1); end
for ii = 1:numel(tmp.names)
    for jj=1:numel(IDS_out.pf_active.coil)
        % Remove strange character in coil names
        IDS_out.pf_active.coil{jj}.name = regexprep(IDS_out.pf_active.coil{jj}.name,'[\n\r]+','');
        if strcmp(tmp.names{ii},IDS_out.pf_active.coil{jj}.name)
          if isempty(IDS_out.pf_active.coil{jj}.element); IDS_out.pf_active.coil{jj}.element(1) = pf_active_default.coil{1}.element(1); end
            IDS_out.pf_active.coil{jj}.element{1}.geometry.geometry_type = 2; % Rectangle description
            IDS_out.pf_active.coil{jj}.element{1}.geometry.rectangle.r = tmp.R(ii);
            IDS_out.pf_active.coil{jj}.element{1}.geometry.rectangle.z = tmp.Z(ii);
            IDS_out.pf_active.coil{jj}.element{1}.geometry.rectangle.width = tmp.dR(ii);
            IDS_out.pf_active.coil{jj}.element{1}.geometry.rectangle.height = tmp.dZ(ii);
            IDS_out.pf_active.coil{jj}.element{1}.turns_with_sign = tmp.N(ii);
        end
    end
end

%% Add the data to the circuit
tmp = data_circuits();
Ncircuit_eff = size(tmp,1);
Ncircuit_in = numel(IDS_out.pf_active.circuit);
Nsupply_in = numel(IDS_out.pf_active.supply);
Ncoils = numel(IDS_out.pf_active.coil);
% time will be set in circuit.current.time so should not put it at top and set homogeneous to 0?
IDS_out.pf_active.time = [];
IDS_out.pf_active.ids_properties.homogeneous_time = 0;
if Ncircuit_eff > Ncircuit_in
  IDS_out.pf_active.circuit(Ncircuit_in+1:Ncircuit_eff,1) = pf_active_default.circuit(1);
end
if Ncircuit_eff > Nsupply_in
  IDS_out.pf_active.supply(Nsupply_in+1:Ncircuit_eff,1) = pf_active_default.supply(1);
end
for ii=1:Ncircuit_eff
    IDS_out.pf_active.circuit{ii}.name = tmp{ii,1}{1};
    IDS_out.pf_active.supply{ii}.name = tmp{ii,1}{1};
    % Find index of the coils belonging to ii circuit
    index_coil = [];
    for jj =1:Ncoils
      if any(strcmp(tmp{ii,2}, IDS_out.pf_active.coil{jj}.name))
           index_coil = [index_coil jj];
      end
    end
    % Add the current data to the circuit from the data on the coils.
    % The coils must share the same current in the circuit so just take the first one.
    IDS_out.pf_active.circuit{ii}.current.data = IDS_out.pf_active.coil{index_coil(1)}.current.data/IDS_out.pf_active.coil{index_coil(1)}.element{1}.turns_with_sign;
    IDS_out.pf_active.circuit{ii}.current.time = IDS_out.pf_active.coil{index_coil(1)}.current.time;

    IDS_out.pf_active.circuit{ii}.connections = zeros(2*numel(index_coil) -1 , 2*Ncircuit_eff + 2*Ncoils);
    for jj = 1:numel(index_coil)
    IDS_out.pf_active.circuit{ii}.connections( jj , 2*Ncircuit_eff + 2*(index_coil(jj)-1) +1) = 1;
    IDS_out.pf_active.circuit{ii}.connections( jj +1 , 2*Ncircuit_eff + 2*(index_coil(jj)-1) +2) = 1;
    end

    % Add the connection to the power supply
    IDS_out.pf_active.circuit{ii}.connections(1,  2*ii -1) =   1;
    IDS_out.pf_active.circuit{ii}.connections(end,  2*ii ) =1;
end

%% Limiter description
tmp = data_limiter();
% Make sure default substructure maintained if insert new indices
wall_default = gdat([],'ids','source','wall');
wall_default = wall_default.wall;
IDS_out.wall.ids_properties.homogeneous_time = 0; % no times are set so just say not homogeneous
if isempty(IDS_out.wall.description_2d); IDS_out.wall.description_2d(1) = wall_default.description_2d(1); end
if isempty(IDS_out.wall.description_2d{1}.limiter.unit); IDS_out.wall.description_2d{1}.limiter.unit(1) = wall_default.description_2d{1}.limiter.unit(1); end
IDS_out.wall.description_2d{1}.limiter.unit{1}.outline.r = tmp.r;
IDS_out.wall.description_2d{1}.limiter.unit{1}.outline.z = tmp.z;

% problem with mex ids_put when time default or empty, make outline  and ggd empty
if ~isempty(IDS_out.wall.description_2d{1}.mobile.unit)
  IDS_out.wall.description_2d{1}.mobile.unit{1}.outline = {};
end
IDS_out.wall.description_ggd = {};

%% Vessel description
% Understand what I need to do for the double layer vessel

%%  -------------- Synthetic diagnostics------------ Need to be recomputed from CORSICA flux map

% Make sure default substructure maintained if insert new indices
magnetics_default = gdat([],'ids','source','magnetics');
magnetics_default = magnetics_default.magnetics;
% since time set in subnode method{1}.ip.time, set homogeneous to 0 (magnetics.time already empty)
IDS_out.magnetics.ids_properties.homogeneous_time = 0;
if isempty(IDS_out.magnetics.method); IDS_out.magnetics.method(1) = magnetics_default.method(1); end
IDS_out.magnetics.method{1}.ip.time = LXC.t;
IDS_out.magnetics.method{1}.ip.data = LXC.Ip;

%% Ff
tmp = data_Ff();
Nflux_loop_in = numel(IDS_out.magnetics.flux_loop);
Nflux_loop_eff = numel(tmp.r);
if Nflux_loop_eff > Nflux_loop_in
  IDS_out.magnetics.flux_loop(Nflux_loop_in+1:Nflux_loop_eff,1) = magnetics_default.flux_loop(1);
end
for ii=1:Nflux_loop_eff
  if isempty(IDS_out.magnetics.flux_loop{ii}.position); IDS_out.magnetics.flux_loop{ii}.position(1) = magnetics_default.flux_loop{ii}.position(1); end
  IDS_out.magnetics.flux_loop{ii}.position{1}.r = tmp.r(ii);
  IDS_out.magnetics.flux_loop{ii}.position{1}.z = tmp.z(ii);
  IDS_out.magnetics.flux_loop{ii}.name = tmp.name{ii};
end

for ii=1:Nflux_loop_eff
  IDS_out.magnetics.flux_loop{ii}.flux.data = -LXC.Ff(ii,:)';
  IDS_out.magnetics.flux_loop{ii}.flux.time = LXC.t;
end

%% Bm
% Correct IDS magnetics
tmp = data_Bm();
% Make sure default substructure maintained if insert new indices
Nbpol_probe_in = numel(IDS_out.magnetics.bpol_probe);
Nbpol_probe_eff = numel(tmp.name);
if Nbpol_probe_eff > Nbpol_probe_in
  IDS_out.magnetics.bpol_probe(Nbpol_probe_in+1:Nbpol_probe_eff,1) = magnetics_default.bpol_probe(1);
end
for ii=1:Nbpol_probe_eff
  IDS_out.magnetics.bpol_probe{ii}.position.r = tmp.r(ii);
  IDS_out.magnetics.bpol_probe{ii}.position.z = tmp.z(ii);
  IDS_out.magnetics.bpol_probe{ii}.poloidal_angle = -tmp.am(ii); % Correct the sign to be consistent with COCOS 11
  IDS_out.magnetics.bpol_probe{ii}.name = tmp.name{ii};
end

for ii=1:Nbpol_probe_eff
    IDS_out.magnetics.bpol_probe{ii}.field.data = LXC.Bm(ii,:)';
    IDS_out.magnetics.bpol_probe{ii}.field.time = LXC.t;
end

%% Ft
IDS_out.magnetics.method{1}.diamagnetic_flux.data = -LXC.Ft;
IDS_out.magnetics.method{1}.diamagnetic_flux.time = LXC.t;

%% rBt
IDS_out.tf.ids_properties.homogeneous_time = 1;
IDS_out.tf.time = LXC.t;
IDS_out.tf.b_field_tor_vacuum_r.time  = LXC.t;
IDS_out.tf.b_field_tor_vacuum_r.data  = LXC.rBt;

%% Convert from cocos_in to cocos_out
cocos_in = 17;
cocos_out = 11;
to_transform = {'pf_active','pf_passive','magnetics','tf','wall','equilibrium'};
to_transform = {'pf_active','magnetics','tf','wall','equilibrium'}; % at this stage pf_passive is empty and not used
for i=1:length(to_transform)
  IDS_out.(to_transform{i}) = ids_generic_cocos_nodes_transformation_symbolic(IDS_out.(to_transform{i}), to_transform{i}, cocos_in, cocos_out,[],[],[],[],[],3);
end

%% Store the resulting data
if dosave
  save(data_path_out, '-struct', 'IDS_out')
  fprintf('\n wrote file %s \n', data_path_out);
end
