function varargout = data_generic_fit_gui(varargin)
% DATA_GENERIC_FIT_GUI MATLAB code for data_generic_fit_gui.fig
%      DATA_GENERIC_FIT_GUI, by itself, creates a new DATA_GENERIC_FIT_GUI or raises the existing
%      singleton*.
%
%      H = DATA_GENERIC_FIT_GUI returns the handle to a new DATA_GENERIC_FIT_GUI or the handle to
%      the existing singleton*.
%
%      DATA_GENERIC_FIT_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DATA_GENERIC_FIT_GUI.M with the given input arguments.
%
%      DATA_GENERIC_FIT_GUI('Property','Value',...) creates a new DATA_GENERIC_FIT_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before data_generic_fit_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to data_generic_fit_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help data_generic_fit_gui

% Last Modified by GUIDE v2.5 19-Apr-2016 12:03:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @data_generic_fit_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @data_generic_fit_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before data_generic_fit_gui is made visible.
function data_generic_fit_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to data_generic_fit_gui (see VARARGIN)

% Choose default command line output for data_generic_fit_gui
handles.output = hObject;

% change defaults here to avoid changing .fig too much:
tension_left_def = str2double(get(handles.set_tension_left,'string'));
tension_left_def_eff = -1;
set(handles.set_tension_left,'string',num2str(tension_left_def_eff));

handles = init_data_generic_gui(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes data_generic_fit_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = data_generic_fit_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function time_set_slider_Callback(hObject, eventdata, handles)
% hObject    handle to time_set_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

time_slide = get(hObject,'Value');
time_eff = handles.data_time_int(1) + (time_slide-handles.slider_range(1))./diff(handles.slider_range).*(handles.data_time_int(end)-handles.data_time_int(1));
handles.it_index_left = iround_os(handles.data_left.t,time_eff);
handles.it_index_right = iround_os(handles.data_right.t,time_eff);
set(handles.time_set_value,'string',num2str(time_eff));


replot(handles);

% --- Executes during object creation, after setting all properties.
function time_set_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_set_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function time_set_value_Callback(hObject, eventdata, handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_set_value as text
%        str2double(get(hObject,'String')) returns contents of time_set_value as a double

time = get(handles.time_set_value,'string');
time = str2num(time);
handles.it_index_left = iround_os(handles.data_left.t,time)
handles.it_index_right = iround_os(handles.data_right.t,time)
time_slide = (time-handles.data_time_int(1))./(handles.data_time_int(end)-handles.data_time_int(1)).*diff(handles.slider_range) + handles.slider_range(1);
set(handles.time_set_slider,'Value',time_slide);

replot(handles);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function time_set_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function replot(handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_set_value as text
%        str2double(get(hObject,'String')) returns contents of time_set_value as a double

% left plot

ihold_left = get(handles.set_hold_left,'Value');
if ihold_left ==0
  hold(handles.left_axes,'off');
else
  hold(handles.left_axes,'all');
end
prov_list=unique(handles.data_left.perDt{handles.it_index_left}.provenance);
for i=1:length(prov_list)
  ij=find(handles.data_left.perDt{handles.it_index_left}.provenance==prov_list(i));
  errorbar(handles.left_axes,handles.data_left.perDt{handles.it_index_left}.rho(ij),handles.data_left.perDt{handles.it_index_left}.data(ij),handles.data_left.perDt{handles.it_index_left}.error_bar(ij),'*');
  hold(handles.left_axes,'all');
end
plot(handles.left_axes,handles.fit_left.rhofit,handles.fit_left.data(:,handles.it_index_left),'-');
set(handles.left_axes,'XLim',[0 1.2]);
left_max = str2double(get(handles.set_left_max,'String'));
if left_max>0
  set(handles.left_axes,'YLim',[0 left_max]);
end

% right
ihold_right = get(handles.set_hold_right,'Value');
if ihold_right ==0
  hold(handles.right_axes,'off');
else
  hold(handles.right_axes,'all');
end
prov_list=unique(handles.data_right.perDt{handles.it_index_right}.provenance);
for i=1:length(prov_list)
  ij=find(handles.data_right.perDt{handles.it_index_right}.provenance==prov_list(i));
  errorbar(handles.right_axes,handles.data_right.perDt{handles.it_index_right}.rho(ij),handles.data_right.perDt{handles.it_index_right}.data(ij),handles.data_right.perDt{handles.it_index_right}.error_bar(ij),'*');
  hold(handles.right_axes,'all');
end
plot(handles.right_axes,handles.fit_right.rhofit,handles.fit_right.data(:,handles.it_index_right),'-');
set(handles.right_axes,'XLim',[0 1.2]);
right_max = str2double(get(handles.set_right_max,'String'));
if right_max>0
  set(handles.right_axes,'YLim',[0 right_max]);
end

% lambda_left
if ihold_left ==0
  hold(handles.set_axes_lambda_left,'off');
else
  hold(handles.set_axes_lambda_left,'all');
end
plot(handles.set_axes_lambda_left,handles.fit_left.rhofit,-handles.fit_left.dydrho(:,handles.it_index_left)./handles.fit_left.data(:,handles.it_index_left),'-');

% lambda_right
if ihold_right ==0
  hold(handles.set_axes_lambda_right,'off');
else
  hold(handles.set_axes_lambda_right,'all');
end
plot(handles.set_axes_lambda_right,handles.fit_right.rhofit,-handles.fit_right.dydrho(:,handles.it_index_right)./handles.fit_right.data(:,handles.it_index_right),'-');
set(handles.set_axes_lambda_right,'XAxisLocation','top')
set(handles.set_axes_lambda_right,'YAxisLocation','right')

zoom(handles.left_axes,'on');
zoom(handles.right_axes,'on');


function set_shot_Callback(hObject, eventdata, handles)
% hObject    handle to set_shot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_shot as text
%        str2double(get(hObject,'String')) returns contents of set_shot as a double

shot = get(handles.set_shot,'String'); shot = str2num(shot);
handles.shot = shot;

[fname,fpath] = uigetfile([{['*' num2str(shot) '*gen_signals*']},{'*shot*gen_signals* files'}; ...
  {'*.mat'},{'*.mat files'};{'*'},{'All files'}],'Select a file with the data_in correct structure' );

handles.fname = fname;
handles.fpath = fpath;
data_gen = load(fullfile(fpath,fname));
if isfield(data_gen,'gen_signals_data')
  gen_signals = data_gen.gen_signals_data;
else
  gen_signals = data_gen;
end
handles.gen_signals = gen_signals;
keyboard
fields_avail = fieldnames(gen_signals);
if length(fields_avail)>0
  set(handles.popup_data_left,'string',fields_avail);
  set(handles.popup_data_right,'string',fields_avail);
else
  disp(['problems with subfields in file: ',fullfile(fname,fpath)]);
  return
end
if length(fields_avail)>=2
  set(handles.popup_data_right,'value',2);
end

handles = init_data_generic_gui(handles);

handles = refit(handles);
replot(handles);

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function set_shot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_shot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function handles_out = init_data_generic_gui(handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
  % left part do the fits
  structs_left = get(handles.popup_data_left,'string');
  valeff = get(handles.popup_data_left,'value');
  struct_left = structs_left{valeff};
  % right part
  structs_right = get(handles.popup_data_right,'string');
  valeff = get(handles.popup_data_right,'value');
  struct_right = structs_right{valeff};

  handles.data_left = handles.gen_signals.(struct_left).combined_data;
  handles.fit_left = handles.gen_signals.(struct_left).fit;
  handles.data_right = handles.gen_signals.(struct_right).combined_data;
  handles.fit_right = handles.gen_signals.(struct_right).fit;
  handles.slider_range = [get(handles.time_set_slider,'Min') get(handles.time_set_slider,'Max')];
  handles.data_time_int = [min(handles.data_left.t(1),handles.data_left.t(end)) max(handles.data_left.t(1),handles.data_left.t(end))];
  time_prev = get(handles.time_set_value,'string');
  if strcmp(time_prev,'Edit Text')
    time_def = 0.5;
    handles.it_index_left = iround_os(handles.data_left.t,time_def);
    handles.it_index_right = iround_os(handles.data_right.t,time_def);
    set(handles.time_set_value,'string',num2str(time_def));
    time_slide = abs((time_def-handles.data_time_int(1))./(handles.data_time_int(end)-handles.data_time_int(1)).*diff(handles.slider_range) + handles.slider_range(1));
    if time_slide>1; time_slide=1; end
    set(handles.time_set_slider,'Value',time_slide);
  else
    time = str2num(time_prev);
    handles.it_index_left = iround_os(handles.data_left.t,time);
    handles.it_index_right = iround_os(handles.data_right.t,time);
    time_slide = abs((time-handles.data_time_int(1))./(handles.data_time_int(end)-handles.data_time_int(1)).*diff(handles.slider_range) + handles.slider_range(1));
    if time_slide>1; time_slide=1; end
    set(handles.time_set_slider,'Value',time_slide);
  end
  
  set(handles.set_shot,'string',num2str(handles.shot));
    
catch
  % no such file
  handles.data = [];
  handles.fit = [];
  handles.it_index = [];
  set(handles.set_shot,'string','enter shot');
end

handles_out = handles;



function set_tension_left_Callback(hObject, eventdata, handles)
% hObject    handle to set_tension_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_tension_left as text
%        str2double(get(hObject,'String')) returns contents of set_tension_left as a double

handles = refit(handles);
replot(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function set_tension_left_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_tension_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_tension_right_Callback(hObject, eventdata, handles)
% hObject    handle to set_tension_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_tension_right as text
%        str2double(get(hObject,'String')) returns contents of set_tension_right as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_tension_right_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_tension_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_coeff_left_Callback(hObject, eventdata, handles)
% hObject    handle to set_coeff_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_coeff_left as text
%        str2double(get(hObject,'String')) returns contents of set_coeff_left as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_coeff_left_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_coeff_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_coeff_right_Callback(hObject, eventdata, handles)
% hObject    handle to set_coeff_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_coeff_right as text
%        str2double(get(hObject,'String')) returns contents of set_coeff_right as a double

set_shot_Callback(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function set_coeff_right_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_coeff_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in set_hold_left.
function set_hold_left_Callback(hObject, eventdata, handles)
% hObject    handle to set_hold_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ihold_left = get(handles.set_hold_left,'Value');
if ihold_left ==0
  set(handles.set_hold_left,'String','Hold off');
else
  set(handles.set_hold_left,'String','Hold on');
end


% --- Executes on button press in set_hold_right.
function set_hold_right_Callback(hObject, eventdata, handles)
% hObject    handle to set_hold_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ihold_right = get(handles.set_hold_right,'Value');
if ihold_right ==0
  set(handles.set_hold_right,'String','Hold off');
else
  set(handles.set_hold_right,'String','Hold on');
end



function set_time_interval_Callback(hObject, eventdata, handles)
% hObject    handle to set_time_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_time_interval as text
%        str2double(get(hObject,'String')) returns contents of set_time_interval as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_time_interval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_time_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_left_max_Callback(hObject, eventdata, handles)
% hObject    handle to set_left_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_left_max as text
%        str2double(get(hObject,'String')) returns contents of set_left_max as a double

replot(handles);

% --- Executes during object creation, after setting all properties.
function set_left_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_left_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_right_max_Callback(hObject, eventdata, handles)
% hObject    handle to set_right_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_right_max as text
%        str2double(get(hObject,'String')) returns contents of set_right_max as a double

replot(handles);

% --- Executes during object creation, after setting all properties.
function set_right_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_right_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_data_left.
function popup_data_left_Callback(hObject, eventdata, handles)
% hObject    handle to popup_data_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_data_left contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_data_left


% --- Executes during object creation, after setting all properties.
function popup_data_left_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_data_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_data_right.
function popup_data_right_Callback(hObject, eventdata, handles)
% hObject    handle to popup_data_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_data_right contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_data_right


% --- Executes during object creation, after setting all properties.
function popup_data_right_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_data_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function handles_out=refit(handles_in)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_set_value as text
%        str2double(get(hObject,'String')) returns contents of time_set_value as a double

% redo the fits because some coefficients, tension have changed
% click on "shot" to reload a new data file


handles_out = handles_in;

tension_left = get(handles_in.set_tension_left,'string'); tension_left = str2num(tension_left);
tension_right = get(handles_in.set_tension_right,'string'); tension_right = str2num(tension_right);
time_interval = get(handles_in.set_time_interval,'string'); time_interval = str2num(time_interval);
coeffs_left = get(handles_in.set_coeff_left,'string'); coeffs_left = str2num(coeffs_left);
coeffs_right = get(handles_in.set_coeff_right,'string'); coeffs_right = str2num(coeffs_right);

% left part do the fits
structs_left = get(handles_in.popup_data_left,'string');
valeff = get(handles_in.popup_data_left,'value');
struct_left = structs_left{valeff};
% right part
structs_right = get(handles_in.popup_data_right,'string');
valeff = get(handles_in.popup_data_right,'value');
struct_right = structs_right{valeff};

[gen_signals_left] = generic_fit(handles_in.shot,handles_in.gen_signals.(struct_left).raw_data,struct_left,time_interval,tension_left,coeffs_left,0);
[gen_signals_right] = generic_fit(handles_in.shot,handles_in.gen_signals.(struct_right).raw_data,struct_right,time_interval,tension_right,coeffs_right,0);

handles_out.data_left = gen_signals_left.(struct_left).combined_data;
handles_out.fit_left = gen_signals_left.(struct_left).fit;
handles_out.data_right = gen_signals_right.(struct_right).combined_data;
handles_out.fit_right = gen_signals_right.(struct_right).fit;

