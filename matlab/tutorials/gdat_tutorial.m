%% gdat tutorial: some basic calls and functionalities
% gdat calls in fact the main function MACHINE/gdat_machine.m within the gdat folder
% gdat can be call with one of the available "data_request" keyword or with a full trace_name
%
% The basic call is:
% data_out_struct = gdat(shot,data_request,'option1',option1_val,...);
%
% But sub-cases are also valid and explained below
%

%% getting the list of available predefined data_request names
%
gdat_data = gdat;
%
% This is the simplest call and returns 3 useful information:
gdat_data.gdat_request % contains the list of available data_request names
gdat_data.gdat_params % contains the list of basic parameters, including :
gdat_data.gdat_params.machine % the (default) machine name
%
% in addition
gdat_data.gdat_call % always contains the string so that the same gdat call can be performed (using eval(), see below)
%% list of data_request names for a specific machine
gdat_data = gdat('machine','aug');
gdat_data.gdat_request
%% a simple example: get plasma current
gdat_data = gdat(48836,'ip'); % shot number is for TCV (default machine if run at CRPP)
gdat_data = gdat(48836,'ip','doplot',1);
gdat_data = gdat(48836,'Ip');
% Note that the return data_request name is 'ip', since lower case is used throughout
gdat_data.gdat_request
%% a 2D example with Te profiles
gdat_data = gdat(48836,'te'); % return thomson data, thus versus Z (vertical height) of local measurements
gdat_data = gdat(48836,'te','doplot',1); % default plot is always versus time, using gdat_data.t
% for more specific plots, use gdat_plot(gdat_data,...) function
%% Explaination of main fields
gdat_data = gdat(48836,'te');
gdat_data
% The following fields are always present:
% gdat_data.data  % (as well as .units) providing the data
% gdat_data.dim   % (as well as .dimunits) providing the coordinates
% gdat_data.t     % corresponding to the time coordinate (copied from dim{timecoord})
% gdat_data.x     % all coordinates but time (typically radial if 2D signal or channel number)
% gdat_data.shot
% gdat_data.gdat_request
% gdat_data.gdat_params
% gdat_data.data_fullpath  % provides information on tracename origin within the database
% gdat_data.gdat_call  % string corresponding to the current call to gdat

% Some extra fields might be present:
% gdat_data.label  % usually present, used as label in plots
% gdat_data.error_bar  % error_bars to be used in errorbar plot e.g: errorbar(gdat_data.x,gdat_data.data(:,20),gdat_data.error_bar(:,20),'*-')
% gdat_data.mapping_for_tcv  % information used to fetch the data,
%                            contains information on the time index, useful for multi-D data
%
%% Explanation of basic parameters fields of gdat_params
gdat_data = gdat(48836,'te');
disp(' ');disp('after "te"');disp(' gdat_data.gdat_params:');disp(gdat_data.gdat_params) % at this stage contains few elements, will depend on specific data_request
%
% gdat_params should contain all the information necessary to recall gdat
% and get the same information, except the shot number, thus
% gdat_data2 = gdat(48836,gdat_data.gdat_params);
% should return the same values. This allows to quickly load the similar data from different shot
shotlist=[48836, 48837, 48839];
for ishot=1:length(shotlist)
    gdat_data_te{ishot} = gdat(shotlist(ishot),gdat_data.gdat_params);
end
gdat_plot(gdat_data_te{3});
%
% In some cases we can see the various options after a first call, where
% defaults are set:
gdat_data = gdat(48836,'eqdsk');
disp(' ');disp('after "eqdsk"');disp(' gdat_data.gdat_params:');disp(gdat_data.gdat_params)
% shows that 'time' should be given (can be an array) and 'zshift' (to shift vertically to zaxis=0 if 'zshift',1 is provided
% It also show you can give the cocos out you want, for example for CHEASE type coordinates you would do: 
%     gdat_data = gdat(48836,'eqdsk','cocos',2);
% (see O. Sauter and S. Y Medvedev, Tokamak Coordinate Conventions: COCOS , Comput. Phys. Commun. 184 (2013) 293 )
%

% gdat_params contains a sub-structure "help" which contains explanations
% for the respective optional parameter specified in the gdat_params structure
% In this case we have:
disp(' ');disp('after "eqdsk"');disp(' gdat_data.gdat_params.help:');disp(gdat_data.gdat_params.help)
