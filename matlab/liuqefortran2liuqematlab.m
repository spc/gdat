function liuqe_fortran_matlab = liuqefortran2liuqematlab(varargin);
%
% liuqe_fortran_matlab = liuqefortran2liuqematlab(varargin);
%
% returns correspondance of node names, when different between fortran (eq_recon) and matlab (equil_1) mds node names
%
% liuqe_fortran_matlab{:,1} give liuqe fortran names
% liuqe_fortran_matlab{:,2} give liuqe matlab names
%
% Note that you can have multiple entries (for liuqe fortran since it has less nodes) but the first one should be the closest match (see r_contour/r_rho/r_surf)
%
% varargin: If absent, then return full table
% varargin{1}: 'node_name_to_convert'
% varargin{2}: origin of node_name given=varargin{1}: 0: fortran, 1(default): matlab
% varargin{3}: type of desired output:                0: fortran, 1(default): matlab
%
% Thus by default there is no conversion
%
% Examples:
%        liuqe_fortran_matlab = liuqefortran2liuqematlab; % => liuqe_fortran_matlab(:,1:2) full cell table
%        liuqe_fortran_matlab = liuqefortran2liuqematlab('q_axis'); => liuqe_fortran_matlab = 'q_axis'
%        liuqe_fortran_matlab = liuqefortran2liuqematlab('q_axis',1,0); => liuqe_fortran_matlab = 'q_zero'
%        liuqe_fortran_matlab = liuqefortran2liuqematlab('q_zero',0,1); => liuqe_fortran_matlab = 'q_axis'
%        liuqe_fortran_matlab = liuqefortran2liuqematlab('r_edge',1,0); => liuqe_fortran_matlab = 'r_contour'
%

liuqe_fortran_matlab_table = [ ...
    {'l_i'}           , {'l_i_3'}     ; ...
    {'i_p'}           , {'i_pl'}  ; ...
    {'i_pol_fit'}     , {'i_pol'}  ; ...
    {'pprime_psi'}    , {'pprime_rho'} ; ... % warning on different x-mesh
    {'surface_flux'}  , {'psi_surf'}  ; ...
    {'q_zero'}        , {'q_axis'}    ; ...
    {'q_psi'}         , {'q'}    ; ...
    {'r_contour'}     , {'r_edge'}     ; ... % r_rho has all the flux surfaces
    {'r_min_psi'}     , {'r_in'}     ; ... % R inboard of rho flux surfaces
    {'r_max_psi'}     , {'r_out'}     ; ... % R outboard of rho flux surfaces
    {'total_energy'}  , {'w_mhd'}     ; ...
    {'ttprime_psi'}   , {'ttprime_rho'} ; ... % warning on different x-mesh
    {'z_contour'}     , {'z_edge'}     ; ... % z_rho has all the flux surfaces
                   ];

liuqe_fortran_matlab = [];
if nargin == 0
  % return full table
  liuqe_fortran_matlab = liuqe_fortran_matlab_table;
  return
end


if isempty(varargin{1})
  error(['liuqefortran2liuqematlab: unexpected empty 1st argument'])
  return
end
if ~ischar(varargin{1})
  error(['liuqefortran2liuqematlab: unexpected 1st argument is not a string'])
  return
end

liuqe_matlab_in = 1;
if nargin>=2 && ~isempty(varargin{2})
  if isnumeric(varargin{2})
    liuqe_matlab_in = varargin{2};
  else
    warning(['liuqefortran2liuqematlab: unexpected 2nd argument type, should be numeric'])
    varargin{2}
    return
  end
end

liuqe_matlab_out = 1;
if nargin>=3 && ~isempty(varargin{3})
  if isnumeric(varargin{3})
    liuqe_matlab_out = varargin{3};
  else
    warning(['liuqefortran2liuqematlab: unexpected 3rd argument type, should be numeric'])
    varargin{3}
    return
  end
end

% find index of input in corresponding column
ij = strmatch(varargin{1},liuqe_fortran_matlab_table(:,1+liuqe_matlab_in),'exact');
if isempty(ij)
  % assume name is the same
  liuqe_fortran_matlab = varargin{1};
else
  liuqe_fortran_matlab = liuqe_fortran_matlab_table{ij(1),1+liuqe_matlab_out};
end
