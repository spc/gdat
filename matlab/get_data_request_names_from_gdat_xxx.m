function [data_request_names] = get_data_request_names_from_gdat_xxx(machine_in);
%
% get list of presently available data_request names grouped and with description
%
% at this stage using first 3 columns of file gdat_data_request_names.xlsx
%

data_request_names = [];

expected_machines = [{'aug'}, {'d3d'}, {'jet'}, {'tcv'} {'chdf'} {'imas'}]; % substrutures created for these at this stage (all means all of these)
try machine_in = validatestring(machine_in,expected_machines);
catch ME,
  disp(ME.message);
  return;
end

% Avoid running this function over and over
% This is another bottleneck in the gdat execution.
persistent last_answer

if ~isempty(last_answer),
  data_request_names = last_answer;
  if  ~isempty(data_request_names.(machine_in)), return;end
else
  for j=1:length(expected_machines)
    data_request_names.(expected_machines{j}) = [];
  end
end

fid=fopen([machine_in '_requests_mapping.m'],'r');
nextline=fgetl(fid);
while isempty(nextline) || ~isnumeric(nextline) || nextline~=-1 %length(nextline)>2 || ~strcmp(nextline,'-1')
  if length(nextline)>7 && ~isempty(regexp(nextline,'^ +case '))
    ij=find(nextline==['''']);
    if ~isempty(ij)
      for i=1:2:length(ij)-1
        keywds_next = nextline(ij(i)+1:ij(i+1)-1);
        i_backslash = find(keywds_next=='\', 1);
        i_column = find(keywds_next==':', 1);
        if isempty(i_backslash) && isempty(i_column)
          data_request_names.(machine_in).(keywds_next) = keywds_next;
        end
      end
    end
  end
  nextline=fgetl(fid);
end
fclose(fid);
data_request_names.all = [];

last_answer = data_request_names;

