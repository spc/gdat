// get ASDEX Upgrade equilibrium
// Wolfgang Suttrop, 31 Jan 2006
// W.S. ignore 1D profiles on open flux surfaces, 27-Jan-2010

function [equ] = get_aug_equil(shot,time,exper,diagn,edition)

  if exists('exper')==0 then, exper='AUGD'; end
  if exists('diagn')==0 then, diagn='EQI'; end
  if exists('edition')==0 then, edition = 0; end
  descript = sprintf('%s %s %d(%d) %f', exper, diagn, shot, edition, time);
  printf('Reading equilibrium from %s\n', descript);

  s1 = 'augdiag('+string(shot)+',""'+diagn+'"",""';
  s2 = '"",""'+exper+'"",'+string(edition)+','+string(time)+','+string(time)+')'

// flux matrix
  printf("%s\n", s1+'PFM'+s2);
  mdsvalue('_s=shape(_psi='+s1+'PFM'+s2+')');
  mdsvalue('_p0=zero(_s[1:2]);');  // trick for 2D result
  PsiRz = mdsvalue('_p0+_psi');
  R = mdsvalue('_r=dim_of(_psi,1)');
  z = mdsvalue('_z=dim_of(_psi,2)');

// number of internal flux labels
  cmd = sprintf("%s\n", '_lpf='+s1+'Lpf'+s2);
  Lpf = mdsvalue(cmd);
  ifl = 1+modulo(Lpf,10000);

// flux quantities (profiles)
  cmd = sprintf("%s\n", '_qpsi='+s1+'Qpsi'+s2);
  Qpsi = mdsvalue(cmd);
  Psi  = mdsvalue('dim_of(_qpsi,1)');

  Jpol = mdsvalue(s1+'Jpol'+s2);
  Pres = mdsvalue(s1+'Pres'+s2);

// Psimag, Psibdry
  cc = 'augconv('+string(shot)+',""'+diagn+'"",""' + ...
      exper + '"",'+string(edition)+','+sprintf("%f",time);
  cmd = cc+',28,[0,1],,_pflux),_pflux'; 
  pflux = mdsvalue(cmd);

// Raxis, zaxis
  cmd = cc+',30,[0,1],0.0,_zaxsepo)'; 
  raxsepo = mdsvalue(cmd);
  zaxsepo = mdsvalue('_zaxsepo');

// build equilibrium structure
  equ = struct( 'Description', descript, ...
      'PsiRz', PsiRz, 'R', R, 'z', z, ... 
      'Psi', Psi(1:ifl), 'Qpsi', Qpsi(1:ifl), ...
      'Fpol', Jpol(1:2:ifl+ifl-1)*2.0e-7, ...
      'FFprime', Jpol(1:2:ifl+ifl-1).*Jpol(2:2:ifl+ifl)*4.0e-14, ...
      'Pres', Pres(1:2:ifl+ifl-1), ...
      'pprime', Pres(2:2:ifl+ifl), ...
      'Psimag', pflux(1), 'Psibdry', pflux(2), ...
      'Raxis', raxsepo(1), 'zaxis', zaxsepo(1));
  
endfunction

