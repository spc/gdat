function [nodeout]=adaptPFM_rda(nodein,ndim1,ndim2,ndim3);
%
% 3D case is special
%
% adapt nodein to keep only 1st ndim1 points in dim1 and ndim2 points in dim2 and same for ndim3
%
% re-generate .x and .t
%
% change .value, .data, .x and .t 
%

nodeout = nodein;

nodeout.value = nodeout.value(1:ndim1,1:ndim2,1:ndim3);
nodeout.data = nodeout.value;
nodeout.x = [1:ndim1];
nodeout.t = [1:ndim3];
