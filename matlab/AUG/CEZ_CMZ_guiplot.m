function varargout = CEZ_CMZ_guiplot(varargin)
% CEZ_CMZ_GUIPLOT MATLAB code for CEZ_CMZ_guiplot.fig
%      CEZ_CMZ_GUIPLOT, by itself, creates a new CEZ_CMZ_GUIPLOT or raises the existing
%      singleton*.
%
%      H = CEZ_CMZ_GUIPLOT returns the handle to a new CEZ_CMZ_GUIPLOT or the handle to
%      the existing singleton*.
%
%      CEZ_CMZ_GUIPLOT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CEZ_CMZ_GUIPLOT.M with the given input arguments.
%
%      CEZ_CMZ_GUIPLOT('Property','Value',...) creates a new CEZ_CMZ_GUIPLOT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CEZ_CMZ_guiplot_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CEZ_CMZ_guiplot_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CEZ_CMZ_guiplot

% Last Modified by GUIDE v2.5 17-Feb-2016 15:49:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CEZ_CMZ_guiplot_OpeningFcn, ...
                   'gui_OutputFcn',  @CEZ_CMZ_guiplot_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CEZ_CMZ_guiplot is made visible.
function CEZ_CMZ_guiplot_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CEZ_CMZ_guiplot (see VARARGIN)

% Choose default command line output for CEZ_CMZ_guiplot
handles.output = hObject;

% change defaults here to avoid changing .fig too much:
tension_ti_def = str2double(get(handles.set_tension_ti,'string'));
tension_ti_def_eff = -1;
set(handles.set_tension_ti,'string',num2str(tension_ti_def_eff));

handles = init_CEZ_gui(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CEZ_CMZ_guiplot wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CEZ_CMZ_guiplot_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function time_set_slider_Callback(hObject, eventdata, handles)
% hObject    handle to time_set_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

time_slide = get(hObject,'Value');
time_eff = handles.data.time(1) + (time_slide-handles.slider_range(1))./diff(handles.slider_range).*(handles.data.time(end)-handles.data.time(1));
handles.it_index = iround_os(handles.data.time,time_eff);
set(handles.time_set_value,'string',num2str(handles.data.time(handles.it_index)));


replot(handles);

% --- Executes during object creation, after setting all properties.
function time_set_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_set_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function time_set_value_Callback(hObject, eventdata, handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_set_value as text
%        str2double(get(hObject,'String')) returns contents of time_set_value as a double

time = get(handles.time_set_value,'string');
time = str2num(time);
handles.it_index = iround_os(handles.data.time,time)
time_slide = (time-handles.data.time(1))./(handles.data.time(end)-handles.data.time(1)).*diff(handles.slider_range) + handles.slider_range(1);
set(handles.time_set_slider,'Value',time_slide);

replot(handles);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function time_set_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function replot(handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_set_value as text
%        str2double(get(hObject,'String')) returns contents of time_set_value as a double

% Ti
ihold_ti = get(handles.set_hold_ti,'Value');
if ihold_ti ==0
  hold(handles.ti_axes,'off');
else
  hold(handles.ti_axes,'all');
end
errorbar(handles.ti_axes,handles.data.ti{handles.it_index}.rhotornorm,handles.data.ti{handles.it_index}.data,handles.data.ti{handles.it_index}.err,'*');
hold(handles.ti_axes,'all');
plot(handles.ti_axes,handles.fit.rhotornorm,handles.fit.ti(:,handles.it_index),'-');
set(handles.ti_axes,'XLim',[0 1.2]);
ti_max = str2double(get(handles.set_ti_max,'String'));
if ti_max>0
  set(handles.ti_axes,'YLim',[0 ti_max*1e3]);
end

% Vrot
ihold_vrot = get(handles.set_hold_vrot,'Value');
if ihold_vrot ==0
  hold(handles.vrot_axes,'off');
else
  hold(handles.vrot_axes,'all');
end
errorbar(handles.vrot_axes,handles.data.vrot{handles.it_index}.rhotornorm,handles.data.vrot{handles.it_index}.data,handles.data.vrot{handles.it_index}.err,'*');
hold(handles.vrot_axes,'all');
plot(handles.vrot_axes,handles.fit.rhotornorm,handles.fit.vrot(:,handles.it_index),'-');
set(handles.ti_axes,'XLim',[0 1.2]);
vrot_max = str2double(get(handles.set_vrot_max,'String'));
if vrot_max>0
  set(handles.vrot_axes,'YLim',[0 vrot_max*1e3]);
end

% lambda_ti
if ihold_ti ==0
  hold(handles.set_axes_lambda_ti,'off');
else
  hold(handles.set_axes_lambda_ti,'all');
end
plot(handles.set_axes_lambda_ti,handles.fit.rhotornorm,-handles.fit.dtidrhotornorm(:,handles.it_index)./handles.fit.ti(:,handles.it_index),'-');

% lambda_vrot
if ihold_vrot ==0
  hold(handles.set_axes_lambda_vrot,'off');
else
  hold(handles.set_axes_lambda_vrot,'all');
end
plot(handles.set_axes_lambda_vrot,handles.fit.rhotornorm,-handles.fit.dvrotdrhotornorm(:,handles.it_index)./handles.fit.vrot(:,handles.it_index),'-');
set(handles.set_axes_lambda_vrot,'XAxisLocation','top')
set(handles.set_axes_lambda_vrot,'YAxisLocation','right')

zoom(handles.ti_axes,'on');


function set_shot_Callback(hObject, eventdata, handles)
% hObject    handle to set_shot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_shot as text
%        str2double(get(hObject,'String')) returns contents of set_shot as a double

shot = get(handles.set_shot,'String'); shot = str2num(shot);

tension_ti = get(handles.set_tension_ti,'string'); tension_ti = str2num(tension_ti);
tension_vrot = get(handles.set_tension_vrot,'string'); tension_vrot = str2num(tension_vrot);
time_interval = get(handles.set_time_interval,'string'); time_interval = str2num(time_interval);
coeff_ti_cmz = get(handles.set_coeffCMZ_ti,'string'); coeff_ti_cmz = str2num(coeff_ti_cmz);
coeff_vrot_cmz = get(handles.set_coeff_vrot,'string'); coeff_vrot_cmz = str2num(coeff_vrot_cmz);

[filename_withfits,filename_data,cez_cmz_data,cez_cmz_fit] = ...
    CEZ_CMZ_fit(shot,time_interval,tension_ti,tension_vrot,coeff_ti_cmz,coeff_vrot_cmz);

handles.filename_withfits = filename_withfits;
handles = init_CEZ_gui(handles);

% Update handles structure
guidata(hObject, handles);

% send to workspace

% --- Executes during object creation, after setting all properties.
function set_shot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_shot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function handles_out = init_CEZ_gui(handles)
% hObject    handle to time_set_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
  load(handles.filename_withfits)
  handles.data = cez_cmz_data;
  handles.fit = cez_cmz_fit;
  handles.slider_range = [get(handles.time_set_slider,'Min') get(handles.time_set_slider,'Max')];

  time_prev = get(handles.time_set_value,'string');
  if strcmp(time_prev,'Edit Text')
    handles.it_index = 1;
    set(handles.time_set_value,'string',num2str(handles.data.time(handles.it_index)));
    set(handles.time_set_slider,'Value',handles.slider_range(1));
  else
    time = str2num(time_prev);
    handles.it_index = iround_os(handles.data.time,time);
    time_slide = abs((time-handles.data.time(1))./(handles.data.time(end)-handles.data.time(1)).*diff(handles.slider_range) + handles.slider_range(1));
    if time_slide>1; time_slide=1; end
    set(handles.time_set_slider,'Value',time_slide);
  end
  
  set(handles.set_shot,'string',num2str(handles.data.shot));
    
  replot(handles);
catch
  % no such file
  handles.data = [];
  handles.fit = [];
  handles.it_index = [];
  set(handles.set_shot,'string','enter shot');
end

handles_out = handles;



function set_tension_ti_Callback(hObject, eventdata, handles)
% hObject    handle to set_tension_ti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_tension_ti as text
%        str2double(get(hObject,'String')) returns contents of set_tension_ti as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_tension_ti_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_tension_ti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_tension_vrot_Callback(hObject, eventdata, handles)
% hObject    handle to set_tension_vrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_tension_vrot as text
%        str2double(get(hObject,'String')) returns contents of set_tension_vrot as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_tension_vrot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_tension_vrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_coeffCMZ_ti_Callback(hObject, eventdata, handles)
% hObject    handle to set_coeffCMZ_ti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_coeffCMZ_ti as text
%        str2double(get(hObject,'String')) returns contents of set_coeffCMZ_ti as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_coeffCMZ_ti_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_coeffCMZ_ti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_coeff_vrot_Callback(hObject, eventdata, handles)
% hObject    handle to set_coeff_vrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_coeff_vrot as text
%        str2double(get(hObject,'String')) returns contents of set_coeff_vrot as a double

set_shot_Callback(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function set_coeff_vrot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_coeff_vrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in set_hold_ti.
function set_hold_ti_Callback(hObject, eventdata, handles)
% hObject    handle to set_hold_ti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ihold_ti = get(handles.set_hold_ti,'Value');
if ihold_ti ==0
  set(handles.set_hold_ti,'String','Hold off');
else
  set(handles.set_hold_ti,'String','Hold on');
end


% --- Executes on button press in set_hold_vrot.
function set_hold_vrot_Callback(hObject, eventdata, handles)
% hObject    handle to set_hold_vrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ihold_vrot = get(handles.set_hold_vrot,'Value');
if ihold_vrot ==0
  set(handles.set_hold_vrot,'String','Hold off');
else
  set(handles.set_hold_vrot,'String','Hold on');
end



function set_time_interval_Callback(hObject, eventdata, handles)
% hObject    handle to set_time_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_time_interval as text
%        str2double(get(hObject,'String')) returns contents of set_time_interval as a double

set_shot_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function set_time_interval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_time_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_ti_max_Callback(hObject, eventdata, handles)
% hObject    handle to set_ti_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_ti_max as text
%        str2double(get(hObject,'String')) returns contents of set_ti_max as a double

replot(handles);

% --- Executes during object creation, after setting all properties.
function set_ti_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_ti_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function set_vrot_max_Callback(hObject, eventdata, handles)
% hObject    handle to set_vrot_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of set_vrot_max as text
%        str2double(get(hObject,'String')) returns contents of set_vrot_max as a double

replot(handles);

% --- Executes during object creation, after setting all properties.
function set_vrot_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to set_vrot_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
