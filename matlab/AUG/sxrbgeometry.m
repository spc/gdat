function [R_B, Z_B, ang_B,varargout]=sxrbgeometry(varargin);
%
% [R_B, Z_B, ang_B]=sxrbgeometry;
%
% returns angles for B SXR camera from AUG
% The R values at a given z=zmag are then given by:
% zmag>Z_B: only ang_B<180
% R= R_B - (zmag-Z_B)./tan(pi-ang_B.*pi/180);
% zmag<Z_B: only ang_B>180
% R= R_B - (zmag-Z_B)./tan(pi-ang_B.*pi/180);
%
% varargin{1}: zmag(t) (scalar or array)
%
% output
%  R_B, Z_B scalars
%  ang_B(nbchannels,1);
%  varargout: R(nbchannels,time), with NaN depending on conditions above
%

R_B=2.351;
Z_B=-0.271;
ang_B= [...
  107.955, ...
  111.645, ...
  115.555, ...
  119.245, ...
  123.155, ...
  126.845, ...
  130.755, ...
  134.445, ...
  138.355, ...
  142.045, ...
  145.955, ...
  149.645, ...
  153.555, ...
  157.245, ...
  161.155, ...
  164.845, ...
  168.755, ...
  172.445, ...
  176.355, ...
  180.045, ...
  183.955, ...
  187.645, ...
  191.555, ...
  195.245, ...
  199.155, ...
  202.845, ...
  206.755, ...
  210.445, ...
  214.355, ...
  218.045]';

varargout{1}=[];
if nargin <1 | isempty(varargin{1})
  return
end

% compute R
zmag=varargin{1};
varargout{1}=NaN*ones(length(ang_B),length(zmag));
for i=1:length(ang_B)
  varargout{1}(i,:) = R_B - (zmag'-Z_B)./tan(pi-ang_B(i).*pi/180);
end
iiout=find(varargout{1}<=0 | varargout{1}>=2.5);
varargout{1}(iiout)=NaN;
