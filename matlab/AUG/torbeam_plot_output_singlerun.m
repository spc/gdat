function [torbeam_std_files_struct] = torbeam_plot_output_singlerun;
%
% [torbeam_std_files_struct] = torbeam_plot_output_singlerun;
%
% reads local topfile, Te.dat, ne.dat input files
% as well as outputs files 't1_LIB.dat', 't1tor_LIB.dat', 'volumes.dat', 't2_new_LIB.dat','cntpr.dat'
% and plot results
%

% read topfile
fid=fopen('topfile','r');
% fscanf(fid,'%s %d %.6f\n',' Number of radial and vertical grid points in ',shot,time_for_torbeam(it));
dummy=fgetl(fid);
dummy=fgetl(fid);
dummyaaa = sscanf(dummy,'%d',2);
zzz.nb_rmesh = dummyaaa(1);
zzz.nb_zmesh = dummyaaa(2);
%  fscanf(fid,'%s\n','Inside and Outside radius and psi_sep');
dummy=fgetl(fid);
dummy=fgetl(fid);
dummyaaa = sscanf(dummy,'%f',3);
zzz.rmesh_min = dummyaaa(1);
zzz.rmesh_max = dummyaaa(2);
zzz.psi_edge = dummyaaa(3);
%  fscanf(fid,'%s\n','Radial grid coordinates');
dummy=fgetl(fid);
zzz.rmesh = fscanf(fid,'%f',zzz.nb_rmesh);
%  fscanf(fid,'%s\n','Vertical grid coordinates');
dummy=fgetl(fid);
if isempty(deblank(dummy)); dummy=fgetl(fid); end
zzz.zmesh = fscanf(fid,'%f',zzz.nb_zmesh);
%  fscanf(fid,'%s\n','B_r values');
dummy=fgetl(fid);
if isempty(deblank(dummy)); dummy=fgetl(fid); end
zzz.BR = fscanf(fid,'%f',[zzz.nb_rmesh,zzz.nb_zmesh]);
%  fscanf(fid,'%s\n','B_t values');
dummy=fgetl(fid);
if isempty(deblank(dummy)); dummy=fgetl(fid); end
zzz.Bphi = fscanf(fid,'%f',[zzz.nb_rmesh,zzz.nb_zmesh]);
%  fscanf(fid,'%s\n','B_z values');
dummy=fgetl(fid);
if isempty(deblank(dummy)); dummy=fgetl(fid); end
zzz.BZ= fscanf(fid,'%f',[zzz.nb_rmesh,zzz.nb_zmesh]);
%  fscanf(fid,'%s\n','psi values');
dummy=fgetl(fid);
if isempty(deblank(dummy)); dummy=fgetl(fid); end
zzz.psi= fscanf(fid,'%f',[zzz.nb_rmesh,zzz.nb_zmesh]);
fclose(fid);

zzz.prof_to_cp = {'Te.dat','ne.dat'};
zzz.prof_to_cp_var = lower(strrep(zzz.prof_to_cp,'.dat',''));
for i=1:length(zzz.prof_to_cp_var)
  zzz.(zzz.prof_to_cp_var{i}) = [];
  try
    [zzz.(zzz.prof_to_cp_var{i})(:,1),zzz.(zzz.prof_to_cp_var{i})(:,2)] = textread(zzz.prof_to_cp{i},'%f%f','headerlines',1);
  catch
  end
end

zzz.tb_out_files_to_cp = {'t1_LIB.dat', 't1tor_LIB.dat', 'volumes.dat', 't2_new_LIB.dat'};
zzz.tb_out_files_to_cp_var = strrep(zzz.tb_out_files_to_cp,'.dat','');
for i=1:length(zzz.tb_out_files_to_cp_var)
  zzz.(zzz.tb_out_files_to_cp_var{i}) = [];
  try
    zzz.(zzz.tb_out_files_to_cp_var{i}) = load(zzz.tb_out_files_to_cp{i});
  catch
  end
end
try
  zzz.tbeg_abs=load('cntpr.dat');
  zzz.tbeg_abs(2) = zzz.tbeg_abs(2) + 1; % counted from 0?
catch
  warning('no cntpr.dat?')
end

% plots


figure
contour(zzz.rmesh,zzz.zmesh,zzz.BR',100);
hold on
contour(zzz.rmesh,zzz.zmesh,zzz.psi',[zzz.psi_edge zzz.psi_edge],'linewidth',2,'linecolor','k');
for i=1:2:size(zzz.t1_LIB,2)
  hpl=plot(zzz.t1_LIB(:,i)/1e2,zzz.t1_LIB(:,i+1)/1e2);
  plotos(zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i)/1e2,zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i+1)/1e2,'-', ...
         [7 0],[],'k');%get(hpl,'color'));
end
axis equal
colorbar
title('BR')

figure
contour(zzz.rmesh,zzz.zmesh,zzz.BZ',100);
hold on
contour(zzz.rmesh,zzz.zmesh,zzz.psi',[zzz.psi_edge zzz.psi_edge],'linewidth',2,'linecolor','k');
for i=1:2:size(zzz.t1_LIB,2)
  hpl=plot(zzz.t1_LIB(:,i)/1e2,zzz.t1_LIB(:,i+1)/1e2);
  plotos(zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i)/1e2,zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i+1)/1e2,'-', ...
         [7 0],[],'k');%get(hpl,'color'));
end
axis equal
colorbar
title('BZ')

figure
contour(zzz.rmesh,zzz.zmesh,zzz.Bphi',100);
hold on
contour(zzz.rmesh,zzz.zmesh,zzz.psi',[zzz.psi_edge zzz.psi_edge],'linewidth',2,'linecolor','k');
for i=1:2:size(zzz.t1_LIB,2)
  hpl=plot(zzz.t1_LIB(:,i)/1e2,zzz.t1_LIB(:,i+1)/1e2);
 ixx= plotos(zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i)/1e2,zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i+1)/1e2,'-', ...
         [7 0],[],'k');%get(hpl,'color'));
end
axis equal
colorbar
title('Bphi')

figure;
subplot(2,1,1)
plot(zzz.te(:,1),zzz.te(:,2))
ylabel('Te')
subplot(2,1,2)
plot(zzz.ne(:,1),zzz.ne(:,2))
ylabel('ne')
xlabel('rhopol')

figure;
subplot(2,1,1)
try
  plot(zzz.t2_new_LIB(:,1),zzz.t2_new_LIB(:,2))
ylabel('Pdens')
subplot(2,1,2)
plot(zzz.t2_new_LIB(:,1),zzz.t2_new_LIB(:,3))
ylabel('jcd')
xlabel('rhopol')
catch
end

figure
theta=linspace(0,2.*pi,300);
Rmintheta = (0.3+zzz.rmesh_min).*cos(theta);
Ymintheta = (0.3+zzz.rmesh_min).*sin(theta);
Rmaxtheta = (0.3+zzz.rmesh_max).*cos(theta);
Ymaxtheta = (0.3+zzz.rmesh_max).*sin(theta);
plot(Rmintheta,Ymintheta,'k')
hold on
plot(Rmaxtheta,Ymaxtheta,'k')
for i=1:2:size(zzz.t1_LIB,2)
  hpl=plot(zzz.t1tor_LIB(:,i)/1e2,zzz.t1tor_LIB(:,i+1)/1e2);
  plotos(zzz.t1tor_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i)/1e2,zzz.t1tor_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i+1)/1e2,'-', ...
         [7 0],[],'k');%get(hpl,'color'));
end

figure
contour(zzz.rmesh,zzz.zmesh,zzz.psi',100);
hold on
contour(zzz.rmesh,zzz.zmesh,zzz.psi',[zzz.psi_edge zzz.psi_edge],'linewidth',2,'linecolor','k');
for i=1:2:size(zzz.t1_LIB,2)
  hpl=plot(zzz.t1_LIB(:,i)/1e2,zzz.t1_LIB(:,i+1)/1e2);
  plotos(zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i)/1e2,zzz.t1_LIB(zzz.tbeg_abs(2):sum(zzz.tbeg_abs),i+1)/1e2,'-', ...
         [7 0],[],'k');%get(hpl,'color'));
end
axis equal
colorbar
title('psi')

torbeam_std_files_struct = zzz;
