function mapping = aug_requests_mapping(data_request)
%
% Information pre-defined for gdat_aug, you can add cases here to match official cases in gdat_aug, allowing backward compatibility
%

% Defaults
mapping = struct(...
  'label', '', ...
  'method', '', ...
  'expression','', ...
  'timedim', -1, ...     % dim which is the time is the database, to copy in .t, the other dims are in .x (-1 means last dimension)
  'gdat_timedim',[], ...  % if need to reshape data and dim orders to have timedim as gdat_timedim (shifting time to gdat_timedim)
  'min', -inf, ...
  'max', inf);
% Note that gdat_timedim is set to timedim at end of this function if empty
% gdat_timedim should have effective index of the time dimension in gdat

if ~exist('data_request') || isempty(data_request)
  return
end

% default label: data_request keyword itself
mapping.label = data_request;

% for AUG, following choices are set so far:
% method = 'signal' then expression contains the shotfile, diagnostic and if needed the experiment
%                expression is a cell array
% method = 'expression', then expression is executed and it should provide the structure gdat_tmp, which fields are copied to gdat_data
% method = 'switchcase', then there will be a specific case within gdat_aug (usual case when not directly a signal)
%
% label is used for plotting
if iscell(data_request) % || (~ischar(data_request) && length(data_request)>1)
  mapping.label = data_request;
  mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
  mapping.expression = data_request;
  mapping.gdat_timedim = mapping.timedim;
  return
end

switch lower(data_request)
  case 'a_minor'
    mapping.timedim = 1;
    mapping.label = 'a\_minor';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''r_inboard'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);gdat_tmp.r_inboard=gdat_tmp.data;' ...
                    'params_eff.data_request=''r_outboard'';' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.r_outboard=gdat_tmp2.data;' ...
                    'gdat_tmp.data = 0.5.*(gdat_tmp2.data-gdat_tmp.data);gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'b0'
    mapping.timedim = 1;
    mapping.label = 'B_0';
    % mapping.method = 'signal';
    % mapping.expression = [{'FPC'},{'BTF'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''FPC''},{''BTF''}];' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);r0exp=1.65;gdat_tmp.r0 = r0exp; gdat_tmp.request_description = ' ...
                    '[''vacuum magnetic field at R0='' num2str(r0exp) ''m; COCOS=17''];' ...
                    'gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'beta'
    mapping.timedim = 1;
    mapping.label = '\beta';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''betan'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);' ...
                    'params_eff.data_request=''ip'';gdat_tmp2=gdat_aug(shot,params_eff);' ...
                    'params_eff.data_request=''b0'';gdat_tmp3=gdat_aug(shot,params_eff);' ...
                    'params_eff.data_request=''a_minor'';gdat_tmp4=gdat_aug(shot,params_eff);' ...
                    'tmp_data_ip=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'tmp_data_b0=interp1(gdat_tmp3.t,gdat_tmp3.data,gdat_tmp.t,[],NaN);' ...
                    'tmp_data_a=interp1(gdat_tmp4.t,gdat_tmp4.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = 0.01.*abs(gdat_tmp.data.*tmp_data_ip./1e6./tmp_data_a./tmp_data_b0);'];
  case 'betan'
    mapping.timedim = 1;
    mapping.label = '\beta_N';
    mapping.method = 'signal';
    mapping.expression = [{'TOT'},{'beta_N'}];
    % in many cases, in particular just after an experiment, betaN is not present in TOT, thus compute it from 2/3Wmhd/V /(B0^2/2mu0)
% $$$   mapping.method = 'expression';
% $$$   mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''TOT''},{''beta_2N''},{''AUGD''}];' ...
% $$$                     'gdat_tmp=gdat_aug(shot,params_eff); if isempty(gdat_tmp.data);' ...
% $$$             'params_eff.data_request=''ip'';gdat_ip=gdat_aug(shot,params_eff);' ...
% $$$             'params_eff.data_request=''b0'';gdat_b0=gdat_aug(shot,params_eff);' ...
% $$$             'params_eff.data_request=''a_minor'';gdat_aminor=gdat_aug(shot,params_eff);' ...
% $$$             'params_eff.data_request=''wmhd'';gdat_tmp=gdat_aug(shot,params_eff);' ...
% $$$             'params_eff.data_request=''volume'';gdat_vol=gdat_aug(shot,params_eff);' ...
% $$$             'tmp_data_ip=interp1(gdat_ip.t,gdat_ip.data,gdat_tmp.t,[],NaN);' ...
% $$$             'tmp_data_b0=interp1(gdat_b0.t,gdat_b0.data,gdat_tmp.t,[],NaN);' ...
% $$$             'tmp_data_a=interp1(gdat_aminor.t,gdat_aminor.data,gdat_tmp.t,[],NaN);' ...
% $$$             'tmp_data_vol=interp1(gdat_vol.t,gdat_vol.data,gdat_tmp.t,[],NaN);' ...
% $$$             'gdat_tmp.data = 100.*abs(2./3.*gdat_tmp.data./tmp_data_vol.*8e-7.*pi./tmp_data_b0.^2./tmp_data_ip.*1e6.*tmp_data_a.*tmp_data_b0);end;'];
  case {'betap', 'beta_p', 'beta_pol'}
    mapping.timedim = 1;
    mapping.label = '\beta_p';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'betpol'}];
  case {'cxrs', 'cxrs_rho'}
    mapping.timedim = 2;
    mapping.label = 'cxrs';
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'delta'
    mapping.timedim = 1;
    mapping.label = 'delta';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''delta_bottom''; ' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);params_eff.data_request=''delta_top'';' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.data = 0.5.*(gdat_tmp.data+gdat_tmp2.data);'];
  case 'delta_top'
    mapping.label = 'delta\_top';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'delRoben'}];
  case 'delta_bottom'
    mapping.label = 'delta\_bottom';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'delRuntn'}];
  case {'ece', 'eced', 'ece_rho', 'eced_rho'}
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.expression = '';
  case 'eqdsk'
    mapping.timedim = 2;
    mapping.method = 'switchcase'; % could use function make_eqdsk directly?
    mapping.expression = '';
  case 'equil'
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase'; % could use function make_eqdsk directly?
    mapping.expression = '';
  case {'gas', 'gas_valve'}
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
  case 'halpha'
    mapping.timedim = 1;
    mapping.label = 'Halpha';
    mapping.method = 'signal';
    mapping.expression = [{'POT'},{'ELMa-Han'}];
  case 'h_scalings'
    mapping.label = 'H_{scal}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'TTH'},{'H/L-facs'},{'AUGD'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''TTH''},{''H/L-facs''},{''AUGD''}];params_eff.source=''TTH'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);S = rdaAUG_eff(shot,''TTH'',''scal_par'',''AUGD'',[],[],''param:descript'');gdat_tmp.dimunits{1}=cellstr(deblank(S.data''));' ...
                    'gdat_tmp.data = min(gdat_tmp.data,10.);'];
  case 'ids'
    mapping.timedim = 1;
    mapping.label = 'ids ala imas';
    mapping.method = 'switchcase';
  case 'ioh'
    mapping.timedim = 1;
    mapping.label = 'I ohmic transformer';
    mapping.method = 'signal';
    mapping.expression = [{'MBI'},{'IOH'}];
  case 'ip'
    mapping.timedim = 1;
    mapping.label = 'Plasma current';
    mapping.method = 'signal';
    mapping.expression = [{'MAG'},{'Ipa'}];
    mapping.expression = [{'FPC'},{'IpiFP'}];
  case 'kappa'
    mapping.timedim = 1;
    mapping.label = '\kappa';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'k'}];
  case 'kappa_top'
    mapping.timedim = 1;
    mapping.label = '\kappa^{top}';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'koben'}];
  case 'kappa_bottom'
    mapping.timedim = 1;
    mapping.label = '\kappa_{bottom}';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'kuntn'}];
  case 'li'
    mapping.timedim = 1;
    mapping.label = 'l_i';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'li'}];
  case 'li3'
    mapping.timedim = 1;
    mapping.label = 'li3';
    mapping.method = 'expression';
    % li and betapol should have similar "normalization", using Bpa=int(Bp dlp)/int(dlp) and V=2pi R0 pi a^2 kappa and int(dlp)=2pi a sqrt(kappa) we get 2/R0 = int(dlp)^2/V so li3 is defined as [
    %           li3 = 2 int(Bp^2 dV) / (mu0^2 Ip^2 R0)
    % while li ala EFIT [https://iopscience.iop.org/article/10.1088/0029-5515/25/10/004 Eq.(5)] or CLISTE/IDE/GPEC [https://doi.org/10.13182/FST15-154 p.5] by
    %           li1 = int(Bp^2 dV) / (V Bpa^2) = int(Bp^2 dV) / (mu0^2 Ip^2) * int(dlp)^2 / V = CONVF * li3
    % CONVF = conversion_factor_liefit_li3 = int(dlp)^2/V * R0/2
    help_text = ['sprintf(''%s\n%s\n%s%s\n%s\n%s'',''li3 = 2 int(Bp^2 dV) / (mu0^2 Ip^2 R0), see Freidberg or '',', ...
                 '''CHEASE https://doi.org/10.1016/0010-4655(96)00046-X p. 5'',', ...
                 '''li ala EFIT [https://iopscience.iop.org/article/10.1088/0029-5515/25/10/004 Eq.(5)], CLISTE, IDE, '',', ...
                 '''GPEC [https://doi.org/10.13182/FST15-154 p.5]'',', ...
                 '''li = int(Bp^2 dV) / (V Bpa^2) = int(Bp^2 dV) / (mu0^2 Ip^2) * int(dlp)^2 / V = CONVF * li3'',', ...
                 '''CONVF = conversion_factor_liefit_li3 = int(dlp)^2/V * R0/2'')'];
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''li'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);ij=find(gdat_tmp.data<=0);gdat_tmp.data(ij)=NaN;' ...
                    'r0exp=1.65;gdat_tmp.r0=r0exp;' ...
                    'params_eff.data_request=''volume'';' ...
                    'gdat_vol=gdat_aug(shot,params_eff);ij=find(gdat_vol.data<0.5);gdat_vol.data(ij)=NaN;' ...
                    'params_eff.data_request=[{''FPG''},{''Circumf''}];' ...
                    'gdat_pol_length=gdat_aug(shot,params_eff);ij=find(gdat_pol_length.data<=0);gdat_pol_length.data(ij)=NaN;' ...
                    'gdat_tmp.conversion_factor_liefit_li3=gdat_tmp.r0./2.*gdat_pol_length.data.^2./max(abs(gdat_vol.data),1e-1);' ...
                    'gdat_tmp.data = gdat_tmp.data./gdat_tmp.conversion_factor_liefit_li3;' ...
                    'gdat_tmp.help = ' help_text ';'];
  case 'mhd'
    mapping.timedim = 1;
    mapping.label = {'n\_odd','n\_even'};% this superseeds the expression
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''MOD'',''OddN''}; ' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);gdat_tmp.data=reshape(gdat_tmp.data,length(gdat_tmp.data),1 );' ...
                    'gdat_tmp.dim{1}=gdat_tmp.t;gdat_tmp.dim{2}=[1 2];gdat_tmp.x=gdat_tmp.dim{2};' ...
                    'gdat_tmp.n_odd.data = gdat_tmp.data;gdat_tmp.n_odd.data_request=params_eff.data_request;' ...
                    'params_eff.data_request={''MOD'',''EvenN''};' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.data(:,2)=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_even.data = gdat_tmp2.data;gdat_tmp.n_even.data_request=params_eff.data_request;gdat_tmp.label={''n\_odd'',''n\_even''};' ...
                    'params_eff.data_request={''MOD'',''OddNAmp''};' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.n_odd.amp=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_odd.amp_t=gdat_tmp2.t;' ...
                    'params_eff.data_request={''MOD'',''EvenNAmp''};' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.n_even.amp=reshape(gdat_tmp2.data,length(gdat_tmp2.data),1);' ...
                    'gdat_tmp.n_even.amp_t=gdat_tmp2.t;' ...
                    'gdat_tmp.full_path=''MOD/Odd in data and .n_odd; .n_even'';' ...
                    'gdat_tmp.gdat_request=''mhd'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;'];
  case 'ne'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
  case 'neint'
    mapping.timedim = 1;
    mapping.label = 'line integrated el. density';
    % mapping.method = 'signal';
    mapping.method = 'expression';
    mapping.expression = [{'DCN'},{'H-1'},{'AUGD'}];
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request={''DCN'',''H-1'',''AUGD''};' ...
                    'if any(shot==[39311, 39599]);params_eff.data_request={''TTH'',''dcn_reco'',''PUETTI''};end;' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);'];
  case 'nel'
    mapping.timedim = 1;
    mapping.label = 'line-averaged el. density';
    mapping.expression = [{'FPG'},{'lenH-1'},{'AUGD'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''neint'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);params_eff.data_request=[{''FPG''},{''lenH-1''},{''AUGD''}];' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'tmp_data=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = gdat_tmp.data./(tmp_data+1e-5);'];
  case 'ne_rho'
    mapping.timedim = 2;
    mapping.label = 'ne';
    mapping.method = 'switchcase';
  case 'nete_rho'
    mapping.timedim = 2;
    mapping.label = 'ne and Te';
    mapping.method = 'switchcase';
  case {'ng','ngreenwald','n_greenwald'}
    mapping.timedim = 1;
    mapping.label = 'nG=Ip[MA]/(\pi a^2)*1e20 on nel times';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);params_eff.data_request=''ip'';' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'params_eff.data_request=''a_minor'';' ...
                    'gdat_tmp3=gdat_aug(shot,params_eff);ij=find(gdat_tmp3.data==0);gdat_tmp3.data(ij)=NaN;' ...
                    'tmp_data3=interp1(gdat_tmp3.t,gdat_tmp3.data,gdat_tmp.t,[],NaN);' ...
                    'gdat_tmp.data = tmp_data2*1e-6./pi./(tmp_data3.^2+1e-5);' ...
                    'ij=find(gdat_tmp.data<0 | gdat_tmp.data>4);gdat_tmp.data(ij)=NaN;gdat_tmp.data = gdat_tmp.data * 1e20;'];
  case {'ngf','greenwald_fraction','f_greenwald','ng_fraction'}
    mapping.timedim = 1;
    mapping.label = 'Greenwald\_fraction=n\_el/n\_G';
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''nel'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);params_eff.data_request=''n_greenwald'';' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);ij=find(gdat_tmp2.data==0);gdat_tmp2.data(ij)=NaN;' ...
                    'if numel(gdat_tmp2.t)>1;tmp_data2=interp1(gdat_tmp2.t,gdat_tmp2.data,gdat_tmp.t,[],NaN);' ...
                    'else;tmp_data2=gdat_tmp2.data;end;' ...
                    'gdat_tmp.ng=gdat_tmp2;gdat_tmp.ng.data_t_nel=tmp_data2;' ...
                    'gdat_tmp.data = gdat_tmp.data./(tmp_data2+1e-5);' ...
                    'gdat_tmp.units='''';gdat_tmp.dimunits{1}=''s'';'];
% $$$  case 'ni'
% $$$   mapping.method = 'switchcase'; % especially since might have option fit, etc
  case 'pellet'
    mapping.timedim = 1;
    mapping.label = 'line integrated el. density';
    mapping.method = 'signal';
    mapping.expression = [{'PEL'},{'5Co'},{'AUGD'}];
  case 'pgyro'
    mapping.timedim = 1;
    mapping.label = 'EC gyros';
    mapping.method = 'switchcase';
  case 'powers'
    mapping.timedim = 1;
    mapping.label = 'various powers';
    mapping.method = 'switchcase';
  case {'prad', 'p_rad'}
    mapping.timedim = 1;
    mapping.label = 'P_{rad}';
    mapping.method = 'signal';
    mapping.expression = [{'BPD'},{'Pradtot'},{'AUGD'}];
  case 'psi_axis'
    mapping.timedim = 1;
    mapping.method = 'switchcase'; % there is psi_axis-psi_edge in FPG but otherwise complicated to get from equil, thus needs swticth case
    mapping.label ='psi_\axis' ;
  case 'psi_edge'
    mapping.timedim = 1;
    mapping.method = 'switchcase'; % is set to zero, so not in tree nodes
    mapping.label = 'psi\_edge';
  case 'q0'
    mapping.timedim = 1;
    mapping.label = 'q_0';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'q0'},{'AUGD'}];
  case 'q95'
    mapping.timedim = 1;
    mapping.label = 'q_{95}';
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'q95'},{'AUGD'}];
% $$$   case 'q_edge'  % should use q_rho and then take q_edge since does not make sense for diverted shots and mainly diverted shots...
% $$$   mapping.timedim = 1;
% $$$   mapping.label = 'q_{edge}}';
% $$$   mapping.method = 'expression';
% $$$   mapping.method = 'switchcase';
% $$$   mapping.expression = [{'FPG'},{'q95'},{'AUGD'}];
% $$$   mapping.expression = [];
  case 'q_rho'
    mapping.timedim = 2;
    mapping.gdat_timedim = 2;
    mapping.label = 'q';
    mapping.method = 'switchcase';
  case 'raptor'
    mapping.timedim = 2;
    mapping.gdat_timedim = 2;
    mapping.label = 'Raptor signals';
    mapping.method = 'switchcase';
  case 'rgeom'
    mapping.label = 'Rgeom';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=''r_inboard'';' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);gdat_tmp.r_inboard=gdat_tmp.data;' ...
                    'params_eff.data_request=''r_outboard'';' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.r_outboard=gdat_tmp2.data;' ...
                    'gdat_tmp.data = 0.5.*(gdat_tmp2.data+gdat_tmp.data);gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];
  case 'r_inboard'
    mapping.label = 'R\_inboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Rin'},{'AUGD'}];
  case 'r_outboard'
    mapping.label = 'R\_outboard';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Raus'},{'AUGD'}];
  case 'rhotor'
    mapping.timedim = 2;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor';
  case 'rhotor_edge'
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_edge';
  case 'rhotor_norm'
    mapping.timedim = 1;
    mapping.method = 'switchcase';
    mapping.label = 'rhotor\_norm';
  case 'rhovol'
    mapping.timedim = 2;
    mapping.label = 'rhovol\_norm';
    mapping.method = 'switchcase';
  case {'rmag', 'r_axis', 'r_mag'}
    mapping.label = 'R\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Rmag'},{'AUGD'}];
  case 'sxr'
    mapping.timedim = 1;
    mapping.gdat_timedim = 2;
    mapping.method = 'switchcase';
  case 'tau_tot'
    mapping.label = '\tau_{tot}';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'TOT'},{'tau_tot'},{'AUGD'}];
  case 'te'
    mapping.timedim = 2;
    mapping.label = 'Te';
    mapping.method = 'switchcase';
  case 'te_rho'
    mapping.timedim = 2;
    mapping.label = 'Te';
    mapping.method = 'switchcase';
% $$$  case 'ti' % case to be set
% $$$   mapping.label = 'Ti';
% $$$   mapping.method = 'switchcase';
  case 'vloop'
    mapping.label = 'Vloop';
    mapping.timedim = 1;
    % mapping.method = 'signal';
    % mapping.expression = [{'MAG'},{'ULid12'},{'AUGD'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''MAG''},{''ULid12''},{''AUGD''}];' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);ij=find(~isnan(gdat_tmp.data));' ...
                    'tmp_data=interpos(gdat_tmp.t,gdat_tmp.data,-3e4);' ...
                    'gdat_tmp.data_smooth = tmp_data;gdat_tmp.gdat_request=''vloop'';gdat_tmp.gdat_params.data_request=gdat_tmp.gdat_request;'];
  case {'volume', 'volume_edge'}
    mapping.label = 'Volume';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Vol'},{'AUGD'}];
  case 'volume_rho'
    mapping.timedim = 2;
    mapping.label = 'Volume';
    mapping.method = 'switchcase';
  case {'wmhd' 'w_mhd'}
    mapping.label = 'Wmhd';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Wmhd'},{'AUGD'}];
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''FPG''},{''Wmhd''},{''AUGD''}];' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);ij=find(~isnan(gdat_tmp.data)); gdat_tmp.data_raw=gdat_tmp.data;' ...
                    'tmp_data=interpos(gdat_tmp.t(ij),gdat_tmp.data(ij),gdat_tmp.t,-1e5);' ...
                    'gdat_tmp.data = max(tmp_data,0.);'];
  case {'zeff', 'z_eff'}
    mapping.timedim = 1;
    mapping.label = 'zeff';
    mapping.method = 'switchcase';
% $$$ ZES obsolete, use cxrs
% $$$   mapping.label = 'zeff from cxrs';
% $$$   mapping.timedim = 1;
% $$$   mapping.method = 'signal';
% $$$   mapping.expression = [{'ZES'},{'Zeff'},{'AUGD'}];
  case 'zgeom'
    mapping.label = 'Zgeom';
    mapping.timedim = 1;
    mapping.method = 'expression';
    mapping.expression = ['params_eff = gdat_data.gdat_params;params_eff.data_request=[{''FPG''},{''Zoben''},{''AUGD''}];' ...
                    'gdat_tmp=gdat_aug(shot,params_eff);gdat_tmp.z_top=gdat_tmp.data;' ...
                    'params_eff.data_request=[{''FPG''},{''Zunt''},{''AUGD''}];' ...
                    'gdat_tmp2=gdat_aug(shot,params_eff);gdat_tmp.z_bottom=gdat_tmp2.data;' ...
                    'gdat_tmp.data = 0.5.*(gdat_tmp2.data+gdat_tmp.data);gdat_tmp.label=''' mapping.label ''';' ...
                    'gdat_tmp.gdat_request=''' data_request ''';'];

  case {'zmag', 'z_mag', 'z_axis'}
    mapping.label = 'Z\_magaxis';
    mapping.timedim = 1;
    mapping.method = 'signal';
    mapping.expression = [{'FPG'},{'Zmag'},{'AUGD'}];
    %
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % extra AUG cases (not necessarily in official data_request name list)
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
  case 'transp'
    mapping.label = 'transp output';
    mapping.method = 'switchcase';


  otherwise
    mapping.label = data_request;
    mapping.method = 'signal'; % assume a full tracename is given, so just try with tdi (could check there are some ":", etc...)
    mapping.expression = data_request;
    mapping.not_found = true;

end

if isempty(mapping.gdat_timedim)
  mapping.gdat_timedim = mapping.timedim;
end
