function [nodeout]=adapt_rda(nodein,ndim1,ndim2,itotransposeback,varargin);
%
% adapt nodein to keep only 1st ndim1 points in dim1 and ndim2 points in dim2
% if itotransposeback==1, transpose back matrix and use ndim1 for ndim2 and vice versa
%
% change .value, .data, .x and .t 
%

nodeout = nodein;

if itotransposeback==1
  nodeout.value = nodeout.value';
  nodeout.data = nodeout.data';
  temp = nodeout.x;
  nodeout.x = nodeout.t;
  nodeout.t = temp;
end

nodeout.value = nodeout.value(1:ndim1,1:ndim2);
nodeout.data = nodeout.value;
nodeout.x = nodeout.x(1:ndim1);
nodeout.t = nodeout.t(1:ndim2);
