function fig_handles=plot_torbeam(torbeam_out_struct,varargin);
%
%       fig_handles=plot_torbeam(torbeam_out_struct,varargin);
%
% Default plots of structure torbeam_out_struct obtained from run_torbeam_fullshot.m
%
% varargin: list of fields to be plotted versus time
%           (default: 'pow','rhopol_dep', 'pdens_peak', 'rhopol_pdens_width')
%
%           []: if empty then a list with multiple choice is proposed
%

fig_handles = [];

if isempty(torbeam_out_struct); return; end

set_colos;
list_fields = {'pow','rhopol_dep', 'pdens_peak', 'rhopol_pdens_width'};

abc=cat(1,torbeam_out_struct{:});
tb_fields = [];
if ~isempty(abc); tb_fields = fields(abc(1)); end

if nargin >= 2
  if isempty(varargin{1})
    % asks from list
    if ~isempty(tb_fields)
      h_for_waitfor=figure;
      global field_chosen
      h1=uicontrol('Style','listbox','string',tb_fields,'pos',[20 20 200 300],'Max',20, ...
          'callback', ['global field_chosen;field_chosen=get(gcbo,''value'') ;']);
      h2=uicontrol('Style','text','pos',[20 330 200 30],'string','choose fields to plot (ctrl-enter for multiple choices) then quit');
      h3=uicontrol('Style','pushbutton','string','quit','pos',[300 20 30 20],'callback', ['close gcf;']);
      waitfor(h_for_waitfor);
      list_fields = tb_fields(field_chosen);
      clear global field_chosen
    else
      disp('torbeam_out_struct empty');
      return
    end
  elseif length(varargin) == 1
    list_fields = varargin{1};
  else
    list_fields = varargin;
  end
end

for ifield=1:length(list_fields)
  titleg = [];
  eval(['fig_handles.' list_fields{ifield} '.fig = figure(''name'',''' list_fields{ifield} ''');']);
  for igyro=1:length(torbeam_out_struct)
    if ~isempty(torbeam_out_struct{igyro})
      eval(['array_to_plot = torbeam_out_struct{igyro}.' list_fields{ifield} ';']);
      plotos(torbeam_out_struct{igyro}.time,array_to_plot,'-',[],[],colos(igyro,:));
      hold on
      titleg{end+1} = ['L' num2str(igyro)];
    end
  end

  if ~isempty(titleg) ; legend(titleg); end
  eval(['fig_handles.' list_fields{ifield} '.titleg = titleg;']);
  xlabel('time [s]')
  eval(['ylabel(''' list_fields{ifield} ''');']);
  title(['AUG #' num2str(torbeam_out_struct{igyro}.shot(1))]);
end
